import {
  Body,
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { RoleAddPermissionsDto } from './dto/update-role.dto';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleRemovePermissionsQueries } from './queries/role-remove-permissions.queries';
import {
  ReadRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class RolePermissionsController {
  constructor(private readonly roleService: RoleService) {}

  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Patch('/permissions')
  async addPermissions(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
    @Body(NotEmptyObjectPipe) dto: RoleAddPermissionsDto,
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id);

    const data = await this.roleService.addPermissions(id, dto);

    return { data };
  }

  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Delete('/permissions')
  async removePermissions(
    @ReqUser() requestUser: RequestUser,
    @Query() queries: RoleRemovePermissionsQueries,
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, queries.id);

    const data = await this.roleService.removePermissions(queries);

    return { data };
  }
}
