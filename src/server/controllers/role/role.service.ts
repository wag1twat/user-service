import { ForbiddenException, Injectable } from '@nestjs/common';
import { RoleEntity, RoleRepository } from 'src/server/entities/role.entity';
import { CreateRoleDto } from './dto/create-role.dto';
import {
  RoleAddPermissionsDto,
  RoleAddUsersDto,
  UpdateRoleDto,
} from './dto/update-role.dto';
import { RoleRemoveUsersQueries } from './queries/role-remove-users.queries';
import { RoleQueries } from './queries/role.queries';
import { RequestUser } from 'src/server/entities/request-user';
import { RoleRemovePermissionsQueries } from './queries/role-remove-permissions.queries';
import { CaslAbilityFactory } from 'src/server/casl/casl-ability.factory';
import { ForbiddenError } from '@casl/ability';
import { CaslPermissionsAction } from 'src/server/casl/casl.permissions';
import { HasAccess } from 'src/server/entities/client.types';
import { RoleEntityUpdateRelationsManager } from './helpers';

@Injectable()
export class RoleService {
  constructor(
    private readonly roleRepository: RoleRepository,
    private readonly caslAbilityFactory: CaslAbilityFactory,
  ) {}

  async findOne(where: RoleQueries) {
    return this.roleRepository.findOneOrFail({ where, relations: ['owner'] });
  }

  async create(requestUser: RequestUser | undefined, dto: CreateRoleDto) {
    const { usersIds = [], permissionsIds = [], ...createDto } = dto;

    const role = await this.roleRepository.save(createDto);

    const manager = new RoleEntityUpdateRelationsManager(
      role.id,
      this.roleRepository,
    );

    await manager.setOwner(requestUser);

    await manager.forceAddUsers(role.id, usersIds);

    await manager.forceAddPermissions(role.id, permissionsIds);

    return await this.roleRepository.findOneOrFail({
      where: { id: role.id },
      relations: ['owner', 'users', 'permissions'],
    });
  }

  async addUsers(id: RoleEntity['id'], dto: RoleAddUsersDto) {
    const manager = new RoleEntityUpdateRelationsManager(
      id,
      this.roleRepository,
    );

    return await manager.forceAddUsers(id, dto.usersIds);
  }

  async addPermissions(id: RoleEntity['id'], dto: RoleAddPermissionsDto) {
    const manager = new RoleEntityUpdateRelationsManager(
      id,
      this.roleRepository,
    );

    return await manager.forceAddPermissions(id, dto.permissionsIds);
  }

  async removeUsers(queries: RoleRemoveUsersQueries) {
    const manager = new RoleEntityUpdateRelationsManager(
      queries.id,
      this.roleRepository,
    );

    return await manager.forceRemoveUsers(queries.id, queries.usersIds);
  }

  async removePermissions(queries: RoleRemovePermissionsQueries) {
    const manager = new RoleEntityUpdateRelationsManager(
      queries.id,
      this.roleRepository,
    );

    return await manager.forceRemovePermissions(
      queries.id,
      queries.permissionsIds,
    );
  }

  async update(id: RoleEntity['id'], dto: UpdateRoleDto) {
    const { permissionsIds = [], usersIds = [], ...updateDto } = dto;

    await this.roleRepository.update(id, updateDto);

    const manager = new RoleEntityUpdateRelationsManager(
      id,
      this.roleRepository,
    );

    return await manager.update({
      users: usersIds,
      permissions: permissionsIds,
    });
  }

  async isHasUpdateAccess(
    requestUser: RequestUser,
    id: RoleEntity['id'],
    field?: keyof RoleEntity | undefined,
  ): Promise<HasAccess> {
    const ability = await this.caslAbilityFactory.createForUser(requestUser);

    const role = await this.roleRepository.findOneOrFail(
      { id },
      { relations: ['owner'] },
    );

    try {
      ForbiddenError.from(ability).throwUnlessCan(
        CaslPermissionsAction.AVAILABLE_UPDATE_ROLE,
        role,
        field,
      );

      return { isHasAccess: true };
    } catch (error) {
      if (error instanceof ForbiddenError) {
        throw new ForbiddenException(error.message);
      }
      throw new ForbiddenException();
    }
  }

  async deactivate(id: RoleEntity['id']) {
    await this.roleRepository.update(id, {
      active: false,
    });

    return await this.findOne({ id });
  }

  async activate(id: RoleEntity['id']) {
    await this.roleRepository.update(id, {
      active: true,
    });

    return await this.findOne({ id });
  }

  async deprotected(id: RoleEntity['id']) {
    await this.roleRepository.update(id, {
      protected: false,
    });

    return await this.findOne({ id });
  }

  async protected(id: RoleEntity['id']) {
    await this.roleRepository.update(id, {
      protected: true,
    });

    return await this.findOne({ id });
  }

  async drop(id: string) {
    await this.roleRepository.delete({ id });
  }
}
