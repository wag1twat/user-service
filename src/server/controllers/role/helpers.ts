import { filter, includes, map } from 'lodash';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { RoleEntity, RoleRepository } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';

interface Dto {
  users: Array<UserEntity['id']>;
  permissions: Array<PermissionEntity['id']>;
}

export class RoleEntityUpdateRelationsManager {
  constructor(
    private readonly id: RoleEntity['id'],
    private readonly roleRepository: RoleRepository,
  ) {}

  async setOwner(requestUser: RequestUser | undefined) {
    if (requestUser) {
      await this.roleRepository
        .createQueryBuilder('role')
        .relation('owner')
        .of(this.id)
        .set(requestUser.id);
    }

    return await this.roleRepository.findOneOrFail(
      {
        id: this.id,
      },
      { relations: ['owner'] },
    );
  }

  async forceAddUsers(id: RoleEntity['id'], users: Array<UserEntity['id']>) {
    await this.roleRepository
      .createQueryBuilder('role')
      .relation('users')
      .of(id)
      .add(users);

    return await this.roleRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'users'] },
    );
  }

  async forceRemoveUsers(id: RoleEntity['id'], users: Array<UserEntity['id']>) {
    await this.roleRepository
      .createQueryBuilder('role')
      .relation('users')
      .of(id)
      .remove(users);

    return await this.roleRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'users'] },
    );
  }

  async forceAddPermissions(
    id: RoleEntity['id'],
    permissions: Array<PermissionEntity['id']>,
  ) {
    await this.roleRepository
      .createQueryBuilder('role')
      .relation('permissions')
      .of(id)
      .add(permissions);

    return await this.roleRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'permissions'] },
    );
  }

  async forceRemovePermissions(
    id: RoleEntity['id'],
    permissions: Array<PermissionEntity['id']>,
  ) {
    await this.roleRepository
      .createQueryBuilder('role')
      .relation('permissions')
      .of(id)
      .remove(permissions);

    return await this.roleRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'permissions'] },
    );
  }

  private async addUsers(dto: Dto) {
    const role = await this.roleRepository.findOneOrFail(
      { id: this.id },
      { relations: ['users'] },
    );

    const usersIds = map(role.users, (user) => user.id);

    if (usersIds.length === 0) {
      return await this.forceAddUsers(this.id, dto.users);
    }

    const forAdd = filter(dto.users, (id) => !includes(usersIds, id));

    return await this.forceAddUsers(this.id, forAdd);
  }

  private async removeUsers(dto: Dto) {
    const role = await this.roleRepository.findOneOrFail(
      { id: this.id },
      { relations: ['users'] },
    );

    const usersIds = map(role.users, (user) => user.id);

    const forRemove = filter(usersIds, (id) => !includes(dto.users, id));

    return await this.forceRemoveUsers(this.id, forRemove);
  }

  private async addPermissions(dto: Dto) {
    const role = await this.roleRepository.findOneOrFail(
      { id: this.id },
      { relations: ['permissions'] },
    );

    const permissionsIds = map(role.permissions, (permission) => permission.id);

    if (permissionsIds.length === 0) {
      return await this.forceAddPermissions(this.id, dto.permissions);
    }

    const forAdd = filter(
      dto.permissions,
      (id) => !includes(permissionsIds, id),
    );

    return await this.forceAddPermissions(this.id, forAdd);
  }

  private async removePermissions(dto: Dto) {
    const role = await this.roleRepository.findOneOrFail(
      { id: this.id },
      { relations: ['permissions'] },
    );

    const permissionsIds = map(role.permissions, (permission) => permission.id);

    const forRemove = filter(
      permissionsIds,
      (id) => !includes(dto.permissions, id),
    );

    return await this.forceRemovePermissions(this.id, forRemove);
  }

  async update(dto: Dto) {
    await this.addUsers(dto);
    await this.removeUsers(dto);
    await this.addPermissions(dto);
    await this.removePermissions(dto);

    return await this.roleRepository.findOneOrFail(
      { id: this.id },
      { relations: ['owner', 'users', 'permissions'] },
    );
  }
}
