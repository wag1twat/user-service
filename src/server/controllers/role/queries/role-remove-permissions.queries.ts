import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsUUID } from 'class-validator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RoleEntity } from 'src/server/entities/role.entity';

export class RoleRemovePermissionsQueries {
  @ApiProperty({ type: [String], required: true })
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  permissionsIds!: PermissionEntity['id'][];

  @ApiProperty({ required: false, type: String })
  @IsUUID()
  id!: RoleEntity['id'];
}
