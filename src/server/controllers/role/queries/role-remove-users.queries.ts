import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsUUID } from 'class-validator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';

export class RoleRemoveUsersQueries {
  @ApiProperty({ type: [String], required: true })
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  usersIds!: UserEntity['id'][];

  @ApiProperty({ required: false, type: String })
  @IsUUID()
  id!: RoleEntity['id'];
}
