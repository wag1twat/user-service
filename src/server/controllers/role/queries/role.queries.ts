import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsUUID } from 'class-validator';
import { RoleEntity, RoleEntityConfig } from 'src/server/entities/role.entity';
import { Raw } from 'typeorm';

export class RoleQueries {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsUUID()
  id?: RoleEntity['id'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: RoleEntityConfig.name.minLength,
    maxLength: RoleEntityConfig.name.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  name?: RoleEntity['name'];

  @ApiProperty({ required: false, type: Boolean, default: true })
  @IsOptional()
  @IsBoolean()
  active?: RoleEntity['active'];

  @ApiProperty({ required: false, type: Boolean, default: false })
  @IsOptional()
  @IsBoolean()
  protected?: RoleEntity['protected'];
}
