import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsArray,
  IsBoolean,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  MinLength,
} from 'class-validator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RoleEntity, RoleEntityConfig } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';

export class CreateRoleDto {
  constructor(data?: CreateRoleDto) {
    Object.assign(this, data);
  }
  @ApiProperty({
    required: true,
    type: String,
    minLength: RoleEntityConfig.name.minLength,
    maxLength: RoleEntityConfig.name.maxLength,
  })
  @IsOptional()
  @IsString()
  @MinLength(RoleEntityConfig.name.minLength)
  @MaxLength(RoleEntityConfig.name.maxLength)
  name!: RoleEntity['name'];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  permissionsIds!: PermissionEntity['id'][];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  usersIds!: UserEntity['id'][];

  @ApiProperty({ required: false, type: Boolean, default: true })
  @IsOptional()
  @IsBoolean()
  active!: RoleEntity['active'];

  @ApiProperty({ required: false, type: Boolean, default: false })
  @IsOptional()
  @IsBoolean()
  protected!: RoleEntity['protected'];
}
