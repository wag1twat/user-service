import { OmitType, PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsArray,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  MinLength,
} from 'class-validator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RoleEntity, RoleEntityConfig } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { CreateRoleDto } from './create-role.dto';

export class UpdateRoleDto extends PartialType(
  OmitType(CreateRoleDto, ['active', 'protected']),
) {
  @ApiProperty({
    required: true,
    type: String,
    minLength: RoleEntityConfig.name.minLength,
    maxLength: RoleEntityConfig.name.maxLength,
  })
  @IsOptional()
  @IsString()
  @MinLength(RoleEntityConfig.name.minLength)
  @MaxLength(RoleEntityConfig.name.maxLength)
  name?: RoleEntity['name'];

  @ApiProperty({
    required: false,
    type: String,
  })
  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  permissionsIds?: PermissionEntity['id'][];

  @ApiProperty({ required: false })
  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  usersIds?: UserEntity['id'][];
}

export class RoleAddUsersDto {
  @ApiProperty({ type: [String], required: true })
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  usersIds: UserEntity['id'][] = [];
}

export class RoleAddPermissionsDto {
  @ApiProperty({ type: [String], required: true })
  @IsArray()
  @ArrayNotEmpty()
  @IsUUID(undefined, { each: true })
  permissionsIds: PermissionEntity['id'][] = [];
}
