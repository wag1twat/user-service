import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleService } from '../role.service';
import { roleModuleMetadata } from '../role.module';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { UserEntity } from 'src/server/entities/user.entity';
import { testsData } from './tests.data';
import { RoleCrudController } from '../role-crud.controller';

describe.each(testsData)(
  'RoleCrudController with params "%s"',
  (roleDto, userDto) => {
    const modules: TestingModule[] = [];
    let service: RoleService;
    let controller: RoleCrudController;
    let userService: UserService;

    let currentRole: RoleEntity;
    let currentUser: UserEntity;

    beforeAll(async () => {
      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      service = roleModule.get<RoleService>(RoleService);
      controller = roleModule.get<RoleCrudController>(RoleCrudController);

      modules.push(roleModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(userModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, userDto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentRole = await service.create(requestUser, roleDto);
    });

    afterEach(async () => {
      await service.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
    });

    it('should be entities created', async () => {
      expect(currentRole).toBeInstanceOf(RoleEntity);
      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('create > should be role created', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const name = TestUtils.randomString(10);

      const result = await controller.create(requestUser, {
        name,
        active: true,
        protected: false,
        usersIds: [],
        permissionsIds: [],
      });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data).toEqual(expect.objectContaining({ name }));

      await service.drop(result.data.id);
    });

    it('update > should be role updated', async () => {
      const { id } = currentRole;

      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const name = TestUtils.randomString(10);

      const result = await controller.update(requestUser, id, { name });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data).toEqual(expect.objectContaining({ name }));
    });
  },
);
