import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleService } from '../role.service';
import { roleModuleMetadata } from '../role.module';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { mockModuleWithConnection } from 'src/server/shared';
import { UserEntity } from 'src/server/entities/user.entity';
import { testsData } from './tests.data';
import { RoleUsersController } from '../role-users.controller';

describe.each(testsData)(
  'RoleUsersController with params "%s"',
  (roleDto, userDto) => {
    const modules: TestingModule[] = [];
    let service: RoleService;
    let controller: RoleUsersController;
    let userService: UserService;

    let currentRole: RoleEntity;
    let currentUser: UserEntity;

    beforeAll(async () => {
      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      service = roleModule.get<RoleService>(RoleService);
      controller = roleModule.get<RoleUsersController>(RoleUsersController);

      modules.push(roleModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(userModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, userDto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentRole = await service.create(requestUser, roleDto);
    });

    afterEach(async () => {
      await service.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
    });

    it('should be entities created', async () => {
      expect(currentRole).toBeInstanceOf(RoleEntity);
      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('addUsers > should be role add user', async () => {
      const { id } = currentRole;

      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const result = await controller.addUsers(requestUser, id, {
        usersIds: [currentUser.id],
      });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data.users).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: currentUser.id,
            login: currentUser.login,
          }),
        ]),
      );
    });

    it('removeUsers > should be role remove user', async () => {
      const { id } = currentRole;

      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const result = await controller.removeUsers(requestUser, {
        usersIds: [currentUser.id],
        id,
      });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data.users?.length).toBe(0);
    });
  },
);
