import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from '../../../entities/role.entity';
import { RoleService } from '../role.service';
import { roleModuleMetadata } from '../role.module';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { mockModuleWithConnection, TestUtils } from '../../../shared';
import { UserEntity } from '../../../entities/user.entity';
import { testsData } from './tests.data';
import { FindRoleController } from '../find-role.controller';
import { ForbiddenException } from '@nestjs/common';

describe.each(testsData)(
  'FindRoleController with params "%s"',
  (roleDto, userDto) => {
    const modules: TestingModule[] = [];
    let service: RoleService;
    let controller: FindRoleController;
    let userService: UserService;

    let currentRole: RoleEntity;
    let currentUser: UserEntity;

    beforeAll(async () => {
      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      service = roleModule.get<RoleService>(RoleService);
      controller = roleModule.get<FindRoleController>(FindRoleController);

      modules.push(roleModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(userModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, userDto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentRole = await service.create(requestUser, roleDto);
    });

    afterEach(async () => {
      await service.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
    });

    it('should be entities created', async () => {
      expect(currentRole).toBeInstanceOf(RoleEntity);
      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('findOne > should be role finded', async () => {
      const result = await controller.findOne({ name: currentRole.name });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data).toEqual(
        expect.objectContaining({ name: currentRole.name }),
      );
    });

    it('isHasAccess > should be can update no protected role no ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });
      const role = await service.create(undefined, {
        name: TestUtils.randomString(10),
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBe(null);

      const result = await controller.isHasUpdateAccess(requestUser, role.id);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(role.id);
    });

    it('isHasAccess > should be can update no protected role ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const role = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBeInstanceOf(UserEntity);

      const result = await controller.isHasUpdateAccess(requestUser, role.id);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(role.id);
    });

    it('isHasAccess > should be can update protected role no ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });
      const invalidUser = await userService.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await userService.findRequestUser({
        id: invalidUser.id,
      });

      const role = await service.create(undefined, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBe(null);

      await expect(
        controller.isHasUpdateAccess(invalidRequestUser, role.id),
      ).rejects.toThrowError(ForbiddenException);

      await service.drop(role.id);
      await userService.drop(invalidUser.id);
    });

    it('isHasAccess > should be can update protected role ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const invalidUser = await userService.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await userService.findRequestUser({
        id: invalidUser.id,
      });

      const role = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBeInstanceOf(UserEntity);

      const result = await controller.isHasUpdateAccess(requestUser, role.id);

      expect(result.isHasAccess).toBeTruthy();

      await expect(
        controller.isHasUpdateAccess(invalidRequestUser, role.id),
      ).rejects.toThrowError(ForbiddenException);

      await service.drop(role.id);
      await userService.drop(invalidUser.id);
    });
  },
);
