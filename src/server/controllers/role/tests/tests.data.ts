import { testsRolesLength } from './constants';
import { testsPermissions } from './tests.permissions';
import { testsRoles } from './tests.roles';
import { testsUsers } from './tests.users';

const roles = testsRoles(testsRolesLength, 'role');
const users = testsUsers(testsRolesLength, 'user');
const permissions = testsPermissions(testsRolesLength, 'permission');

export const testsData = roles.map(
  (role, i) => [role, users[i], permissions[i]] as const,
);
