import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleService } from '../role.service';
import { roleModuleMetadata } from '../role.module';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { mockModuleWithConnection } from 'src/server/shared';
import { PermissionService } from '../../permission/permission.service';
import { permissionModuleMetadata } from '../../permission/permission.module';
import { UserEntity } from 'src/server/entities/user.entity';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { testsData } from './tests.data';
import { RolePermissionsController } from '../role-permissions.controller';

describe.each(testsData)(
  'RolePermissionsController with params "%s"',
  (roleDto, userDto, permissionDto) => {
    const modules: TestingModule[] = [];
    let service: RoleService;
    let controller: RolePermissionsController;
    let userService: UserService;
    let permissionService: PermissionService;

    let currentRole: RoleEntity;
    let currentUser: UserEntity;
    let currentPermission: PermissionEntity;

    beforeAll(async () => {
      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      service = roleModule.get<RoleService>(RoleService);
      controller = roleModule.get<RolePermissionsController>(
        RolePermissionsController,
      );

      modules.push(roleModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(userModule);

      const permissionModule: TestingModule = await mockModuleWithConnection(
        permissionModuleMetadata,
      );

      permissionService =
        permissionModule.get<PermissionService>(PermissionService);

      modules.push(permissionModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, userDto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentPermission = await permissionService.create(
        requestUser,
        permissionDto,
      );
    });
    afterEach(async () => {
      await permissionService.drop(currentPermission.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentRole = await service.create(requestUser, roleDto);
    });

    afterEach(async () => {
      await service.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
      expect(permissionService).toBeDefined();
    });

    it('should be entities created', async () => {
      expect(currentRole).toBeInstanceOf(RoleEntity);
      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
      expect(currentPermission).toBeInstanceOf(PermissionEntity);
      expect(currentPermission).toEqual(
        expect.objectContaining({
          name: permissionDto.name,
        }),
      );
    });

    it('addPermissions > should be role add permission', async () => {
      const { id } = currentRole;

      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const result = await controller.addPermissions(requestUser, id, {
        permissionsIds: [currentPermission.id],
      });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data.permissions).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: currentPermission.id,
            name: currentPermission.name,
          }),
        ]),
      );
    });

    it('removePermissions > should be role remove permission', async () => {
      const { id } = currentRole;

      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const result = await controller.removePermissions(requestUser, {
        permissionsIds: [currentPermission.id],
        id,
      });

      expect(result.data).toBeInstanceOf(RoleEntity);

      expect(result.data.permissions?.length).toBe(0);
    });
  },
);
