import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleService } from '../role.service';
import { roleModuleMetadata } from '../role.module';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { PermissionService } from '../../permission/permission.service';
import { permissionModuleMetadata } from '../../permission/permission.module';
import { UserEntity } from 'src/server/entities/user.entity';
import { Permission } from 'src/server/entities/client.types';
import { testsData } from './tests.data';
import { ForbiddenException } from '@nestjs/common';

describe.each(testsData)(
  'RoleService with params "%s"',
  (roleDto, userDto, permissionDto) => {
    const modules: TestingModule[] = [];
    let service: RoleService;
    let userService: UserService;
    let permissionService: PermissionService;

    let currentRole: RoleEntity;
    let currentUser: UserEntity;
    let currentPermission: Permission;

    beforeAll(async () => {
      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      modules.push(roleModule);

      service = roleModule.get<RoleService>(RoleService);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(userModule);

      userService = userModule.get<UserService>(UserService);

      const permissionModule: TestingModule = await mockModuleWithConnection(
        permissionModuleMetadata,
      );

      modules.push(permissionModule);

      permissionService =
        permissionModule.get<PermissionService>(PermissionService);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, userDto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentPermission = await permissionService.create(
        requestUser,
        permissionDto,
      );
    });
    afterEach(async () => {
      await permissionService.drop(currentPermission.id);
    });

    beforeEach(async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      currentRole = await service.create(requestUser, roleDto);
    });

    afterEach(async () => {
      await service.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
      expect(permissionService).toBeDefined();
    });

    it('created > should be role created', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const name = TestUtils.randomString(10);

      const result = await service.create(requestUser, {
        name,
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(result).toEqual(expect.objectContaining({ name }));

      await service.drop(result.id);
    });

    it('findOne > should be role finded', async () => {
      const result = await service.findOne({ name: currentRole.name });

      expect(result).toEqual(
        expect.objectContaining({ name: currentRole.name }),
      );
    });

    it('isHasAccess > should be can update no protected role no ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const role = await service.create(undefined, {
        name: TestUtils.randomString(10),
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      const invalidUser = await userService.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await userService.findRequestUser({
        id: invalidUser.id,
      });

      expect(role.owner).toBe(null);

      const result = await service.isHasUpdateAccess(
        invalidRequestUser,
        role.id,
      );

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(role.id);
      await userService.drop(invalidUser.id);
    });

    it('isHasAccess > should be can update no protected role ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const role = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBeInstanceOf(UserEntity);

      const result = await service.isHasUpdateAccess(requestUser, role.id);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(role.id);
    });

    it('isHasAccess > should be can update protected role no ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const role = await service.create(undefined, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      expect(role.owner).toBe(null);

      await expect(
        service.isHasUpdateAccess(requestUser, role.id),
      ).rejects.toThrowError(ForbiddenException);

      await service.drop(role.id);
    });

    it('isHasAccess > should be can update protected role ownered', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const role = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      const invalidUser = await userService.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await userService.findRequestUser({
        id: invalidUser.id,
      });

      expect(role.owner).toBeInstanceOf(UserEntity);

      const result = await service.isHasUpdateAccess(requestUser, role.id);

      await expect(
        service.isHasUpdateAccess(invalidRequestUser, role.id),
      ).rejects.toThrowError(ForbiddenException);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(role.id);
      await userService.drop(invalidUser.id);
    });

    it('update > should be role updated non protected', async () => {
      const name = TestUtils.randomString(10);

      const role = await service.create(undefined, {
        name,
        protected: false,
        active: true,
        usersIds: [],
        permissionsIds: [],
      });

      const { id } = role;

      const result = await service.update(id, { name });

      expect(result).toEqual(expect.objectContaining({ name }));

      await service.drop(role.id);
    });

    it('update > should be role updated protected', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const name = TestUtils.randomString(10);

      const role = await service.create(requestUser, {
        name,
        active: true,
        protected: true,
        usersIds: [],
        permissionsIds: [],
      });

      const { id } = role;

      const result = await service.update(id, { name });

      expect(result).toEqual(expect.objectContaining({ name }));

      await service.drop(role.id);
    });

    it('addUsers > should be role add user', async () => {
      const { id } = currentRole;

      const result = await service.addUsers(id, { usersIds: [currentUser.id] });

      expect(result.users).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: currentUser.id,
            login: currentUser.login,
          }),
        ]),
      );
    });

    it('removeUsers > should be role remove user', async () => {
      const { id } = currentRole;

      const result = await service.removeUsers({
        usersIds: [currentUser.id],
        id,
      });

      expect(result.users?.length).toBe(0);
    });

    it('addPermissions > should be role add permission', async () => {
      const { id } = currentRole;

      const result = await service.addPermissions(id, {
        permissionsIds: [currentPermission.id],
      });

      expect(result.permissions).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: currentPermission.id,
            name: currentPermission.name,
          }),
        ]),
      );
    });

    it('removePermissions > should be role remove permission', async () => {
      const { id } = currentRole;

      const result = await service.removePermissions({
        permissionsIds: [currentPermission.id],
        id,
      });

      expect(result.permissions?.length).toBe(0);
    });

    it('activate > should be role activate', async () => {
      const result = await service.activate(currentRole.id);

      expect(result).toBeInstanceOf(RoleEntity);

      expect(result.active).toBeTruthy();
    });

    it('deactivate > should be role deactivate', async () => {
      const result = await service.deactivate(currentRole.id);

      expect(result).toBeInstanceOf(RoleEntity);

      expect(result.active).toBeFalsy();
    });

    it('protected > should be role protected', async () => {
      const result = await service.protected(currentRole.id);

      expect(result).toBeInstanceOf(RoleEntity);

      expect(result.protected).toBeTruthy();
    });

    it('deprotected > should be role deprotected', async () => {
      const result = await service.deprotected(currentRole.id);

      expect(result).toBeInstanceOf(RoleEntity);

      expect(result.protected).toBeFalsy();
    });
  },
);
