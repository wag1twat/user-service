import { TestUtils } from 'src/server/shared';
import { CreateRoleDto } from '../dto/create-role.dto';

export const testsRoles = (length: number, name: string): CreateRoleDto[] =>
  Array.from(Array(length).keys()).map((i) => ({
    name: name + TestUtils.randomString(10) + i,
    active: true,
    protected: false,
    usersIds: [],
    permissionsIds: [],
  }));
