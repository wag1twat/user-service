import {
  Body,
  Controller,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  CreateRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class RoleCrudController {
  constructor(private readonly roleService: RoleService) {}

  @CheckPermissions(new CreateRolesPermissionHandler())
  @Post()
  async create(
    @ReqUser() requestUser: RequestUser | undefined,
    @Body(NotEmptyObjectPipe) queries: CreateRoleDto,
  ) {
    const data = await this.roleService.create(requestUser, queries);

    return { data };
  }

  @CheckPermissions(new UpdateRolesPermissionHandler())
  @Patch()
  async update(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
    @Body(NotEmptyObjectPipe) dto: UpdateRoleDto,
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id);

    const data = await this.roleService.update(id, dto);

    return { data };
  }
}
