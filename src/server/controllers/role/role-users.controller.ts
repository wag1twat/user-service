import {
  Body,
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { RoleAddUsersDto } from './dto/update-role.dto';
import { RoleRemoveUsersQueries } from './queries/role-remove-users.queries';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class RoleUsersController {
  constructor(private readonly roleService: RoleService) {}

  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Patch('/users')
  async addUsers(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
    @Body(NotEmptyObjectPipe) dto: RoleAddUsersDto,
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id);

    const data = await this.roleService.addUsers(id, dto);

    return { data };
  }

  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Delete('/users')
  async removeUsers(
    @ReqUser() requestUser: RequestUser,
    @Query() queries: RoleRemoveUsersQueries,
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, queries.id);

    const data = await this.roleService.removeUsers(queries);

    return { data };
  }
}
