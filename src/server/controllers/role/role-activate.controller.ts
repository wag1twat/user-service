import {
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class RoleActivateController {
  constructor(private readonly roleService: RoleService) {}

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Patch('/activate')
  async activate(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id);

    const data = await this.roleService.activate(id);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Delete('/deactivate')
  async deactivate(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id);

    const data = await this.roleService.deactivate(id);

    return { data };
  }
}
