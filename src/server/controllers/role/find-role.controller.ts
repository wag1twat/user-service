import { Controller, Get, ParseUUIDPipe, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { RoleQueries } from './queries/role.queries';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  DeleteRolesPermissionHandler,
  ReadRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class FindRoleController {
  constructor(private readonly roleService: RoleService) {}

  @CheckPermissions(new ReadRolesPermissionHandler())
  @Get()
  async findOne(@Query(NotEmptyObjectPipe) queries: RoleQueries) {
    const data = await this.roleService.findOne(queries);

    return { data };
  }

  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
    new DeleteRolesPermissionHandler(),
  )
  @Get('/has-update-access')
  async isHasUpdateAccess(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
  ) {
    return await this.roleService.isHasUpdateAccess(requestUser, id);
  }
}
