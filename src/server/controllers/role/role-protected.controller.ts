import {
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { RoleService } from './role.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadRolesPermissionHandler,
  UpdateRolesPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/role')
export class RoleProtectedController {
  constructor(private readonly roleService: RoleService) {}

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Patch('/protected')
  async protected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id, 'protected');

    const data = await this.roleService.protected(id);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadRolesPermissionHandler(),
    new UpdateRolesPermissionHandler(),
  )
  @Delete('/deprotected')
  async deprotected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: RoleEntity['id'],
  ) {
    await this.roleService.isHasUpdateAccess(requestUser, id, 'protected');

    const data = await this.roleService.deprotected(id);

    return { data };
  }
}
