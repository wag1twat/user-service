import { Module, ModuleMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaslModule } from 'src/server/casl/casl.module';
import { RoleRepository } from 'src/server/entities/role.entity';
import { FindRoleController } from './find-role.controller';
import { RoleActivateController } from './role-activate.controller';
import { RoleCrudController } from './role-crud.controller';
import { RolePermissionsController } from './role-permissions.controller';
import { RoleProtectedController } from './role-protected.controller';
import { RoleUsersController } from './role-users.controller';
import { RoleService } from './role.service';

export const roleModuleMetadata: ModuleMetadata = {
  imports: [TypeOrmModule.forFeature([RoleRepository]), CaslModule],
  exports: [RoleService],
  controllers: [
    RoleUsersController,
    RoleProtectedController,
    RolePermissionsController,
    RoleCrudController,
    RoleActivateController,
    FindRoleController,
  ],
  providers: [RoleService],
};
@Module(roleModuleMetadata)
export class RoleModule {}
