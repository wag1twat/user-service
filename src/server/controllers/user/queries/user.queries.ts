import { ApiProperty } from '@nestjs/swagger';
import { plainToInstance, Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsUUID } from 'class-validator';
import { UserEntity, UserEntityConfig } from 'src/server/entities/user.entity';
import { Raw } from 'typeorm';

export class UserQueries {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsUUID()
  id?: UserEntity['id'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.login.minLength,
    maxLength: UserEntityConfig.login.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  login?: UserEntity['login'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.email.minLength,
    maxLength: UserEntityConfig.email.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  email?: UserEntity['email'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.firstName.minLength,
    maxLength: UserEntityConfig.firstName.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  firstName?: UserEntity['firstName'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.lastName.minLength,
    maxLength: UserEntityConfig.lastName.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  lastName?: UserEntity['lastName'];

  @ApiProperty({ required: false, type: Boolean, default: true })
  @IsOptional()
  @IsBoolean()
  active?: UserEntity['active'];

  @ApiProperty({ required: false, type: Boolean, default: false })
  @IsOptional()
  @IsBoolean()
  protected?: UserEntity['protected'];
}

export const ConstructorUserQueries = (data: UserQueries) =>
  plainToInstance(UserQueries, data);
