import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsUUID } from 'class-validator';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';

export class UserRemoveRolesQueries {
  @ApiProperty({ required: true, type: [String] })
  @IsArray()
  @IsUUID(undefined, { each: true })
  rolesIds!: RoleEntity['id'][];

  @ApiProperty({ required: true, type: String })
  @IsUUID()
  id!: UserEntity['id'];
}
