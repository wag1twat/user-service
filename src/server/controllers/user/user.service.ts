import { ForbiddenException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity, UserRepository } from 'src/server/entities/user.entity';
import { UserQueries } from './queries/user.queries';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RolesService } from '../roles/roles.service';
import { PermissionsService } from '../permissions/permissions.service';
import { RequestUser } from 'src/server/entities/request-user';
import { SignupUserDto } from './dto/signup-user.dto';
import { UpdateUserRolesDto } from './dto/update-user-roles.dto';
import { UpdateUserProfileDto } from './dto/update-profile.dto';
import { plainToInstance } from 'class-transformer';
import { CaslAbilityFactory } from 'src/server/casl/casl-ability.factory';
import { FindConditions } from 'typeorm';
import { CaslPermissionsAction } from 'src/server/casl/casl.permissions';
import { ForbiddenError } from '@casl/ability';
import { HasAccess } from 'src/server/entities/client.types';
import { UserEntityUpdateRelationsManager } from './helpers';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly rolesService: RolesService,
    private readonly permissionsService: PermissionsService,
    private caslAbilityFactory: CaslAbilityFactory,
  ) {}
  async findOne(where: UserQueries) {
    return await this.userRepository.findOneOrFail(where, {
      relations: ['owner'],
    });
  }

  async findRoles(id: UserEntity['id']) {
    return this.rolesService.findAll({ usersIds: [id] });
  }

  async findPermissions(id: UserEntity['id']) {
    return this.permissionsService.findAll({ usersIds: [id] });
  }

  async findRequestUser(
    where?: FindConditions<UserEntity>,
  ): Promise<RequestUser> {
    const user = await this.userRepository.findOneOrFail({
      where,
      select: ['id', 'login', 'password', 'active'],
    });

    return new RequestUser(user);
  }

  async signup(dto: SignupUserDto) {
    const user = await this.userRepository.save(
      plainToInstance(UserEntity, dto),
    );

    return await this.findOne({ id: user.id });
  }

  async create(requestUser: RequestUser | undefined, dto: CreateUserDto) {
    const user = await this.userRepository.save(
      plainToInstance(UserEntity, dto),
    );

    const manager = new UserEntityUpdateRelationsManager(
      user.id,
      this.userRepository,
    );

    await manager.setOwner(requestUser);

    return await this.findOne({ id: user.id });
  }

  async update(id: UserEntity['id'], dto: UpdateUserDto) {
    await this.userRepository.update(id, dto);

    return await this.findOne({ id });
  }

  async updateUserProfile(id: UserEntity['id'], dto: UpdateUserProfileDto) {
    await this.userRepository.update(id, dto);

    return await this.findOne({ id });
  }

  async addRoles(id: UserEntity['id'], rolesIds: Array<RoleEntity['id']>) {
    const manager = new UserEntityUpdateRelationsManager(
      id,
      this.userRepository,
    );

    return await manager.forceAddRoles(id, rolesIds);
  }

  async removeRoles(id: UserEntity['id'], rolesIds: RoleEntity['id'][]) {
    const manager = new UserEntityUpdateRelationsManager(
      id,
      this.userRepository,
    );

    return await manager.forceRemoveRoles(id, rolesIds);
  }

  async updateRoles(id: UserEntity['id'], dto: UpdateUserRolesDto) {
    const manager = new UserEntityUpdateRelationsManager(
      id,
      this.userRepository,
    );

    return await manager.update({ roles: dto.rolesIds });
  }

  async updateUserProfilePassword(
    id: UserEntity['id'],
    password: UserEntity['password'],
  ) {
    await this.userRepository.update(
      id,
      plainToInstance(UserEntity, { password }),
    );

    return await this.findOne({ id });
  }

  async activate(id: UserEntity['id']) {
    await this.userRepository.update(id, {
      active: true,
    });

    return await this.findOne({ id });
  }

  async deactivate(id: UserEntity['id']) {
    await this.userRepository.update(id, {
      active: false,
    });

    return await this.findOne({ id });
  }

  async protected(id: UserEntity['id']) {
    await this.userRepository.update(id, {
      protected: true,
    });

    return await this.findOne({ id });
  }

  async deprotected(id: UserEntity['id']) {
    await this.userRepository.update(id, {
      protected: false,
    });

    return await this.findOne({ id });
  }

  async hasAccess(
    requestUser: RequestUser,
    id: UserEntity['id'],
    field?: keyof UserEntity | undefined,
  ): Promise<HasAccess> {
    const ability = await this.caslAbilityFactory.createForUser(requestUser);

    const user = await this.findOne({ id });

    try {
      ForbiddenError.from(ability).throwUnlessCan(
        CaslPermissionsAction.AVAILABLE_UPDATE_USER,
        user,
        field,
      );

      return { isHasAccess: true };
    } catch (error) {
      if (error instanceof ForbiddenError) {
        throw new ForbiddenException(error.message);
      }
      throw new ForbiddenException();
    }
  }

  async drop(id: UserEntity['id']) {
    await this.userRepository.delete({ id });
  }
}
