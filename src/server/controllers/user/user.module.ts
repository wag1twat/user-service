import { Module, ModuleMetadata } from '@nestjs/common';
import { UserService } from './user.service';
import { UserCrudController } from './user-crud.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/server/entities/user.entity';
import { AuthModule } from '../auth/auth.module';
import { RolesService } from '../roles/roles.service';
import { PermissionsService } from '../permissions/permissions.service';
import { RoleRepository } from 'src/server/entities/role.entity';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import { FindUserController } from './find-user.controller';
import { UserRolesController } from './user-roles.controller';
import { UserActivateController } from './user-activate.controller';
import { UserProtectedController } from './user-protected.controller';
import { CaslModule } from 'src/server/casl/casl.module';

export const userModuleMetadata: ModuleMetadata = {
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      RoleRepository,
      PermissionRepository,
    ]),
    AuthModule,
    CaslModule,
  ],
  exports: [UserService],
  controllers: [
    UserCrudController,
    FindUserController,
    UserRolesController,
    UserActivateController,
    UserProtectedController,
  ],
  providers: [UserService, RolesService, PermissionsService],
};
@Module(userModuleMetadata)
export class UserModule {}
