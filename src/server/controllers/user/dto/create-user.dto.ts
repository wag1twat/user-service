import { PickType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';

import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { UserEntity, UserEntityConfig } from 'src/server/entities/user.entity';

export class CreateUserDto extends PickType(UserEntity, [
  'login',
  'firstName',
  'lastName',
  'email',
  'password',
  'active',
  'protected',
]) {
  constructor(data?: CreateUserDto) {
    super();
    Object.assign(this, data);
  }
  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.login.minLength,
    maxLength: UserEntityConfig.login.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.login.minLength)
  @MaxLength(UserEntityConfig.login.maxLength)
  login!: UserEntity['login'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.email.minLength,
    maxLength: UserEntityConfig.email.maxLength,
  })
  @IsEmail({
    message: UserEntityConfig.email.notMatchMessage,
  })
  @MinLength(UserEntityConfig.email.minLength)
  @MaxLength(UserEntityConfig.email.maxLength)
  email!: UserEntity['email'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.firstName.minLength,
    maxLength: UserEntityConfig.firstName.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.firstName.minLength)
  @MaxLength(UserEntityConfig.firstName.maxLength)
  firstName!: UserEntity['firstName'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.lastName.minLength,
    maxLength: UserEntityConfig.lastName.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.lastName.minLength)
  @MaxLength(UserEntityConfig.lastName.maxLength)
  lastName!: UserEntity['lastName'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.password.minLength,
    maxLength: UserEntityConfig.password.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.password.minLength)
  @MaxLength(UserEntityConfig.password.maxLength)
  @Matches(UserEntityConfig.password.regexp, {
    message: UserEntityConfig.password.notMatchMessage,
  })
  password!: UserEntity['password'];

  @ApiProperty({ required: false, type: Boolean })
  @IsOptional()
  @IsBoolean()
  active!: UserEntity['active'];

  @ApiProperty({ required: false, type: Boolean })
  @IsOptional()
  @IsBoolean()
  protected!: UserEntity['protected'];
}
