import { OmitType, PartialType } from '@nestjs/mapped-types';
import { SignupUserDto } from './signup-user.dto';
import { UpdateUserDto } from './update-user.dto';

export class UpdateUserProfileDto extends PartialType(
  OmitType(SignupUserDto, ['password']),
) {
  constructor(data?: Partial<UpdateUserDto>) {
    super();
    Object.assign(this, data);
  }
}
