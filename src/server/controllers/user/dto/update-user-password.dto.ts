import { ApiProperty } from '@nestjs/swagger';
import { IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { Match } from 'src/server/decorators/class-match.decorator';
import { UserEntity, UserEntityConfig } from 'src/server/entities/user.entity';

export class UpdateUserPasswordDto {
  constructor(data?: UpdateUserPasswordDto) {
    Object.assign(this, data);
  }
  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.password.minLength,
    maxLength: UserEntityConfig.password.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.password.minLength)
  @MaxLength(UserEntityConfig.password.maxLength)
  @Matches(UserEntityConfig.password.regexp, {
    message: UserEntityConfig.password.notMatchMessage,
  })
  previosPassword!: UserEntity['password'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.password.minLength,
    maxLength: UserEntityConfig.password.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.password.minLength)
  @MaxLength(UserEntityConfig.password.maxLength)
  @Matches(UserEntityConfig.password.regexp, {
    message: UserEntityConfig.password.notMatchMessage,
  })
  nextPassword!: UserEntity['password'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.password.minLength,
    maxLength: UserEntityConfig.password.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.password.minLength)
  @MaxLength(UserEntityConfig.password.maxLength)
  @Match('nextPassword', {
    message: UserEntityConfig.password.notMatchNextPasswordMessage,
  })
  confirmNextPassword!: UserEntity['password'];
}
