import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsUUID } from 'class-validator';
import { RoleEntity } from 'src/server/entities/role.entity';

export class UpdateUserRolesDto {
  @ApiProperty({ required: false, type: [String] })
  @IsArray()
  @IsUUID(undefined, { each: true })
  rolesIds!: RoleEntity['id'][];
}
