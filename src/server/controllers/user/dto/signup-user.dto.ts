import { OmitType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

export class SignupUserDto extends OmitType(CreateUserDto, [
  'protected',
  'active',
]) {
  constructor(data?: SignupUserDto) {
    super();
    Object.assign(this, data);
  }
}
