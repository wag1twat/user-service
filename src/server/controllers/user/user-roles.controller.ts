import {
  Controller,
  Body,
  Patch,
  Delete,
  Query,
  ParseUUIDPipe,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './user.service';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { RequestUser } from 'src/server/entities/request-user';
import { UserRemoveRolesQueries } from './queries/user-remove-roles.queries';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { UpdateUserRolesDto } from './dto/update-user-roles.dto';
import { UserEntity } from 'src/server/entities/user.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadUsersPermissionHandler,
  UpdateUsersPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('user')
@Controller('/api/v1/user')
export class UserRolesController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
  )
  @Patch('/roles')
  async updateRoles(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: UserEntity['id'],
    @Body(NotEmptyObjectPipe)
    dto: UpdateUserRolesDto,
  ) {
    await this.userService.hasAccess(requestUser, id);

    const data = await this.userService.updateRoles(id, dto);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
  )
  @Delete('/roles')
  async removeRoles(
    @ReqUser() requestUser: RequestUser,
    @Query() queries: UserRemoveRolesQueries,
  ) {
    await this.userService.hasAccess(requestUser, queries.id);

    const data = await this.userService.removeRoles(
      queries.id,
      queries.rolesIds,
    );

    return { data };
  }
}
