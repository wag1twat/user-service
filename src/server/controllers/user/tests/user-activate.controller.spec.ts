import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { UserActivateController } from '../user-activate.controller';

describe.each(testsData)(
  'UserActivateController with params "%s"',
  (userDto) => {
    const modules: TestingModule[] = [];
    let service: UserService;
    let controller: UserActivateController;

    let currentUser: UserEntity;

    beforeAll(async () => {
      const module: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(module);

      service = module.get<UserService>(UserService);
      controller = module.get<UserActivateController>(UserActivateController);
    });

    beforeEach(async () => {
      currentUser = await service.create(undefined, userDto);
    });
    afterEach(async () => {
      await service.drop(currentUser.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);

      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('deactivate > should be user deactivate', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.deactivate(requestUser, user.id);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data.active).toBeFalsy();

      await service.drop(user.id);
    });

    it('activate > should be user activate', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.activate(requestUser, user.id);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data.active).toBeTruthy();

      await service.drop(user.id);
    });
  },
);
