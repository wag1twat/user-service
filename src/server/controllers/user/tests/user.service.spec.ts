import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { UpdateUserDto } from '../dto/update-user.dto';
import { RoleService } from '../../role/role.service';
import { roleModuleMetadata } from '../../role/role.module';
import { testsData } from './tests.data';
import { RoleEntity } from 'src/server/entities/role.entity';
import { ForbiddenException } from '@nestjs/common';

describe.each(testsData)(
  'UserService with params "%s"',
  (userDto, signUpDto, roleDto) => {
    const modules: TestingModule[] = [];
    let service: UserService;
    let roleService: RoleService;

    let currentUser: UserEntity;
    let currentRole: RoleEntity;

    beforeAll(async () => {
      const module: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(module);

      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      modules.push(roleModule);

      service = module.get<UserService>(UserService);
      roleService = roleModule.get<RoleService>(RoleService);
    });

    beforeEach(async () => {
      currentUser = await service.create(undefined, userDto);
    });
    afterEach(async () => {
      await service.drop(currentUser.id);
    });

    beforeEach(async () => {
      const requsetUser = await service.findRequestUser({ id: currentUser.id });
      currentRole = await roleService.create(requsetUser, roleDto);
    });
    afterEach(async () => {
      await roleService.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(roleService).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);

      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );

      expect(currentRole).toBeInstanceOf(RoleEntity);

      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
    });

    it('findOne > should be user finded', async () => {
      const result = await service.findOne({ login: currentUser.login });

      expect(result).toBeInstanceOf(UserEntity);

      expect(result.login === currentUser.login).toBeTruthy();
    });

    it('signup > should be user signup', async () => {
      const result = await service.signup(signUpDto);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result).toEqual(
        expect.objectContaining({
          login: signUpDto.login,
          firstName: signUpDto.firstName,
          lastName: signUpDto.lastName,
          email: signUpDto.email,
        }),
      );

      await service.drop(result.id);
    });

    it('isHasAccess > should be can update no protected user no ownered', async () => {
      const user = await service.create(undefined, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        protected: false,
        active: true,
      });

      const invalidUser = await service.create(undefined, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await service.findRequestUser({
        id: invalidUser.id,
      });

      expect(user.owner).toBe(null);

      const result = await service.hasAccess(invalidRequestUser, user.id);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(user.id);
      await service.drop(invalidUser.id);
    });

    it('isHasAccess > should be can update no protected user ownered', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        protected: false,
        active: true,
      });

      expect(user.owner).toBeInstanceOf(UserEntity);

      const result = await service.hasAccess(requestUser, user.id);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(user.id);
    });

    it('isHasAccess > should be can update protected user no ownered', async () => {
      const requestUser = await service.findRequestUser({ id: currentUser.id });

      const user = await service.create(undefined, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        protected: true,
        active: true,
      });

      expect(user.owner).toBe(null);

      await expect(
        service.hasAccess(requestUser, user.id),
      ).rejects.toThrowError(ForbiddenException);

      await service.drop(user.id);
    });

    it('isHasAccess > should be can update protected user ownered', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidUser = await service.create(requestUser, {
        login: TestUtils.randomString(10),
        firstName: TestUtils.randomString(10),
        lastName: TestUtils.randomString(10),
        password: `${TestUtils.randomString(10)}@123`,
        email: `${TestUtils.randomString(10)}@gmail.com`,
        active: true,
        protected: true,
      });

      const invalidRequestUser = await service.findRequestUser({
        id: invalidUser.id,
      });

      expect(user.owner).toBeInstanceOf(UserEntity);

      const result = await service.hasAccess(requestUser, user.id);

      await expect(
        service.hasAccess(invalidRequestUser, user.id),
      ).rejects.toThrowError(ForbiddenException);

      expect(result.isHasAccess).toBeTruthy();

      await service.drop(user.id);
      await service.drop(invalidUser.id);
    });

    it('updated > should be user updated', async () => {
      const updateDto = new UpdateUserDto({
        login: 'Vasya',
        firstName: 'Vasya',
        lastName: 'Vasya',
        email: 'vasya@gmail.com',
      });

      const result = await service.update(currentUser.id, updateDto);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result).toEqual(expect.objectContaining(updateDto));
    });

    it('updateRoles > should be roles added', async () => {
      const result = await service.addRoles(currentUser.id, [currentRole.id]);

      expect(result.roles).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ id: currentRole.id }),
        ]),
      );
    });

    it('removeRoles > shoul be roles remove', async () => {
      const result = await service.removeRoles(currentUser.id, [
        currentRole.id,
      ]);

      expect(result.roles?.length).toBe(0);
    });

    it('updateRoles > should be roles added', async () => {
      const result = await service.updateRoles(currentUser.id, {
        rolesIds: [currentRole.id],
      });

      expect(result.roles?.length).toBe(1);
    });

    it('updateRoles > should be roles removed', async () => {
      const result = await service.updateRoles(currentUser.id, {
        rolesIds: [],
      });

      expect(result.roles?.length).toBe(0);
    });

    it('deactivate > should be user deactivate', async () => {
      const result = await service.deactivate(currentUser.id);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result.active).toBeFalsy();
    });

    it('activate > should be user activate', async () => {
      const result = await service.activate(currentUser.id);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result.active).toBeTruthy();
    });

    it('deprotected > should be user deprotected', async () => {
      const result = await service.deprotected(currentUser.id);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result.protected).toBeFalsy();
    });

    it('protected > should be user protected', async () => {
      const result = await service.protected(currentUser.id);

      expect(result).toBeInstanceOf(UserEntity);

      expect(result.protected).toBeTruthy();
    });
  },
);
