import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { UserProtectedController } from '../user-protected.controller';

describe.each(testsData)(
  'UserProtectedController with params "%s"',
  (userDto) => {
    const modules: TestingModule[] = [];
    let service: UserService;
    let controller: UserProtectedController;

    let currentUser: UserEntity;

    beforeAll(async () => {
      const module: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(module);

      service = module.get<UserService>(UserService);
      controller = module.get<UserProtectedController>(UserProtectedController);
    });

    beforeEach(async () => {
      currentUser = await service.create(undefined, userDto);
    });
    afterEach(async () => {
      await service.drop(currentUser.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);

      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('deprotected > should be user deprotected', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.deprotected(requestUser, user.id);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data.protected).toBeFalsy();

      await service.drop(user.id);
    });

    it('protected > should be user protected', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.protected(requestUser, user.id);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data.protected).toBeTruthy();

      await service.drop(user.id);
    });
  },
);
