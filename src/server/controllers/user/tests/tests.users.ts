import { TestUtils } from 'src/server/shared';
import { CreateUserDto } from '../../user/dto/create-user.dto';
import { SignupUserDto } from '../dto/signup-user.dto';

export const testsUsers = (length: number, name: string): CreateUserDto[] =>
  Array.from(Array(length).keys()).map((i) => ({
    login: name + TestUtils.randomString(10) + i,
    firstName: name + TestUtils.randomString(10) + i,
    lastName: name + TestUtils.randomString(10) + i,
    email: `${name}${TestUtils.randomString(10)}${i}@gmail.com`,
    password: 'User@123',
    active: true,
    protected: false,
  }));

export const testsSignups = (length: number, name: string): SignupUserDto[] =>
  Array.from(Array(length).keys()).map((i) => ({
    login: name + TestUtils.randomString(10) + i,
    firstName: name + TestUtils.randomString(10) + i,
    lastName: name + TestUtils.randomString(10) + i,
    email: `${name}${TestUtils.randomString(10)}${i}@gmail.com`,
    password: 'User@123',
  }));
