import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { RoleService } from '../../role/role.service';
import { roleModuleMetadata } from '../../role/role.module';
import { testsData } from './tests.data';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserRolesController } from '../user-roles.controller';

describe.each(testsData)(
  'UserRolesController with params "%s"',
  (userDto, _, roleDto) => {
    const modules: TestingModule[] = [];
    let service: UserService;
    let controller: UserRolesController;
    let roleService: RoleService;

    let currentUser: UserEntity;
    let currentRole: RoleEntity;

    beforeAll(async () => {
      const module: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(module);

      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      modules.push(roleModule);

      service = module.get<UserService>(UserService);
      controller = module.get<UserRolesController>(UserRolesController);
      roleService = roleModule.get<RoleService>(RoleService);
    });

    beforeEach(async () => {
      currentUser = await service.create(undefined, userDto);
    });
    afterEach(async () => {
      await service.drop(currentUser.id);
    });

    beforeEach(async () => {
      currentRole = await roleService.create(undefined, roleDto);
    });
    afterEach(async () => {
      await roleService.drop(currentRole.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
      expect(roleService).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);

      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );

      expect(currentRole).toBeInstanceOf(RoleEntity);

      expect(currentRole).toEqual(
        expect.objectContaining({
          name: roleDto.name,
        }),
      );
    });

    it('updateRoles > should be roles added', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.updateRoles(requestUser, user.id, {
        rolesIds: [currentRole.id],
      });

      expect(result.data.roles).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ id: currentRole.id }),
        ]),
      );

      await service.drop(user.id);
    });

    it('updateRoles > should be roles remove', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const result = await controller.updateRoles(requestUser, user.id, {
        rolesIds: [],
      });

      expect(result.data.roles?.length).toBe(0);

      await service.drop(user.id);
    });
  },
);
