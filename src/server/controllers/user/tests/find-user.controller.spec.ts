import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { testsData } from './tests.data';
import { FindUserController } from '../find-user.controller';
import { ForbiddenException } from '@nestjs/common';

describe.each(testsData)('FindUserController with params "%s"', (userDto) => {
  const modules: TestingModule[] = [];
  let service: UserService;
  let controller: FindUserController;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    modules.push(module);

    service = module.get<UserService>(UserService);
    controller = module.get<FindUserController>(FindUserController);
  });

  beforeEach(async () => {
    currentUser = await service.create(undefined, userDto);
  });
  afterEach(async () => {
    await service.drop(currentUser.id);
  });

  afterAll(async () => {
    await Promise.all(modules.map((module) => module.close()));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findOne > should be user finded', async () => {
    const result = await controller.findOne({ login: currentUser.login });

    expect(result.data).toBeInstanceOf(UserEntity);

    expect(result.data.login === currentUser.login).toBeTruthy();
  });

  it('isHasAccess > should be can update no protected user no ownered', async () => {
    const user = await service.create(undefined, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      protected: false,
      active: true,
    });

    const invalidUser = await service.create(undefined, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await service.findRequestUser({
      id: invalidUser.id,
    });

    expect(user.owner).toBe(null);

    try {
      const result = await controller.hasAccess(invalidRequestUser, user.id);

      expect(result.isHasAccess).toBeTruthy();
    } catch (error) {
      expect(error).toBeInstanceOf(ForbiddenException);
    }

    await service.drop(user.id);
    await service.drop(invalidUser.id);
  });

  it('isHasAccess > should be can update no protected user ownered', async () => {
    const requestUser = await service.findRequestUser({
      id: currentUser.id,
    });

    const user = await service.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      protected: false,
      active: true,
    });

    expect(user.owner).toBeInstanceOf(UserEntity);

    const result = await controller.hasAccess(requestUser, user.id);

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(user.id);
  });

  it('isHasAccess > should be can update protected user no ownered', async () => {
    const requestUser = await service.findRequestUser({ id: currentUser.id });

    const user = await service.create(undefined, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      protected: true,
      active: true,
    });

    expect(user.owner).toBe(null);

    await expect(
      controller.hasAccess(requestUser, user.id),
    ).rejects.toThrowError(ForbiddenException);

    await service.drop(user.id);
  });

  it('isHasAccess > should be can update protected user ownered', async () => {
    const requestUser = await service.findRequestUser({
      id: currentUser.id,
    });

    const user = await service.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidUser = await service.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await service.findRequestUser({
      id: invalidUser.id,
    });

    expect(user.owner).toBeInstanceOf(UserEntity);

    const result = await controller.hasAccess(requestUser, user.id);

    expect(result.isHasAccess).toBeTruthy();

    await expect(
      controller.hasAccess(invalidRequestUser, user.id),
    ).rejects.toThrowError(ForbiddenException);

    await service.drop(user.id);
    await service.drop(invalidUser.id);
  });
});
