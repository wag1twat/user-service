import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from '../user.service';
import { userModuleMetadata } from '../user.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { UpdateUserDto } from '../dto/update-user.dto';
import { roleModuleMetadata } from '../../role/role.module';
import { testsData } from './tests.data';
import { UserCrudController } from '../user-crud.controller';

describe.each(testsData)(
  'UserCrudController with params "%s"',
  (userDto, signUpDto) => {
    const modules: TestingModule[] = [];
    let service: UserService;
    let controller: UserCrudController;

    let currentUser: UserEntity;

    beforeAll(async () => {
      const module: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      modules.push(module);

      const roleModule: TestingModule = await mockModuleWithConnection(
        roleModuleMetadata,
      );

      modules.push(roleModule);

      service = module.get<UserService>(UserService);
      controller = module.get<UserCrudController>(UserCrudController);
    });

    beforeEach(async () => {
      currentUser = await service.create(undefined, userDto);
    });
    afterEach(async () => {
      await service.drop(currentUser.id);
    });

    afterAll(async () => {
      await Promise.all(modules.map((module) => module.close()));
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);

      expect(currentUser).toEqual(
        expect.objectContaining({
          login: userDto.login,
          firstName: userDto.firstName,
          lastName: userDto.lastName,
          email: userDto.email,
        }),
      );
    });

    it('signup > should be user signup', async () => {
      const result = await controller.signup(signUpDto);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data).toEqual(
        expect.objectContaining({
          login: signUpDto.login,
          firstName: signUpDto.firstName,
          lastName: signUpDto.lastName,
          email: signUpDto.email,
        }),
      );

      await service.drop(result.data.id);
    });

    it('update > should be user updated', async () => {
      const requestUser = await service.findRequestUser({
        id: currentUser.id,
      });

      const user = await service.create(requestUser, {
        login: 'user_test',
        firstName: 'user_test',
        lastName: 'user_test',
        email: 'user_test@gmail.com',
        password: 'User@123',
        active: true,
        protected: false,
      });

      const updateDto = new UpdateUserDto({
        login: 'Vasya',
        firstName: 'Vasya',
        lastName: 'Vasya',
        email: 'vasya@gmail.com',
      });

      const result = await controller.update(requestUser, user.id, updateDto);

      expect(result.data).toBeInstanceOf(UserEntity);

      expect(result.data).toEqual(expect.objectContaining(updateDto));

      await service.drop(user.id);
    });
  },
);
