import { CreateRoleDto } from '../../role/dto/create-role.dto';

export const testsRoles = (length: number, name: string): CreateRoleDto[] =>
  Array.from(Array(length).keys()).map((i) => ({
    name: `${name}_${i}`,
    active: true,
    protected: false,
    permissionsIds: [],
    usersIds: [],
  }));
