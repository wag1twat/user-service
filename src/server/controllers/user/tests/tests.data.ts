import { testsUsersLength } from './constants';
import { testsRoles } from './tests.roles';
import { testsUsers, testsSignups } from './tests.users';

const testsSignUpUsers = testsSignups(testsUsersLength, 'signup');

const roles = testsRoles(testsUsersLength, 'role');

export const testsData = testsUsers(testsUsersLength, 'user').map(
  (user, i) => [user, testsSignUpUsers[i], roles[i]] as const,
);
