import { Controller, Query, Get, ParseUUIDPipe } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './user.service';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { UserQueries } from './queries/user.queries';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { UserEntity } from 'src/server/entities/user.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  DeleteUsersPermissionHandler,
  ReadUsersPermissionHandler,
  UpdateUsersPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('user')
@Controller('/api/v1/user')
export class FindUserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @CheckPermissions(new ReadUsersPermissionHandler())
  @Get()
  async findOne(@Query(NotEmptyObjectPipe) queries: UserQueries) {
    const data = await this.userService.findOne(queries);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
    new DeleteUsersPermissionHandler(),
  )
  @Get('/has-update-access')
  async hasAccess(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: UserEntity['id'],
  ) {
    return await this.userService.hasAccess(requestUser, id);
  }
}
