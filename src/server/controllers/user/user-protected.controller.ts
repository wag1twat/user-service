import {
  Controller,
  Patch,
  Delete,
  Query,
  ParseUUIDPipe,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './user.service';
import { RequestUser } from 'src/server/entities/request-user';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { UserEntity } from 'src/server/entities/user.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadUsersPermissionHandler,
  UpdateUsersPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('user')
@Controller('/api/v1/user')
export class UserProtectedController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
  )
  @Patch('/protected')
  async protected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: UserEntity['id'],
  ) {
    await this.userService.hasAccess(requestUser, id, 'protected');

    const data = await this.userService.protected(id);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
  )
  @Delete('/deprotected')
  async deprotected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: UserEntity['id'],
  ) {
    await this.userService.hasAccess(requestUser, id, 'protected');

    const data = await this.userService.deprotected(id);

    return { data };
  }
}
