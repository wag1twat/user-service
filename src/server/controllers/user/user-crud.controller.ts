import {
  Controller,
  Post,
  Body,
  Patch,
  Query,
  ParseUUIDPipe,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PublicRoute } from 'src/server/controllers/auth/jwt-auth.guard';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { RequestUser } from 'src/server/entities/request-user';
import { AuthService } from 'src/server/controllers/auth/auth.service';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { SignupUserDto } from './dto/signup-user.dto';
import { UserEntity } from 'src/server/entities/user.entity';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  CreateUsersPermissionHandler,
  ReadUsersPermissionHandler,
  UpdateUsersPermissionHandler,
} from '../../permissions/permissions-handlers';

@ApiTags('user')
@Controller('/api/v1/user')
export class UserCrudController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @Post('/signup')
  @PublicRoute()
  async signup(
    @Body(NotEmptyObjectPipe)
    dto: SignupUserDto,
  ) {
    const data = await this.userService.signup(dto);

    const loginResult = await this.authService.login({
      login: dto.login,
      password: dto.password,
    });

    return {
      data: Object.assign(data, loginResult),
    };
  }

  @ApiBearerAuth()
  @CheckPermissions(new CreateUsersPermissionHandler())
  @Post('/create')
  async create(
    @ReqUser() requestUser: RequestUser,
    @Body(NotEmptyObjectPipe)
    dto: CreateUserDto,
  ) {
    const data = await this.userService.create(requestUser, dto);

    return {
      data,
    };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadUsersPermissionHandler(),
    new UpdateUsersPermissionHandler(),
  )
  @Patch()
  async update(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: UserEntity['id'],
    @Body(NotEmptyObjectPipe)
    dto: UpdateUserDto,
  ) {
    await this.userService.hasAccess(requestUser, id);

    const data = await this.userService.update(id, dto);

    return { data };
  }
}
