export const message =
  'You cannot update a user. You cannot update yourself, the user is protected or you are not its creator.';
