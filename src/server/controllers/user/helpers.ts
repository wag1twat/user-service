import { filter, includes, map } from 'lodash';
import { RequestUser } from 'src/server/entities/request-user';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity, UserRepository } from 'src/server/entities/user.entity';

interface Dto {
  roles: Array<RoleEntity['id']>;
}

export class UserEntityUpdateRelationsManager {
  constructor(
    private readonly id: UserEntity['id'],
    private readonly userRepository: UserRepository,
  ) {}

  async setOwner(requestUser: RequestUser | undefined) {
    if (requestUser) {
      await this.userRepository
        .createQueryBuilder('user')
        .relation('owner')
        .of(this.id)
        .set(requestUser.id);
    }

    return await this.userRepository.findOneOrFail(
      {
        id: this.id,
      },
      { relations: ['owner'] },
    );
  }

  async forceAddRoles(id: UserEntity['id'], roles: Array<RoleEntity['id']>) {
    await this.userRepository
      .createQueryBuilder('user')
      .relation('roles')
      .of(id)
      .add(roles);

    return await this.userRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'roles'] },
    );
  }

  async forceRemoveRoles(id: UserEntity['id'], roles: Array<RoleEntity['id']>) {
    await this.userRepository
      .createQueryBuilder('user')
      .relation('roles')
      .of(id)
      .remove(roles);

    return await this.userRepository.findOneOrFail(
      {
        id,
      },
      { relations: ['owner', 'roles'] },
    );
  }

  private async addRoles(dto: Dto) {
    const user = await this.userRepository.findOneOrFail(
      {
        id: this.id,
      },
      { relations: ['roles'] },
    );

    const rolesIds = map(user.roles, (role) => role.id);

    if (rolesIds.length === 0) {
      return await this.forceAddRoles(this.id, dto.roles);
    }

    const forAdd = filter(dto.roles, (id) => !rolesIds.includes(id));

    return await this.forceAddRoles(this.id, forAdd);
  }

  private async removeRoles(dto: Dto) {
    const user = await this.userRepository.findOneOrFail(
      {
        id: this.id,
      },
      { relations: ['roles'] },
    );

    const rolesIds = map(user.roles, (role) => role.id);

    const forRemove = filter(rolesIds, (id) => !includes(dto.roles, id));

    return await this.forceRemoveRoles(this.id, forRemove);
  }

  async update(dto: Dto) {
    await this.addRoles(dto);
    await this.removeRoles(dto);

    return await this.userRepository.findOneOrFail(
      { id: this.id },
      { relations: ['owner', 'roles'] },
    );
  }
}
