import { map } from 'rxjs';
import { Order } from 'src/server/queries/queries-sort';
import { SelectQueryBuilder } from 'typeorm';

export function withLeftJoin<T = unknown>(alias: string, property: string) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    queryBuilder.leftJoin(`${alias}.${property}`, property),
  );
}

export function withWhere<T = unknown>(
  ...where: Parameters<SelectQueryBuilder<T>['where']>
) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    queryBuilder.where(where),
  );
}

export function withPagination<T = unknown>(page?: number, limit?: number) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    page && limit
      ? queryBuilder.skip((page - 1) * limit).take(limit)
      : queryBuilder,
  );
}

export function withSorting<T = unknown, S = string>(
  alias: string,
  order: Order,
  sortBy?: S,
) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    sortBy
      ? queryBuilder.orderBy(`${alias}.${sortBy}`, order, 'NULLS LAST')
      : queryBuilder,
  );
}

export function withOwner<T = unknown>(alias: string) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    queryBuilder.leftJoinAndSelect(`${alias}.owner`, 'owner'),
  );
}

export function withAndWhereIds<T = unknown>(
  enabled: boolean,
  ...where: Parameters<SelectQueryBuilder<T>['andWhere']>
) {
  return map<SelectQueryBuilder<T>, SelectQueryBuilder<T>>((queryBuilder) =>
    enabled ? queryBuilder.andWhere(...where) : queryBuilder,
  );
}

export function withGetManyAndCount<T = unknown>() {
  return map<SelectQueryBuilder<T>, Promise<[T[], number]>>((queryBuilder) =>
    queryBuilder.getManyAndCount(),
  );
}
