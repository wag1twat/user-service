"use strict";
exports.__esModule = true;
exports.withGetManyAndCount = exports.withAndWhereIds = exports.withOwner = exports.withSorting = exports.withPagination = exports.withWhere = exports.withLeftJoin = void 0;
var rxjs_1 = require("rxjs");
function withLeftJoin(alias, property) {
    return (0, rxjs_1.map)(function (queryBuilder) {
        return queryBuilder.leftJoin("".concat(alias, ".").concat(property), property);
    });
}
exports.withLeftJoin = withLeftJoin;
function withWhere() {
    var where = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        where[_i] = arguments[_i];
    }
    return (0, rxjs_1.map)(function (queryBuilder) {
        return queryBuilder.where(where);
    });
}
exports.withWhere = withWhere;
function withPagination(page, limit) {
    return (0, rxjs_1.map)(function (queryBuilder) {
        return page && limit
            ? queryBuilder.skip((page - 1) * limit).take(limit)
            : queryBuilder;
    });
}
exports.withPagination = withPagination;
function withSorting(alias, order, sortBy) {
    return (0, rxjs_1.map)(function (queryBuilder) {
        return sortBy
            ? queryBuilder.orderBy("".concat(alias, ".").concat(sortBy), order, 'NULLS LAST')
            : queryBuilder;
    });
}
exports.withSorting = withSorting;
function withOwner(alias) {
    return (0, rxjs_1.map)(function (queryBuilder) {
        return queryBuilder.leftJoinAndSelect("".concat(alias, ".owner"), 'owner');
    });
}
exports.withOwner = withOwner;
function withAndWhereIds(enabled) {
    var where = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        where[_i - 1] = arguments[_i];
    }
    return (0, rxjs_1.map)(function (queryBuilder) {
        return enabled ? queryBuilder.andWhere.apply(queryBuilder, where) : queryBuilder;
    });
}
exports.withAndWhereIds = withAndWhereIds;
function withGetManyAndCount() {
    return (0, rxjs_1.map)(function (queryBuilder) {
        return queryBuilder.getManyAndCount();
    });
}
exports.withGetManyAndCount = withGetManyAndCount;
