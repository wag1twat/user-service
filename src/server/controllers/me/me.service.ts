import { Injectable } from '@nestjs/common';
import { UpdateUserPasswordDto } from 'src/server/controllers/user/dto/update-user-password.dto';
import { AuthService } from 'src/server/controllers/auth/auth.service';
import { UserService } from 'src/server/controllers/user/user.service';
import { RequestUser } from 'src/server/entities/request-user';
import { UpdateUserProfileDto } from '../user/dto/update-profile.dto';

@Injectable()
export class MeService {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}
  async findOne(userCrendials: RequestUser) {
    return await this.userService.findOne({
      id: userCrendials.id,
    });
  }

  async findRoles(userCrendials: RequestUser) {
    return await this.userService.findRoles(userCrendials.id);
  }

  async findPermissions(userCrendials: RequestUser) {
    return await this.userService.findPermissions(userCrendials.id);
  }

  async update(RequestUser: RequestUser, dto: UpdateUserProfileDto) {
    const result = await this.userService.updateUserProfile(
      RequestUser.id,
      dto,
    );

    const access_token = this.authService.signToken({
      id: RequestUser.id,
      login: RequestUser.login,
    });

    return Object.assign(result, { access_token });
  }

  async deactivate(userCrendials: RequestUser) {
    return await this.userService.deactivate(userCrendials.id);
  }

  async activate(userCrendials: RequestUser) {
    return await this.userService.activate(userCrendials.id);
  }

  async updateUserProfilePassword(
    userCrendials: RequestUser,
    dto: UpdateUserPasswordDto,
  ) {
    const checkPassowrdResult = await userCrendials.checkPassword(
      dto.previosPassword,
    );

    const result = await this.userService.updateUserProfilePassword(
      checkPassowrdResult.id,
      dto.nextPassword,
    );

    return await this.authService.login({
      login: result.login,
      password: dto.nextPassword,
    });
  }
}
