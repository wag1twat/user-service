import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/server/controllers/auth/jwt-auth.guard';
import { MeService } from './me.service';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RequestUser } from 'src/server/entities/request-user';

@ApiTags('me')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('/api/v1/me')
export class FindMeController {
  constructor(private readonly meService: MeService) {}

  @Get()
  async findOne(@ReqUser() requestUser: RequestUser) {
    const data = await this.meService.findOne(requestUser);

    return { data };
  }

  @Get('/roles')
  async findRoles(@ReqUser() requestUser: RequestUser) {
    const [data, count] = await this.meService.findRoles(requestUser);

    return { data, count };
  }

  @Get('/permissions')
  async findPermissions(@ReqUser() requestUser: RequestUser) {
    const [data, count] = await this.meService.findPermissions(requestUser);

    return { data, count };
  }
}
