import { Body, Controller, Patch, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/server/controllers/auth/jwt-auth.guard';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { UpdateUserPasswordDto } from 'src/server/controllers/user/dto/update-user-password.dto';
import { MeService } from './me.service';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RequestUser } from 'src/server/entities/request-user';
import { UpdateUserProfileDto } from '../user/dto/update-profile.dto';

@ApiTags('me')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('/api/v1/me')
export class MeCrudController {
  constructor(private readonly meService: MeService) {}

  @Patch()
  async update(
    @ReqUser() requestUser: RequestUser,
    @Body(NotEmptyObjectPipe)
    dto: UpdateUserProfileDto,
  ) {
    const data = await this.meService.update(requestUser, dto);

    return { data };
  }

  @Patch('/change-password')
  async updateUserProfilePassword(
    @ReqUser() requestUser: RequestUser,
    @Body(NotEmptyObjectPipe) dto: UpdateUserPasswordDto,
  ) {
    const data = await this.meService.updateUserProfilePassword(
      requestUser,
      dto,
    );

    return { data };
  }
}
