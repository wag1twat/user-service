import { Module, ModuleMetadata } from '@nestjs/common';
import { MeService } from './me.service';
import { AuthModule } from '../auth/auth.module';
import { UserService } from '../user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/server/entities/user.entity';
import { RoleRepository } from 'src/server/entities/role.entity';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import { RolesService } from '../roles/roles.service';
import { PermissionsService } from '../permissions/permissions.service';
import { FindMeController } from './find-me.controller';
import { MeActivateController } from './me-activate.controller';
import { MeCrudController } from './me-crud.controller';
import { CaslModule } from 'src/server/casl/casl.module';

export const meModuleMetadata: ModuleMetadata = {
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      RoleRepository,
      PermissionRepository,
    ]),
    AuthModule,
    CaslModule,
  ],
  exports: [MeService],
  controllers: [FindMeController, MeActivateController, MeCrudController],
  providers: [MeService, UserService, RolesService, PermissionsService],
};

@Module(meModuleMetadata)
export class MeModule {}
