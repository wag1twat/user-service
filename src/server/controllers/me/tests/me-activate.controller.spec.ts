import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from 'src/server/controllers/user/user.service';
import { meModuleMetadata } from '../me.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { MeActivateController } from '../me-activate.controller';

describe.each(testsData)('MeActivateController with params "%s"', (dto) => {
  let testingModule: TestingModule;
  let controller: MeActivateController;
  let userService: UserService;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      meModuleMetadata,
    );

    testingModule = module;

    controller = module.get<MeActivateController>(MeActivateController);

    userService = module.get<UserService>(UserService);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await testingModule.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('activate > should be user activate', async () => {
    const requestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const result = await controller.activate(requestUser);

    expect(result.data).toBeInstanceOf(UserEntity);

    expect(result.data.active).toBeTruthy();
  });

  it('deactivate > should be user deactivate', async () => {
    const requestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const result = await controller.deactivate(requestUser);

    expect(result.data).toBeInstanceOf(UserEntity);

    expect(result.data.active).toBeFalsy();
  });
});
