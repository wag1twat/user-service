import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from 'src/server/controllers/user/user.service';
import { meModuleMetadata } from '../me.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { FindMeController } from '../find-me.controller';

describe.each(testsData)('FindMeController with params "%s"', (dto) => {
  let testingModule: TestingModule;
  let controller: FindMeController;
  let userService: UserService;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      meModuleMetadata,
    );

    testingModule = module;

    controller = module.get<FindMeController>(FindMeController);

    userService = module.get<UserService>(UserService);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await testingModule.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('findOne > should be user finded', async () => {
    const requestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const result = await controller.findOne(requestUser);

    expect(result.data).toBeInstanceOf(UserEntity);
  });
});
