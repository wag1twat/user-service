import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserService } from 'src/server/controllers/user/user.service';
import { UpdateUserDto } from 'src/server/controllers/user/dto/update-user.dto';
import { UpdateUserPasswordDto } from 'src/server/controllers/user/dto/update-user-password.dto';
import { meModuleMetadata } from '../me.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { isJWT } from 'class-validator';
import { MeCrudController } from '../me-crud.controller';

describe.each(testsData)('MeCrudController with params "%s"', (dto) => {
  let testingModule: TestingModule;
  let controller: MeCrudController;
  let userService: UserService;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      meModuleMetadata,
    );

    testingModule = module;

    controller = module.get<MeCrudController>(MeCrudController);

    userService = module.get<UserService>(UserService);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await testingModule.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('update > should be user updated', async () => {
    const requestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const updateUserDto = new UpdateUserDto({
      login: 'Vasya',
      firstName: 'Vasya',
      lastName: 'Vasya',
      email: 'Vasya@gmail.com',
    });

    const result = await controller.update(requestUser, updateUserDto);

    expect(isJWT(result.data.access_token)).toBeTruthy();
    expect(result.data).toEqual(expect.objectContaining(updateUserDto));
  });

  it('updateUserProfilePassword > should be user password changed', async () => {
    const requestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const changePasswordDto = new UpdateUserPasswordDto({
      previosPassword: dto.password,
      nextPassword: dto.password,
      confirmNextPassword: dto.password,
    });

    const result = await controller.updateUserProfilePassword(
      requestUser,
      changePasswordDto,
    );

    expect(isJWT(result.data.access_token)).toBeTruthy();
  });
});
