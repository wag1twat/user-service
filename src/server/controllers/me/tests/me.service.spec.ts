import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { MeService } from '../me.service';
import { UpdateUserPasswordDto } from '../../user/dto/update-user-password.dto';
import { UpdateUserDto } from '../../user/dto/update-user.dto';
import { meModuleMetadata } from '../me.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { UserService } from '../../user/user.service';
import { isJWT } from 'class-validator';
import { testsData } from './tests.data';

describe.each(testsData)('MeService with params "%s"', (dto) => {
  let service: MeService;
  let testingModule: TestingModule;
  let userService: UserService;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      meModuleMetadata,
    );

    testingModule = module;
    userService = module.get<UserService>(UserService);
    service = module.get<MeService>(MeService);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await testingModule.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('findOne > should be user finded', async () => {
    const RequestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const user = await service.findOne(RequestUser);

    expect(user).toBeInstanceOf(UserEntity);

    expect(user).toEqual(expect.objectContaining({ login: currentUser.login }));
  });

  it('deactivate > should be user deactivate', async () => {
    const RequestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const result = await service.deactivate(RequestUser);

    expect(result).toBeInstanceOf(UserEntity);

    expect(result.active).toBeFalsy();
  });

  it('activate > should be user activate', async () => {
    const RequestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const result = await service.activate(RequestUser);

    expect(result).toBeInstanceOf(UserEntity);

    expect(result.active).toBeTruthy();
  });

  it('update > should be user updated', async () => {
    const RequestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const updateUserDto = new UpdateUserDto({
      login: 'Vasya',
      firstName: 'Vasya',
      lastName: 'Vasya',
      email: 'vasya@gmail.com',
    });

    const result = await service.update(RequestUser, updateUserDto);

    expect(isJWT(result.access_token)).toBeTruthy();
    expect(result).toEqual(expect.objectContaining(updateUserDto));
  });

  it('updateUserProfilePassword > should be user password changed', async () => {
    const RequestUser = await userService.findRequestUser({
      login: currentUser.login,
    });

    const changePasswordDto = new UpdateUserPasswordDto({
      previosPassword: dto.password,
      nextPassword: dto.password,
      confirmNextPassword: dto.password,
    });

    const result = await service.updateUserProfilePassword(
      RequestUser,
      changePasswordDto,
    );

    expect(isJWT(result.access_token)).toBeTruthy();
  });
});
