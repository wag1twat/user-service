import { Controller, Delete, Patch, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/server/controllers/auth/jwt-auth.guard';
import { MeService } from './me.service';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { RequestUser } from 'src/server/entities/request-user';

@ApiTags('me')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('/api/v1/me')
export class MeActivateController {
  constructor(private readonly meService: MeService) {}

  @Patch('/activate')
  async activate(@ReqUser() requestUser: RequestUser) {
    const data = await this.meService.activate(requestUser);

    return { data };
  }

  @Delete('/deactivate')
  async deactivate(@ReqUser() requestUser: RequestUser) {
    const data = await this.meService.deactivate(requestUser);

    return { data };
  }
}
