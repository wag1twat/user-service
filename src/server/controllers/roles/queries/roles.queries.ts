import { IntersectionType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsArray, IsOptional, IsUUID } from 'class-validator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { QueriesBase } from 'src/server/queries/queries-base';
import { QueriesPagination } from 'src/server/queries/queries-pagination';
import { RoleEntity, RoleEntityConfig } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { FindOperator, Raw } from 'typeorm';

type SortBy = keyof Pick<
  RoleEntity,
  'id' | 'name' | 'createdAt' | 'active' | 'owner' | 'updatedAt'
>;

export class RolesQueries extends IntersectionType<
  QueriesBase<SortBy>,
  QueriesPagination
>(QueriesBase, QueriesPagination) {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsUUID()
  id?: RoleEntity['id'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: RoleEntityConfig.name.minLength,
    maxLength: RoleEntityConfig.name.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) Like LOWER('%${params.value}%')`),
  )
  name?: RoleEntity['name'];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @IsUUID(undefined, { each: true })
  usersIds?: UserEntity['id'][];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @IsUUID(undefined, { each: true })
  permissionsIds?: PermissionEntity['id'][];
}
