import { testsRolesLength } from './constants';
import { testsRoles } from './tests.roles';

export const testsData = testsRoles(testsRolesLength, 'role');
