import { TestingModule } from '@nestjs/testing';
import { RoleEntity } from 'src/server/entities/role.entity';
import { rolesModuleMetadata } from '../roles.module';
import { RolesService } from '../roles.service';
import { mockModuleWithConnection } from 'src/server/shared';
import { testsData } from './tests.data';
import { roleModuleMetadata } from '../../role/role.module';
import { RoleService } from '../../role/role.service';

describe.each(testsData)('RolesService with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let service: RolesService;
  let roleService: RoleService;
  let currentRole: RoleEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      rolesModuleMetadata,
    );

    service = module.get<RolesService>(RolesService);

    modules.push(module);

    const roleModule: TestingModule = await mockModuleWithConnection(
      roleModuleMetadata,
    );

    roleService = roleModule.get<RoleService>(RoleService);

    modules.push(roleModule);
  });

  beforeEach(async () => {
    currentRole = await roleService.create(undefined, dto);
  });

  afterEach(async () => {
    await roleService.drop(currentRole.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(roleService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentRole).toBeInstanceOf(RoleEntity);
    expect(currentRole).toEqual(
      expect.objectContaining({
        name: dto.name,
      }),
    );
  });

  it('findAll > shoud be finded roles', async () => {
    const [roles, count] = await service.findAll();

    expect(roles).toEqual(expect.arrayContaining([expect.any(RoleEntity)]));
    expect(count).toBe(1);
  });

  it('findAll > shoud be finded roles by queries', async () => {
    const [roles, count] = await service.findAll({
      id: currentRole.id,
      name: currentRole.name,
      active: currentRole.active,
    });

    expect(roles).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: currentRole.id,
          name: currentRole.name,
          active: currentRole.active,
        }),
      ]),
    );

    expect(count).toBe(1);
  });
});
