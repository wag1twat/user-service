import { TestUtils } from '../../../shared/test-utils';
import { CreateRoleDto } from '../../role/dto/create-role.dto';

export const testsRoles = (length: number, name: string): CreateRoleDto[] =>
  Array.from(Array(length).keys()).map((i) => {
    return {
      name: `${name}${TestUtils.randomString(10)}${i}`,
      active: true,
      protected: false,
      permissionsIds: [],
      usersIds: [],
    };
  });
