import { Module, ModuleMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleRepository } from '../../entities/role.entity';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';

export const rolesModuleMetadata: ModuleMetadata = {
  imports: [TypeOrmModule.forFeature([RoleRepository])],
  exports: [RolesService],
  controllers: [RolesController],
  providers: [RolesService],
};
@Module(rolesModuleMetadata)
export class RolesModule {}
