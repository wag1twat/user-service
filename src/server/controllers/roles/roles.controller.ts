import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import { ReadRolesPermissionHandler } from '../../permissions/permissions-handlers';
import { RolesQueries } from './queries/roles.queries';
import { RolesService } from './roles.service';

@ApiTags('roles')
@ApiBearerAuth()
@Controller('/api/v1/roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}
  @CheckPermissions(new ReadRolesPermissionHandler())
  @Get()
  async findAll(@Query() queries: RolesQueries) {
    const [data, count] = await this.rolesService.findAll(queries);
    return { data, count };
  }
}
