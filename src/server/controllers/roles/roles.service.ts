import { Injectable } from '@nestjs/common';
import { RolesQueries } from './queries/roles.queries';
import { RoleEntity, RoleRepository } from 'src/server/entities/role.entity';
import { lastValueFrom, of } from 'rxjs';
import {
  withAndWhereIds,
  withGetManyAndCount,
  withLeftJoin,
  withOwner,
  withPagination,
  withSorting,
  withWhere,
} from '../rxjs-operators';

@Injectable()
export class RolesService {
  constructor(private readonly roleRepository: RoleRepository) {}

  async findAll(where: RolesQueries = {}): Promise<[RoleEntity[], number]> {
    const {
      usersIds = [],
      permissionsIds = [],
      page,
      limit,
      sortBy,
      order,
      ...conditions
    } = where;

    const alias = 'role';

    return lastValueFrom(
      of(this.roleRepository.createQueryBuilder(alias)).pipe(
        withOwner(alias),
        withWhere(conditions),
        withLeftJoin(alias, 'users'),
        withLeftJoin(alias, 'permissions'),
        withAndWhereIds(
          Boolean(usersIds.length),
          'users.id IN (:...usersIds)',
          { usersIds },
        ),
        withAndWhereIds(
          Boolean(permissionsIds.length),
          'permissions.id IN (:...permissionsIds)',
          {
            permissionsIds,
          },
        ),
        withPagination(page, limit),
        withSorting(alias, order, sortBy),
        withGetManyAndCount(),
      ),
    );
  }
}
