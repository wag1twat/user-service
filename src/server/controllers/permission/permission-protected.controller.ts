import {
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import { UpdatePermissionsPermissionHandler } from '../../permissions/permissions-handlers';
import { PermissionService } from './permission.service';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/permission')
export class PermissionProtectedController {
  constructor(private readonly permissionService: PermissionService) {}

  @ApiBearerAuth()
  @CheckPermissions(new UpdatePermissionsPermissionHandler())
  @Patch('/protected')
  async protected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
  ) {
    await this.permissionService.isHasUpdateAccess(
      requestUser,
      id,
      'protected',
    );

    const data = await this.permissionService.protected(id);
    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(new UpdatePermissionsPermissionHandler())
  @Delete('/deprotected')
  async deprotected(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
  ) {
    await this.permissionService.isHasUpdateAccess(
      requestUser,
      id,
      'protected',
    );

    const data = await this.permissionService.deprotected(id);
    return { data };
  }
}
