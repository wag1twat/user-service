import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CreatePermissionDto } from './create-permission.dto';

export class UpdatePermissionDto extends PartialType(
  OmitType(CreatePermissionDto, ['active', 'protected']),
) {
  constructor(data?: UpdatePermissionDto) {
    super();
    Object.assign(this, data);
  }
}
