import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import {
  PermissionEntity,
  PermissionEntityConfig,
} from 'src/server/entities/permission.entity';

export class CreatePermissionDto {
  constructor(data?: CreatePermissionDto) {
    Object.assign(this, data);
  }

  @ApiProperty({
    required: true,
    type: String,
    minLength: PermissionEntityConfig.name.minLength,
    maxLength: PermissionEntityConfig.name.maxLength,
  })
  @IsString()
  @MinLength(PermissionEntityConfig.name.minLength)
  @MaxLength(PermissionEntityConfig.name.maxLength)
  name: PermissionEntity['name'] = '';

  @ApiProperty({ required: false, type: Boolean, default: true })
  @IsOptional()
  @IsBoolean()
  active: PermissionEntity['active'] = true;

  @ApiProperty({ required: false, type: Boolean, default: false })
  @IsOptional()
  @IsBoolean()
  protected: PermissionEntity['protected'] = false;
}
