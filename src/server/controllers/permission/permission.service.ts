import { ForbiddenError } from '@casl/ability';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { CaslAbilityFactory } from 'src/server/casl/casl-ability.factory';
import { CaslPermissionsAction } from 'src/server/casl/casl.permissions';
import { HasAccess } from 'src/server/entities/client.types';
import {
  PermissionEntity,
  PermissionRepository,
} from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { PermissionQueries } from './queries/permission.queries';

@Injectable()
export class PermissionService {
  constructor(
    private readonly permissionsRepository: PermissionRepository,
    private readonly caslAbilityFactory: CaslAbilityFactory,
  ) {}
  async findOne(queries: PermissionQueries) {
    return this.permissionsRepository.findOneOrFail({
      where: queries,
      relations: ['owner'],
    });
  }

  async setOwner(
    id: PermissionEntity['id'],
    requestUser: RequestUser | undefined,
  ) {
    await this.permissionsRepository
      .createQueryBuilder('permission')
      .relation('owner')
      .of(id)
      .set(requestUser?.id);
  }

  async create(requestUser: RequestUser | undefined, dto: CreatePermissionDto) {
    const permission = await this.permissionsRepository.save(dto);

    await this.setOwner(permission.id, requestUser);

    return this.permissionsRepository.findOneOrFail(
      {
        id: permission.id,
      },
      { relations: ['owner'] },
    );
  }

  async update(id: PermissionEntity['id'], dto: UpdatePermissionDto) {
    await this.permissionsRepository.update(id, dto);

    return await this.permissionsRepository.findOneOrFail({
      where: { id },
      relations: ['owner'],
    });
  }

  async isHasUpdateAccess(
    requestUser: RequestUser,
    id: PermissionEntity['id'],
    field?: keyof PermissionEntity | undefined,
  ): Promise<HasAccess> {
    const ability = await this.caslAbilityFactory.createForUser(requestUser);

    const permission = await this.permissionsRepository.findOneOrFail(
      { id },
      { relations: ['owner'] },
    );

    try {
      ForbiddenError.from(ability).throwUnlessCan(
        CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
        permission,
        field,
      );

      return { isHasAccess: true };
    } catch (error) {
      if (error instanceof ForbiddenError) {
        throw new ForbiddenException(error.message);
      }
      throw new ForbiddenException();
    }
  }

  async activate(id: PermissionEntity['id']) {
    await this.permissionsRepository.update(id, {
      active: true,
    });

    return await this.findOne({ id });
  }

  async deactivate(id: PermissionEntity['id']) {
    await this.permissionsRepository.update(id, {
      active: false,
    });

    return await this.findOne({ id });
  }

  async protected(id: PermissionEntity['id']) {
    await this.permissionsRepository.update(id, {
      protected: true,
    });

    return await this.findOne({ id });
  }

  async deprotected(id: PermissionEntity['id']) {
    await this.permissionsRepository.update(id, {
      protected: false,
    });

    return await this.findOne({ id });
  }

  async drop(id: PermissionEntity['id']) {
    return await this.permissionsRepository.delete({ id });
  }
}
