import { Controller, Get, ParseUUIDPipe, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import { ReadPermissionsPermissionHandler } from '../../permissions/permissions-handlers';
import { PermissionService } from './permission.service';
import { PermissionQueries } from './queries/permission.queries';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/permission')
export class FindPermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  @CheckPermissions(new ReadPermissionsPermissionHandler())
  @Get()
  async findOne(@Query() queries: PermissionQueries) {
    const data = await this.permissionService.findOne(queries);

    return { data };
  }

  @CheckPermissions(new ReadPermissionsPermissionHandler())
  @Get('/has-update-access')
  async isHasUpdateAccess(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
  ) {
    return await this.permissionService.isHasUpdateAccess(requestUser, id);
  }
}
