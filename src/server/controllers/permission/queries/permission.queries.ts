import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsUUID } from 'class-validator';
import { Permission } from 'src/server/entities/client.types';
import {
  PermissionEntity,
  PermissionEntityConfig,
} from 'src/server/entities/permission.entity';
import { Raw } from 'typeorm';

export class PermissionQueries {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsUUID()
  id?: Permission['id'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: PermissionEntityConfig.name.minLength,
    maxLength: PermissionEntityConfig.name.maxLength,
  })
  @IsOptional()
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) = LOWER('${params.value}')`),
  )
  name?: Permission['name'];

  @ApiProperty({ required: false, type: Boolean, default: true })
  @IsOptional()
  @IsBoolean()
  active?: PermissionEntity['active'];

  @ApiProperty({ required: false, type: Boolean, default: false })
  @IsOptional()
  @IsBoolean()
  protected?: PermissionEntity['protected'];
}
