import { Module, ModuleMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaslModule } from 'src/server/casl/casl.module';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import { FindPermissionController } from './find-permission.controller';
import { PermissionActivateController } from './permission-activate.controller';
import { PermissionCrudController } from './permission-crud.controler';
import { PermissionProtectedController } from './permission-protected.controller';
import { PermissionService } from './permission.service';

export const permissionModuleMetadata: ModuleMetadata = {
  imports: [TypeOrmModule.forFeature([PermissionRepository]), CaslModule],
  exports: [PermissionService],
  providers: [PermissionService],
  controllers: [
    PermissionProtectedController,
    PermissionCrudController,
    PermissionActivateController,
    FindPermissionController,
  ],
};
@Module(permissionModuleMetadata)
export class PermissionModule {}
