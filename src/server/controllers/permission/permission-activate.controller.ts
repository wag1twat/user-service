import {
  Controller,
  Delete,
  ParseUUIDPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  ReadPermissionsPermissionHandler,
  UpdatePermissionsPermissionHandler,
} from '../../permissions/permissions-handlers';
import { PermissionService } from './permission.service';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/permission')
export class PermissionActivateController {
  constructor(private readonly permissionService: PermissionService) {}

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadPermissionsPermissionHandler(),
    new UpdatePermissionsPermissionHandler(),
  )
  @Patch('/activate')
  async activate(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
  ) {
    await this.permissionService.isHasUpdateAccess(requestUser, id);

    const data = await this.permissionService.activate(id);

    return { data };
  }

  @ApiBearerAuth()
  @CheckPermissions(
    new ReadPermissionsPermissionHandler(),
    new UpdatePermissionsPermissionHandler(),
  )
  @Delete('/deactivate')
  async deactivate(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
  ) {
    await this.permissionService.isHasUpdateAccess(requestUser, id);

    const data = await this.permissionService.deactivate(id);

    return { data };
  }
}
