import { TestingModule } from '@nestjs/testing';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { PermissionProtectedController } from '../permission-protected.controller';
import { permissionModuleMetadata } from '../permission.module';
import { PermissionService } from '../permission.service';
import { testsData } from './tests.data';

describe.each(testsData)(
  'PermissionProtectedController with params "%s"',
  (dto) => {
    const modules: TestingModule[] = [];
    let controller: PermissionProtectedController;
    let service: PermissionService;
    let userService: UserService;
    let currentUser: UserEntity;

    beforeAll(async () => {
      const permissionModule: TestingModule = await mockModuleWithConnection(
        permissionModuleMetadata,
      );

      controller = permissionModule.get<PermissionProtectedController>(
        PermissionProtectedController,
      );

      service = permissionModule.get<PermissionService>(PermissionService);

      modules.push(permissionModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(permissionModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, dto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    afterAll(async () => Promise.all(modules.map((module) => module.close())));

    it('should be defined', () => {
      expect(controller).toBeDefined();
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: dto.login,
          firstName: dto.firstName,
          lastName: dto.lastName,
          email: dto.email,
        }),
      );
    });

    it('protected > should be permission protected', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const permission = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
      });

      const result = await controller.protected(requestUser, permission.id);

      expect(result.data).toBeInstanceOf(PermissionEntity);

      expect(result.data.protected).toBeTruthy();

      await service.drop(permission.id);
    });

    it('deprotected > should be permission deprotected', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const permission = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
      });

      const result = await controller.deprotected(requestUser, permission.id);

      expect(result.data).toBeInstanceOf(PermissionEntity);

      expect(result.data.protected).toBeFalsy();

      await service.drop(permission.id);
    });
  },
);
