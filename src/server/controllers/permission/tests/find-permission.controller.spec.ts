import { ForbiddenException } from '@nestjs/common';
import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { CreatePermissionDto } from '../dto/create-permission.dto';
import { FindPermissionController } from '../find-permission.controller';
import { permissionModuleMetadata } from '../permission.module';
import { PermissionService } from '../permission.service';
import { testsData } from './tests.data';

describe.each(testsData)('FindPermissionController with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let controller: FindPermissionController;
  let service: PermissionService;
  let userService: UserService;
  let currentUser: UserEntity;

  beforeAll(async () => {
    const permissionModule: TestingModule = await mockModuleWithConnection(
      permissionModuleMetadata,
    );

    controller = permissionModule.get<FindPermissionController>(
      FindPermissionController,
    );

    service = permissionModule.get<PermissionService>(PermissionService);

    modules.push(permissionModule);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(permissionModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('isHasAccess > should be can update no protected permission no ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: false,
      active: true,
    });

    expect(permission.owner).toBe(null);

    const result = await controller.isHasUpdateAccess(
      requestUser,
      permission.id,
    );

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
  });

  it('isHasAccess > should be can update no protected permission ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const permission = await service.create(requestUser, {
      name: TestUtils.randomString(10),
      protected: false,
      active: true,
    });

    expect(permission.owner).toBeInstanceOf(UserEntity);

    const result = await controller.isHasUpdateAccess(
      requestUser,
      permission.id,
    );

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
  });

  it('isHasAccess > should be can update protected permission no ownered', async () => {
    const invalidUser = await userService.create(undefined, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await userService.findRequestUser({
      id: invalidUser.id,
    });

    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    expect(permission.owner).toBe(null);

    await expect(
      controller.isHasUpdateAccess(invalidRequestUser, permission.id),
    ).rejects.toThrowError(ForbiddenException);

    await service.drop(permission.id);
    await userService.drop(invalidUser.id);
  });

  it('isHasAccess > should be can update protected permission ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const invalidUser = await userService.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await userService.findRequestUser({
      id: invalidUser.id,
    });

    const permission = await service.create(requestUser, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    expect(permission.owner).toBeInstanceOf(UserEntity);

    const result = await controller.isHasUpdateAccess(
      requestUser,
      permission.id,
    );

    await expect(
      controller.isHasUpdateAccess(invalidRequestUser, permission.id),
    ).rejects.toThrowError(ForbiddenException);

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
    await userService.drop(invalidRequestUser.id);
  });

  it('findOne > should be finded', async () => {
    const RequestUser = await userService.findRequestUser({
      login: dto.login,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const createResult = await service.create(RequestUser, createPermissionDto);

    const findResult = await controller.findOne({
      id: createResult.id,
      name: createResult.name,
    });

    expect(findResult.data).toEqual(
      expect.objectContaining({
        id: createResult.id,
        name: createResult.name,
      }),
    );

    await service.drop(createResult.id);
  });
});
