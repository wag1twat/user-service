import { TestingModule } from '@nestjs/testing';
import { UserEntity } from 'src/server/entities/user.entity';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { CreatePermissionDto } from '../dto/create-permission.dto';
import { UpdatePermissionDto } from '../dto/update-permission.dto';
import { PermissionCrudController } from '../permission-crud.controler';
import { permissionModuleMetadata } from '../permission.module';
import { PermissionService } from '../permission.service';
import { testsData } from './tests.data';

describe.each(testsData)('PermissionCrudController with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let controller: PermissionCrudController;
  let service: PermissionService;
  let userService: UserService;
  let currentUser: UserEntity;

  beforeAll(async () => {
    const permissionModule: TestingModule = await mockModuleWithConnection(
      permissionModuleMetadata,
    );

    controller = permissionModule.get<PermissionCrudController>(
      PermissionCrudController,
    );

    service = permissionModule.get<PermissionService>(PermissionService);

    modules.push(permissionModule);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(permissionModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('create > should be created', async () => {
    const requestUser = await userService.findRequestUser({
      login: dto.login,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const permission = await controller.create(
      requestUser,
      createPermissionDto,
    );

    expect(permission.data).toEqual(
      expect.objectContaining({
        name: createPermissionDto.name,
      }),
    );

    expect(permission.data.owner).toBeInstanceOf(UserEntity);

    expect(permission.data.owner).toEqual(
      expect.objectContaining({
        id: currentUser.id,
      }),
    );

    await service.drop(permission.data.id);
  });

  it('update > should be updated non protected', async () => {
    const requestUser = await userService.findRequestUser({
      login: dto.login,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const createResult = await controller.create(
      requestUser,
      createPermissionDto,
    );

    const updatePermissionDto = new UpdatePermissionDto();

    const updateResult = await controller.update(
      requestUser,
      createResult.data.id,
      updatePermissionDto,
    );

    expect(updateResult.data).toEqual(
      expect.objectContaining({
        name: updatePermissionDto.name,
      }),
    );

    await service.drop(createResult.data.id);
  });

  it('update > should be updated  protected', async () => {
    const requestUser = await userService.findRequestUser({
      login: dto.login,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const createResult = await controller.create(
      requestUser,
      createPermissionDto,
    );

    const updatePermissionDto = new UpdatePermissionDto({
      name: TestUtils.randomString(10),
    });

    const updateResult = await controller.update(
      requestUser,
      createResult.data.id,
      updatePermissionDto,
    );

    expect(updateResult.data).toEqual(
      expect.objectContaining({
        name: updatePermissionDto.name,
      }),
    );

    await service.drop(createResult.data.id);
  });
});
