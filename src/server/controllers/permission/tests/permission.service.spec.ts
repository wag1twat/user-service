import { ForbiddenException } from '@nestjs/common';
import { TestingModule } from '@nestjs/testing';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { CreatePermissionDto } from '../dto/create-permission.dto';
import { UpdatePermissionDto } from '../dto/update-permission.dto';
import { permissionModuleMetadata } from '../permission.module';
import { PermissionService } from '../permission.service';
import { testsData } from './tests.data';

describe.each(testsData)('PermissionService with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let service: PermissionService;
  let userService: UserService;
  let currentUser: UserEntity;

  beforeAll(async () => {
    const permissionModule: TestingModule = await mockModuleWithConnection(
      permissionModuleMetadata,
    );
    service = permissionModule.get<PermissionService>(PermissionService);

    modules.push(permissionModule);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(permissionModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('create > should be created', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const permission = await service.create(requestUser, createPermissionDto);

    expect(permission).toEqual(
      expect.objectContaining({
        name: createPermissionDto.name,
      }),
    );

    expect(permission.owner).toBeInstanceOf(UserEntity);

    expect(permission.owner).toEqual(
      expect.objectContaining({
        id: currentUser.id,
      }),
    );

    await service.drop(permission.id);
  });

  it('isHasAccess > should be can update no protected permission no ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: false,
      active: true,
    });

    expect(permission.owner).toBe(null);

    const result = await service.isHasUpdateAccess(requestUser, permission.id);

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
  });

  it('isHasAccess > should be can update no protected permission ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const permission = await service.create(requestUser, {
      name: TestUtils.randomString(10),
      protected: false,
      active: true,
    });

    expect(permission.owner).toBeInstanceOf(UserEntity);

    const result = await service.isHasUpdateAccess(requestUser, permission.id);

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
  });

  it('isHasAccess > should be can update protected permission no ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const invalidUser = await userService.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await userService.findRequestUser({
      id: invalidUser.id,
    });

    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    expect(permission.owner).toBe(null);

    await expect(
      service.isHasUpdateAccess(invalidRequestUser, permission.id),
    ).rejects.toThrowError(ForbiddenException);

    await service.drop(permission.id);
    await userService.drop(invalidUser.id);
  });

  it('isHasAccess > should be can update protected permission ownered', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const invalidUser = await userService.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      password: `${TestUtils.randomString(10)}@123`,
      email: `${TestUtils.randomString(10)}@gmail.com`,
      active: true,
      protected: true,
    });

    const invalidRequestUser = await userService.findRequestUser({
      id: invalidUser.id,
    });

    const permission = await service.create(requestUser, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    expect(permission.owner).toBeInstanceOf(UserEntity);

    const result = await service.isHasUpdateAccess(requestUser, permission.id);

    await expect(
      service.isHasUpdateAccess(invalidRequestUser, permission.id),
    ).rejects.toThrowError(ForbiddenException);

    expect(result.isHasAccess).toBeTruthy();

    await service.drop(permission.id);
    await userService.drop(invalidUser.id);
  });

  it('update > should be updated non protected', async () => {
    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      protected: false,
      active: true,
    });

    const createResult = await service.create(undefined, createPermissionDto);

    const updatePermissionDto = new UpdatePermissionDto({
      name: TestUtils.randomString(10),
    });

    const updateResult = await service.update(
      createResult.id,
      updatePermissionDto,
    );

    expect(updateResult).toEqual(
      expect.objectContaining({
        name: updatePermissionDto.name,
      }),
    );

    await service.drop(createResult.id);
  });

  it('update > should be updated protected', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    const createResult = await service.create(requestUser, createPermissionDto);

    const updatePermissionDto = new UpdatePermissionDto({
      name: TestUtils.randomString(10),
    });

    const updateResult = await service.update(
      createResult.id,
      updatePermissionDto,
    );

    expect(updateResult).toEqual(
      expect.objectContaining({
        name: updatePermissionDto.name,
      }),
    );

    await service.drop(createResult.id);
  });

  it('findOne > should be finded', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const createPermissionDto = new CreatePermissionDto({
      name: TestUtils.randomString(10),
      active: true,
      protected: false,
    });

    const createResult = await service.create(requestUser, createPermissionDto);

    const findResult = await service.findOne({
      id: createResult.id,
      name: createResult.name,
    });

    expect(findResult).toEqual(
      expect.objectContaining({
        id: createResult.id,
        name: createResult.name,
      }),
    );

    await service.drop(createResult.id);
  });

  it('deactivate > should be permission deactivate', async () => {
    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    const result = await service.deactivate(permission.id);

    expect(result).toBeInstanceOf(PermissionEntity);

    expect(result.active).toBeFalsy();

    await service.drop(permission.id);
  });

  it('activate > should be permission activate', async () => {
    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    const result = await service.activate(permission.id);

    expect(result).toBeInstanceOf(PermissionEntity);

    expect(result.active).toBeTruthy();

    await service.drop(permission.id);
  });

  it('deprotected > should be permission deprotected', async () => {
    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    const result = await service.deprotected(permission.id);

    expect(result).toBeInstanceOf(PermissionEntity);

    expect(result.protected).toBeFalsy();

    await service.drop(permission.id);
  });

  it('protected > should be permission protected', async () => {
    const permission = await service.create(undefined, {
      name: TestUtils.randomString(10),
      protected: true,
      active: true,
    });

    const result = await service.protected(permission.id);

    expect(result).toBeInstanceOf(PermissionEntity);

    expect(result.protected).toBeTruthy();

    await service.drop(permission.id);
  });
});
