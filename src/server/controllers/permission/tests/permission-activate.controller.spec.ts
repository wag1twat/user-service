import { TestingModule } from '@nestjs/testing';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { PermissionActivateController } from '../permission-activate.controller';
import { permissionModuleMetadata } from '../permission.module';
import { PermissionService } from '../permission.service';
import { testsData } from './tests.data';

describe.each(testsData)(
  'PermissionActivateController with params "%s"',
  (dto) => {
    const modules: TestingModule[] = [];
    let controller: PermissionActivateController;
    let service: PermissionService;
    let userService: UserService;
    let currentUser: UserEntity;

    beforeAll(async () => {
      const permissionModule: TestingModule = await mockModuleWithConnection(
        permissionModuleMetadata,
      );

      controller = permissionModule.get<PermissionActivateController>(
        PermissionActivateController,
      );

      service = permissionModule.get<PermissionService>(PermissionService);

      modules.push(permissionModule);

      const userModule: TestingModule = await mockModuleWithConnection(
        userModuleMetadata,
      );

      userService = userModule.get<UserService>(UserService);

      modules.push(permissionModule);
    });

    beforeEach(async () => {
      currentUser = await userService.create(undefined, dto);
    });
    afterEach(async () => {
      await userService.drop(currentUser.id);
    });

    afterAll(async () => Promise.all(modules.map((module) => module.close())));

    it('should be defined', () => {
      expect(controller).toBeDefined();
      expect(service).toBeDefined();
      expect(userService).toBeDefined();
    });

    it('should be entities created', () => {
      expect(currentUser).toBeInstanceOf(UserEntity);
      expect(currentUser).toEqual(
        expect.objectContaining({
          login: dto.login,
          firstName: dto.firstName,
          lastName: dto.lastName,
          email: dto.email,
        }),
      );
    });

    it('activate > should be permission activate', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const permission = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
      });

      const result = await controller.activate(requestUser, permission.id);

      expect(result.data).toBeInstanceOf(PermissionEntity);

      expect(result.data.active).toBeTruthy();

      await service.drop(permission.id);
    });

    it('deactivate > should be permission deactivate', async () => {
      const requestUser = await userService.findRequestUser({
        id: currentUser.id,
      });

      const permission = await service.create(requestUser, {
        name: TestUtils.randomString(10),
        protected: true,
        active: true,
      });

      const result = await controller.deactivate(requestUser, permission.id);

      expect(result.data).toBeInstanceOf(PermissionEntity);

      expect(result.data.active).toBeFalsy();

      await service.drop(permission.id);
    });
  },
);
