import {
  Body,
  Controller,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ReqUser } from 'src/server/decorators/request-user.decorator';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RequestUser } from 'src/server/entities/request-user';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import {
  CreatePermissionsPermissionHandler,
  ReadPermissionsPermissionHandler,
  UpdatePermissionsPermissionHandler,
} from '../../permissions/permissions-handlers';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { PermissionService } from './permission.service';

@ApiTags('role')
@ApiBearerAuth()
@Controller('/api/v1/permission')
export class PermissionCrudController {
  constructor(private readonly permissionService: PermissionService) {}

  @CheckPermissions(new CreatePermissionsPermissionHandler())
  @Post()
  async create(
    @ReqUser() requestUser: RequestUser | undefined,
    @Body(NotEmptyObjectPipe) dto: CreatePermissionDto,
  ) {
    const data = await this.permissionService.create(requestUser, dto);

    return { data };
  }

  @CheckPermissions(
    new ReadPermissionsPermissionHandler(),
    new UpdatePermissionsPermissionHandler(),
  )
  @Patch()
  async update(
    @ReqUser() requestUser: RequestUser,
    @Query('id', ParseUUIDPipe) id: PermissionEntity['id'],
    @Body(NotEmptyObjectPipe) dto: UpdatePermissionDto,
  ) {
    await this.permissionService.isHasUpdateAccess(requestUser, id);

    const data = await this.permissionService.update(id, dto);

    return { data };
  }
}
