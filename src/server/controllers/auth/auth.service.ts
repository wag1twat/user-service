import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login-dto';
import { UserService } from 'src/server/controllers/user/user.service';
import {
  UnauthorizedException,
  WrongCredentialsException,
} from 'src/server/exceptions';
import { isDecodeToken, JwtPayload } from 'src/shared/guards';
import { RequestUser } from 'src/server/entities/request-user';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  async validate(payload: JwtPayload) {
    try {
      const RequestUser = await this.userService.findRequestUser({
        id: payload.id,
        login: payload.login,
      });

      return RequestUser;
    } catch (error) {
      throw new WrongCredentialsException();
    }
  }

  signToken(user: Pick<RequestUser, 'login' | 'id'>) {
    return this.jwtService.sign(user);
  }

  async login(dto: LoginDto) {
    const requestUser = await this.userService.findRequestUser({
      login: dto.login,
    });

    const checkResult = await requestUser.checkPassword(dto.password);

    const plain = checkResult.plain();

    if (plain) {
      return {
        access_token: this.signToken(plain),
      };
    }

    throw new UnauthorizedException();
  }

  decodeToken(token: string | null) {
    if (token) {
      const decode = this.jwtService.decode(token);
      if (isDecodeToken(decode)) {
        return decode;
      }
      throw new UnauthorizedException();
    }

    throw new UnauthorizedException();
  }
}
