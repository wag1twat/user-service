import { JwtModule as CoreJwtModule } from '@nestjs/jwt';

const JwtModule = CoreJwtModule.register({
  secret: process.env.JWT_SECRET,
  signOptions: { expiresIn: '1d' },
});

export { JwtModule };
