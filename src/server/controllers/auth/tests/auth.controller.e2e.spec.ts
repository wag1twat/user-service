import { TestingModule } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { isJWT } from 'class-validator';
import { authModuleMetadata } from '../auth.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { UserEntity } from 'src/server/entities/user.entity';
import { testsData } from './tests.data';
import { UserService } from '../../user/user.service';
import { userModuleMetadata } from '../../user/user.module';

describe.each(testsData)('AuthController with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let controller: AuthController;
  let userService: UserService;
  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      authModuleMetadata,
    );

    controller = module.get<AuthController>(AuthController);

    modules.push(module);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(userModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await Promise.all(modules.map((m) => m.close()));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('login > should be valid jwt', async () => {
    const loginResult = await controller.login({
      login: dto.login,
      password: dto.password,
    });

    expect(isJWT(loginResult.data.access_token)).toBeTruthy();
  });
});
