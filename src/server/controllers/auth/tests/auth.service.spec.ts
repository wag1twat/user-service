import { TestingModule } from '@nestjs/testing';
import { AuthService } from '../auth.service';
import { isJWT } from 'class-validator';
import { authModuleMetadata } from '../auth.module';
import { mockModuleWithConnection } from 'src/server/shared';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { UserEntity } from 'src/server/entities/user.entity';
import { testsData } from './tests.data';

describe.each(testsData)('AuthService with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let authService: AuthService;
  let userService: UserService;
  let currentUser: UserEntity;

  beforeAll(async () => {
    const authModule: TestingModule = await mockModuleWithConnection(
      authModuleMetadata,
    );

    authService = authModule.get<AuthService>(AuthService);

    modules.push(authModule);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(userModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await Promise.all(modules.map((m) => m.close()));
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentUser).toBeInstanceOf(UserEntity);
    expect(currentUser).toEqual(
      expect.objectContaining({
        login: dto.login,
        firstName: dto.firstName,
        lastName: dto.lastName,
        email: dto.email,
      }),
    );
  });

  it('login > should be jwt is valid', async () => {
    const loginResult = await authService.login({
      login: dto.login,
      password: dto.password,
    });

    expect(isJWT(loginResult.access_token)).toBeTruthy();
  });

  it('decodeToken > should be valid jwt payload', async () => {
    const loginResult = await authService.login({
      login: dto.login,
      password: dto.password,
    });

    const decode = authService.decodeToken(loginResult.access_token);

    expect(decode).toEqual(
      expect.objectContaining({
        id: currentUser.id,
        login: currentUser.login,
      }),
    );
  });
});
