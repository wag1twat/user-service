import { IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PickType } from '@nestjs/mapped-types';
import { UserEntity, UserEntityConfig } from 'src/server/entities/user.entity';

export class LoginDto extends PickType(UserEntity, ['login', 'password']) {
  constructor(data?: Partial<LoginDto>) {
    super();
    Object.assign(this, data);
  }
  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.login.minLength,
    maxLength: UserEntityConfig.login.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.login.minLength)
  @MaxLength(UserEntityConfig.login.maxLength)
  login!: UserEntity['login'];

  @ApiProperty({
    required: true,
    type: String,
    minLength: UserEntityConfig.password.minLength,
    maxLength: UserEntityConfig.password.maxLength,
  })
  @IsString()
  @MinLength(UserEntityConfig.password.minLength)
  @MaxLength(UserEntityConfig.password.maxLength)
  @Matches(UserEntityConfig.password.regexp, {
    message:
      'The password must have uppercase and lowercase Latin letters as well as symbols',
  })
  password!: UserEntity['password'];
}
