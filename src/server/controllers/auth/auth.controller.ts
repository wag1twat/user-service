import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, Post } from '@nestjs/common';
import { NotEmptyObjectPipe } from 'src/server/pipes';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login-dto';
import { PublicRoute } from './jwt-auth.guard';

@ApiTags('auth')
@Controller('/api/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('/login')
  @PublicRoute()
  async login(@Body(NotEmptyObjectPipe) dto: LoginDto) {
    const data = await this.authService.login(dto);

    return { data };
  }
}
