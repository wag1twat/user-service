import { Module, ModuleMetadata } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { AuthController } from './auth.controller';
import * as dotenv from 'dotenv';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/server/entities/user.entity';
import { UserService } from 'src/server/controllers/user/user.service';
import { JwtModule } from './jwt.module';
import { RolesService } from '../roles/roles.service';
import { PermissionsService } from '../permissions/permissions.service';
import { RoleRepository } from 'src/server/entities/role.entity';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import { CaslModule } from 'src/server/casl/casl.module';

dotenv.config();

export const authModuleMetadata: ModuleMetadata = {
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      RoleRepository,
      PermissionRepository,
    ]),
    JwtModule,
    PassportModule,
    CaslModule,
  ],
  exports: [AuthService],
  controllers: [AuthController],
  providers: [
    AuthService,
    UserService,
    RolesService,
    PermissionsService,
    JwtStrategy,
  ],
};

@Module(authModuleMetadata)
export class AuthModule {}
