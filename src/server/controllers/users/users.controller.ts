import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import { ReadUsersPermissionHandler } from '../../permissions/permissions-handlers';
import { UsersQueries } from './queries/users-queries';
import { UsersService } from './users.service';

@ApiBearerAuth()
@ApiTags('users')
@Controller('/api/v1/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @CheckPermissions(new ReadUsersPermissionHandler())
  async findAll(@Query() queries: UsersQueries) {
    const [data, count] = await this.usersService.findAll(queries);

    return { data, count };
  }
}
