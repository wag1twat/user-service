import { testsUsersLength } from './constants';
import { testsUsers } from './tests.users';

export const testsData = testsUsers(testsUsersLength, 'user').map(
  (user) => [user] as const,
);
