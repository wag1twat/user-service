import { TestingModule } from '@nestjs/testing';
import { UsersService } from '../users.service';
import { usersModuleMetadata } from '../users.module';
import { mockModuleWithConnection, TestUtils } from 'src/server/shared';
import { testsData } from './tests.data';
import { userModuleMetadata } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { UserEntity } from 'src/server/entities/user.entity';

describe.each(testsData)('UsersService with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let service: UsersService;
  let userService: UserService;

  let currentUser: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await mockModuleWithConnection(
      usersModuleMetadata,
    );

    service = module.get<UsersService>(UsersService);

    modules.push(module);

    const userModule: TestingModule = await mockModuleWithConnection(
      userModuleMetadata,
    );

    userService = userModule.get<UserService>(UserService);

    modules.push(userModule);
  });

  beforeEach(async () => {
    currentUser = await userService.create(undefined, dto);
  });
  afterEach(async () => {
    await userService.drop(currentUser.id);
  });

  afterAll(async () => {
    await Promise.all(modules.map((m) => m.close()));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(userService).toBeDefined();
  });

  it('findAll > should users finded', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const user = await userService.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      email: `${TestUtils.randomString(10)}@gmail.com`,
      password: 'User@123',
      active: true,
      protected: false,
    });

    const [users] = await service.findAll({
      login: user.login,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      active: user.active,
      protected: user.protected,
    });

    expect(users).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          login: user.login,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
        }),
      ]),
    );

    await userService.drop(user.id);
  });

  it('findAll > should users finded with queries', async () => {
    const requestUser = await userService.findRequestUser({
      id: currentUser.id,
    });

    const user = await userService.create(requestUser, {
      login: TestUtils.randomString(10),
      firstName: TestUtils.randomString(10),
      lastName: TestUtils.randomString(10),
      email: `${TestUtils.randomString(10)}@gmail.com`,
      password: 'User@123',
      active: true,
      protected: false,
    });

    const [users] = await service.findAll({
      login: user.login,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      active: user.active,
    });

    expect(users).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          login: user.login,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
        }),
      ]),
    );

    await userService.drop(user.id);
  });
});
