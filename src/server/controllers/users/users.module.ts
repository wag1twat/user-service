import { Module, ModuleMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/server/entities/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

export const usersModuleMetadata: ModuleMetadata = {
  imports: [TypeOrmModule.forFeature([UserRepository])],
  exports: [UsersService],
  controllers: [UsersController],
  providers: [UsersService],
};
@Module(usersModuleMetadata)
export class UsersModule {}
