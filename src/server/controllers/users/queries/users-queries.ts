import { ApiProperty, IntersectionType } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsArray, IsOptional, IsUUID } from 'class-validator';
import { QueriesBase } from 'src/server/queries/queries-base';
import { QueriesPagination } from 'src/server/queries/queries-pagination';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity, UserEntityConfig } from 'src/server/entities/user.entity';
import { FindOperator, Raw } from 'typeorm';

type SortBy = keyof Pick<
  UserEntity,
  | 'id'
  | 'login'
  | 'firstName'
  | 'lastName'
  | 'createdAt'
  | 'updatedAt'
  | 'email'
  | 'active'
  | 'owner'
>;

export class UsersQueries extends IntersectionType<
  QueriesBase<SortBy>,
  QueriesPagination
>(QueriesBase, QueriesPagination) {
  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.login.minLength,
    maxLength: UserEntityConfig.login.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) LIKE LOWER('%${params.value}%')`),
  )
  login?: UserEntity['login'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.firstName.minLength,
    maxLength: UserEntityConfig.firstName.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) LIKE LOWER('%${params.value}%')`),
  )
  firstName?: UserEntity['firstName'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.lastName.minLength,
    maxLength: UserEntityConfig.lastName.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) LIKE LOWER('%${params.value}%')`),
  )
  lastName?: UserEntity['lastName'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: UserEntityConfig.email.minLength,
    maxLength: UserEntityConfig.email.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) LIKE LOWER('%${params.value}%')`),
  )
  email?: UserEntity['email'];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @IsUUID(undefined, { each: true })
  rolesIds?: RoleEntity['id'][];
}
