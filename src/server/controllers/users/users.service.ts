import { Injectable } from '@nestjs/common';
import { lastValueFrom, of } from 'rxjs';
import { UserEntity, UserRepository } from 'src/server/entities/user.entity';
import {
  withAndWhereIds,
  withGetManyAndCount,
  withLeftJoin,
  withOwner,
  withPagination,
  withSorting,
  withWhere,
} from '../rxjs-operators';
import { UsersQueries } from './queries/users-queries';

@Injectable()
export class UsersService {
  constructor(private readonly userRepository: UserRepository) {}

  async findAll(where: UsersQueries = {}): Promise<[UserEntity[], number]> {
    const { rolesIds = [], page, limit, sortBy, order, ...conditions } = where;

    const alias = 'user';

    return lastValueFrom(
      of(this.userRepository.createQueryBuilder(alias)).pipe(
        withLeftJoin(alias, 'roles'),
        withOwner(alias),
        withWhere(conditions),
        withAndWhereIds(
          Boolean(rolesIds.length),
          'roles.id IN (:...rolesIds)',
          { rolesIds },
        ),
        withPagination(page, limit),
        withSorting(alias, order, sortBy),
        withGetManyAndCount(),
      ),
    );
  }
}
