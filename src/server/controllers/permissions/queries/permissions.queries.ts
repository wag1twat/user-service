import { IntersectionType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { plainToInstance, Transform, Type } from 'class-transformer';
import { IsArray, IsOptional, IsUUID } from 'class-validator';
import {
  PermissionEntity,
  PermissionEntityConfig,
} from 'src/server/entities/permission.entity';
import { QueriesBase } from 'src/server/queries/queries-base';
import { QueriesPagination } from 'src/server/queries/queries-pagination';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { FindOperator, In, Raw } from 'typeorm';

type SortBy = keyof Pick<
  PermissionEntity,
  'id' | 'createdAt' | 'active' | 'updatedAt' | 'name' | 'owner'
>;

export class PermissionsQueries extends IntersectionType<
  QueriesBase<SortBy>,
  QueriesPagination
>(QueriesBase, QueriesPagination) {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) => In(params.value))
  id?: PermissionEntity['id'];

  @ApiProperty({
    required: false,
    type: String,
    minLength: PermissionEntityConfig.name.minLength,
    maxLength: PermissionEntityConfig.name.maxLength,
  })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) =>
    Raw((alias) => `LOWER(${alias}) Like LOWER('%${params.value}%')`),
  )
  name?: PermissionEntity['name'];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @IsUUID(undefined, { each: true })
  usersIds?: UserEntity['id'][];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @IsArray()
  @IsUUID(undefined, { each: true })
  rolesIds?: RoleEntity['id'][];
}

export const ConstructorPermissionsQueries = (data: PermissionsQueries) =>
  plainToInstance(PermissionsQueries, data);
