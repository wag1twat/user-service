import { Module, ModuleMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import { PermissionsController } from './permissions.controller';
import { PermissionsService } from './permissions.service';

export const permissionsModuleMetadata: ModuleMetadata = {
  imports: [TypeOrmModule.forFeature([PermissionRepository])],
  exports: [
    TypeOrmModule.forFeature([PermissionRepository]),
    PermissionsService,
  ],
  controllers: [PermissionsController],
  providers: [PermissionsService],
};
@Module(permissionsModuleMetadata)
export class PermissionsModule {}
