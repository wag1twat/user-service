import { TestingModule } from '@nestjs/testing';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { mockModuleWithConnection } from 'src/server/shared';
import { permissionModuleMetadata } from '../../permission/permission.module';
import { PermissionService } from '../../permission/permission.service';
import { PermissionsController } from '../permissions.controller';
import { permissionsModuleMetadata } from '../permissions.module';
import { testsData } from './tests.data';

describe.each(testsData)('PermissionsController with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let controller: PermissionsController;
  let permissionService: PermissionService;

  let currentPermission: PermissionEntity;

  beforeAll(async () => {
    const permissionsModule: TestingModule = await mockModuleWithConnection(
      permissionsModuleMetadata,
    );

    controller = permissionsModule.get<PermissionsController>(
      PermissionsController,
    );

    modules.push(permissionsModule);

    const permissionModule: TestingModule = await mockModuleWithConnection(
      permissionModuleMetadata,
    );

    permissionService =
      permissionModule.get<PermissionService>(PermissionService);
  });

  beforeEach(async () => {
    currentPermission = await permissionService.create(undefined, dto);
  });
  afterEach(async () => {
    await permissionService.drop(currentPermission.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(permissionService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentPermission).toBeInstanceOf(PermissionEntity);
    expect(currentPermission).toEqual(
      expect.objectContaining({
        name: dto.name,
      }),
    );
  });

  it('findAll > should be finded', async () => {
    const { data, count } = await controller.findAll({
      id: currentPermission.id,
      name: currentPermission.name,
    });

    expect(count).toBe(1);

    expect(data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: currentPermission.id,
          name: currentPermission.name,
        }),
      ]),
    );
  });
});
