import { testsPermissionsLength } from './constants';
import { testsPermissions } from './tests.permissions';

export const testsData = testsPermissions(testsPermissionsLength, 'permission');
