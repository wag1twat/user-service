import { TestingModule } from '@nestjs/testing';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { mockModuleWithConnection } from 'src/server/shared';
import { permissionModuleMetadata } from '../../permission/permission.module';
import { PermissionService } from '../../permission/permission.service';
import { permissionsModuleMetadata } from '../permissions.module';
import { PermissionsService } from '../permissions.service';
import { testsData } from './tests.data';

describe.each(testsData)('PermissionsService with params "%s"', (dto) => {
  const modules: TestingModule[] = [];
  let service: PermissionsService;
  let permissionService: PermissionService;

  let currentPermission: PermissionEntity;

  beforeAll(async () => {
    const permissionsModule: TestingModule = await mockModuleWithConnection(
      permissionsModuleMetadata,
    );

    service = permissionsModule.get<PermissionsService>(PermissionsService);

    modules.push(permissionsModule);

    const permissionModule: TestingModule = await mockModuleWithConnection(
      permissionModuleMetadata,
    );

    permissionService =
      permissionModule.get<PermissionService>(PermissionService);
  });

  beforeEach(async () => {
    currentPermission = await permissionService.create(undefined, dto);
  });
  afterEach(async () => {
    await permissionService.drop(currentPermission.id);
  });

  afterAll(async () => Promise.all(modules.map((module) => module.close())));

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(permissionService).toBeDefined();
  });

  it('should be entities created', () => {
    expect(currentPermission).toBeInstanceOf(PermissionEntity);
    expect(currentPermission).toEqual(
      expect.objectContaining({
        name: dto.name,
      }),
    );
  });

  it('findAll > should be finded', async () => {
    const [data, count] = await service.findAll({
      id: currentPermission.id,
      name: currentPermission.name,
    });

    expect(count).toBe(1);

    expect(data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: currentPermission.id,
          name: currentPermission.name,
        }),
      ]),
    );
  });
});
