import { TestUtils } from 'src/server/shared';
import { CreatePermissionDto } from '../../permission/dto/create-permission.dto';

export const testsPermissions = (
  length: number,
  name: string,
): CreatePermissionDto[] =>
  Array.from(Array(length).keys()).map((i) => ({
    name: name + TestUtils.randomString(10) + i,
    protected: false,
    active: true,
  }));
