import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { CheckPermissions } from '../../permissions/check-permissions.decorator';
import { ReadPermissionsPermissionHandler } from '../../permissions/permissions-handlers';
import { PermissionsService } from './permissions.service';
import { PermissionsQueries } from './queries/permissions.queries';

@ApiTags('permissions')
@ApiBearerAuth()
@Controller('/api/v1/permissions')
export class PermissionsController {
  constructor(private readonly permissionsService: PermissionsService) {}
  @CheckPermissions(new ReadPermissionsPermissionHandler())
  @Get()
  async findAll(@Query() queries: PermissionsQueries) {
    const [data, count] = await this.permissionsService.findAll(queries);

    return { data, count };
  }
}
