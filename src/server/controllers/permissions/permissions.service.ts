import { Injectable } from '@nestjs/common';
import { lastValueFrom, of } from 'rxjs';
import { PermissionsQueries } from 'src/server/controllers/permissions/queries/permissions.queries';
import { PermissionRepository } from 'src/server/entities/permission.entity';
import {
  withAndWhereIds,
  withGetManyAndCount,
  withLeftJoin,
  withOwner,
  withPagination,
  withSorting,
  withWhere,
} from '../rxjs-operators';

@Injectable()
export class PermissionsService {
  constructor(private readonly permissionsRepository: PermissionRepository) {}

  async findAll(where: PermissionsQueries) {
    const {
      usersIds = [],
      rolesIds = [],
      sortBy,
      order,
      page,
      limit,
      ...conditions
    } = where;

    const alias = 'permission';

    return lastValueFrom(
      of(this.permissionsRepository.createQueryBuilder(alias)).pipe(
        withOwner(alias),
        withLeftJoin(alias, 'roles'),
        withLeftJoin('roles', 'users'),
        withWhere(conditions),
        withAndWhereIds(
          Boolean(rolesIds.length),
          'roles.id IN (:...rolesIds)',
          {
            rolesIds,
          },
        ),
        withAndWhereIds(
          Boolean(usersIds.length),
          'users.id IN (:...usersIds)',
          { usersIds },
        ),
        withPagination(page, limit),
        withSorting(alias, order, sortBy),
        withGetManyAndCount(),
      ),
    );
  }
}
