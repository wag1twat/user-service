import bcrypt from 'bcryptjs';
import { Exclude, instanceToPlain } from 'class-transformer';
import { has } from 'lodash';
import { WrongCredentialsException } from '../exceptions';
import { UserEntity } from './user.entity';

const isPlainRequestUser = (
  plain: unknown,
): plain is Pick<RequestUser, 'id' | 'login' | 'active'> => {
  return has(plain, 'id') && has(plain, 'login') && has(plain, 'active');
};

export class RequestUser {
  constructor(user: UserEntity) {
    this.id = user.id;
    this.login = user.login;
    this.password = user.password;
    this.active = user.active;
  }

  id: UserEntity['id'];
  login: UserEntity['login'];
  active: UserEntity['active'];
  @Exclude({ toPlainOnly: true })
  password: UserEntity['password'];

  plain() {
    const plain = instanceToPlain(this);

    if (isPlainRequestUser(plain)) {
      return plain;
    }
  }

  @Exclude({ toClassOnly: true })
  async checkPassword(plainPassword: string): Promise<RequestUser> {
    const isMatch = await bcrypt.compare(plainPassword, this.password);

    if (isMatch) {
      return this;
    }

    throw new WrongCredentialsException();
  }
}
