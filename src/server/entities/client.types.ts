import { PermissionEntity } from './permission.entity';
import { RoleEntity } from './role.entity';
import { UserEntity } from './user.entity';

type User = UserEntity;
type Role = RoleEntity;
type Permission = PermissionEntity;

interface HasAccess {
  isHasAccess: boolean;
}

export type { User, Role, Permission, HasAccess };
