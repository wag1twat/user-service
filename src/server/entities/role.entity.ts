import {
  Column,
  Entity,
  EntityRepository,
  JoinColumn,
  JoinTable,
  ManyToMany,
  Repository,
} from 'typeorm';
import { PermissionEntity } from './permission.entity';
import { UserEntity } from './user.entity';
import { BaseEntity } from './base-entity';

export const RoleEntityConfig = {
  name: {
    minLength: 4,
    maxLength: 64,
    requiredMessage: 'Name must be required',
  },
};

@Entity({ name: 'roles' })
export class RoleEntity extends BaseEntity {
  constructor(data: Partial<RoleEntity>) {
    super();
    Object.assign(this, data);
  }

  @ManyToMany(() => UserEntity, (user) => user.roles)
  @JoinColumn()
  users?: UserEntity[];

  @ManyToMany(() => PermissionEntity, (permission) => permission.roles, {
    cascade: true,
  })
  @JoinTable({ name: 'roles_has_permissions' })
  permissions?: PermissionEntity[];

  @Column({
    type: 'varchar',
    length: RoleEntityConfig.name.maxLength,
    unique: true,
  })
  name!: string;
}

@EntityRepository(RoleEntity)
export class RoleRepository extends Repository<RoleEntity> {}
