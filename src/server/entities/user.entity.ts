import bcrypt from 'bcryptjs';
import {
  Entity,
  Column,
  EntityRepository,
  Repository,
  ManyToMany,
  JoinTable,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import { BaseEntity } from './base-entity';
import { RoleEntity } from './role.entity';

export const UserEntityConfig = {
  login: {
    minLength: 4,
    maxLength: 20,
    requiredMessage: 'Login must be required',
  },
  password: {
    minLength: 8,
    maxLength: 255,
    regexp: /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    notMatchMessage:
      'The password must have uppercase and lowercase Latin letters as well as symbols',
    notMatchNextPasswordMessage:
      'The confirm next password must be match next password',
    requiredMessage: 'Password must be required',
  },
  email: {
    minLength: 8,
    maxLength: 255,
    notMatchMessage: 'Email must be example email format user@gmail.com',
    requiredMessage: 'Email must be required',
  },
  firstName: {
    minLength: 4,
    maxLength: 20,
    requiredMessage: 'First name must be required',
  },
  lastName: {
    minLength: 4,
    maxLength: 20,
    requiredMessage: 'Last name must be required',
  },
};

@Entity({ name: 'users' })
export class UserEntity extends BaseEntity {
  @ManyToMany(() => RoleEntity, (role) => role.users, { cascade: true })
  @JoinTable({ name: 'users_has_roles' })
  roles?: RoleEntity[];

  @Column({
    type: 'varchar',
    length: UserEntityConfig.login?.maxLength,
    nullable: false,
    unique: true,
  })
  login!: string;

  @Column({
    type: 'varchar',
    length: UserEntityConfig.password?.maxLength,
    nullable: false,
    select: false,
  })
  password!: string;

  @Column({
    type: 'varchar',
    length: UserEntityConfig.email?.maxLength,
    nullable: false,
    unique: true,
  })
  email!: string;

  @Column({
    type: 'varchar',
    length: UserEntityConfig.firstName?.maxLength,
    nullable: false,
  })
  firstName!: string;

  @Column({
    type: 'varchar',
    length: UserEntityConfig.lastName?.maxLength,
    nullable: false,
  })
  lastName!: string;

  constructor(data: Partial<UserEntity>) {
    super();
    Object.assign(this, data);
  }

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    const salt = await bcrypt.genSalt();
    if (UserEntityConfig.password.regexp.test(this.password)) {
      this.password = await bcrypt.hash(this.password, salt);
    }
  }
}

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {}
