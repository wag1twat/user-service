import {
  Column,
  Entity,
  EntityRepository,
  ManyToMany,
  Repository,
} from 'typeorm';
import { RoleEntity } from './role.entity';
import { BaseEntity } from './base-entity';
import { CaslPermissionsActionKeysOrString } from '../casl/casl.permissions';

export const PermissionEntityConfig = {
  name: {
    minLength: 4,
    maxLength: 64,
    requiredMessage: 'Name must be required',
  },
};

@Entity({ name: 'permissions' })
export class PermissionEntity extends BaseEntity {
  constructor(data?: Partial<PermissionEntity>) {
    super();
    Object.assign(this, data);
  }

  @Column({
    type: 'varchar',
    length: PermissionEntityConfig.name.maxLength,
    unique: true,
  })
  name!: CaslPermissionsActionKeysOrString;

  @ManyToMany(() => RoleEntity, (role) => role.permissions, {
    onDelete: 'CASCADE',
  })
  roles?: RoleEntity[];
}

@EntityRepository(PermissionEntity)
export class PermissionRepository extends Repository<PermissionEntity> {}
