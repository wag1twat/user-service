import {
  Column,
  CreateDateColumn,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './user.entity';

export abstract class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  // () => UserEntity circular deps
  @ManyToOne('users', {
    cascade: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'owner' })
  owner: UserEntity | null = null;

  @CreateDateColumn({
    type: 'timestamp with time zone',
  })
  createdAt!: string;

  @UpdateDateColumn({ type: 'timestamp with time zone' })
  updatedAt!: string;

  @Column({ type: 'boolean', default: true })
  active = true;

  @Column({
    type: 'boolean',
    default: false,
  })
  protected = false;
}
