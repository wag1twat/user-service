import { SetMetadata } from '@nestjs/common';
import { PermissionsHandler } from './permissions.guard';

export const CHECK_PERMISSIONS_KEY = 'check_permissions';

export const CheckPermissions = (...handlers: PermissionsHandler[]) =>
  SetMetadata(CHECK_PERMISSIONS_KEY, handlers);
