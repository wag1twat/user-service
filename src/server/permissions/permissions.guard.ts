import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { CaslAbilityFactory } from '../casl/casl-ability.factory';
import { CaslAppAbilityType } from '../casl/casl.types';
import { CHECK_PERMISSIONS_KEY } from './check-permissions.decorator';

export interface IPermissionsHandler {
  handle(ability: CaslAppAbilityType): boolean;
}

type PermissionsHandlerCallback = (ability: CaslAppAbilityType) => boolean;

type PermissionsHandler = IPermissionsHandler | PermissionsHandlerCallback;

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private caslAbilityFactory: CaslAbilityFactory,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublicRoute = this.reflector.get<boolean>(
      'isPublicRoute',
      context.getHandler(),
    );

    if (isPublicRoute) {
      return true;
    }

    const permissionsHandlers =
      this.reflector.get<PermissionsHandler[]>(
        CHECK_PERMISSIONS_KEY,
        context.getHandler(),
      ) || [];

    const { user } = context.switchToHttp().getRequest<Request>();

    const ability = await this.caslAbilityFactory.createForUser(user);

    return permissionsHandlers.every((handler) => {
      return this.execPermissionsHandler(handler, ability);
    });
  }

  private execPermissionsHandler(
    handler: PermissionsHandler,
    ability: CaslAppAbilityType,
  ) {
    if (typeof handler === 'function') {
      return handler(ability);
    }
    return handler.handle(ability);
  }
}

export type { PermissionsHandler };
