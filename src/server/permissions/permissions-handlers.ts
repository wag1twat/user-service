import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import { CaslPermissionsAction } from '../casl/casl.permissions';
import { CaslAppAbilityType } from '../casl/casl.types';
import { IPermissionsHandler } from './permissions.guard';

/* UserEntity */
export class ReadUsersPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_READ_USER, UserEntity);
  }
}

export class CreateUsersPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_CREATE_USER, UserEntity);
  }
}

export class UpdateUsersPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_UPDATE_USER, UserEntity);
  }
}

export class DeleteUsersPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_DELETE_USER, UserEntity);
  }
}

/* RoleEntity */
export class ReadRolesPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_READ_ROLE, RoleEntity);
  }
}

export class CreateRolesPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_CREATE_ROLE, RoleEntity);
  }
}

export class UpdateRolesPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_UPDATE_ROLE, RoleEntity);
  }
}

export class DeleteRolesPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(CaslPermissionsAction.AVAILABLE_DELETE_ROLE, RoleEntity);
  }
}

/* PermissionEntity */
export class ReadPermissionsPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(
      CaslPermissionsAction.AVAILABLE_READ_PERMISSIONS,
      PermissionEntity,
    );
  }
}

export class CreatePermissionsPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(
      CaslPermissionsAction.AVAILABLE_CREATE_PERMISSIONS,
      PermissionEntity,
    );
  }
}

export class UpdatePermissionsPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(
      CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
      PermissionEntity,
    );
  }
}

export class DeletePermissionsPermissionHandler implements IPermissionsHandler {
  handle(ability: CaslAppAbilityType) {
    return ability.can(
      CaslPermissionsAction.AVAILABLE_DELETE_PERMISSIONS,
      PermissionEntity,
    );
  }
}
