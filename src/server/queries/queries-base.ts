import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsOptional } from 'class-validator';
import { FindOperator, In } from 'typeorm';
import { BetweenDates } from '../decorators';
import { BaseEntity } from '../entities/base-entity';
import { QueriesSort } from './queries-sort';
import { UserEntity } from '../entities/user.entity';

export class QueriesBase<
  SortBy extends string = string,
> extends QueriesSort<SortBy> {
  @ApiProperty({ required: false, type: [String, String] })
  @IsOptional()
  @BetweenDates()
  @Type(() => FindOperator)
  createdAt?: [
    BaseEntity['createdAt'] | undefined,
    BaseEntity['createdAt'] | undefined,
  ];

  @ApiProperty({ required: false, type: [String, String] })
  @IsOptional()
  @BetweenDates()
  @Type(() => FindOperator)
  updatedAt?: [
    BaseEntity['updatedAt'] | undefined,
    BaseEntity['updatedAt'] | undefined,
  ];

  @ApiProperty({ required: false, type: Boolean, default: undefined })
  @IsOptional()
  @IsBoolean()
  @Transform((params) => {
    if (typeof params.value === 'string') {
      if (params.value === 'true') {
        return true;
      }
      if (params.value === 'false') {
        return false;
      }
    }

    if (typeof params.value === 'boolean') {
      return params.value;
    }

    return Symbol;
  })
  active?: BaseEntity['active'];

  @ApiProperty({ required: false, type: Boolean, default: undefined })
  @IsOptional()
  @IsBoolean()
  @Transform((params) => {
    if (typeof params.value === 'string') {
      if (params.value === 'true') {
        return true;
      }
      if (params.value === 'false') {
        return false;
      }
    }

    if (typeof params.value === 'boolean') {
      return params.value;
    }

    return Symbol;
  })
  protected?: BaseEntity['protected'];

  @ApiProperty({ required: false, type: [String] })
  @IsOptional()
  @Type(() => FindOperator)
  @Transform((params) => In(params.value))
  owner?: UserEntity['id'][];
}
