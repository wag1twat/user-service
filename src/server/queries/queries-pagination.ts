import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';
import { toNumber } from 'lodash';

export class QueriesPagination {
  @ApiProperty({ required: false, type: Number })
  @Transform((params) => toNumber(params.value))
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiProperty({ required: false, type: Number })
  @Transform((params) => toNumber(params.value))
  @IsOptional()
  @IsNumber()
  limit?: number;
}
