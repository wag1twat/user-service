import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional } from 'class-validator';

export type Order = 'ASC' | 'DESC' | undefined;

export class QueriesSort<SortBy extends string = string> {
  @ApiProperty({
    required: false,
    type: String,
  })
  @IsOptional()
  @Type(() => String)
  sortBy?: SortBy;

  @ApiProperty({
    required: false,
    enum: ['DESC', 'ASC'],
    type: String,
    default: ['DESC'],
  })
  @IsOptional()
  @Type(() => String)
  order?: Order;
}
