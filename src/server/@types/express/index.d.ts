import 'express';

import { RequestUser } from 'src/server/entities/request-user';

declare module 'express' {
  export interface Request {
    user: RequestUser;
  }
}
