import { PermissionEntity } from '../../entities/permission.entity';
import { RoleEntity } from '../../entities/role.entity';
import { UserEntity } from '../../entities/user.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { CaslPermissionsAction } from 'src/server/casl/casl.permissions';

export class dev1652186460796 implements MigrationInterface {
  name = 'dev1652186460796';

  async up(queryRunner: QueryRunner) {
    const rootEntity = new UserEntity({
      login: 'root',
      firstName: 'root',
      lastName: 'root',
      email: 'root@gmail.com',
      password: 'Root@123',
      active: true,
      protected: true,
      owner: null,
    });

    const owneredUserEntity = new UserEntity({
      login: 'user3',
      firstName: 'user3',
      lastName: 'user3',
      email: 'user3@gmail.com',
      password: 'Root@123',
      active: true,
      protected: true,
      owner: null,
    });

    const rootUser = await queryRunner.manager
      .getRepository(UserEntity)
      .save(rootEntity);

    const owneredUser = await queryRunner.manager
      .getRepository(UserEntity)
      .save(owneredUserEntity);

    await queryRunner.manager
      .getRepository(UserEntity)
      .createQueryBuilder('user')
      .relation('owner')
      .of(owneredUser.id)
      .set(rootUser.id);

    const defaultUsersEntities = [
      new UserEntity({
        login: 'user1',
        firstName: 'user1',
        lastName: 'user1',
        email: 'user1@gmail.com',
        password: 'Root@123',
        active: true,
        protected: false,
        owner: null,
      }),
      new UserEntity({
        login: 'user2',
        firstName: 'user2',
        lastName: 'user2',
        email: 'user2@gmail.com',
        password: 'Root@123',
        active: true,
        protected: true,
        owner: null,
      }),
    ];

    await queryRunner.manager
      .getRepository(UserEntity)
      .save(defaultUsersEntities);

    const permissionsEntities = Object.keys(CaslPermissionsAction).map(
      (permission) =>
        new PermissionEntity({
          name: permission,
          active: true,
          protected: true,
        }),
    );

    const permissions = await queryRunner.manager
      .getRepository(PermissionEntity)
      .save(permissionsEntities);

    const roleEntity = new RoleEntity({
      name: 'root',
      active: true,
      protected: true,
    });

    const role = await queryRunner.manager
      .getRepository(RoleEntity)
      .save(roleEntity);

    await queryRunner.manager
      .getRepository(RoleEntity)
      .createQueryBuilder()
      .relation('permissions')
      .of(role.id)
      .add(permissions);

    await queryRunner.manager
      .getRepository(UserEntity)
      .createQueryBuilder()
      .relation('roles')
      .of(rootUser.id)
      .add(role);
  }

  async down(queryRunner: QueryRunner) {
    await queryRunner.manager.getRepository(UserEntity).clear();
    await queryRunner.manager.getRepository(PermissionEntity).clear();
    await queryRunner.manager.getRepository(RoleEntity).clear();
  }
}
