import { MigrationInterface, QueryRunner } from 'typeorm';

export class prod1652795264407 implements MigrationInterface {
  name = 'prod1652795264407';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "permissions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "active" boolean NOT NULL DEFAULT true, "protected" boolean NOT NULL DEFAULT false, "name" character varying(64) NOT NULL, "owner" uuid, CONSTRAINT "UQ_48ce552495d14eae9b187bb6716" UNIQUE ("name"), CONSTRAINT "PK_920331560282b8bd21bb02290df" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "roles" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "active" boolean NOT NULL DEFAULT true, "protected" boolean NOT NULL DEFAULT false, "name" character varying(64) NOT NULL, "owner" uuid, CONSTRAINT "UQ_648e3f5447f725579d7d4ffdfb7" UNIQUE ("name"), CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "active" boolean NOT NULL DEFAULT true, "protected" boolean NOT NULL DEFAULT false, "login" character varying(20) NOT NULL, "password" character varying(255) NOT NULL, "email" character varying(255) NOT NULL, "firstName" character varying(20) NOT NULL, "lastName" character varying(20) NOT NULL, "owner" uuid, CONSTRAINT "UQ_2d443082eccd5198f95f2a36e2c" UNIQUE ("login"), CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "roles_has_permissions" ("rolesId" uuid NOT NULL, "permissionsId" uuid NOT NULL, CONSTRAINT "PK_3632cd2acbec45a0e18717f2224" PRIMARY KEY ("rolesId", "permissionsId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_dbe4254adcd77d1defec062f5e" ON "roles_has_permissions" ("rolesId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_4b30e2ea230cd0a5c0a9fe5336" ON "roles_has_permissions" ("permissionsId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "users_has_roles" ("usersId" uuid NOT NULL, "rolesId" uuid NOT NULL, CONSTRAINT "PK_4982083f0d8f4232e72dd1f9665" PRIMARY KEY ("usersId", "rolesId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_5c6f7ee1254cb3a70a5340bdfc" ON "users_has_roles" ("usersId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_a9babf5d78da9a61e3fd30e0f3" ON "users_has_roles" ("rolesId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "permissions" ADD CONSTRAINT "FK_04ef5595a6ee541f1e2fa3987b1" FOREIGN KEY ("owner") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" ADD CONSTRAINT "FK_e0d8957b1cdfc1ba234a6d632e1" FOREIGN KEY ("owner") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_864093c7ac34e1ca6f51857ab3c" FOREIGN KEY ("owner") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles_has_permissions" ADD CONSTRAINT "FK_dbe4254adcd77d1defec062f5e9" FOREIGN KEY ("rolesId") REFERENCES "roles"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles_has_permissions" ADD CONSTRAINT "FK_4b30e2ea230cd0a5c0a9fe53369" FOREIGN KEY ("permissionsId") REFERENCES "permissions"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_has_roles" ADD CONSTRAINT "FK_5c6f7ee1254cb3a70a5340bdfca" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_has_roles" ADD CONSTRAINT "FK_a9babf5d78da9a61e3fd30e0f36" FOREIGN KEY ("rolesId") REFERENCES "roles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users_has_roles" DROP CONSTRAINT "FK_a9babf5d78da9a61e3fd30e0f36"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_has_roles" DROP CONSTRAINT "FK_5c6f7ee1254cb3a70a5340bdfca"`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles_has_permissions" DROP CONSTRAINT "FK_4b30e2ea230cd0a5c0a9fe53369"`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles_has_permissions" DROP CONSTRAINT "FK_dbe4254adcd77d1defec062f5e9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_864093c7ac34e1ca6f51857ab3c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" DROP CONSTRAINT "FK_e0d8957b1cdfc1ba234a6d632e1"`,
    );
    await queryRunner.query(
      `ALTER TABLE "permissions" DROP CONSTRAINT "FK_04ef5595a6ee541f1e2fa3987b1"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_a9babf5d78da9a61e3fd30e0f3"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_5c6f7ee1254cb3a70a5340bdfc"`,
    );
    await queryRunner.query(`DROP TABLE "users_has_roles"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_4b30e2ea230cd0a5c0a9fe5336"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_dbe4254adcd77d1defec062f5e"`,
    );
    await queryRunner.query(`DROP TABLE "roles_has_permissions"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TABLE "roles"`);
    await queryRunner.query(`DROP TABLE "permissions"`);
  }
}
