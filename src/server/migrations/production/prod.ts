import { PermissionEntity } from '../../entities/permission.entity';
import { RoleEntity } from '../../entities/role.entity';
import { UserEntity } from '../../entities/user.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { CaslPermissionsAction } from '../../casl/casl.permissions';

export class prod1652795264408 implements MigrationInterface {
  name = 'prod1652795264408';

  async up(queryRunner: QueryRunner) {
    const rootEntity = new UserEntity({
      login: 'root',
      firstName: 'root',
      lastName: 'root',
      email: 'root@gmail.com',
      password: 'Root@123',
      active: true,
      protected: true,
      owner: null,
    });

    const rootUser = await queryRunner.manager
      .getRepository(UserEntity)
      .save(rootEntity);

    const permissionsEntities = Object.keys(CaslPermissionsAction).map(
      (permission) =>
        new PermissionEntity({
          name: permission,
          active: true,
          protected: true,
        }),
    );

    const permissions = await queryRunner.manager
      .getRepository(PermissionEntity)
      .save(permissionsEntities);

    const roleEntity = new RoleEntity({
      name: 'root',
      active: true,
      protected: true,
    });

    const role = await queryRunner.manager
      .getRepository(RoleEntity)
      .save(roleEntity);

    await queryRunner.manager
      .getRepository(RoleEntity)
      .createQueryBuilder()
      .relation('permissions')
      .of(role.id)
      .add(permissions);

    await queryRunner.manager
      .getRepository(UserEntity)
      .createQueryBuilder()
      .relation('roles')
      .of(rootUser.id)
      .add(role);
  }

  async down(queryRunner: QueryRunner) {
    await queryRunner.manager.getRepository(UserEntity).clear();
    await queryRunner.manager.getRepository(PermissionEntity).clear();
    await queryRunner.manager.getRepository(RoleEntity).clear();
  }
}
