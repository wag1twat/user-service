import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1651995111531 implements MigrationInterface {
  name = 'test1651995111531';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" ALTER COLUMN "active" SET DEFAULT true`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" ALTER COLUMN "active" SET DEFAULT true`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "active" SET DEFAULT true`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users" ALTER COLUMN "active" SET DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" ALTER COLUMN "active" SET DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "permissions" ALTER COLUMN "active" SET DEFAULT false`,
    );
  }
}
