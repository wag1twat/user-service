import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1651754746642 implements MigrationInterface {
  name = 'test1651754746642';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" ADD "protected" boolean NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" DROP COLUMN "protected"`,
    );
  }
}
