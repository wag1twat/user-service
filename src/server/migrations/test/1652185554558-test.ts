import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1652185554558 implements MigrationInterface {
  name = 'test1652185554558';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "roles" ADD "protected" boolean NOT NULL DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "protected" boolean NOT NULL DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "protected"`);
    await queryRunner.query(`ALTER TABLE "roles" DROP COLUMN "protected"`);
  }
}
