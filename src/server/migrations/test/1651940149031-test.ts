import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1651940149031 implements MigrationInterface {
  name = 'test1651940149031';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" RENAME COLUMN "deleted" TO "active"`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" RENAME COLUMN "deleted" TO "active"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" RENAME COLUMN "deleted" TO "active"`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users" RENAME COLUMN "active" TO "deleted"`,
    );
    await queryRunner.query(
      `ALTER TABLE "roles" RENAME COLUMN "active" TO "deleted"`,
    );
    await queryRunner.query(
      `ALTER TABLE "permissions" RENAME COLUMN "active" TO "deleted"`,
    );
  }
}
