import { MigrationInterface, QueryRunner } from 'typeorm';

export class test1652112375164 implements MigrationInterface {
  name = 'test1652112375164';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" ALTER COLUMN "protected" SET DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permissions" ALTER COLUMN "protected" DROP DEFAULT`,
    );
  }
}
