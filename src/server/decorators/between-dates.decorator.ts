import { Transform } from 'class-transformer';
import { DateTime } from 'luxon';
import { Between, LessThan, MoreThan } from 'typeorm';

export function BetweenDates() {
  return Transform((params) => {
    const [from, to] = params.value;

    if (DateTime.fromISO(from).isValid && DateTime.fromISO(to).isValid) {
      return Between(from, to);
    }

    if (DateTime.fromISO(from).isValid) {
      return MoreThan(from);
    }

    if (DateTime.fromISO(to).isValid) {
      return LessThan(to);
    }
  });
}
