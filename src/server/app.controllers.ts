import { Controller, Get, Render, UseInterceptors } from '@nestjs/common';
import { PublicRoute } from './controllers/auth/jwt-auth.guard';
import { ParamsInterceptor } from './interceptrors/params.interceptor';

@Controller()
export class AppController {
  @Get('/')
  @PublicRoute()
  @Render('Main')
  @UseInterceptors(ParamsInterceptor)
  main() {
    return { title: 'Main' };
  }

  @Get('/users')
  @PublicRoute()
  @Render('Users')
  @UseInterceptors(ParamsInterceptor)
  users() {
    return { title: 'Users' };
  }

  @Get('/users/:id')
  @PublicRoute()
  @Render('User')
  @UseInterceptors(ParamsInterceptor)
  user() {
    return { title: 'User' };
  }

  @Get('/roles')
  @PublicRoute()
  @Render('Roles')
  @UseInterceptors(ParamsInterceptor)
  roles() {
    return { title: 'Roles' };
  }

  @Get('/permissions')
  @PublicRoute()
  @Render('Permissions')
  @UseInterceptors(ParamsInterceptor)
  permissions() {
    return { title: 'Permissions' };
  }
}
