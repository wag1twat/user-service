import { AbilityBuilder, ExtractSubjectType } from '@casl/ability';
import { Injectable } from '@nestjs/common';
import { NODE_ENV } from 'src/shared/constants';
import { PermissionsService } from '../controllers/permissions/permissions.service';
import { PermissionEntity } from '../entities/permission.entity';
import { RequestUser } from '../entities/request-user';
import { RoleEntity } from '../entities/role.entity';
import { UserEntity } from '../entities/user.entity';
import caslMatchers from './casl.matchers';
import {
  cannotAccessProtectedField,
  cannotCreate,
  cannotDelete,
  cannotIfSystemProtected,
  cannotIfYourself,
  cannotInvalidCreator,
  cannotRead,
  cannotUpdate,
  isCannotAccessProtectedField,
  isInvalidCreator,
  isSystemProtected,
  isYourself,
} from './casl.becauses';
import { CaslPermissionsAction } from './casl.permissions';
import {
  CaslAppAbility,
  CaslAppAbilityType,
  CaslEntitiesSubjects,
  Permission,
  Role,
  User,
} from './casl.types';

@Injectable()
class CaslAbilityFactory {
  constructor(private readonly permissionsService: PermissionsService) {}

  async createForUser(requestUser: RequestUser) {
    const { can, cannot, build } = new AbilityBuilder<CaslAppAbilityType>(
      CaslAppAbility,
    );

    let [permissions] = await this.permissionsService.findAll({
      usersIds: [requestUser.id],
    });

    if (NODE_ENV === 'test') {
      permissions = Object.keys(CaslPermissionsAction).map(
        (name) => new PermissionEntity({ name }),
      );
    }
    const names = permissions.map((permission) => permission.name);

    /* UserEntity */
    if (names.includes(CaslPermissionsAction.AVAILABLE_READ_USER)) {
      can(CaslPermissionsAction.AVAILABLE_READ_USER, UserEntity).because(
        cannotRead(User),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_CREATE_USER)) {
      can(CaslPermissionsAction.AVAILABLE_CREATE_USER, UserEntity).because(
        cannotCreate(User),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_UPDATE_USER)) {
      can(CaslPermissionsAction.AVAILABLE_UPDATE_USER, UserEntity).because(
        cannotUpdate(User),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_DELETE_USER)) {
      can(CaslPermissionsAction.AVAILABLE_DELETE_USER, UserEntity).because(
        cannotDelete(User),
      );
    }

    cannot(
      CaslPermissionsAction.AVAILABLE_UPDATE_USER,
      UserEntity,
      ['protected'],
      (props) => isCannotAccessProtectedField(requestUser, props),
    ).because(cannotAccessProtectedField());
    cannot(CaslPermissionsAction.AVAILABLE_UPDATE_USER, UserEntity, (props) =>
      isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(User));
    cannot(CaslPermissionsAction.AVAILABLE_UPDATE_USER, UserEntity, (props) =>
      isYourself(requestUser, props),
    ).because(cannotIfYourself());
    cannot(CaslPermissionsAction.AVAILABLE_UPDATE_USER, UserEntity, (props) =>
      isSystemProtected(props),
    ).because(cannotIfSystemProtected(User));

    cannot(CaslPermissionsAction.AVAILABLE_DELETE_USER, UserEntity, (props) =>
      isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(User));
    cannot(CaslPermissionsAction.AVAILABLE_DELETE_USER, UserEntity, (props) =>
      isYourself(requestUser, props),
    ).because(cannotIfYourself());
    cannot(CaslPermissionsAction.AVAILABLE_DELETE_USER, UserEntity, (props) =>
      isSystemProtected(props),
    ).because(cannotIfSystemProtected(User));

    /* RoleEntity */
    if (names.includes(CaslPermissionsAction.AVAILABLE_READ_ROLE)) {
      can(CaslPermissionsAction.AVAILABLE_READ_ROLE, RoleEntity).because(
        cannotRead(Role),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_CREATE_ROLE)) {
      can(CaslPermissionsAction.AVAILABLE_CREATE_ROLE, RoleEntity).because(
        cannotCreate(Role),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_UPDATE_ROLE)) {
      can(CaslPermissionsAction.AVAILABLE_UPDATE_ROLE, RoleEntity).because(
        cannotUpdate(Role),
      );
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_DELETE_ROLE)) {
      can(CaslPermissionsAction.AVAILABLE_DELETE_ROLE, RoleEntity).because(
        cannotDelete(Role),
      );
    }

    cannot(
      CaslPermissionsAction.AVAILABLE_UPDATE_ROLE,
      RoleEntity,
      ['protected'],
      (props) => isCannotAccessProtectedField(requestUser, props),
    ).because(cannotAccessProtectedField());
    cannot(CaslPermissionsAction.AVAILABLE_UPDATE_ROLE, RoleEntity, (props) =>
      isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(Role));
    cannot(CaslPermissionsAction.AVAILABLE_UPDATE_ROLE, RoleEntity, (props) =>
      isSystemProtected(props),
    ).because(cannotIfSystemProtected(Role));

    cannot(CaslPermissionsAction.AVAILABLE_DELETE_ROLE, RoleEntity, (props) =>
      isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(Role));
    cannot(CaslPermissionsAction.AVAILABLE_DELETE_ROLE, RoleEntity, (props) =>
      isSystemProtected(props),
    ).because(cannotIfSystemProtected(Role));

    /* PermissionEntity */
    if (names.includes(CaslPermissionsAction.AVAILABLE_READ_PERMISSIONS)) {
      can(
        CaslPermissionsAction.AVAILABLE_READ_PERMISSIONS,
        PermissionEntity,
      ).because(cannotRead(Permission));
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_CREATE_PERMISSIONS)) {
      can(
        CaslPermissionsAction.AVAILABLE_CREATE_PERMISSIONS,
        PermissionEntity,
      ).because(cannotCreate(Permission));
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS)) {
      can(
        CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
        PermissionEntity,
      ).because(cannotUpdate(Permission));
    }
    if (names.includes(CaslPermissionsAction.AVAILABLE_DELETE_PERMISSIONS)) {
      can(
        CaslPermissionsAction.AVAILABLE_DELETE_PERMISSIONS,
        PermissionEntity,
      ).because(cannotDelete(Permission));
    }

    cannot(
      CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
      PermissionEntity,
      ['protected'],
      (props) => isCannotAccessProtectedField(requestUser, props),
    ).because(cannotAccessProtectedField());
    cannot(
      CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
      PermissionEntity,
      (props) => isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(Permission));
    cannot(
      CaslPermissionsAction.AVAILABLE_UPDATE_PERMISSIONS,
      PermissionEntity,
      (props) => isSystemProtected(props),
    ).because(cannotIfSystemProtected(Permission));

    cannot(
      CaslPermissionsAction.AVAILABLE_DELETE_PERMISSIONS,
      PermissionEntity,
      (props) => isInvalidCreator(requestUser, props),
    ).because(cannotInvalidCreator(Permission));
    cannot(
      CaslPermissionsAction.AVAILABLE_DELETE_PERMISSIONS,
      PermissionEntity,
      (props) => isSystemProtected(props),
    ).because(cannotIfSystemProtected(Permission));

    return build({
      detectSubjectType: (item) =>
        item.constructor as ExtractSubjectType<CaslEntitiesSubjects>,
      conditionsMatcher: caslMatchers.conditionsMatcher,
      fieldMatcher: caslMatchers.fieldMatcher,
    });
  }
}

export { CaslAbilityFactory };
