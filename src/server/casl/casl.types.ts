import {
  AbilityClass,
  AbilityTuple,
  InferSubjects,
  MatchConditions,
  PureAbility,
} from '@casl/ability';
import { PermissionEntity } from '../entities/permission.entity';
import { RoleEntity } from '../entities/role.entity';
import { UserEntity } from '../entities/user.entity';
import { CaslPermissionsAction } from './casl.permissions';

const User = 'User';
const Role = 'Role';
const Permission = 'Permission';

type CaslEntitiesDisplaynames = 'User' | 'Role' | 'Permission';

type CaslEntitiesSubjects = InferSubjects<
  | typeof UserEntity
  | UserEntity
  | typeof RoleEntity
  | RoleEntity
  | typeof PermissionEntity
  | PermissionEntity
>;

type CaslAppAbilityType = PureAbility<
  AbilityTuple<CaslPermissionsAction, CaslEntitiesSubjects>,
  MatchConditions
>;

const CaslAppAbility = PureAbility as AbilityClass<CaslAppAbilityType>;

export { CaslAppAbility, User, Role, Permission };
export type {
  CaslEntitiesDisplaynames,
  CaslEntitiesSubjects,
  CaslAppAbilityType,
};
