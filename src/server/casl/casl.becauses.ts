import { PermissionEntity } from '../entities/permission.entity';
import { RequestUser } from '../entities/request-user';
import { RoleEntity } from '../entities/role.entity';
import { UserEntity } from '../entities/user.entity';
import { CaslEntitiesDisplaynames } from './casl.types';

export const cannotRead = (entity: CaslEntitiesDisplaynames) =>
  `Does not have permission for read ${entity}`;
export const cannotCreate = (entity: CaslEntitiesDisplaynames) =>
  `Does not have permission for read ${entity}`;
export const cannotUpdate = (entity: CaslEntitiesDisplaynames) =>
  `Does not have permissions for update ${entity}`;
export const cannotDelete = (entity: CaslEntitiesDisplaynames) =>
  `Does not have permissions for update ${entity}`;
export const cannotInvalidCreator = (entity: CaslEntitiesDisplaynames) =>
  `Are you not creator the ${entity}`;
export const cannotIfYourself = () => 'Does not update yourself';
export const cannotIfSystemProtected = (entity: CaslEntitiesDisplaynames) =>
  `The ${entity} protected by system`;
export const cannotAccessProtectedField = () =>
  'The field is protected and can only be modified by the creator.';

export const isSystemProtected = (
  entity: UserEntity | RoleEntity | PermissionEntity,
) => {
  return Boolean(entity.owner === null && entity.protected);
};
export const isInvalidCreator = (
  requestUser: RequestUser,
  entity: UserEntity | RoleEntity | PermissionEntity,
) => {
  return (
    Boolean(entity.owner && entity.owner.id !== requestUser.id) &&
    entity.protected
  );
};
export const isYourself = (
  requestUser: RequestUser,
  entity: UserEntity | RoleEntity | PermissionEntity,
) => {
  return entity.id === requestUser.id;
};
export const isCannotAccessProtectedField = (
  requestUser: RequestUser,
  entity: UserEntity | RoleEntity | PermissionEntity,
) => {
  return Boolean(entity.owner && entity.owner.id !== requestUser.id);
};
