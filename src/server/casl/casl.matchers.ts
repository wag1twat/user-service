import { FieldMatcher, MatchConditions } from '@casl/ability';

const conditionsMatcher = (matchConditions: MatchConditions) => matchConditions;
const fieldMatcher: FieldMatcher = (fields) => (field) =>
  fields.includes(field);

export default { conditionsMatcher, fieldMatcher };
