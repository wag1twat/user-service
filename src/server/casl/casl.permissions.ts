import { LiteralUnion } from '../shared/types';

enum CaslPermissionsAction {
  'AVAILABLE_CREATE_USER' = 'AVAILABLE_CREATE_USER',
  'AVAILABLE_READ_USER' = 'AVAILABLE_READ_USER',
  'AVAILABLE_UPDATE_USER' = 'AVAILABLE_UPDATE_USER',
  'AVAILABLE_DELETE_USER' = 'AVAILABLE_DELETE_USER',
  'AVAILABLE_CREATE_ROLE' = 'AVAILABLE_CREATE_ROLE',
  'AVAILABLE_READ_ROLE' = 'AVAILABLE_READ_ROLE',
  'AVAILABLE_UPDATE_ROLE' = 'AVAILABLE_UPDATE_ROLE',
  'AVAILABLE_DELETE_ROLE' = 'AVAILABLE_DELETE_ROLE',
  'AVAILABLE_CREATE_PERMISSIONS' = 'AVAILABLE_CREATE_PERMISSIONS',
  'AVAILABLE_READ_PERMISSIONS' = 'AVAILABLE_READ_PERMISSIONS',
  'AVAILABLE_UPDATE_PERMISSIONS' = 'AVAILABLE_UPDATE_PERMISSIONS',
  'AVAILABLE_DELETE_PERMISSIONS' = 'AVAILABLE_DELETE_PERMISSIONS',
}

type CaslPermissionsActionKeys = keyof typeof CaslPermissionsAction;
type CaslPermissionsActionKeysOrString =
  LiteralUnion<CaslPermissionsActionKeys>;

export type { CaslPermissionsActionKeys, CaslPermissionsActionKeysOrString };
export { CaslPermissionsAction };
