import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionsService } from '../controllers/permissions/permissions.service';
import { PermissionRepository } from '../entities/permission.entity';
import { CaslAbilityFactory } from './casl-ability.factory';

@Module({
  imports: [TypeOrmModule.forFeature([PermissionRepository])],
  providers: [CaslAbilityFactory, PermissionsService],
  exports: [CaslAbilityFactory],
})
export class CaslModule {}
