import { HttpException, HttpStatus } from '@nestjs/common';
import { ExceptionMessages } from './constants';

export class WrongCredentialsException extends HttpException {
  constructor(response?: string | Record<string, any>, status?: number) {
    super(
      response || ExceptionMessages.CREDENTIALS,
      status || HttpStatus.BAD_REQUEST,
    );
  }
}
