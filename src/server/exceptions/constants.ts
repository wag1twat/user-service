export enum ExceptionMessages {
  DEACTIVE_USER = 'You account was been deactive, please activate in profile',
  UNAUTHORIZED = 'Unauthorized, please sign in or sign up :)',
  CREDENTIALS = 'Wrong credentials',
  FORBIDDEN = 'Forbidden resource',
  PROTECTED = 'Forbidden protected resource',
}
