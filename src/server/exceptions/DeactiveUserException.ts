import { HttpException, HttpStatus } from '@nestjs/common';
import { ExceptionMessages } from './constants';

export class DeactiveUserException extends HttpException {
  constructor(response?: string | Record<string, any>, status?: number) {
    super(
      response || ExceptionMessages.DEACTIVE_USER,
      status || HttpStatus.BAD_REQUEST,
    );
  }
}
