import { HttpException, HttpStatus } from '@nestjs/common';
import { ExceptionMessages } from './constants';

export class ProtectedException extends HttpException {
  constructor(response?: string | Record<string, any>, status?: number) {
    super(
      response || ExceptionMessages.PROTECTED,
      status || HttpStatus.BAD_REQUEST,
    );
  }
}
