import { HttpException, HttpStatus } from '@nestjs/common';
import { ExceptionMessages } from './constants';

export class UnauthorizedException extends HttpException {
  constructor(response?: string | Record<string, any>, status?: number) {
    super(
      response || ExceptionMessages.UNAUTHORIZED,
      status || HttpStatus.UNAUTHORIZED,
    );
  }
}
