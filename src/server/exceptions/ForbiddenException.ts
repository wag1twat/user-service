import { HttpException, HttpStatus } from '@nestjs/common';
import { ExceptionMessages } from './constants';

export class ForbiddenException extends HttpException {
  constructor(response?: string | Record<string, any>, status?: number) {
    super(
      response || ExceptionMessages.FORBIDDEN,
      status || HttpStatus.FORBIDDEN,
    );
  }
}
