import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './controllers/user/user.module';
import { UsersModule } from './controllers/users/users.module';
import { AuthModule } from './controllers/auth/auth.module';
import { MeModule } from './controllers/me/me.module';
import { RolesModule } from './controllers/roles/roles.module';
import { RoleModule } from './controllers/role/role.module';
import { ormconfig } from './ormconfig';
import { AppController } from './app.controllers';
import { RenderModule } from 'nest-next';
import next from 'next';
import { NODE_ENV } from 'src/shared/constants/env';
import { PermissionsModule } from './controllers/permissions/permissions.module';
import { PermissionModule } from './controllers/permission/permission.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from './controllers/auth/jwt-auth.guard';
import { CaslModule } from './casl/casl.module';
import { PermissionsGuard } from './permissions/permissions.guard';

declare const module: any;

const Next = next({ dev: NODE_ENV === 'development' });

@Module({})
export class AppModule {
  public static initialize(): DynamicModule {
    /* При инициализации модуля попробуем извлечь инстанс RenderModule
            из персистентных данных между перезагрузками модуля */
    Next.prepare();

    const renderModule =
      module.hot?.data?.renderModule ??
      RenderModule.forRootAsync(Next, { dev: NODE_ENV === 'development' });

    if (module.hot) {
      /* При завершении работы старого модуля
                будем кэшировать инстанс RenderModule */
      module.hot.dispose((data: any) => {
        data.renderModule = renderModule;
      });
    }

    return {
      module: AppModule,
      imports: [
        ConfigModule.forRoot({ isGlobal: true, load: [ormconfig] }),
        TypeOrmModule.forRoot(ormconfig()),
        renderModule,
        AuthModule,
        CaslModule,
        UserModule,
        UsersModule,
        MeModule,
        RoleModule,
        RolesModule,
        PermissionModule,
        PermissionsModule,
      ],
      controllers: [AppController],
      providers: [
        { provide: APP_GUARD, useClass: JwtAuthGuard },
        {
          provide: APP_GUARD,
          useClass: PermissionsGuard,
        },
      ],
    };
  }
}
