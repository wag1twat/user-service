import { NestApplication, NestFactory } from '@nestjs/core';
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import morgan from 'morgan';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { swaggerSetup } from './process/swagger-setup';
import { NextErrorHandling } from './process/next-error-handling';
import { RenderService } from 'nest-next';
import config from './process/config';

declare const module: any;

async function bootstrap() {
  const server = await NestFactory.create<NestApplication>(
    AppModule.initialize(),
  );

  server.use(bodyParser.json({ limit: config.bodyLimit }));

  server.use(
    bodyParser.urlencoded({ limit: config.bodyLimit, extended: true }),
  );

  // server.use(morgan('tiny'));

  server.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      skipNullProperties: false,
      skipUndefinedProperties: false,
    }),
  );

  server.enableCors();

  const service = server.get(RenderService);

  service.setErrorHandler(NextErrorHandling);

  swaggerSetup(server);

  await server.listen(3000);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
  }
}

bootstrap();
