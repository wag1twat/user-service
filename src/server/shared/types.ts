import { FindOperator } from 'typeorm';

export type InferFindOperatorGeneric<F> = F extends FindOperator<infer G>
  ? G
  : F;

export type QueriesTransformPlain<Q> = {
  [K in keyof Q]: InferFindOperatorGeneric<Q[K]>;
};

export type Overwrite<T, U extends Partial<Record<keyof T, any>>> = Pick<
  T,
  Exclude<keyof T, keyof U>
> &
  U;

export type LiteralUnion<T extends U, U = string> = T | (U & {});
