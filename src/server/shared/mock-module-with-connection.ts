import { ModuleMetadata } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ormconfig } from '../ormconfig';

export const mockModuleWithConnection = async (
  meta: ModuleMetadata,
): Promise<TestingModule> => {
  return await Test.createTestingModule({
    ...meta,
    imports: meta.imports?.concat([
      ConfigModule.forRoot({ isGlobal: true, load: [ormconfig] }),
      TypeOrmModule.forRoot(ormconfig()),
    ]),
  }).compile();
};
