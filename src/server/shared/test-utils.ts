import { Request, Response } from 'express';
import {
  createRequest,
  createResponse,
  RequestOptions,
  ResponseOptions,
} from 'node-mocks-http';

type Class = { new (...args: any[]): any };

export class TestUtils {
  static request(options: RequestOptions) {
    return createRequest(options) as Request;
  }

  static response(options: ResponseOptions) {
    return createResponse(options) as Response;
  }

  static equalObjects<T extends object = object>(o1: T, o2: T) {
    return expect(o1).toStrictEqual(expect.objectContaining(o2));
  }

  static equalArrays<T extends unknown[] = unknown[]>(a1: T, a2: T) {
    return expect(a1).toStrictEqual(expect.arrayContaining(a2));
  }

  static arrayOfObjects<T extends object[] = object[]>(array: T, o: T[number]) {
    return expect(array).toEqual(expect.arrayContaining([expect.any(o)]));
  }

  static arrayOfContains<T extends unknown[] = unknown[]>(
    array: T,
    o: Partial<T[number]>,
  ) {
    return expect(array).toEqual(
      expect.arrayContaining([expect.objectContaining(o)]),
    );
  }

  static arrayOfClass<T extends Class[] = Class[]>(
    array: unknown[],
    c: T[number],
  ) {
    return expect(array).toEqual(expect.arrayContaining([expect.any(c)]));
  }

  static randomString(length: number) {
    let result = '';

    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  static getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
  }
}
