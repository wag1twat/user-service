import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigObject, registerAs } from '@nestjs/config';
import { join } from 'path/posix';
import * as dotenv from 'dotenv';
import { NODE_ENV } from '../shared/constants';
import { UserEntity } from './entities/user.entity';
import { RoleEntity } from './entities/role.entity';
import { PermissionEntity } from './entities/permission.entity';

dotenv.config();

const configs: Record<typeof NODE_ENV, TypeOrmModuleOptions> = {
  development: {
    type: 'postgres',
    url: process.env.DEV_DATABASE_URL,
    synchronize: false,
    migrationsRun: false,
    autoLoadEntities: false,
    logging: true,
    logger: 'file',
    entities: [UserEntity, RoleEntity, PermissionEntity],
    migrations: [join(__dirname, '/migrations/development/**/*{.ts,.js}')],
    cli: {
      entitiesDir: join(__dirname, '/entities'),
      migrationsDir: join(__dirname, '/migrations/development'),
    },
    keepConnectionAlive: true,
    dropSchema: false,
  },
  test: {
    type: 'postgres',
    url: process.env.TEST_DATABASE_URL,
    synchronize: false,
    migrationsRun: false,
    autoLoadEntities: false,
    logging: true,
    logger: 'file',
    entities: [UserEntity, RoleEntity, PermissionEntity],
    migrations: [join(__dirname, '/migrations/test/**/*{.ts,.js}')],
    cli: {
      entitiesDir: join(__dirname, '/entities'),
      migrationsDir: join(__dirname, '/migrations/test'),
    },
    keepConnectionAlive: true,
    dropSchema: false,
  },
  production: {
    type: 'postgres',
    url: process.env.PROD_DATABASE_URL,
    synchronize: false,
    migrationsRun: false,
    autoLoadEntities: false,
    logging: true,
    logger: 'file',
    entities: [UserEntity, RoleEntity, PermissionEntity],
    migrations: [join(__dirname, '/migrations/production/**/*{.ts,.js}')],
    cli: {
      entitiesDir: join(__dirname, '/entities'),
      migrationsDir: join(__dirname, '/migrations/production'),
    },
    keepConnectionAlive: true,
    dropSchema: false,
  },
};

const config = configs[NODE_ENV];

export const ormconfig = registerAs<ConfigObject, () => TypeOrmModuleOptions>(
  'typeOrmConfig',
  (): TypeOrmModuleOptions => config,
);

export default config;
