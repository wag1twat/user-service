import { NestApplication } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { PermissionQueries } from '../controllers/permission/queries/permission.queries';
import { PermissionsQueries } from '../controllers/permissions/queries/permissions.queries';
import { RoleQueries } from '../controllers/role/queries/role.queries';
import { RoleRemoveUsersQueries } from '../controllers/role/queries/role-remove-users.queries';
import { RolesQueries } from '../controllers/roles/queries/roles.queries';
import { UserRemoveRolesQueries } from '../controllers/user/queries/user-remove-roles.queries';
import { UserQueries } from '../controllers/user/queries/user.queries';
import { UsersQueries } from '../controllers/users/queries/users-queries';
import { QueriesBase } from '../queries/queries-base';
import { QueriesPagination } from '../queries/queries-pagination';
import { QueriesSort } from '../queries/queries-sort';
import { RoleRemovePermissionsQueries } from '../controllers/role/queries/role-remove-permissions.queries';

const config = new DocumentBuilder()
  .setTitle('User service API')
  .setVersion('0.0.2')
  .addBearerAuth({ in: 'header', type: 'http', name: 'Authorization' })
  .build();

export const swaggerPath = '/api-docs';

export function swaggerSetup(app: NestApplication) {
  const document = SwaggerModule.createDocument(app, config, {
    extraModels: [
      UserQueries,
      UserRemoveRolesQueries,
      UsersQueries,
      RoleQueries,
      RoleRemoveUsersQueries,
      RoleRemovePermissionsQueries,
      RolesQueries,
      PermissionQueries,
      PermissionsQueries,
      QueriesBase,
      QueriesPagination,
      QueriesSort,
    ],
  });

  SwaggerModule.setup(swaggerPath, app, document);
}
