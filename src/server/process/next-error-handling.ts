import { HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { SerializeException } from 'src/shared/helpers';
import {
  TypeORMError,
  QueryFailedError,
  EntityNotFoundError,
  MetadataAlreadyExistsError,
} from 'typeorm';

type Exception =
  | Error
  | HttpException
  | TypeORMError
  | QueryFailedError
  | EntityNotFoundError
  | MetadataAlreadyExistsError;

export async function NextErrorHandling(
  exception: Exception,
  req: Request,
  res: Response,
) {
  const serializeException = new SerializeException({
    message: exception.message,
    statusCode: 0,
    error: null,
  });

  serializeException.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
  serializeException.message = exception.message;

  if (exception instanceof HttpException) {
    serializeException.statusCode = exception.getStatus();

    const response = exception.getResponse();

    if (typeof response === 'string') {
      serializeException.error = { response };
    }

    if (typeof response === 'object') {
      serializeException.error = {
        ...response,
        parameters: [],
        stack: exception.stack,
        detail: undefined,
      };
    }

    return res.status(serializeException.statusCode).json(serializeException);
  }

  if (exception instanceof QueryFailedError) {
    serializeException.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    serializeException.message = exception.message;
    serializeException.error = {
      parameters: exception.parameters,
      stack: exception.stack,
      detail: exception.driverError.detail,
    };

    return res.status(serializeException.statusCode).json(serializeException);
  }

  if (exception instanceof EntityNotFoundError) {
    serializeException.statusCode = HttpStatus.NOT_FOUND;
    serializeException.message = exception.message;
    serializeException.error = {
      parameters: [],
      stack: exception.stack,
      detail: undefined,
    };

    return res.status(serializeException.statusCode).json(serializeException);
  }

  if (exception instanceof MetadataAlreadyExistsError) {
    serializeException.statusCode = HttpStatus.BAD_REQUEST;
    serializeException.message = exception.message;
    serializeException.error = {
      parameters: [],
      stack: exception.stack,
      detail: undefined,
    };

    return res.status(serializeException.statusCode).json(serializeException);
  }

  if (exception instanceof TypeORMError) {
    serializeException.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    serializeException.message = exception.message;
    serializeException.error = {
      parameters: [],
      stack: exception.stack,
      detail: undefined,
    };

    return res.status(serializeException.statusCode).json(serializeException);
  }

  if (exception instanceof Error) {
    serializeException.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    serializeException.message = exception.message;
    serializeException.error = { stack: exception.stack };
    return res.status(serializeException.statusCode).json(serializeException);
  }

  return res.status(serializeException.statusCode).json(serializeException);
}
