import React from 'react';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { ChakraProvider, localStorageManager } from '@chakra-ui/react';
import { AppProps } from 'next/app';
import { theme } from '../client/process/theme';
import Fonts from '../client/process/fonts';
import { AuthProvider, GlobalDisclosuresProvider } from '../client/providers';

export default function App({ Component, pageProps }: AppProps) {
  const [queryClient] = React.useState(
    () => new QueryClient({ defaultOptions: { queries: { retry: 0 } } }),
  );
  return (
    <ChakraProvider
      resetCSS
      theme={theme}
      colorModeManager={localStorageManager}
    >
      <Fonts />
      <QueryClientProvider client={queryClient}>
        <Hydrate state={pageProps?.dehydratedState}>
          <GlobalDisclosuresProvider>
            <AuthProvider>
              {/* @ts-ignore */}
              <Component {...pageProps} />
            </AuthProvider>
          </GlobalDisclosuresProvider>
        </Hydrate>
      </QueryClientProvider>
    </ChakraProvider>
  );
}
