import { NextPage } from 'next';
import UsersPage from '../../client/pages/UsersPage/UsersPage';

const Users: NextPage<{}> = () => <UsersPage />;

export default Users;
