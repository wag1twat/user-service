import { NextPage } from 'next';
import MainPage from '../../client/pages/MainPage/MainPage';

const Main: NextPage<{}> = () => <MainPage />;

export default Main;
