import { NextPage } from 'next';
import PermissionsPage from '../../client/pages/PermissionsPage/PermissionsPage';

const Permissions: NextPage<{}> = () => <PermissionsPage />;

export default Permissions;
