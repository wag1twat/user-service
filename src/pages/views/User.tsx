import { NextPage, NextPageContext } from 'next';
import React from 'react';
import { User } from 'src/server/entities/client.types';
import { UserPage } from '../../client/pages/UserPage';

type UserPageQueries = {
  id: User['id'];
};

interface Context extends NextPageContext {
  query: UserPageQueries;
}

const getServerSideProps = async (ctx: Context) => {
  const id = ctx.query.id;
  return { props: { id } };
};

const User: NextPage<UserPageQueries> = ({ id }) => <UserPage id={id} />;

export { getServerSideProps };

export default User;
