import { NextPage } from 'next';
import RolesPage from '../../client/pages/RolesPage/RolesPage';

const Roles: NextPage<{}> = () => <RolesPage />;

export default Roles;
