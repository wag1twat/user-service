import { extendTheme, withDefaultSize } from '@chakra-ui/react';
import { colors } from './colors';

const theme = extendTheme(
  withDefaultSize({
    size: 'sm',
    components: [
      'Button',
      'Input',
      'Table',
      'Checkbox',
      'Select',
      'Text',
      'Tag',
      'Tab',
    ],
  }),
  {
    config: {
      initialColorMode: 'light',
    },
    colors,
    fonts: {
      heading: 'Roboto',
      body: 'Roboto',
    },

    components: {
      Tag: {
        sizes: {
          lg: {
            closeButton: {
              fontSize: 'lg',
            },
          },
          md: {
            closeButton: {
              fontSize: 'md',
            },
          },
          sm: {
            closeButton: {
              fontSize: 'sm',
            },
          },
          xs: {
            closeButton: {
              marginEnd: '-4px',
              marginStart: '0.25rem',
              fontSize: 'xx-small',
            },
            container: {
              borderRadius: 'md',
              fontSize: 'xx-small',
              minH: '0.75rem',
              minW: '0.75rem',
              px: 1,
            },
          },
        },
      },
    },
  },
);

export { theme };
