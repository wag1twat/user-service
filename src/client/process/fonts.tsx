import { Global } from '@emotion/react';

const Fonts = () => <Global styles={`@font-face { font-family: Roboto; }`} />;

export default Fonts;
