import { Flex, Heading } from '@chakra-ui/react';
import React from 'react';
import { GlobalSpinner } from 'src/client/features';

type PreviewProps = {};

const Preview: React.FC<PreviewProps> = () => {
  return (
    <Flex alignItems="center" justifyContent="center" width="100%" mb="80px">
      <Heading size="4xl" textAlign="center" zIndex={1}>
        User service for manage authorizations
      </Heading>
      <GlobalSpinner />
    </Flex>
  );
};

export { Preview };
