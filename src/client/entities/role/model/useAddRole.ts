import { MutationKey } from 'react-query';
import { CreateRoleDto } from 'src/server/controllers/role/dto/create-role.dto';
import { RoleEntity } from 'src/server/entities/role.entity';
import { SerializeException } from 'src/shared/helpers';
import { PostHookOptions, usePost } from 'src/shared/axios';

export const useAddRole = (
  key: MutationKey,
  options?: PostHookOptions<
    RoleEntity,
    SerializeException,
    { payload: CreateRoleDto }
  >,
) => {
  return usePost<RoleEntity, SerializeException, { payload: CreateRoleDto }>(
    key,
    '/api/v1/role',
    options,
  );
};
