import {
  PatchHookOptions,
  RemoveHookOptions,
  usePatch,
  useRemove,
} from 'src/shared/axios';
import { MutationKey } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { RoleEntity } from 'src/server/entities/role.entity';

const useDeprotectRole = (
  key: MutationKey,
  options?: RemoveHookOptions<
    RoleEntity,
    SerializeException,
    { id: RoleEntity['id'] }
  >,
) => {
  return useRemove<RoleEntity, SerializeException, { id: RoleEntity['id'] }>(
    key,
    '/api/v1/role/deprotected',
    options,
  );
};

const useProtectRole = (
  key: MutationKey,
  options?: PatchHookOptions<
    RoleEntity,
    SerializeException,
    { params: { id: RoleEntity['id'] } }
  >,
) => {
  return usePatch<
    RoleEntity,
    SerializeException,
    { params: { id: RoleEntity['id'] } }
  >(key, '/api/v1/role/protected', options);
};

export { useDeprotectRole, useProtectRole };
