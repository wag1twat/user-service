import { RoleQueries } from 'src/server/controllers/role/queries/role.queries';
import { Role } from 'src/server/entities/client.types';
import { GetHookOptions, QueryKey, useGet } from 'src/shared/axios';

export const useRole = (
  key: QueryKey,
  options?: GetHookOptions<{ data: Role }, RoleQueries>,
) => {
  return useGet(key, '/api/v1/role', options);
};
