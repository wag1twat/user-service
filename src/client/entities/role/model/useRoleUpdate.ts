import { MutationKey } from 'react-query';
import {
  PatchHookOptions,
  RemoveHookOptions,
  usePatch,
  useRemove,
} from 'src/shared/axios';
import {
  RoleAddPermissionsDto,
  RoleAddUsersDto,
  UpdateRoleDto,
} from 'src/server/controllers/role/dto/update-role.dto';
import { RoleRemoveUsersQueries } from 'src/server/controllers/role/queries/role-remove-users.queries';
import { SerializeException } from 'src/shared/helpers';
import { RoleEntity } from 'src/server/entities/role.entity';
import { RoleRemovePermissionsQueries } from 'src/server/controllers/role/queries/role-remove-permissions.queries';

export const useRoleUpdate = (
  key: MutationKey,
  options?: PatchHookOptions<
    unknown,
    SerializeException,
    { payload: Partial<UpdateRoleDto>; params: { id: RoleEntity['id'] } }
  >,
) => {
  return usePatch(key, '/api/v1/role', options);
};

export const useRoleAddUsers = (
  key: MutationKey,
  options?: PatchHookOptions<
    unknown,
    SerializeException,
    { payload: Partial<RoleAddUsersDto>; params: { id: RoleEntity['id'] } }
  >,
) => {
  return usePatch(key, '/api/v1/role/users', options);
};

export const useRoleAddPermissions = (
  key: MutationKey,
  options?: PatchHookOptions<
    unknown,
    SerializeException,
    {
      payload: Partial<RoleAddPermissionsDto>;
      params: { id: RoleEntity['id'] };
    }
  >,
) => {
  return usePatch(key, '/api/v1/role/permissions', options);
};

export const useRoleRemoveUsers = (
  key: MutationKey,
  options?: RemoveHookOptions<
    unknown,
    SerializeException,
    RoleRemoveUsersQueries
  >,
) => {
  return useRemove(key, '/api/v1/role/users', options);
};

export const useRoleRemovePermissions = (
  key: MutationKey,
  options?: RemoveHookOptions<
    unknown,
    SerializeException,
    RoleRemovePermissionsQueries
  >,
) => {
  return useRemove(key, '/api/v1/role/permissions', options);
};
