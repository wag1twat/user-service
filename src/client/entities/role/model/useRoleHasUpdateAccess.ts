import { HasAccess } from 'src/server/entities/client.types';
import { GetHookOptions, QueryKey, useGet } from 'src/shared/axios';

export const useRoleHasUpdateAccess = (
  key: QueryKey,
  options?: GetHookOptions<HasAccess, { id?: string }>,
) => {
  return useGet<HasAccess, { id?: string }>(
    key,
    '/api/v1/role/has-update-access',
    options,
  );
};
