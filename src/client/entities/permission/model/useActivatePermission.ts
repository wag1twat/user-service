import {
  PatchHookOptions,
  RemoveHookOptions,
  usePatch,
  useRemove,
} from 'src/shared/axios';
import { MutationKey } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { PermissionEntity } from 'src/server/entities/permission.entity';

const useDeactivatePermission = (
  key: MutationKey,
  options?: RemoveHookOptions<
    PermissionEntity,
    SerializeException,
    { id: PermissionEntity['id'] }
  >,
) => {
  return useRemove<
    PermissionEntity,
    SerializeException,
    { id: PermissionEntity['id'] }
  >(key, '/api/v1/permission/deactivate', options);
};

const useActivatePermission = (
  key: MutationKey,
  options?: PatchHookOptions<
    PermissionEntity,
    SerializeException,
    { params: { id: PermissionEntity['id'] } }
  >,
) => {
  return usePatch<
    PermissionEntity,
    SerializeException,
    { params: { id: PermissionEntity['id'] } }
  >(key, '/api/v1/permission/activate', options);
};

export { useDeactivatePermission, useActivatePermission };
