import { GetHookOptions, useGet, QueryKey } from 'src/shared/axios';
import { PermissionQueries } from 'src/server/controllers/permission/queries/permission.queries';
import { Permission } from 'src/server/entities/client.types';

const usePermission = (
  key: QueryKey,
  options?: GetHookOptions<{ data: Permission }, PermissionQueries>,
) => {
  return useGet<{ data: Permission }, PermissionQueries>(
    key,
    '/api/v1/permission',
    options,
  );
};

export { usePermission };
