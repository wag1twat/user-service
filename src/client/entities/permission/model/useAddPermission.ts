import { MutationKey } from 'react-query';
import { CreatePermissionDto } from 'src/server/controllers/permission/dto/create-permission.dto';
import { Permission } from 'src/server/entities/client.types';
import { PostHookOptions, usePost } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';

export const useAddPermission = (
  key: MutationKey,
  options?: PostHookOptions<
    {
      data: Permission;
    },
    SerializeException,
    { payload: CreatePermissionDto }
  >,
) => {
  return usePost<
    {
      data: Permission;
    },
    SerializeException,
    { payload: CreatePermissionDto }
  >(key, `/api/v1/permission`, options);
};
