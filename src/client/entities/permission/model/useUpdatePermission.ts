import { MutationKey } from 'react-query';
import { UpdatePermissionDto } from 'src/server/controllers/permission/dto/update-permission.dto';
import { Permission } from 'src/server/entities/client.types';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { PatchHookOptions, usePatch } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';

export const useUpdatePermission = (
  key: MutationKey,
  options?: PatchHookOptions<
    {
      data: Permission;
    },
    SerializeException,
    { payload: UpdatePermissionDto; params: { id: PermissionEntity['id'] } }
  >,
) => {
  return usePatch<
    {
      data: Permission;
    },
    SerializeException,
    { payload: UpdatePermissionDto; params: { id: PermissionEntity['id'] } }
  >(key, `/api/v1/permission`, options);
};
