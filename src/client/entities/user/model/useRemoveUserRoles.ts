import { MutationKey } from 'react-query';
import { UserRemoveRolesQueries } from 'src/server/controllers/user/queries/user-remove-roles.queries';
import { User } from 'src/server/entities/client.types';
import { RemoveHookOptions, useRemove } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';

export const useRemoveUserRoles = (
  key: MutationKey,
  options: RemoveHookOptions<User, SerializeException, UserRemoveRolesQueries>,
) => {
  return useRemove<User, SerializeException, UserRemoveRolesQueries>(
    key,
    '/api/v1/user/roles',
    options,
  );
};
