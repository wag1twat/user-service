import { PatchHookOptions, usePatch } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';
import { MutationKey } from 'react-query';
import { UpdateUserDto } from 'src/server/controllers/user/dto/update-user.dto';
import { UserEntity } from 'src/server/entities/user.entity';

const useUpdateUser = (
  key: MutationKey,
  options?: PatchHookOptions<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserDto; params: { id: UserEntity['id'] } }
  >,
) => {
  return usePatch<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserDto; params: { id: UserEntity['id'] } }
  >(key, '/api/v1/user', options);
};

export { useUpdateUser };
