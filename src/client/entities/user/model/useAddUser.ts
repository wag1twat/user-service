import { SerializeException } from 'src/shared/helpers';
import { PostHookOptions, usePost } from 'src/shared/axios';
import { CreateUserDto } from 'src/server/controllers/user/dto/create-user.dto';
import { User } from 'src/server/entities/client.types';

const useAddUser = (
  options?: PostHookOptions<
    User,
    SerializeException,
    { payload: CreateUserDto }
  >,
) => {
  return usePost<User, SerializeException, { payload: CreateUserDto }>(
    ['add-user'],
    '/api/v1/user/create',
    options,
  );
};

export { useAddUser };
