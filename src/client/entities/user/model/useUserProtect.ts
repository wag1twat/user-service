import {
  PatchHookOptions,
  RemoveHookOptions,
  usePatch,
  useRemove,
} from 'src/shared/axios';
import { MutationKey } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { User } from 'src/server/entities/client.types';
import { UserEntity } from 'src/server/entities/user.entity';

const useDeprotectUser = (
  key: MutationKey,
  options?: RemoveHookOptions<
    User,
    SerializeException,
    { id: UserEntity['id'] }
  >,
) => {
  return useRemove<User, SerializeException, { id: UserEntity['id'] }>(
    key,
    '/api/v1/user/deprotected',
    options,
  );
};

const useProtectUser = (
  key: MutationKey,
  options?: PatchHookOptions<
    User,
    SerializeException,
    { params: { id: UserEntity['id'] } }
  >,
) => {
  return usePatch<
    User,
    SerializeException,
    { params: { id: UserEntity['id'] } }
  >(key, '/api/v1/user/protected', options);
};

export { useDeprotectUser, useProtectUser };
