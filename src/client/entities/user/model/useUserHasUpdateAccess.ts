import { HasAccess } from 'src/server/entities/client.types';
import { GetHookOptions, QueryKey, useGet } from 'src/shared/axios';

export const useUserHasUpdateAccess = (
  key: QueryKey,
  options?: GetHookOptions<HasAccess, { id?: string }>,
) => {
  return useGet<HasAccess, { id?: string }>(
    key,
    '/api/v1/user/has-update-access',
    options,
  );
};
