import { UserQueries } from 'src/server/controllers/user/queries/user.queries';
import { User } from 'src/server/entities/client.types';
import { GetHookOptions, QueryKey, useGet } from 'src/shared/axios';

export const useUser = (
  key: QueryKey,
  options?: GetHookOptions<{ data: User }, UserQueries>,
) => {
  return useGet<{ data: User }, UserQueries>(key, '/api/v1/user', options);
};
