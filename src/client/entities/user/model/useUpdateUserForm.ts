import { useFormik } from 'formik';
import { UpdateUserDto } from 'src/server/controllers/user/dto/update-user.dto';
import { UserEntityConfig } from 'src/server/entities/user.entity';
import * as yup from 'yup';

export const UpdateUserSchema: yup.SchemaOf<UpdateUserDto> = yup.object({
  login: yup
    .string()
    .min(UserEntityConfig.login.minLength)
    .max(UserEntityConfig.login.maxLength)
    .required(UserEntityConfig.login.requiredMessage),
  firstName: yup
    .string()
    .min(UserEntityConfig.firstName.minLength)
    .max(UserEntityConfig.firstName.maxLength)
    .required(UserEntityConfig.firstName.requiredMessage),
  lastName: yup
    .string()
    .min(UserEntityConfig.lastName.minLength)
    .max(UserEntityConfig.lastName.maxLength)
    .required(UserEntityConfig.lastName.requiredMessage),
  email: yup
    .string()
    .email(UserEntityConfig.email.notMatchMessage)
    .required(UserEntityConfig.email.requiredMessage),
  active: yup.boolean(),
  protected: yup.boolean(),
});

const useUpdateUserForm = (
  initialValues: UpdateUserDto,
  onSubmit: (values: UpdateUserDto) => void,
) => {
  return useFormik<UpdateUserDto>({
    enableReinitialize: true,
    validationSchema: UpdateUserSchema,
    initialValues,
    onSubmit,
  });
};

export { useUpdateUserForm };
