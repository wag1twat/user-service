import { MutationKey } from 'react-query';
import { UpdateUserRolesDto } from 'src/server/controllers/user/dto/update-user-roles.dto';
import { UserEntity } from 'src/server/entities/user.entity';
import { PatchHookOptions, usePatch } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';

export const useUpdateUserRoles = (
  key: MutationKey,
  options: PatchHookOptions<
    unknown,
    SerializeException,
    { payload: UpdateUserRolesDto; params: { id: UserEntity['id'] } }
  >,
) => {
  return usePatch<
    unknown,
    SerializeException,
    { payload: UpdateUserRolesDto; params: { id: UserEntity['id'] } }
  >(key, '/api/v1/user/roles', options);
};
