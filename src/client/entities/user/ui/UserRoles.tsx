import React from 'react';
import { Stack } from '@chakra-ui/react';
import { Role, User } from 'src/server/entities/client.types';
import { CellProps } from 'react-table';
import {
  UserRemoveRoleButton,
  EmptyData,
  InlineLoader,
  RolesTable,
} from 'src/client/features';
import { QueryStatus } from 'react-query';
import { UpdateUserRolesModal } from 'src/client/widgets';
import { useUserProvider } from '../providers';

interface UserRolesProps {
  id: User['id'] | undefined;
  roles: Role[] | undefined;
  isLoading: boolean;
  status: QueryStatus;
  error: string | undefined;
  refetch: () => void;
}

const UserRoles: React.FC<UserRolesProps> = React.memo(
  ({ id, isLoading, status, error, roles = [], refetch }) => {
    const { accessibilities } = useUserProvider();

    return (
      <InlineLoader isLoading={isLoading} error={error} retry={refetch}>
        <Stack as="section" spacing={4} width="100%">
          <UpdateUserRolesModal
            id={id}
            refetch={refetch}
            accessibilities={accessibilities}
          />
          <RolesTable
            data={roles}
            headProps={{ top: 0 }}
            containerProps={{
              maxHeight: '500px',
              overflow: 'scroll',
              display: 'block',
            }}
            extraColumns={[
              {
                id: 'unset-role',
                Cell: (cell: CellProps<Role>) => (
                  <UserRemoveRoleButton
                    roleId={cell.row.original.id}
                    id={id}
                    refetch={refetch}
                    accessibilities={accessibilities}
                  />
                ),
                disableSortBy: true,
              },
            ]}
          />
          <EmptyData
            isEmpty={status === 'success' && roles.length === 0}
            message="User doesn't have roles"
          />
        </Stack>
      </InlineLoader>
    );
  },
);

export { UserRoles };
