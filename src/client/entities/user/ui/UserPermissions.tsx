import { Stack } from '@chakra-ui/react';
import React from 'react';
import { Permission } from 'src/server/entities/client.types';
import { QueryStatus } from 'react-query';
import { InlineLoader, EmptyData, PermissionsTable } from 'src/client/features';

type UserCardPermissionsProps = {
  isLoading: boolean;
  error: string | undefined;
  status: QueryStatus;
  permissions: Permission[] | undefined;
  refetch: () => void;
};

const UserCardPermissions: React.FC<UserCardPermissionsProps> = React.memo(
  ({ isLoading, error, status, permissions = [], refetch }) => {
    return (
      <InlineLoader isLoading={isLoading} error={error} retry={refetch}>
        <Stack spacing={4} as="section" width="100%">
          <PermissionsTable
            data={permissions}
            headProps={{ top: 0 }}
            containerProps={{
              maxHeight: '500px',
              overflow: 'scroll',
              display: 'block',
            }}
          />
          <EmptyData
            isEmpty={status === 'success' && permissions.length === 0}
            message="User doesn't have permissions"
          />
        </Stack>
      </InlineLoader>
    );
  },
);

export { UserCardPermissions };
