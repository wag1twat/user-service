import React from 'react';
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  Box,
  AccordionIcon,
  AccordionPanel,
  Flex,
  Stack,
} from '@chakra-ui/react';
import { UserMainInformation } from './UserMainInformation';
import { UserRoles } from './UserRoles';
import { UserCardPermissions } from './UserPermissions';
import { ParseSerializeException } from 'src/shared/helpers';
import { WarningText } from 'src/client/shared';
import { useUserProvider } from '../providers';

type UserAccordionProps = {};

const UserAccordion: React.FC<UserAccordionProps> = React.memo(() => {
  const { user, isUserProfile, error, isLoading, refetch, roles, permissions } =
    useUserProvider();

  return (
    <Flex as="section" width="100%">
      <Accordion width="100%" defaultIndex={[0, 1, 2]} allowMultiple>
        <AccordionItem width="100%">
          <h2>
            <AccordionButton>
              <Box flex="1" textAlign="left">
                Main information
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} width="100%">
            <Stack spacing={4}>
              {isUserProfile && (
                <WarningText>
                  Attention! This user belongs to you. Change its capabilities
                  more carefully.
                </WarningText>
              )}
              <UserMainInformation
                user={user?.data}
                isLoading={isLoading}
                error={ParseSerializeException.getString(error)}
                refetch={refetch}
              />
            </Stack>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem width="100%">
          <h2>
            <AccordionButton>
              <Box flex="1" textAlign="left">
                Roles
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} width="100%">
            <UserRoles
              id={user?.data?.id}
              isLoading={roles.isLoading}
              error={ParseSerializeException.getString(roles.error)}
              status={roles.status}
              roles={roles.data?.data}
              refetch={() => {
                roles.refetch();
                permissions.refetch();
              }}
            />
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem width="100%">
          <h2>
            <AccordionButton>
              <Box flex="1" textAlign="left">
                Permissions
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} width="100%">
            <UserCardPermissions
              error={ParseSerializeException.getString(permissions.error)}
              status={permissions.status}
              permissions={permissions.data?.data}
              isLoading={permissions.isLoading}
              refetch={() => {
                roles.refetch();
                permissions.refetch();
              }}
            />
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </Flex>
  );
});

export { UserAccordion };
