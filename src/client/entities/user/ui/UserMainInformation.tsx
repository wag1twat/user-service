import { Flex, Stack } from '@chakra-ui/react';
import React from 'react';
import {
  InlineLoader,
  UpdateUserForm,
  ToggleProtectedUserButton,
  ToggleActiveUserButton,
} from 'src/client/features';
import { useUIToasts } from 'src/client/shared';
import { User } from 'src/server/entities/client.types';
import { ParseSerializeException } from 'src/shared/helpers';
import { useUpdateUser, useUpdateUserForm } from '../model';
import { useUserProvider } from '../providers';

type UserMainInformationProps = {
  user: User | undefined;
  isLoading: boolean;
  error: string | undefined;
  refetch: () => void;
};

const UserMainInformation: React.FC<UserMainInformationProps> = React.memo(
  ({ user, isLoading, error, refetch }) => {
    const { accessibilities } = useUserProvider();

    const toasts = useUIToasts();

    const update = useUpdateUser(['update-user', user?.id], {
      onSuccess: () => {
        toasts.success('Update user successfully');
        refetch();
      },
      onError: (err) => toasts.error(ParseSerializeException.getString(err)),
    });

    const form = useUpdateUserForm(
      {
        login: user?.login ?? '',
        firstName: user?.firstName ?? '',
        lastName: user?.lastName ?? '',
        email: user?.email ?? '',
      },
      (values) => {
        if (user?.id) {
          update.patch({
            payload: values,
            params: { id: user.id },
          });
        }
      },
    );
    return (
      <InlineLoader isLoading={isLoading} error={error} retry={refetch}>
        <Stack as="section" width="100%" spacing={4}>
          <Flex my={2}>
            <UpdateUserForm
              preventDisabledFields={['login']}
              fieldsHelperTexts={{
                login: 'Login cannot be change',
              }}
              id={user?.id}
              gridTemplateColumns={[
                'repeat(1, 1fr)',
                'repeat(1, 1fr)',
                'repeat(2, 1fr)',
                'repeat(4, 1fr)',
              ]}
              header={undefined}
              isLoading={update.isLoading}
              values={form.values}
              errors={form.errors}
              handleChange={form.handleChange}
              handleSubmit={form.handleSubmit}
              availables={['AVAILABLE_UPDATE_USER']}
              accessibilities={accessibilities}
            />
          </Flex>
          <Stack direction="row" alignItems="center" justifyContent="flex-end">
            <ToggleProtectedUserButton
              id={user?.id}
              isProtected={Boolean(user?.protected)}
              isLoading={isLoading}
              refetch={refetch}
              accessibilities={accessibilities}
            />
            <ToggleActiveUserButton
              id={user?.id}
              isActive={Boolean(user?.active)}
              isLoading={isLoading}
              refetch={refetch}
              accessibilities={accessibilities}
            />
          </Stack>
        </Stack>
      </InlineLoader>
    );
  },
);

export { UserMainInformation };
