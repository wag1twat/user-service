import React from 'react';
import { UserEntity } from 'src/server/entities/user.entity';
import { UserAccordion } from './UserAccordion';
import { UserProvider } from '../providers';

type UserProps = { id: UserEntity['id'] };

const User: React.FC<UserProps> = ({ id }) => {
  return (
    <UserProvider id={id}>
      <UserAccordion />
    </UserProvider>
  );
};

export { User };
