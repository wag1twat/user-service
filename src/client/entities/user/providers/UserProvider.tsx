import React from 'react';
import { QueryStatus } from 'react-query';
import { GlobalLoader } from 'src/client/features';
import { useAuth } from 'src/client/providers';
import { Accessibilities } from 'src/client/shared';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { RoleEntity } from 'src/server/entities/role.entity';
import { UserEntity } from 'src/server/entities/user.entity';
import {
  ParseSerializeException,
  SerializeException,
} from 'src/shared/helpers';
import { usePermissions } from '../../permissions/model';
import { useRoles } from '../../roles/model';
import { useUser, useUserHasUpdateAccess } from '../model';

interface UserProviderContextType extends Accessibilities {
  isUserProfile: boolean;
  user:
    | {
        data: UserEntity;
      }
    | undefined;
  error: SerializeException | undefined;
  isLoading: boolean;
  refetch: () => void;
  roles: {
    data:
      | {
          data: RoleEntity[];
          count: number;
        }
      | undefined;
    error: SerializeException | undefined;
    isLoading: boolean;
    status: QueryStatus;
    refetch: () => void;
  };
  permissions: {
    data:
      | {
          data: PermissionEntity[];
          count: number;
        }
      | undefined;
    error: SerializeException | undefined;
    isLoading: boolean;
    status: QueryStatus;
    refetch: () => void;
  };
}

const UserProviderContext = React.createContext<UserProviderContextType>({
  isUserProfile: false,
  accessibilities: [{ isLoading: false, isDisabled: true, message: '' }],
  user: undefined,
  error: undefined,
  isLoading: false,
  refetch: () => void 0,
  roles: {
    data: undefined,
    error: undefined,
    isLoading: false,
    status: 'idle',
    refetch: () => void 0,
  },
  permissions: {
    data: undefined,
    error: undefined,
    isLoading: false,
    status: 'idle',
    refetch: () => void 0,
  },
});

const useUserProvider = () => {
  return React.useContext(UserProviderContext);
};

interface UserProviderProps {
  id: UserEntity['id'];
}

const UserProvider: React.FC<UserProviderProps> = ({ id, children }) => {
  const { me } = useAuth();

  const {
    data: user,
    error,
    isLoading,
    refetch,
  } = useUser(['user-page', id, me?.data], {
    params: {
      id,
    },
    enabled: Boolean(id && me?.data),
  });

  const roles = useRoles(['user-roles', user?.data?.id], {
    params: { usersIds: user?.data?.id ? [user.data.id] : undefined },
    enabled: Boolean(user?.data?.id),
  });

  const permissions = usePermissions(['user-permissions', user?.data?.id], {
    params: { usersIds: user?.data?.id ? [user.data.id] : undefined },
    enabled: Boolean(user?.data?.id),
  });

  const access = useUserHasUpdateAccess(['user-has-update-access', id], {
    params: { id },
    enabled: Boolean(id),
  });

  const isUserProfile = React.useMemo(
    () => Boolean(me?.data?.id && me.data.id === id),
    [id, me?.data?.id],
  );

  return (
    <UserProviderContext.Provider
      value={{
        isUserProfile,
        accessibilities: [
          {
            isLoading: access.isLoading,
            isDisabled: Boolean(!access.data?.isHasAccess),
            message: ParseSerializeException.getString(access.error),
          },
        ],
        user,
        isLoading,
        error,
        refetch: () => refetch(),
        roles: {
          data: roles.data,
          isLoading: roles.isLoading,
          error: roles.error,
          status: roles.status,
          refetch: () => roles.refetch(),
        },
        permissions: {
          data: permissions.data,
          isLoading: permissions.isLoading,
          error: permissions.error,
          status: permissions.status,
          refetch: () => permissions.refetch(),
        },
      }}
    >
      <GlobalLoader
        isLoading={Boolean(me?.isLoading)}
        error={ParseSerializeException.getString(me?.error)}
        retry={me?.refetch}
      >
        {children}
      </GlobalLoader>
    </UserProviderContext.Provider>
  );
};

export { UserProvider, useUserProvider };
