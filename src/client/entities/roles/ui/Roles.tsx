import React from 'react';
import {
  RolesTable,
  GlobalLoader,
  EmptyData,
  Pagination,
  ToggleProtectedRoleButton,
  ToggleActiveRoleButton,
  RolesFiltersAccordion,
} from 'src/client/features';
import { useAuth } from 'src/client/providers';
import { ButtonGroup, Stack } from '@chakra-ui/react';
import { useRoles } from '../model';
import {
  ParseSerializeException,
  SerializeException,
} from 'src/shared/helpers';
import {
  useDebounce,
  validateDateRange,
  useStateDisclosure,
  useLocalStorageManager,
} from 'src/client/shared';
import { RolesQueries } from 'src/server/controllers/roles/queries/roles.queries';
import { CellProps } from 'react-table';
import { Role } from 'src/server/entities/client.types';
import { UpdateRoleModalButton } from 'src/client/widgets/UpdateRoleModal/ui';
import { UpdateRoleModalProvider } from 'src/client/widgets/UpdateRoleModal/providers';
import { AddRoleModal, UpdateRoleModal } from 'src/client/widgets';
import { useQueries } from 'react-query';
import { find, map } from 'lodash';
import { RoleEntity } from 'src/server/entities/role.entity';
import { AxiosError } from 'axios';
import { useRoleHasUpdateAccess } from '../../role/model';

type RolesProps = {};

const Roles: React.FC<RolesProps> = React.memo(() => {
  const { me } = useAuth();

  const {
    state: roleIdForUpdateRoleModal,
    isOpen,
    onOpenWithState,
    onCloseWithReset,
  } = useStateDisclosure<string>();

  const { filter, table } = useLocalStorageManager('roles');

  const [filters, setFilters] = React.useState<RolesQueries>({
    page: 1,
    limit: table.settings.limit,
    sortBy: 'name',
    order: 'DESC',
  });

  const debouncedFilters = useDebounce(filters, 200);

  const {
    isLoading,
    error,
    status,
    data = { data: [], count: 0 },
    refetch,
  } = useRoles(['roles-page', Object.values(debouncedFilters), me?.data], {
    params: {
      ...debouncedFilters,
      createdAt: validateDateRange(debouncedFilters.createdAt),
      updatedAt: validateDateRange(debouncedFilters.updatedAt),
    },
    enabled: Boolean(me?.data),
  });

  const { native } = useRoleHasUpdateAccess(['role-has-update-access'], {
    enabled: false,
  });

  const results = useQueries(
    map(data?.data, (role) => {
      return {
        initialData: {
          isHasAccess: false,
          message: '',
          id: role.id,
        },
        enabled: Boolean(data?.data.length),
        queryKey: [...Object.values(role), me?.data],
        queryFn: () =>
          native({ params: { id: role.id } })
            .then((response) => ({
              isHasAccess: response.data.isHasAccess,
              message: '',
              id: role.id,
            }))
            .catch((error: AxiosError<SerializeException>) => ({
              isHasAccess: false,
              message: ParseSerializeException.getString(error.response?.data),
              id: role.id,
            })),
      };
    }),
  );

  const findHasAccess = React.useCallback(
    (id: RoleEntity['id']) => {
      return find(results, (result) => result.data?.id === id);
    },
    [results],
  );

  return (
    <GlobalLoader
      isLoading={Boolean(me?.isLoading)}
      error={ParseSerializeException.getString(me?.error)}
      retry={me?.refetch}
    >
      <Stack width="100%" spacing={4}>
        <AddRoleModal refetch={refetch} />
        <RolesFiltersAccordion
          defaultIndex={filter.settings.defaultIndex}
          setDefaultIndex={(defaultIndex) =>
            filter.setSettings({ defaultIndex })
          }
          filters={filters}
          setFilters={setFilters}
        />
        {filters.page && filters.limit && (
          <Pagination
            totalCount={data.count}
            currentPage={filters.page}
            setCurrentPage={(page) => setFilters((prev) => ({ ...prev, page }))}
            limit={filters.limit}
            setLimit={(limit) => {
              table.setSettings({ limit });
              setFilters((prev) => ({ ...prev, limit }));
            }}
          />
        )}
        <GlobalLoader
          isLoading={isLoading}
          error={ParseSerializeException.getString(error)}
          retry={refetch}
          containerProps={{ flex: 1 }}
        >
          <EmptyData
            isEmpty={
              status === 'success' &&
              Boolean(data?.data && data.data.length === 0)
            }
            message="Could not find roles"
          >
            <RolesTable
              data={data?.data}
              sortBy={filters.sortBy}
              order={filters.order}
              sortByServer={({ sortBy, order }) => {
                setFilters((prev) => ({
                  ...prev,
                  sortBy,
                  order,
                }));
              }}
              extraColumns={[
                {
                  id: 'update-role',
                  Cell: (cell: CellProps<Role>) => {
                    const hasAccess = findHasAccess(cell.row.original.id);

                    return (
                      <ButtonGroup>
                        <UpdateRoleModalButton
                          onOpen={() => onOpenWithState(cell.row.original.id)}
                          accessibilities={[
                            {
                              isLoading: Boolean(hasAccess?.isLoading),
                              isDisabled: Boolean(
                                !hasAccess?.data?.isHasAccess,
                              ),
                              message: hasAccess?.data?.message || '',
                            },
                          ]}
                        />
                        <ToggleActiveRoleButton
                          id={cell.row.original.id}
                          isActive={cell.row.original.active}
                          isLoading={isLoading}
                          refetch={refetch}
                          accessibilities={[
                            {
                              isLoading: Boolean(hasAccess?.isLoading),
                              isDisabled: Boolean(
                                !hasAccess?.data?.isHasAccess,
                              ),
                              message: hasAccess?.data?.message || '',
                            },
                          ]}
                        />
                        <ToggleProtectedRoleButton
                          id={cell.row.original.id}
                          isProtected={cell.row.original.protected}
                          isLoading={isLoading}
                          refetch={refetch}
                          accessibilities={[
                            {
                              isLoading: Boolean(hasAccess?.isLoading),
                              isDisabled: Boolean(
                                !hasAccess?.data?.isHasAccess,
                              ),
                              message: hasAccess?.data?.message || '',
                            },
                          ]}
                        />
                      </ButtonGroup>
                    );
                  },
                  disableSortBy: true,
                },
              ]}
            />
          </EmptyData>
          <UpdateRoleModalProvider
            id={roleIdForUpdateRoleModal}
            isOpen={isOpen}
            refetch={refetch}
          >
            <UpdateRoleModal isOpen={isOpen} onClose={onCloseWithReset} />
          </UpdateRoleModalProvider>
        </GlobalLoader>
      </Stack>
    </GlobalLoader>
  );
});

export { Roles };
