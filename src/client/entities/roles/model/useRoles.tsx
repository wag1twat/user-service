import { GetHookOptions, useGet, QueryKey } from 'src/shared/axios';
import { Role } from 'src/server/entities/client.types';
import { RolesQueries } from 'src/server/controllers/roles/queries/roles.queries';

const useRoles = (
  key: QueryKey,
  options?: GetHookOptions<{ data: Role[]; count: number }, RolesQueries>,
) => {
  return useGet<{ data: Role[]; count: number }, RolesQueries>(
    key,
    '/api/v1/roles',
    options,
  );
};

export { useRoles };
