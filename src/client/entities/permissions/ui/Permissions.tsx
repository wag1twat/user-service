import { ButtonGroup, Stack } from '@chakra-ui/react';
import { AxiosError } from 'axios';
import { find, map } from 'lodash';
import React from 'react';
import { useQueries } from 'react-query';
import { CellProps } from 'react-table';
import {
  EmptyData,
  GlobalLoader,
  Pagination,
  PermissionsFiltersAccordion,
  PermissionsTable,
  ToggleActivePermissionButton,
  ToggleProtectedPermissionButton,
} from 'src/client/features';
import { useAuth } from 'src/client/providers';
import {
  useDebounce,
  useLocalStorageManager,
  useStateDisclosure,
  validateDateRange,
} from 'src/client/shared';
import { AddPermissionModal, UpdatePermissionModal } from 'src/client/widgets';
import { UpdatePermissionModalButton } from 'src/client/widgets/UpdatePermissionModal/ui';
import { PermissionsQueries } from 'src/server/controllers/permissions/queries/permissions.queries';
import { Permission } from 'src/server/entities/client.types';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import {
  ParseSerializeException,
  SerializeException,
} from 'src/shared/helpers';
import { usePermissionHasUpdateAccess } from '../../permission/model';
import { usePermissions } from '../model';

type PermissionsProps = {};

const Permissions: React.FC<PermissionsProps> = () => {
  const { me } = useAuth();

  const {
    state: permissionId,
    isOpen,
    onOpenWithState,
    onCloseWithReset,
  } = useStateDisclosure<PermissionEntity['id']>();

  const { filter, table } = useLocalStorageManager('permissions');

  const [filters, setFilters] = React.useState<PermissionsQueries>({
    page: 1,
    limit: table.settings.limit,
    sortBy: 'name',
    order: 'DESC',
  });

  const debouncedFilters = useDebounce(filters, 200);

  const {
    isLoading,
    error,
    data = { data: [], count: 0 },
    status,
    refetch,
  } = usePermissions(
    ['permissions-page', ...Object.values(debouncedFilters), me?.data],
    {
      params: {
        ...debouncedFilters,
        createdAt: validateDateRange(debouncedFilters.createdAt),
        updatedAt: validateDateRange(debouncedFilters.updatedAt),
      },
      enabled: Boolean(me?.data),
      refetchInterval: 2000,
    },
  );

  const { native } = usePermissionHasUpdateAccess(
    ['permission-has--update-access'],
    {
      enabled: false,
    },
  );

  const results = useQueries(
    map(data?.data, (permission) => {
      return {
        initialData: {
          isHasAccess: false,
          message: '',
          id: permission.id,
        },
        enabled: Boolean(data?.data.length),
        queryKey: [...Object.values(permission), me?.data],
        queryFn: () =>
          native({ params: { id: permission.id } })
            .then((response) => ({
              ...response.data,
              message: '',
              id: permission.id,
            }))
            .catch((error: AxiosError<SerializeException>) => ({
              isHasAccess: false,
              message:
                ParseSerializeException.getString(error.response?.data) || '',
              id: permission.id,
            })),
      };
    }),
  );

  const findHasAccess = React.useCallback(
    (id: PermissionEntity['id']) => {
      return find(results, (result) => result.data?.id === id);
    },
    [results],
  );

  return (
    <GlobalLoader
      isLoading={Boolean(me?.isLoading)}
      error={ParseSerializeException.getString(me?.error)}
      retry={me?.refetch}
    >
      <Stack spacing={4} width="full">
        <AddPermissionModal refetch={refetch} />
        <PermissionsFiltersAccordion
          defaultIndex={filter.settings.defaultIndex}
          setDefaultIndex={(defaultIndex) =>
            filter.setSettings({ defaultIndex })
          }
          filters={filters}
          setFilters={setFilters}
        />
        {filters.page && filters.limit && (
          <Pagination
            totalCount={data.count}
            currentPage={filters.page}
            setCurrentPage={(page) => setFilters((prev) => ({ ...prev, page }))}
            limit={filters.limit}
            setLimit={(limit) => {
              table.setSettings({ limit });
              setFilters((prev) => ({ ...prev, limit }));
            }}
          />
        )}
        <GlobalLoader
          isLoading={isLoading}
          error={ParseSerializeException.getString(error)}
          retry={refetch}
          containerProps={{ flex: 1 }}
        >
          <EmptyData
            isEmpty={
              status === 'success' &&
              Boolean(data?.data && data.data.length === 0)
            }
            message="Could not find permissions"
          >
            <PermissionsTable
              data={data?.data}
              extraColumns={[
                {
                  id: 'permissions-table-actions',
                  Cell: (cell: CellProps<Permission>) => {
                    const access = findHasAccess(cell.row.original.id);

                    return (
                      <ButtonGroup>
                        <UpdatePermissionModalButton
                          onOpen={() => onOpenWithState(cell.row.original.id)}
                          accessibilities={[
                            {
                              isLoading: Boolean(access?.isLoading),
                              isDisabled: Boolean(!access?.data?.isHasAccess),
                              message: access?.data?.message || '',
                            },
                          ]}
                        />
                        <ToggleProtectedPermissionButton
                          id={cell.row.original.id}
                          isLoading={isLoading}
                          refetch={refetch}
                          isProtected={cell.row.original.protected}
                          accessibilities={[
                            {
                              isLoading: Boolean(access?.isLoading),
                              isDisabled: Boolean(!access?.data?.isHasAccess),
                              message: access?.data?.message || '',
                            },
                          ]}
                        />
                        <ToggleActivePermissionButton
                          id={cell.row.original.id}
                          isLoading={isLoading}
                          refetch={refetch}
                          isActive={cell.row.original.active}
                          accessibilities={[
                            {
                              isLoading: Boolean(access?.isLoading),
                              isDisabled: Boolean(!access?.data?.isHasAccess),
                              message: access?.data?.message || '',
                            },
                          ]}
                        />
                      </ButtonGroup>
                    );
                  },
                  disableSortBy: true,
                },
              ]}
            />
          </EmptyData>
          <UpdatePermissionModal
            permissionId={permissionId}
            isOpen={isOpen}
            onClose={onCloseWithReset}
            refetch={refetch}
          />
        </GlobalLoader>
      </Stack>
    </GlobalLoader>
  );
};

export { Permissions };
