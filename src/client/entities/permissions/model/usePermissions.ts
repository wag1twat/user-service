import { Permission } from 'src/server/entities/client.types';
import { GetHookOptions, useGet, QueryKey } from 'src/shared/axios';
import { PermissionsQueries } from 'src/server/controllers/permissions/queries/permissions.queries';

const usePermissions = (
  key: QueryKey,
  options?: GetHookOptions<
    { data: Permission[]; count: number },
    PermissionsQueries
  >,
) => {
  return useGet<{ data: Permission[]; count: number }, PermissionsQueries>(
    key,
    '/api/v1/permissions',
    options,
  );
};

export { usePermissions };
