import { GetHookOptions, useGet, QueryKey } from 'src/shared/axios';
import { UsersQueries } from 'src/server/controllers/users/queries/users-queries';
import { User } from 'src/server/entities/client.types';

const useUsers = (
  key: QueryKey,
  options?: GetHookOptions<{ data: User[]; count: number }, UsersQueries>,
) => {
  return useGet<{ data: User[]; count: number }, UsersQueries>(
    key,
    '/api/v1/users',
    options,
  );
};

export { useUsers };
