import { Stack } from '@chakra-ui/react';
import React from 'react';
import { UsersQueries } from 'src/server/controllers/users/queries/users-queries';
import {
  GlobalLoader,
  EmptyData,
  UsersTable,
  UsersFiltersAccordion,
  Pagination,
} from 'src/client/features';
import { useAuth } from 'src/client/providers';

import {
  validateDateRange,
  useDebounce,
  useLocalStorageManager,
} from 'src/client/shared';
import { useUsers } from '../model';
import { ParseSerializeException } from 'src/shared/helpers';
import { AddUserModal } from 'src/client/widgets';

type UsersProps = {};

const Users: React.FC<UsersProps> = () => {
  const { me } = useAuth();

  const { filter, table } = useLocalStorageManager('users');

  const [filters, setFilters] = React.useState<UsersQueries>({
    page: 1,
    limit: table.settings.limit,
    sortBy: 'login',
    order: 'DESC',
  });

  const debouncedFilters = useDebounce(filters, 200);

  const {
    isLoading,
    error,
    data = { data: [], count: 0 },
    refetch,
  } = useUsers(['users-page', Object.values(debouncedFilters), me?.data], {
    params: {
      ...debouncedFilters,
      createdAt: validateDateRange(debouncedFilters.createdAt),
      updatedAt: validateDateRange(debouncedFilters.updatedAt),
    },
    enabled: Boolean(me?.data),
  });

  return (
    <GlobalLoader
      isLoading={Boolean(me?.isLoading)}
      error={ParseSerializeException.getString(me?.error)}
      retry={me?.refetch}
    >
      <Stack spacing={4} width="100%">
        <AddUserModal refetch={refetch} />
        <UsersFiltersAccordion
          defaultIndex={filter.settings.defaultIndex}
          setDefaultIndex={(defaultIndex) =>
            filter.setSettings({ defaultIndex })
          }
          filters={filters}
          setFilters={(nextFilters) =>
            setFilters((prevFilters) => ({ ...prevFilters, ...nextFilters }))
          }
        />
        {filters.page && filters.limit && (
          <Pagination
            totalCount={data.count}
            currentPage={filters.page}
            setCurrentPage={(page) => setFilters((prev) => ({ ...prev, page }))}
            limit={filters.limit}
            setLimit={(limit) => {
              table.setSettings({ limit });
              setFilters((prev) => ({ ...prev, limit }));
            }}
          />
        )}
        <GlobalLoader
          isLoading={isLoading}
          error={ParseSerializeException.getString(error)}
          retry={refetch}
          containerProps={{ flex: 1 }}
        >
          <EmptyData
            isEmpty={
              !isLoading && Boolean(data?.data && data.data.length === 0)
            }
            message="Users not found"
          >
            <UsersTable
              data={data.data}
              sortBy={filters.sortBy}
              order={filters.order}
              sortByServer={({ sortBy, order }) => {
                setFilters((prev) => ({
                  ...prev,
                  sortBy,
                  order,
                }));
              }}
            />
          </EmptyData>
        </GlobalLoader>
      </Stack>
    </GlobalLoader>
  );
};

export { Users };
