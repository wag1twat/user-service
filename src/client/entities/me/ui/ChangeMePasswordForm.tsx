import {
  Heading,
  Stack,
  ButtonGroup,
  Button,
  Skeleton,
} from '@chakra-ui/react';
import React from 'react';
import {
  FloatingInputFormControlPassword,
  useUIToasts,
} from 'src/client/shared';
import { useAuth } from 'src/client/providers';
import { useChangeMePassword, useChangeMePasswordForm } from '../model';
import { ParseSerializeException } from 'src/shared/helpers';

const ChangeMePasswordForm = () => {
  const { me } = useAuth();

  const { success, error } = useUIToasts({ position: 'bottom-start' });

  const { patch, isLoading: isLoadingUpdate } = useChangeMePassword(
    ['change-me-password'],
    {
      onSuccess: () => {
        success('Your password was been changed');
        me?.refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    },
  );

  const form = useChangeMePasswordForm((values) => patch({ payload: values }));

  const isLoading = React.useMemo(
    () => isLoadingUpdate || me?.isLoading,
    [isLoadingUpdate, me?.isLoading],
  );

  return (
    <Stack spacing={8}>
      <Stack spacing={4}>
        <Heading size="sm">Change password</Heading>
        <Skeleton isLoaded={!me?.isLoading}>
          <FloatingInputFormControlPassword
            label="Previos password"
            controlProps={{
              id: 'previous-password',
              isRequired: true,
              isInvalid: Boolean(form.errors.previosPassword),
            }}
            inputProps={{
              'aria-label': 'Previos password',
              name: 'previosPassword',
              value: form.values.previosPassword ?? '',
              onChange: form.handleChange,
              isDisabled: isLoadingUpdate,
            }}
            errorMessage={form.errors.previosPassword}
          />
        </Skeleton>
        <Skeleton isLoaded={!me?.isLoading}>
          <FloatingInputFormControlPassword
            label="Next password"
            controlProps={{
              id: 'next-password',
              isRequired: true,
              isInvalid: Boolean(form.errors.nextPassword),
            }}
            inputProps={{
              'aria-label': 'Next password',
              name: 'nextPassword',
              value: form.values.nextPassword ?? '',
              onChange: form.handleChange,
              isDisabled: isLoadingUpdate,
            }}
            errorMessage={form.errors.nextPassword}
          />
        </Skeleton>
        <Skeleton isLoaded={!me?.isLoading}>
          <FloatingInputFormControlPassword
            label="Confirm next password"
            controlProps={{
              id: 'confirm-next-password',
              isRequired: true,
              isInvalid: Boolean(form.errors.confirmNextPassword),
            }}
            inputProps={{
              'aria-label': 'Confirm next password',
              name: 'confirmNextPassword',
              value: form.values.confirmNextPassword ?? '',
              onChange: form.handleChange,
              isDisabled: isLoadingUpdate,
            }}
            errorMessage={form.errors.confirmNextPassword}
          />
        </Skeleton>
        <ButtonGroup justifyContent="flex-end">
          <Button
            colorScheme="blue"
            onClick={() => form.handleSubmit()}
            isLoading={isLoading}
          >
            Update
          </Button>
        </ButtonGroup>
      </Stack>
    </Stack>
  );
};

export { ChangeMePasswordForm };
