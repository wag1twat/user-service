import React from 'react';
import { LoadingDataList } from 'src/client/features';
import { useAuth } from 'src/client/providers';
import { ParseSerializeException } from 'src/shared/helpers';

type MeRolesProps = {};

const MeRoles: React.FC<MeRolesProps> = () => {
  const { roles } = useAuth();

  return (
    <LoadingDataList
      header="My roles"
      isLoading={Boolean(roles?.isLoading)}
      error={ParseSerializeException.getString(roles?.error)}
      retry={roles?.refetch}
      data={roles?.data}
      labelBy="name"
      keyBy="id"
    />
  );
};

export { MeRoles };
