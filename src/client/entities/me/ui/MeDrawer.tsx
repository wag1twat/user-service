import {
  Stack,
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Flex,
} from '@chakra-ui/react';
import {
  TabWithoutFocusBorder,
  InSystemTimeBadge,
  IsActiveBadge,
  LastUpdatedBadge,
  PersonalAreaDrawer,
  useUIToasts,
} from 'src/client/shared';
import { UpdateProfileForm } from 'src/client/features';
import React from 'react';
import { useAuth, useGlobalDisclosures } from 'src/client/providers';
import { ChangeMePasswordForm } from './ChangeMePasswordForm';
import { MePermissions } from './MePermissions';
import { MeRoles } from './MeRoles';
import { ToggleActiveMe } from './ToggleActiveMe';
import { useUpdateMe } from '../model';
import { useUpdateUserForm } from 'src/client/entities/user/model';
import { ParseSerializeException } from 'src/shared/helpers';

const MeDrawer = () => {
  const { profile } = useGlobalDisclosures();

  const { me } = useAuth();

  const { success, error } = useUIToasts({ position: 'bottom-left' });

  const { patch, isLoading: isLoadingUpdate } = useUpdateMe(['me-update'], {
    onSuccess: () => {
      success('Your account was been updated');
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  const form = useUpdateUserForm(
    {
      login: me?.data?.login ?? '',
      firstName: me?.data?.firstName ?? '',
      lastName: me?.data?.lastName ?? '',
      email: me?.data?.email ?? '',
    },
    (values) => patch({ payload: values }),
  );

  const isLoading = React.useMemo(
    () => isLoadingUpdate || Boolean(me?.isLoading),
    [isLoadingUpdate, me?.isLoading],
  );

  return (
    <PersonalAreaDrawer
      header="Your profile"
      confirmText="Refresh"
      disclosure={profile}
      onConfirm={me?.refetch}
      isConfirmLoading={isLoading}
      size="md"
    >
      <Flex position="relative" width="100%">
        <Tabs isFitted width="inherit" defaultIndex={0}>
          <TabList
            mb="1em"
            position="sticky"
            top={0}
            width="inherit"
            zIndex={3}
            _dark={{
              background: 'gray.500',
            }}
            _light={{
              background: '#fff',
            }}
          >
            <TabWithoutFocusBorder>Information</TabWithoutFocusBorder>
            <TabWithoutFocusBorder>Security</TabWithoutFocusBorder>
          </TabList>
          <TabPanels>
            <TabPanel p={0}>
              <Stack spacing={8}>
                <Stack>
                  <InSystemTimeBadge
                    isLoaded={!isLoading}
                    createdAt={me?.data?.createdAt}
                  />
                  <LastUpdatedBadge
                    isLoaded={!isLoading}
                    updatedAt={me?.data?.updatedAt}
                  />
                  <IsActiveBadge
                    isLoaded={!isLoading}
                    isActive={Boolean(me?.data?.active)}
                  />
                </Stack>
                <UpdateProfileForm
                  header="Main information"
                  isLoading={isLoading}
                  values={form.values}
                  errors={form.errors}
                  handleChange={form.handleChange}
                  handleSubmit={form.handleSubmit}
                />
                <MeRoles />
                <MePermissions />
              </Stack>
            </TabPanel>
            <TabPanel p={0}>
              <Stack spacing={8}>
                <ChangeMePasswordForm />
                <ToggleActiveMe />
              </Stack>
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Flex>
    </PersonalAreaDrawer>
  );
};

export { MeDrawer };
