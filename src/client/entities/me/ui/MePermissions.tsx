import React from 'react';
import { LoadingDataList } from 'src/client/features';
import { useAuth } from 'src/client/providers';
import { ParseSerializeException } from 'src/shared/helpers';

type MePermissionsProps = {};

const MePermissions: React.FC<MePermissionsProps> = () => {
  const { permissions } = useAuth();

  return (
    <LoadingDataList
      header="My permissions"
      isLoading={Boolean(permissions?.isLoading)}
      error={ParseSerializeException.getString(permissions?.error)}
      retry={permissions?.refetch}
      data={permissions?.data}
      labelBy="name"
      keyBy="id"
    />
  );
};

export { MePermissions };
