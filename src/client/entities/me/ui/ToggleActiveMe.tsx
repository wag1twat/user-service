import React from 'react';
import { useAuth } from 'src/client/providers';
import {
  ActivateIcon,
  DeactivateIcon,
  ToggleActionButton,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import { useDeactivateMe, useActivateMe } from '../model';

type ToggleActiveMeProps = {};

const ToggleActiveMe: React.FC<ToggleActiveMeProps> = () => {
  const { me, roles, permissions } = useAuth();

  const { warning, success, error } = useUIToasts({ position: 'bottom-start' });

  const deactivate = useDeactivateMe(['me-deactivate'], {
    onSuccess: () => {
      warning('Your profile was been deactivate');
      me?.refetch();
      roles?.refetch();
      permissions?.refetch();
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  const activate = useActivateMe(['me-activate'], {
    onSuccess: () => {
      success('Your profile was been activate');
      me?.refetch();
      roles?.refetch();
      permissions?.refetch();
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  return (
    <ToggleActionButton
      ActionIcon={<ActivateIcon />}
      RevertIcon={<DeactivateIcon />}
      isActive={Boolean(me?.data?.active)}
      isLoading={Boolean(me?.isLoading)}
      isLoadingAction={deactivate.isLoading}
      isLoadingRevert={activate.isLoading}
      Revert={() => deactivate.remove()}
      Action={() => activate.patch({})}
      RevertName="Deactivate account"
      ActionName="Activate account"
      availables={[]}
      accessibilities={[]}
    />
  );
};

export { ToggleActiveMe };
