import { PatchHookOptions, usePatch } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';
import { MutationKey } from 'react-query';
import { UpdateUserDto } from 'src/server/controllers/user/dto/update-user.dto';

const useUpdateMe = (
  key: MutationKey,
  options?: PatchHookOptions<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserDto }
  >,
) => {
  return usePatch<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserDto }
  >(key, '/api/v1/me', options);
};

export { useUpdateMe };
