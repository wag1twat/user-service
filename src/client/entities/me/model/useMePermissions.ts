import { GetHookOptions, useGet, QueryKey } from 'src/shared/axios';
import { PermissionsQueries } from 'src/server/controllers/permissions/queries/permissions.queries';
import { Permission } from 'src/server/entities/client.types';

const useMePermissions = (
  key: QueryKey,
  options?: GetHookOptions<{ data: Permission[] }, PermissionsQueries>,
) => {
  return useGet<{ data: Permission[] }, PermissionsQueries>(
    key,
    '/api/v1/me/permissions',
    options,
  );
};

export { useMePermissions };
