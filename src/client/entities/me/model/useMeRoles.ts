import { Role } from 'src/server/entities/client.types';
import { QueryKey, GetHookOptions, useGet } from 'src/shared/axios';

const useMeRoles = (
  key: QueryKey,
  options?: GetHookOptions<{ data: Role[] }>,
) => {
  return useGet<{ data: Role[] }>(key, '/api/v1/me/roles', options);
};

export { useMeRoles };
