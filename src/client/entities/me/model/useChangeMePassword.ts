import { PatchHookOptions, usePatch } from 'src/shared/axios';
import { MutationKey } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { UpdateUserPasswordDto } from 'src/server/controllers/user/dto/update-user-password.dto';

const useChangeMePassword = (
  key: MutationKey,
  options?: PatchHookOptions<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserPasswordDto }
  >,
) => {
  return usePatch<
    { access_token: string },
    SerializeException,
    { payload: UpdateUserPasswordDto }
  >(key, '/api/v1/me/change-password', options);
};

export { useChangeMePassword };
