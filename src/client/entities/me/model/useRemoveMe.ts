import {
  PatchHookOptions,
  RemoveHookOptions,
  usePatch,
  useRemove,
} from 'src/shared/axios';
import { MutationKey } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { User } from 'src/server/entities/client.types';

const useDeactivateMe = (
  key: MutationKey,
  options?: RemoveHookOptions<User, SerializeException>,
) => {
  return useRemove<User, SerializeException>(
    key,
    '/api/v1/me/deactivate',
    options,
  );
};

const useActivateMe = (
  key: MutationKey,
  options?: PatchHookOptions<User, SerializeException>,
) => {
  return usePatch<User, SerializeException>(
    key,
    '/api/v1/me/activate',
    options,
  );
};

export { useDeactivateMe, useActivateMe };
