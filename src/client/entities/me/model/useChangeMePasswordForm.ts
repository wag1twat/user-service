import { useFormik } from 'formik';
import { UpdateUserPasswordDto } from 'src/server/controllers/user/dto/update-user-password.dto';
import { UserEntityConfig } from 'src/server/entities/user.entity';
import * as yup from 'yup';

export const ChangeMePasswordSchema: yup.SchemaOf<UpdateUserPasswordDto> =
  yup.object({
    previosPassword: yup.string().required(),
    nextPassword: yup
      .string()
      .min(8)
      .max(20)
      .matches(UserEntityConfig.password.regexp, {
        message: UserEntityConfig.password.notMatchMessage,
      })
      .required(),
    confirmNextPassword: yup
      .string()
      .when('nextPassword', function test(nextPassword: string) {
        return yup
          .string()
          .oneOf(
            [nextPassword],
            UserEntityConfig.password.notMatchNextPasswordMessage,
          );
      })
      .required(),
  });

const useChangeMePasswordForm = (
  onSubmit: (values: UpdateUserPasswordDto) => void,
) => {
  return useFormik<UpdateUserPasswordDto>({
    enableReinitialize: true,
    validationSchema: ChangeMePasswordSchema,
    initialValues: {
      previosPassword: '',
      nextPassword: '',
      confirmNextPassword: '',
    },
    onSubmit,
  });
};

export { useChangeMePasswordForm };
