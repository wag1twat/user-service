import { QueryKey } from 'src/shared/axios/useGet';
import { GetHookOptions, useGet } from 'src/shared/axios';
import { User } from 'src/server/entities/client.types';

export const useMe = (
  key: QueryKey,
  options?: GetHookOptions<{ data: User }>,
) => {
  return useGet<{ data: User }>(key, '/api/v1/me', options);
};
