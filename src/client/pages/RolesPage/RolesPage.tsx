import React from 'react';
import { Roles } from 'src/client/entities';
import { Header } from 'src/client/widgets';
import { Content } from 'src/client/wrappers';

type RolesPageProps = {};

export const RolesPage: React.FC<RolesPageProps> = React.memo(() => {
  return (
    <>
      <Header />
      <Content>
        <Roles />
      </Content>
    </>
  );
});

export default RolesPage;
