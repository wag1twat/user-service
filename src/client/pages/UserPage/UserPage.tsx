import React from 'react';
import { User } from 'src/client/entities';
import { Header } from 'src/client/widgets';
import { Content } from 'src/client/wrappers';
import { UserEntity } from 'src/server/entities/user.entity';

type UserPageProps = { id: UserEntity['id'] };

export const UserPage: React.FC<UserPageProps> = React.memo(({ id }) => {
  return (
    <>
      <Header />
      <Content>
        <User id={id} />
      </Content>
    </>
  );
});

export default UserPage;
