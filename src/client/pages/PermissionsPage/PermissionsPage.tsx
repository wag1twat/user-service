import React from 'react';
import { Header } from 'src/client/widgets';
import { Content } from 'src/client/wrappers';
import { Permissions } from 'src/client/entities/permissions';

type PermissionsPageProps = {};

export const PermissionsPage: React.FC<PermissionsPageProps> = React.memo(
  () => {
    return (
      <>
        <Header />
        <Content>
          <Permissions />
        </Content>
      </>
    );
  },
);

export default PermissionsPage;
