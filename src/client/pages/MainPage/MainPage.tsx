import { Preview } from 'src/client/entities/main';
import { Header } from 'src/client/widgets';
import { Content } from 'src/client/wrappers';

export const MainPage: React.FC = () => {
  return (
    <>
      <Header />
      <Content>
        <Preview />
      </Content>
    </>
  );
};

export default MainPage;
