import React from 'react';
import { Users } from 'src/client/entities';
import { Header } from 'src/client/widgets';
import { Content } from 'src/client/wrappers';

type UsersPageProps = {};

export const UsersPage: React.FC<UsersPageProps> = () => {
  return (
    <>
      <Header />
      <Content>
        <Users />
      </Content>
    </>
  );
};

export default UsersPage;
