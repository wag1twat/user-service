import { useDisclosure } from '@chakra-ui/react';
import React, { createContext, useContext } from 'react';

interface GlobalDisclosuresCtx {
  signin?: ReturnType<typeof useDisclosure>;
  signup?: ReturnType<typeof useDisclosure>;
  profile?: ReturnType<typeof useDisclosure>;
}

const globalDisclosuresContext = createContext<GlobalDisclosuresCtx>({});

const GlobalDisclosuresProvider: React.FC = (props) => {
  const signin = useDisclosure();
  const signup = useDisclosure();
  const profile = useDisclosure();

  return (
    <globalDisclosuresContext.Provider
      value={{ signin, signup, profile }}
      {...props}
    />
  );
};

const useGlobalDisclosures = () => {
  return useContext(globalDisclosuresContext);
};

export { GlobalDisclosuresProvider, useGlobalDisclosures };
