import React from 'react';
import { removeToken, setToken } from 'src/shared/axios/axios-instance';
import { SerializeException } from 'src/shared/helpers';
import { Role, User, Permission } from 'src/server/entities/client.types';
import { reduce } from 'lodash';
import { QueryStatus } from 'react-query';
import { useGlobalDisclosures } from './useGlobalDisclosuresProvider';
import {
  useMe,
  useMePermissions,
  useMeRoles,
} from 'src/client/entities/me/model';
import { ExceptionMessages } from 'src/server/exceptions/constants';
import { CaslPermissionsActionKeysOrString } from 'src/server/casl/casl.permissions';

interface AuthCtx {
  isAuth: boolean;
  isActive: boolean;
  deactiveMessage: string;
  setAccess?: (data: { access_token: string }) => void;
  logout?: () => void;
  me?: {
    data: User | undefined;
    isLoading: boolean;
    isFetching: boolean;
    error: SerializeException | undefined;
    refetch: () => void;
    remove: () => void;
    status: QueryStatus;
  };
  roles?: {
    data: Role[] | undefined;
    isLoading: boolean;
    isFetching: boolean;
    error: SerializeException | undefined;
    refetch: () => void;
    remove: () => void;
    status: QueryStatus;
  };
  permissions?: {
    availables: Partial<Record<CaslPermissionsActionKeysOrString, boolean>>;
    data: Permission[] | undefined;
    isLoading: boolean;
    isFetching: boolean;
    error: SerializeException | undefined;
    refetch: () => void;
    remove: () => void;
    status: QueryStatus;
  };
}
const AuthContext = React.createContext<AuthCtx>({
  deactiveMessage: ExceptionMessages.DEACTIVE_USER,
  isAuth: false,
  isActive: false,
  logout: undefined,
  setAccess: undefined,
  me: undefined,
  roles: undefined,
  permissions: undefined,
});

const useAuth = () => {
  return React.useContext(AuthContext);
};

const AuthProvider = (props: React.PropsWithChildren<{}>) => {
  const { profile } = useGlobalDisclosures();

  const [isAuth, setIsAuth] = React.useState(false);

  const [isActive, setIsActive] = React.useState(false);

  const setAccess = React.useCallback((data: { access_token: string }) => {
    setToken(data.access_token);
    setIsAuth(true);
  }, []);

  const logout = React.useCallback(() => {
    removeToken();
    setIsAuth(false);
    setIsActive(false);
  }, []);

  const me = useMe(['me', isAuth, profile?.isOpen], {
    onSuccess: (data) => {
      setIsAuth(true);
      setIsActive(data.data.active);
    },
    onError: () => {
      setIsAuth(false);
    },
  });

  const roles = useMeRoles(
    ['me-roles', isAuth, me.data?.data, profile?.isOpen],
    {
      params: { usersIds: me.data?.data?.id ? [me.data?.data?.id] : undefined },
      enabled: Boolean(isAuth && me.data?.data),
    },
  );

  const permissions = useMePermissions(
    ['me-permissions', isAuth, me.data?.data, profile?.isOpen],
    {
      params: { usersIds: me.data?.data.id ? [me.data?.data.id] : undefined },
      enabled: Boolean(isAuth && me.data?.data),
    },
  );

  const _me = React.useMemo(
    () => ({
      data: me.data?.data,
      isLoading: me.isLoading,
      isFetching: me.isFetching,
      error: me.error,
      refetch: () => me.refetch(),
      remove: me.remove,
      status: me.status,
    }),
    [me],
  );

  const _roles = React.useMemo(
    () => ({
      data: roles.data?.data,
      isLoading: roles.isLoading,
      isFetching: roles.isFetching,
      error: roles.error,
      refetch: () => roles.refetch(),
      remove: roles.remove,
      status: roles.status,
    }),
    [roles],
  );

  const _permissions = React.useMemo(() => {
    const availables = reduce<
      Permission,
      Partial<Record<CaslPermissionsActionKeysOrString, boolean>>
    >(
      permissions.data?.data,
      (acc, current) => ({ ...acc, [current.name]: true }),
      {},
    );

    return {
      availables,
      data: permissions.data?.data,
      isLoading: permissions.isLoading,
      isFetching: permissions.isFetching,
      error: permissions.error,
      refetch: () => permissions.refetch(),
      remove: permissions.remove,
      status: permissions.status,
    };
  }, [permissions]);

  return (
    <AuthContext.Provider
      value={{
        isAuth,
        isActive,
        deactiveMessage: ExceptionMessages.DEACTIVE_USER,
        setAccess,
        logout,
        me: _me,
        roles: _roles,
        permissions: _permissions,
      }}
      {...props}
    />
  );
};

export { AuthProvider, useAuth };
