import { Flex } from '@chakra-ui/react';
import React from 'react';

const Content: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  return (
    <Flex
      marginTop="0px"
      padding={4}
      width="100%"
      minH="calc(100vh - 80px)"
      maxWidth="1600px"
      margin="auto"
    >
      {children}
    </Flex>
  );
};

export { Content };
