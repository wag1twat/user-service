import { Button, Flex, FlexProps, Progress, Text } from '@chakra-ui/react';
import React from 'react';

export interface InlineLoaderProps extends FlexProps {
  isLoading: boolean;
  error?: string;
  retry?: () => void;
}

const InlineLoader: React.FC<InlineLoaderProps> = React.memo(
  ({ isLoading, error, retry, children, ...props }) => {
    if (isLoading) {
      return (
        <Flex
          width="100%"
          justifyContent="center"
          alignItems="center"
          {...props}
        >
          <Progress width="inherit" size="xs" isIndeterminate />
        </Flex>
      );
    }

    if (error) {
      return (
        <Flex
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          {...props}
        >
          <Text color="red.500">{error}</Text>
          {retry && (
            <Button minWidth={24} colorScheme="blue" onClick={retry}>
              Retry
            </Button>
          )}
        </Flex>
      );
    }

    return <>{children}</>;
  },
);

export { InlineLoader };
