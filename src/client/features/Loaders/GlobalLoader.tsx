import { Box, Stack } from '@chakra-ui/layout';
import {
  BoxProps,
  Button,
  ColorProps,
  CSSObject,
  CSSWithMultiValues,
  StackProps,
  Text,
  useTheme,
} from '@chakra-ui/react';
import { get } from 'lodash';
import React from 'react';

interface GlobalSpinnerProps extends BoxProps {
  ballSize?: BoxProps['width'];
}
interface GlobalLoaderProps {
  isLoading: boolean;
  error?: string;
  retry?: () => void;
  containerProps?: StackProps;
  spinnerProps?: GlobalSpinnerProps;
}

const _beforeLight = (
  color: ColorProps['color'],
  position: 'top' | 'bottom',
  ballSize: GlobalSpinnerProps['ballSize'],
): CSSWithMultiValues => ({
  content: `''`,
  position: 'absolute',
  width: ballSize,
  height: ballSize,
  background: color,
  left: '50%',
  transform: 'translateX(-50%)',
  top: position === 'top' ? '-12px' : 'calc(100% - 24px)',
  borderRadius: '50%',
  zIndex: -1,
});

const _beforeDark = (
  color: ColorProps['color'],
  position: 'top' | 'bottom',
  ballSize: GlobalSpinnerProps['ballSize'],
): CSSWithMultiValues => ({
  ..._beforeLight(color, position, ballSize),
  boxShadow: `0 0 20px ${color}, 0 0 60px ${color}`,
});

const __css = (
  color: ColorProps['color'],
  animationDuration: string,
  animationDirection: 'alternate-reverse' | 'alternate',
  position: 'top' | 'bottom',
  ballSize: GlobalSpinnerProps['ballSize'],
): CSSObject => ({
  animationDuration,
  animationName: 'animate',
  animationIterationCount: 'infinite',
  animationDirection,
  _dark: { _before: _beforeDark(color, position, ballSize) },
  _light: { _before: _beforeLight(color, position, ballSize) },
  '@keyframes animate': {
    '0%': {
      // transform: 'rotate(0deg)',
      width: `100%`,
      height: `100%`,
      transform: 'rotate3d(0, 0, 1, 0deg)',
    },
    '100%': {
      // transform: 'rotate(360deg)',
      width: '25%',
      height: '25%',
      transform: 'rotate3d(0, 1, 1, 360deg)',
    },
  },
});

const GlobalSpinner = ({ ballSize = '20px', ...props }: GlobalSpinnerProps) => {
  const { colors } = useTheme();

  return (
    <Box
      position="absolute"
      background="transparent"
      display="flex"
      justifyContent="center"
      alignItems="center"
      borderRadius="50%"
      width="200px"
      height="200px"
      __css={{
        animationDuration: '2s',
        animationName: 'animate-container',
        animationIterationCount: 'infinite',
        animationDirection: 'alternate',
        '@keyframes animate-container': {
          '0%': {
            transform: 'rotate(0deg)',
          },
          '100%': {
            transform: 'rotate(360deg)',
          },
        },
      }}
      {...props}
    >
      {/* top */}
      <Box
        position="absolute"
        borderRadius="50%"
        width="100%"
        height="100%"
        __css={__css(
          get(colors, 'green.accent-2'),
          '1.5s',
          'alternate',
          'top',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="80%"
        height="80%"
        __css={__css(
          get(colors, 'yellow.accent-2'),
          '1.75s',
          'alternate-reverse',
          'top',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="60%"
        height="60%"
        __css={__css(
          get(colors, 'blue.accent-2'),
          '2s',
          'alternate',
          'top',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="40%"
        height="40%"
        __css={__css(
          get(colors, 'pink.accent-2'),
          '2.25s',
          'alternate-reverse',
          'top',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="20%"
        height="20%"
        __css={__css(
          get(colors, 'orange.accent-2'),
          '2.5s',
          'alternate-reverse',
          'top',
          ballSize,
        )}
      />
      {/* bottom */}
      <Box
        position="absolute"
        borderRadius="50%"
        width="100%"
        height="100%"
        __css={__css(
          get(colors, 'light-green.accent-2'),
          '1.5s',
          'alternate',
          'bottom',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="80%"
        height="80%"
        __css={__css(
          get(colors, 'red.accent-2'),
          '1.75s',
          'alternate-reverse',
          'bottom',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="60%"
        height="60%"
        __css={__css(
          get(colors, 'teal.accent-2'),
          '2s',
          'alternate',
          'bottom',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="40%"
        height="40%"
        __css={__css(
          get(colors, 'deep-purple.accent-2'),
          '2.25s',
          'alternate-reverse',
          'bottom',
          ballSize,
        )}
      />
      <Box
        position="absolute"
        borderRadius="50%"
        width="20%"
        height="20%"
        __css={__css(
          get(colors, 'lime.accent-2'),
          '2.5s',
          'alternate-reverse',
          'bottom',
          ballSize,
        )}
      />
    </Box>
  );
};

const GlobalLoader = React.memo(
  ({
    isLoading,
    error,
    retry,
    children,
    spinnerProps,
    containerProps,
  }: React.PropsWithChildren<GlobalLoaderProps>) => {
    if (isLoading) {
      return (
        <Stack
          width="100%"
          justifyContent="center"
          alignItems="center"
          {...containerProps}
        >
          <GlobalSpinner {...spinnerProps} />
        </Stack>
      );
    }

    if (error) {
      return (
        <Stack
          width="100%"
          justifyContent="center"
          alignItems="center"
          {...containerProps}
        >
          <Text color="red.500">{error}</Text>
          <Text>You can</Text>
          <Button colorScheme="blue" onClick={retry}>
            Retry
          </Button>
        </Stack>
      );
    }

    return <>{children}</>;
  },
);

export { GlobalSpinner, GlobalLoader };
