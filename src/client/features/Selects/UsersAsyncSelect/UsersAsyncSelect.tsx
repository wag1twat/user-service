import { debounce, find, map } from 'lodash';
import React from 'react';
import { useUsers } from 'src/client/entities/users/model';
import {
  AsyncSelectFormControl,
  AsyncSelectFormControlProps,
} from 'src/client/shared';
import { SelectOption } from 'src/client/shared/FormControls/Selects/types';
import { User } from 'src/server/entities/client.types';
import { ParseSerializeException } from 'src/shared/helpers';
import { InlineLoader } from 'src/client/features';

type UsersAsyncSelectProps = {
  label: string;
  selectedId: User['id'] | undefined;
  onChange: AsyncSelectFormControlProps<
    SelectOption,
    false
  >['selectProps']['onChange'];
};

const usersOptions = (users?: User[]) =>
  map(users, (user) => ({
    label: user.login,
    value: user.id,
  }));

const getValue = (
  users?: User[],
  selectedId: UsersAsyncSelectProps['selectedId'] | undefined = undefined,
): SelectOption | null => {
  const user = find(users, (user) => user.id === selectedId);

  return user ? { label: user.login, value: user.id } : null;
};

const UsersAsyncSelect: React.FC<UsersAsyncSelectProps> = React.memo(
  ({ label, selectedId, onChange }) => {
    const { data, isLoading, error, refetch, native } = useUsers([
      'users-async-select',
    ]);

    const loadOptions = debounce(
      (
        inputValue: string,
        callback: (
          options: Array<{
            label: User['login'];
            value: User['id'];
          }>,
        ) => void,
      ) => {
        native({ params: { login: inputValue } })
          .then((response) => {
            callback(usersOptions(response.data.data));
          })
          .catch(() => callback([]));
      },
      200,
    );

    return (
      <InlineLoader
        isLoading={isLoading}
        error={ParseSerializeException.getString(error)}
        retry={refetch}
      >
        <AsyncSelectFormControl<SelectOption, false>
          label={label}
          selectProps={{
            isClearable: true,
            isMulti: false,
            loadOptions: loadOptions,
            noOptionsMessage: () => 'Users not found',
            defaultOptions: usersOptions(data?.data),
            placeholder: 'Inter user login...',
            value: getValue(data?.data, selectedId),
            onChange,
          }}
        />
      </InlineLoader>
    );
  },
);

export { UsersAsyncSelect };
