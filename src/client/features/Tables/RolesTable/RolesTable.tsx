import { Text } from '@chakra-ui/react';
import React from 'react';
import { Column, OverrideSortByToggleProps } from 'react-table';
import { Table, TableProps, OwnerCell, BooleanBadge } from 'src/client/shared';
import { Role } from 'src/server/entities/client.types';
import { DateTimeUtils } from 'src/shared/helpers';

interface RolesTableProps
  extends Pick<TableProps<Role>, 'data' | 'headProps' | 'containerProps'>,
    OverrideSortByToggleProps<Role> {
  extraColumns?: Column<Role>[];
}

const RolesTable: React.FC<RolesTableProps> = React.memo(
  ({
    data = [],
    extraColumns = [],
    headProps,
    containerProps,
    sortBy,
    order,
    sortByServer,
  }) => {
    const columns = React.useMemo<Column<Role>[]>(
      () => [
        {
          Header: 'Name',
          accessor: 'name',
          Cell: (cell) => <Text>{cell.row.original.name}</Text>,
          sortBy,
          order,
          sortByServer,
        },
        {
          Header: 'Created at',
          accessor: 'createdAt',
          Cell: (cell) => (
            <Text>
              {DateTimeUtils.toDateTimeNumericFromISO(
                cell.row.original.createdAt,
              )}
            </Text>
          ),
          sortBy,
          order,
          sortByServer,
        },
        {
          Header: 'Updated at',
          accessor: 'updatedAt',
          Cell: (cell) => (
            <Text>
              {DateTimeUtils.toDateTimeNumericFromISO(
                cell.row.original.updatedAt,
              )}
            </Text>
          ),
          sortBy,
          order,
          sortByServer,
        },
        {
          Header: 'Active status',
          accessor: 'active',
          Cell: (cell) => {
            return <BooleanBadge is={cell.row.original.active} />;
          },
          sortBy,
          order,
          sortByServer,
        },
        {
          Header: 'Protected status',
          accessor: 'protected',
          Cell: (cell) => {
            return <BooleanBadge is={cell.row.original.protected} />;
          },
          sortBy,
          order,
          sortByServer,
        },
        {
          Header: 'Owner',
          accessor: 'owner',
          Cell: (cell) => <OwnerCell owner={cell.row.original.owner} />,
          sortBy,
          order,
          sortByServer,
        },
        ...extraColumns,
      ],
      [extraColumns, order, sortBy, sortByServer],
    );

    return (
      <Table
        data={data}
        columns={columns}
        cardBreakpoint="md"
        headProps={headProps}
        containerProps={containerProps}
      />
    );
  },
);

export { RolesTable };
