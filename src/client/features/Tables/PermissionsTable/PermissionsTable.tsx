import { Text } from '@chakra-ui/react';
import React from 'react';
import { Column } from 'react-table';
import { Table, TableProps, OwnerCell, BooleanBadge } from 'src/client/shared';
import { DateTimeUtils } from 'src/shared/helpers';
import { Permission } from 'src/server/entities/client.types';

interface PermissionsTableProps
  extends Pick<
    TableProps<Permission>,
    'data' | 'headProps' | 'containerProps'
  > {
  extraColumns?: Column<Permission>[];
}

const PermissionsTable: React.FC<PermissionsTableProps> = React.memo(
  ({ data = [], extraColumns = [], headProps, containerProps }) => {
    const columns = React.useMemo<Column<Permission>[]>(
      () => [
        {
          Header: 'Name',
          accessor: 'name',
          Cell: (cell) => {
            return <Text>{cell.row.original.name}</Text>;
          },
        },
        {
          Header: 'Created at',
          accessor: 'createdAt',
          Cell: (cell) => (
            <Text>
              {DateTimeUtils.toDateTimeNumericFromISO(
                cell.row.original.createdAt,
              )}
            </Text>
          ),
        },
        {
          Header: 'Updated at',
          accessor: 'updatedAt',
          Cell: (cell) => (
            <Text>
              {DateTimeUtils.toDateTimeNumericFromISO(
                cell.row.original.updatedAt,
              )}
            </Text>
          ),
        },
        {
          Header: 'Active status',
          accessor: 'active',
          Cell: (cell) => {
            return <BooleanBadge is={cell.row.original.active} />;
          },
        },
        {
          Header: 'Protected status',
          accessor: 'protected',
          Cell: (cell) => {
            return <BooleanBadge is={cell.row.original.protected} />;
          },
        },
        {
          Header: 'Owner',
          accessor: 'owner',
          Cell: (cell) => <OwnerCell owner={cell.row.original.owner} />,
        },
        ...extraColumns,
      ],
      [extraColumns],
    );

    return (
      <Table
        data={data}
        columns={columns}
        cardBreakpoint="md"
        headProps={headProps}
        containerProps={containerProps}
      />
    );
  },
);

export { PermissionsTable };
