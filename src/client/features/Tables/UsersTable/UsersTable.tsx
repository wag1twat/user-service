import { Text } from '@chakra-ui/react';
import React from 'react';
import { Column, OverrideSortByToggleProps } from 'react-table';
import {
  Table,
  OwnerCell,
  RouterLink,
  useRoutes,
  BooleanBadge,
} from 'src/client/shared';
import { User } from 'src/server/entities/client.types';
import { DateTimeUtils } from 'src/shared/helpers';

interface UsersTableProps extends OverrideSortByToggleProps<User> {
  data?: User[];
}

function UsersTable({
  data = [],
  sortBy,
  order,
  sortByServer,
}: UsersTableProps) {
  const { user } = useRoutes();

  const columns = React.useMemo<Column<User>[]>(
    () => [
      {
        Header: 'Login',
        accessor: 'login',
        Cell: (cell) => (
          <RouterLink
            variant="link"
            colorScheme="blue"
            href={user(cell.row.original.id)}
            p={0}
          >
            {cell.row.original.login}
          </RouterLink>
        ),
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Email',
        accessor: 'email',
        Cell: (cell) => <Text>{cell.row.original.email}</Text>,
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'First name',
        accessor: 'firstName',
        Cell: (cell) => <Text>{cell.row.original.firstName}</Text>,
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Last name',
        accessor: 'lastName',
        Cell: (cell) => <Text>{cell.row.original.lastName}</Text>,
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: (cell) => (
          <Text>
            {DateTimeUtils.toDateTimeNumericFromISO(
              cell.row.original.createdAt,
            )}
          </Text>
        ),
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Updated at',
        accessor: 'updatedAt',
        Cell: (cell) => (
          <Text>
            {DateTimeUtils.toDateTimeNumericFromISO(
              cell.row.original.updatedAt,
            )}
          </Text>
        ),
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Active status',
        accessor: 'active',
        Cell: (cell) => {
          return <BooleanBadge is={cell.row.original.active} />;
        },
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Protected status',
        accessor: 'protected',
        Cell: (cell) => {
          return <BooleanBadge is={cell.row.original.protected} />;
        },
        sortBy,
        order,
        sortByServer,
      },
      {
        Header: 'Owner',
        accessor: 'owner',
        Cell: (cell) => <OwnerCell owner={cell.row.original.owner} />,
        sortBy,
        order,
        sortByServer,
      },
    ],
    [order, sortBy, sortByServer, user],
  );

  return <Table data={data} columns={columns} />;
}

export { UsersTable };
