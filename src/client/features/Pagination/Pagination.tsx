import { DoubleLeftOutlined, DoubleRightOutlined } from '@ant-design/icons';
import {
  Button,
  ButtonProps,
  Flex,
  IconButton,
  List,
  ListItem,
  SelectProps,
} from '@chakra-ui/react';
import React from 'react';
import { generatePages, LEFT_PAGE, RIGHT_PAGE } from './generatePages';
import { SelectLimit } from './SelectLimit';

export type PaginationData = {
  currentPage: number;
  limit: number;
};

type PaginationProps = {
  sizes?: {
    button?: ButtonProps['size'];
    select?: SelectProps['size'];
  };
  totalCount: number;
  pageNeighbours?: 0 | 1 | 2;
  currentPage: number;
  setCurrentPage: (nextPage: number) => void;
  limit: number;
  setLimit: (limit: number) => void;
};

const pseudos = {
  _active: {
    color: 'blue.500',
  },
  _hover: {
    color: 'blue.300',
  },
};

const Pagination: React.FC<PaginationProps> = React.memo(
  ({
    totalCount,
    pageNeighbours = 1,
    currentPage,
    setCurrentPage,
    limit,
    setLimit,
    sizes = { button: 'xs', select: 'xs' },
  }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars

    const totalPages = React.useMemo(
      () => Math.ceil(totalCount / limit),
      [limit, totalCount],
    );

    const pages = React.useMemo(
      () => generatePages({ pageNeighbours, totalPages, currentPage }),
      [currentPage, pageNeighbours, totalPages],
    );

    const gotoPage = React.useCallback(
      (page: number) => {
        const currentPage = Math.max(0, Math.min(page, totalPages));

        setCurrentPage(currentPage);
      },
      [setCurrentPage, totalPages],
    );

    const handleClick = React.useCallback(
      (page: number) => (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        gotoPage(page);
      },
      [gotoPage],
    );

    const handleMoveLeft = React.useCallback(
      (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        gotoPage(currentPage - pageNeighbours * 2 - 1);
      },
      [currentPage, gotoPage, pageNeighbours],
    );

    const handleMoveRight = React.useCallback(
      (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        gotoPage(currentPage + pageNeighbours * 2 + 1);
      },
      [currentPage, gotoPage, pageNeighbours],
    );

    const handleChangeLimit = React.useCallback(
      (limit: number) => {
        setLimit(limit);
        gotoPage(1);
      },
      [gotoPage, setLimit],
    );

    if (totalCount === 0) {
      return null;
    }

    return (
      <Flex
        alignItems="center"
        justifyContent="space-between"
        flexDirection="row"
      >
        <List display="flex" alignItems="center">
          {pages.map((page, index) => {
            if (page === LEFT_PAGE)
              return (
                <ListItem key={index} display="flex" alignItems="center">
                  <IconButton
                    size={sizes.button}
                    variant="link"
                    aria-label="Previous"
                    onClick={handleMoveLeft}
                    {...pseudos}
                  >
                    <DoubleLeftOutlined />
                  </IconButton>
                </ListItem>
              );

            if (page === RIGHT_PAGE)
              return (
                <ListItem key={index} display="flex" alignItems="center">
                  <IconButton
                    size={sizes.button}
                    variant="link"
                    aria-label="Next"
                    onClick={handleMoveRight}
                    {...pseudos}
                  >
                    <DoubleRightOutlined />
                  </IconButton>
                </ListItem>
              );

            return (
              <ListItem key={index} display="flex" alignItems="center">
                <Button
                  size={sizes.button}
                  variant="link"
                  aria-label="Page"
                  onClick={handleClick(page)}
                  isActive={currentPage === page}
                  {...pseudos}
                >
                  {page}
                </Button>
              </ListItem>
            );
          })}
        </List>
        <SelectLimit
          size={sizes.select}
          value={limit}
          onChange={handleChangeLimit}
        />
      </Flex>
    );
  },
);

export { Pagination };
