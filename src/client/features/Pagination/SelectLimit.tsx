import { chakra, Flex, Select, SelectProps } from '@chakra-ui/react';
import React from 'react';

type SelectLimitProps = {
  size: SelectProps['size'];
  value: number;
  onChange: (limit: number) => void;
};

const limitOptions = [5, 10, 15, 20, 25];

const SelectLimit: React.FC<SelectLimitProps> = ({ size, value, onChange }) => {
  return (
    <Flex alignItems="center" justifyContent="space-between">
      <chakra.span fontSize={size} marginRight={2}>
        Limit:
      </chakra.span>
      <Select
        size={size}
        value={value}
        onChange={(e) => onChange(Number(e.target.value))}
      >
        {limitOptions.map((limit) => (
          <option key={limit} value={limit}>
            {limit}
          </option>
        ))}
      </Select>
    </Flex>
  );
};

export { SelectLimit };
