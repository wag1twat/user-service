import { Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useRemoveUserRoles } from 'src/client/entities/user/model';
import { useAuth } from 'src/client/providers';
import {
  DeleteIconButton,
  AccessTooltip,
  useUIToasts,
  Accessibilities,
} from 'src/client/shared';
import { Role, User } from 'src/server/entities/client.types';
import { ParseSerializeException } from 'src/shared/helpers';

interface UserRemoveRoleButtonProps extends Accessibilities {
  roleId: Role['id'];
  id: User['id'] | undefined;
  refetch: () => void;
}

export const UserRemoveRoleButton = ({
  roleId,
  id,
  accessibilities,
  refetch,
}: UserRemoveRoleButtonProps) => {
  const { permissions, me, roles } = useAuth();

  const { success, error } = useUIToasts({ position: 'top' });

  const { remove, isLoading } = useRemoveUserRoles(['user-remove-role', id], {
    onSuccess: () => {
      success('Unset successfully');
      refetch();
      if (id === me?.data?.id) {
        permissions?.refetch();
        roles?.refetch();
        roles?.refetch();
      }
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  const handle = React.useCallback(() => {
    if (id) {
      remove({
        id,
        rolesIds: [roleId],
      });
    }
  }, [remove, id, roleId]);

  return (
    <AccessTooltip
      shouldWrapChildren
      availables={['AVAILABLE_UPDATE_ROLE', 'AVAILABLE_UPDATE_USER']}
      accessibilities={accessibilities}
    >
      {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
        <Skeleton width="fit-content" isLoaded={!isLoadingAccessibilities}>
          <DeleteIconButton
            aria-label="Detele user role"
            colorScheme="red"
            isLoading={isLoading || isLoadingAccessibilities}
            isDisabled={isDisabledAccessibilities}
            onClick={handle}
          />
        </Skeleton>
      )}
    </AccessTooltip>
  );
};
