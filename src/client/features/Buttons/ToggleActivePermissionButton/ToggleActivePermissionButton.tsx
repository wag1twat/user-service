import React from 'react';
import {
  Accessibilities,
  ActivateIcon,
  DeactivateIcon,
  ToggleActionButton,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import {
  useActivatePermission,
  useDeactivatePermission,
} from 'src/client/entities/permission/model';
import { PermissionEntity } from 'src/server/entities/permission.entity';

interface ToggleActivePermissionButtonProps extends Accessibilities {
  id: PermissionEntity['id'] | undefined;
  isActive: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleActivePermissionButton: React.FC<ToggleActivePermissionButtonProps> =
  React.memo(({ id, isActive, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deactivate = useDeactivatePermission(['permission-deactivate', id], {
      onSuccess: () => {
        warning('Permission was been deactivated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const activate = useActivatePermission(['permission-activate', id], {
      onSuccess: () => {
        success('Permission was been activated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isActive}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            activate.patch({ params: { id } });
          }
        }}
        ActionName="Activate"
        ActionIcon={<ActivateIcon />}
        isLoadingAction={activate.isLoading}
        Revert={() => {
          if (id) {
            deactivate.remove({ id });
          }
        }}
        RevertName="Deactivate"
        RevertIcon={<DeactivateIcon />}
        isLoadingRevert={deactivate.isLoading}
        availables={['AVAILABLE_UPDATE_PERMISSIONS']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleActivePermissionButton };
