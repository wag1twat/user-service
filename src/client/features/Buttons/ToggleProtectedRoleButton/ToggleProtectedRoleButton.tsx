import React from 'react';
import {
  Accessibilities,
  useUIToasts,
  ToggleActionButton,
  DeprotectedIcon,
  ProtectedIcon,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import {
  useDeprotectRole,
  useProtectRole,
} from 'src/client/entities/role/model';
import { RoleEntity } from 'src/server/entities/role.entity';

interface ToggleProtectedRoleButtonProps extends Accessibilities {
  id: RoleEntity['id'] | undefined;
  isProtected: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleProtectedRoleButton: React.FC<ToggleProtectedRoleButtonProps> =
  React.memo(({ id, isProtected, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deprotect = useDeprotectRole(['role-deprotect', id], {
      onSuccess: () => {
        warning('Role was been deprotected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const protect = useProtectRole(['role-protect', id], {
      onSuccess: () => {
        success('Role was been protected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isProtected}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            protect.patch({ params: { id } });
          }
        }}
        ActionName="Protect"
        ActionIcon={<ProtectedIcon />}
        isLoadingAction={protect.isLoading}
        Revert={() => {
          if (id) {
            deprotect.remove({ id });
          }
        }}
        RevertName="Deprotect"
        RevertIcon={<DeprotectedIcon />}
        isLoadingRevert={deprotect.isLoading}
        availables={['AVAILABLE_UPDATE_ROLE']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleProtectedRoleButton };
