import React from 'react';
import {
  Accessibilities,
  ActivateIcon,
  DeactivateIcon,
  ToggleActionButton,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import {
  useActivateRole,
  useDeactivateRole,
} from 'src/client/entities/role/model';
import { RoleEntity } from 'src/server/entities/role.entity';

interface ToggleActiveRoleButtonProps extends Accessibilities {
  id: RoleEntity['id'] | undefined;
  isActive: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleActiveRoleButton: React.FC<ToggleActiveRoleButtonProps> =
  React.memo(({ id, isActive, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deactivate = useDeactivateRole(['role-deactivate', id], {
      onSuccess: () => {
        warning('Role was been deactivated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const activate = useActivateRole(['role-activate', id], {
      onSuccess: () => {
        success('Role was been activated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isActive}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            activate.patch({ params: { id } });
          }
        }}
        ActionName="Activate"
        ActionIcon={<ActivateIcon />}
        isLoadingAction={activate.isLoading}
        Revert={() => {
          if (id) {
            deactivate.remove({ id });
          }
        }}
        RevertName="Deactivate"
        RevertIcon={<DeactivateIcon />}
        isLoadingRevert={deactivate.isLoading}
        availables={['AVAILABLE_UPDATE_ROLE']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleActiveRoleButton };
