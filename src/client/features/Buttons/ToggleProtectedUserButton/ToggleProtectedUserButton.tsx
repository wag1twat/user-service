import React from 'react';
import {
  Accessibilities,
  DeprotectedIcon,
  ProtectedIcon,
  ToggleActionButton,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import { RoleEntity } from 'src/server/entities/role.entity';
import {
  useDeprotectUser,
  useProtectUser,
} from 'src/client/entities/user/model';

interface ToggleProtectedUserButtonProps extends Accessibilities {
  id: RoleEntity['id'] | undefined;
  isProtected: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleProtectedUserButton: React.FC<ToggleProtectedUserButtonProps> =
  React.memo(({ id, isProtected, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deprotect = useDeprotectUser(['user-deprotect', id], {
      onSuccess: () => {
        warning('User was been deprotected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const protect = useProtectUser(['user-protect', id], {
      onSuccess: () => {
        success('User was been protected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isProtected}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            protect.patch({ params: { id } });
          }
        }}
        ActionName="Protect"
        ActionIcon={<ProtectedIcon />}
        isLoadingAction={protect.isLoading}
        Revert={() => {
          if (id) {
            deprotect.remove({ id });
          }
        }}
        RevertName="Deprotect"
        RevertIcon={<DeprotectedIcon />}
        isLoadingRevert={deprotect.isLoading}
        availables={['AVAILABLE_UPDATE_USER']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleProtectedUserButton };
