import { IconButton } from '@chakra-ui/react';
import { useColorMode } from '@chakra-ui/system';
import React from 'react';
import { DarkThemeIcon, LightThemeIcon } from 'src/client/shared/icons';

type ToggleThemeModeButtonProps = {};

const ToggleThemeModeButton: React.FC<ToggleThemeModeButtonProps> = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <IconButton
      aria-label="Toggle theme"
      onClick={toggleColorMode}
      icon={
        <>
          {colorMode === 'light' && <DarkThemeIcon />}
          {colorMode === 'dark' && <LightThemeIcon />}
        </>
      }
    />
  );
};

export { ToggleThemeModeButton };
