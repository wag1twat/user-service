import React from 'react';
import {
  Accessibilities,
  useUIToasts,
  ToggleActionButton,
  DeprotectedIcon,
  ProtectedIcon,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import { RoleEntity } from 'src/server/entities/role.entity';
import {
  useDeprotectPermission,
  useProtectPermission,
} from 'src/client/entities/permission/model';

interface ToggleProtectedPermissionButtonProps extends Accessibilities {
  id: RoleEntity['id'] | undefined;
  isProtected: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleProtectedPermissionButton: React.FC<ToggleProtectedPermissionButtonProps> =
  React.memo(({ id, isProtected, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deprotect = useDeprotectPermission(['Permission-deprotect', id], {
      onSuccess: () => {
        warning('Permission was been deprotected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const protect = useProtectPermission(['Permission-protect', id], {
      onSuccess: () => {
        success('Permission was been protected');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isProtected}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            protect.patch({ params: { id } });
          }
        }}
        ActionName="Protect"
        ActionIcon={<ProtectedIcon />}
        isLoadingAction={protect.isLoading}
        Revert={() => {
          if (id) {
            deprotect.remove({ id });
          }
        }}
        RevertName="Deprotect"
        RevertIcon={<DeprotectedIcon />}
        isLoadingRevert={deprotect.isLoading}
        availables={['AVAILABLE_UPDATE_PERMISSIONS']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleProtectedPermissionButton };
