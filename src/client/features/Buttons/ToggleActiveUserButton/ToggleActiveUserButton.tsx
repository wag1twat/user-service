import React from 'react';
import {
  Accessibilities,
  ActivateIcon,
  DeactivateIcon,
  ToggleActionButton,
  useUIToasts,
} from 'src/client/shared';
import {
  useDeactivateUser,
  useActivateUser,
} from 'src/client/entities/user/model';
import { ParseSerializeException } from 'src/shared/helpers';
import { UserEntity } from 'src/server/entities/user.entity';

interface ToggleActiveUserButtonProps extends Accessibilities {
  id: UserEntity['id'] | undefined;
  isActive: boolean;
  isLoading: boolean;
  refetch: () => void;
}

const ToggleActiveUserButton: React.FC<ToggleActiveUserButtonProps> =
  React.memo(({ id, isActive, isLoading, accessibilities, refetch }) => {
    const { warning, success, error } = useUIToasts({
      position: 'bottom-start',
    });

    const deactivate = useDeactivateUser(['user-deactivate', id], {
      onSuccess: () => {
        warning('User was been deactivated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const activate = useActivateUser(['user-activate', id], {
      onSuccess: () => {
        success('User was been activated');
        refetch();
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    return (
      <ToggleActionButton
        isActive={isActive}
        isLoading={isLoading}
        Action={() => {
          if (id) {
            activate.patch({ params: { id } });
          }
        }}
        ActionName="Activate"
        ActionIcon={<ActivateIcon />}
        isLoadingAction={activate.isLoading}
        Revert={() => {
          if (id) {
            deactivate.remove({ id });
          }
        }}
        RevertName="Deactivate"
        RevertIcon={<DeactivateIcon />}
        isLoadingRevert={deactivate.isLoading}
        availables={['AVAILABLE_UPDATE_USER']}
        accessibilities={accessibilities}
      />
    );
  });

export { ToggleActiveUserButton };
