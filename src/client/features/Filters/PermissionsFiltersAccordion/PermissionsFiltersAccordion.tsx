import React from 'react';
import {
  Grid,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Accordion,
  AccordionItem,
  AccordionButton,
  Box,
  AccordionIcon,
  AccordionPanel,
  ExpandedIndex,
} from '@chakra-ui/react';

import { CheckboxFormControl, DateRangePicker } from 'src/client/shared';
import { UsersAsyncSelect } from '../../Selects/UsersAsyncSelect';
import { useAuth } from 'src/client/providers';
import { PermissionsQueries } from 'src/server/controllers/permissions/queries/permissions.queries';

interface PermissionsFiltersAccordionProps {
  defaultIndex: ExpandedIndex;
  setDefaultIndex: (defaultIndex: ExpandedIndex) => void;
  filters: PermissionsQueries;
  setFilters: React.Dispatch<React.SetStateAction<PermissionsQueries>>;
}

const PermissionsFiltersAccordion: React.FC<
  PermissionsFiltersAccordionProps
> = ({ defaultIndex, setDefaultIndex, filters, setFilters }) => {
  const { isActive } = useAuth();
  return (
    <Accordion
      hidden={!isActive}
      width="100%"
      defaultIndex={defaultIndex}
      onChange={setDefaultIndex}
      allowMultiple
    >
      <AccordionItem width="100%">
        <h2>
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Filters
            </Box>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel pb={4} width="100%">
          <Stack spacing={1}>
            <Grid gap={1} gridTemplateColumns="repeat(6, 1fr)">
              <FormControl>
                <FormLabel>Name</FormLabel>
                <Input
                  value={filters.name}
                  onChange={(e) => setFilters({ name: e.target.value })}
                  placeholder="Enter name..."
                />
              </FormControl>

              <DateRangePicker
                label="Created at"
                startDate={filters.createdAt?.[0]}
                endDate={filters.createdAt?.[1]}
                onChange={(startDate, endDate) => {
                  setFilters({
                    createdAt: [startDate, endDate],
                  });
                }}
              />

              <DateRangePicker
                label="Updated at"
                startDate={filters.updatedAt?.[0]}
                endDate={filters.updatedAt?.[1]}
                onChange={(startDate, endDate) => {
                  setFilters({
                    updatedAt: [startDate, endDate],
                  });
                }}
              />

              <Stack>
                <CheckboxFormControl
                  checkboxProps={{
                    isChecked: filters.active,
                    onChange: (e) =>
                      setFilters({
                        active: e.target.checked,
                      }),
                  }}
                >
                  Active status
                </CheckboxFormControl>

                <CheckboxFormControl
                  checkboxProps={{
                    isChecked: filters.protected,
                    onChange: (e) =>
                      setFilters({
                        protected: e.target.checked,
                      }),
                  }}
                >
                  Protected status
                </CheckboxFormControl>
              </Stack>

              <UsersAsyncSelect
                label="Ownered by"
                selectedId={filters.owner?.[0]}
                onChange={(nextSelected) => {
                  setFilters({
                    owner: nextSelected ? [nextSelected.value] : undefined,
                  });
                }}
              />
            </Grid>
          </Stack>
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
};

export { PermissionsFiltersAccordion };
