import { Heading, List, ListItem, ListProps, Stack } from '@chakra-ui/react';
import { map } from 'lodash';
import React from 'react';

export interface DataListProps<D extends Record<string, any>>
  extends ListProps {
  header?: string;
  data: D[] | undefined;
  keyBy: keyof D;
  labelBy: keyof D;
}

function DataList<D extends Record<string, any>>(props: DataListProps<D>) {
  const { header, data, labelBy, keyBy, ...rest } = props;
  return (
    <Stack>
      <Heading size="sm" hidden={!header}>
        {header}
      </Heading>
      <List {...rest}>
        {map(data, (item) => {
          return <ListItem key={item[keyBy]}>{item[labelBy]}</ListItem>;
        })}
      </List>
    </Stack>
  );
}

export { DataList };
