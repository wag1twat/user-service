import React from 'react';
import { InlineLoader, InlineLoaderProps } from 'src/client/features';
import { DataList, DataListProps } from './DataList';

export interface LoadingDataListProps<D extends Record<string, any>>
  extends DataListProps<D>,
    Pick<InlineLoaderProps, 'isLoading' | 'error' | 'retry'> {}

function LoadingDataList<D extends Record<string, any>>(
  props: LoadingDataListProps<D>,
) {
  const { isLoading, error, retry, ...rest } = props;
  return (
    <InlineLoader isLoading={isLoading} error={error} retry={retry}>
      <DataList {...rest} />
    </InlineLoader>
  );
}

export { LoadingDataList };
