import { Stack } from '@chakra-ui/react';
import { FormikErrors, FormikHandlers } from 'formik';
import React from 'react';
import {
  CheckboxFormControl,
  FloatingInputFormControl,
  FloatingInputFormControlPassword,
} from 'src/client/shared';
import { CreateUserDto } from 'src/server/controllers/user/dto/create-user.dto';

type CreateUserFormProps = {
  isLoading: boolean;
  errors: FormikErrors<CreateUserDto>;
  values: CreateUserDto;
  handleChange: FormikHandlers['handleChange'];
};

const CreateUserForm: React.FC<CreateUserFormProps> = React.memo(
  ({ isLoading, errors, values, handleChange }) => {
    return (
      <Stack spacing={4} width="full">
        <FloatingInputFormControl
          label="Login"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.login),
          }}
          inputProps={{
            'aria-label': 'Login',
            name: 'login',
            value: values.login ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.login}
        />
        <FloatingInputFormControl
          label="First name"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.firstName),
          }}
          inputProps={{
            'aria-label': 'First name',
            name: 'firstName',
            value: values.firstName ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.firstName}
        />
        <FloatingInputFormControl
          label="Last name"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.lastName),
          }}
          inputProps={{
            'aria-label': 'Last name',
            name: 'lastName',
            value: values.lastName ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.lastName}
        />
        <FloatingInputFormControl
          label="Email"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.email),
          }}
          inputProps={{
            'aria-label': 'Email',
            name: 'email',
            value: values.email ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.email}
        />
        <FloatingInputFormControlPassword
          label="Password"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.password),
          }}
          inputProps={{
            'aria-label': 'Password',
            name: 'password',
            value: values.password ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.password}
        />
        <CheckboxFormControl
          label="Active"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.password),
          }}
          checkboxProps={{
            'aria-label': 'Active',
            name: 'active',
            isChecked: values.active,
            onChange: handleChange,
          }}
          errorMessage={errors.active}
        />
        <CheckboxFormControl
          label="Protected"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.password),
          }}
          checkboxProps={{
            'aria-label': 'Protected',
            name: 'protected',
            isChecked: values.protected,
            onChange: handleChange,
          }}
          errorMessage={errors.protected}
        />
      </Stack>
    );
  },
);

export { CreateUserForm };
