import { Stack } from '@chakra-ui/react';
import { FormikErrors, FormikHandlers } from 'formik';
import React from 'react';
import {
  FloatingInputFormControl,
  FloatingInputFormControlPassword,
} from 'src/client/shared';
import { SignupUserDto } from 'src/server/controllers/user/dto/signup-user.dto';

type SignupUserFormProps = {
  isLoading: boolean;
  errors: FormikErrors<SignupUserDto>;
  values: SignupUserDto;
  handleChange: FormikHandlers['handleChange'];
};

const SignupUserForm: React.FC<SignupUserFormProps> = React.memo(
  ({ isLoading, errors, values, handleChange }) => {
    return (
      <Stack spacing={4} width="full">
        <FloatingInputFormControl
          label="Login"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.login),
          }}
          inputProps={{
            'aria-label': 'Login',
            name: 'login',
            value: values.login ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.login}
        />
        <FloatingInputFormControl
          label="First name"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.firstName),
          }}
          inputProps={{
            'aria-label': 'First name',
            name: 'firstName',
            value: values.firstName ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.firstName}
        />
        <FloatingInputFormControl
          label="Last name"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.lastName),
          }}
          inputProps={{
            'aria-label': 'Last name',
            name: 'lastName',
            value: values.lastName ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.lastName}
        />
        <FloatingInputFormControl
          label="Email"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.email),
          }}
          inputProps={{
            'aria-label': 'Email',
            name: 'email',
            value: values.email ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.email}
        />
        <FloatingInputFormControlPassword
          label="Password"
          controlProps={{
            isRequired: true,
            isInvalid: Boolean(errors.password),
          }}
          inputProps={{
            'aria-label': 'Password',
            name: 'password',
            value: values.password ?? '',
            onChange: handleChange,
            isDisabled: isLoading,
          }}
          errorMessage={errors.password}
        />
      </Stack>
    );
  },
);

export { SignupUserForm };
