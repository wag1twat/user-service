import {
  Heading,
  Stack,
  Skeleton,
  ButtonGroup,
  Grid,
  GridProps,
} from '@chakra-ui/react';

import React from 'react';
import { FormikErrors, FormikHandlers } from 'formik';
import { FloatingInputFormControl, UpdateIconButton } from 'src/client/shared';
import { UpdateUserDto } from 'src/server/controllers/user/dto/update-user.dto';

interface UpdateProfileFormProps {
  header: string | undefined;
  gridTemplateColumns?: GridProps['gridTemplateColumns'];
  preventDisabledFields?: Array<keyof UpdateUserDto>;
  fieldsHelperTexts?: Partial<Record<keyof UpdateUserDto, string>>;
  isLoading: boolean;
  values: UpdateUserDto;
  errors: FormikErrors<UpdateProfileFormProps['values']>;
  handleChange: FormikHandlers['handleChange'];
  handleSubmit: FormikHandlers['handleSubmit'];
}

const UpdateProfileForm: React.FC<UpdateProfileFormProps> = ({
  gridTemplateColumns = '1fr',
  preventDisabledFields = [],
  fieldsHelperTexts = {},
  header,
  values,
  errors,
  handleChange,
  handleSubmit,
  isLoading,
}) => {
  return (
    <Stack as="section" spacing={4} width="100%">
      <Grid gridTemplateColumns={gridTemplateColumns} gap={4} width="100%">
        {header && <Heading size="sm">{header}</Heading>}
        <Skeleton isLoaded={!isLoading}>
          <FloatingInputFormControl
            label="Login"
            errorMessage={errors.login}
            helpMessage={fieldsHelperTexts.login}
            controlProps={{
              id: `profile-login`,
              isRequired: true,
              isInvalid: Boolean(errors.login),
            }}
            inputProps={{
              'aria-label': 'Profile login',
              name: 'login',
              value: values.login ?? '',
              onChange: handleChange,
              isDisabled: preventDisabledFields.includes('login'),
            }}
          />
        </Skeleton>
        <Skeleton isLoaded={!isLoading}>
          <FloatingInputFormControl
            label="First name"
            errorMessage={errors.firstName}
            helpMessage={fieldsHelperTexts.firstName}
            controlProps={{
              id: `profile-first-name`,
              isRequired: true,
              isInvalid: Boolean(errors.firstName),
            }}
            inputProps={{
              'aria-label': 'Profile first name',
              name: 'firstName',
              value: values.firstName ?? '',
              onChange: handleChange,
              isDisabled: preventDisabledFields.includes('firstName'),
            }}
          />
        </Skeleton>
        <Skeleton isLoaded={!isLoading}>
          <FloatingInputFormControl
            label="First name"
            errorMessage={errors.lastName}
            helpMessage={fieldsHelperTexts.lastName}
            controlProps={{
              id: `profile-last-name`,
              isRequired: true,
              isInvalid: Boolean(errors.lastName),
            }}
            inputProps={{
              'aria-label': 'Profile last name',
              name: 'lastName',
              value: values.lastName ?? '',
              onChange: handleChange,
              isDisabled: preventDisabledFields.includes('lastName'),
            }}
          />
        </Skeleton>
        <Skeleton isLoaded={!isLoading}>
          <FloatingInputFormControl
            label="Email"
            errorMessage={errors.email}
            helpMessage={fieldsHelperTexts.email}
            controlProps={{
              id: `profile-email`,
              isRequired: true,
              isInvalid: Boolean(errors.email),
            }}
            inputProps={{
              'aria-label': 'Profile email',
              name: 'email',
              value: values.email ?? '',
              onChange: handleChange,
              isDisabled: preventDisabledFields.includes('email'),
            }}
          />
        </Skeleton>
      </Grid>
      <ButtonGroup justifyContent="flex-end">
        <Skeleton width="fit-content" isLoaded={!isLoading}>
          <UpdateIconButton
            isLoading={isLoading}
            isDisabled={isLoading}
            onClick={() => handleSubmit()}
            label="Update"
            aria-label="Update"
          />
        </Skeleton>
      </ButtonGroup>
    </Stack>
  );
};

export { UpdateProfileForm };
