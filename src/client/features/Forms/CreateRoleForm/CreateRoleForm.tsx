import { Grid, Stack } from '@chakra-ui/react';
import { Checkbox, Input } from '@chakra-ui/react';
import { FormikErrors, FormikHandlers } from 'formik';
import { map } from 'lodash';
import React from 'react';
import {
  CheckboxFormControl,
  CheckboxGroupFormControl,
  FloatingInputFormControl,
  useDebounce,
} from 'src/client/shared';
import { useUsers } from 'src/client/entities/users/model';
import { usePermissions } from 'src/client/entities/permissions/model';
import { ParseSerializeException } from 'src/shared/helpers';
import { InlineLoader } from 'src/client/features';
import { CreateRoleDto } from 'src/server/controllers/role/dto/create-role.dto';

type CreateRoleFormProps = {
  isOpen: boolean;
  isLoading: boolean;
  errors: FormikErrors<CreateRoleDto>;
  values: CreateRoleDto;
  handleChange: FormikHandlers['handleChange'];
  handleChangeCheckboxes: (
    fieldName: keyof CreateRoleDto,
    value: (string | number)[],
  ) => void;
  isDisabled?: boolean;
};

const CreateRoleForm: React.FC<CreateRoleFormProps> = React.memo(
  ({
    isOpen,
    isLoading,
    isDisabled,
    errors,
    values,
    handleChange,
    handleChangeCheckboxes,
  }) => {
    const [userLogin, setUserLogin] = React.useState('');

    const debouncedUserLogin = useDebounce(userLogin, 200);

    const users = useUsers(['add-role-users', debouncedUserLogin], {
      enabled: isOpen,
      resetOnSuccess: !isOpen,
      params: { login: debouncedUserLogin },
    });

    const [permissionName, setPermissionName] = React.useState('');

    const debouncedPermissionName = useDebounce(permissionName, 200);

    const permissions = usePermissions(
      ['add-role-permissions', debouncedPermissionName],
      {
        enabled: isOpen,
        resetOnSuccess: !isOpen,
        params: { name: debouncedPermissionName },
      },
    );

    const usersOptions = React.useMemo(
      () =>
        map(users.data?.data, ({ id, login }) => ({ label: login, value: id })),
      [users.data],
    );

    const permissionsOptions = React.useMemo(
      () =>
        map(permissions.data?.data, ({ id, name }) => ({
          label: name,
          value: id,
        })),
      [permissions.data],
    );
    return (
      <Stack width="full" my={2} spacing={4}>
        <FloatingInputFormControl
          label="Name"
          controlProps={{
            id: 'create-role-name',
            isRequired: true,
            isInvalid: Boolean(errors.name),
          }}
          inputProps={{
            'aria-label': 'Name',
            name: 'name',
            value: values.name ?? '',
            onChange: handleChange,
            isDisabled: isLoading || Boolean(isDisabled),
          }}
          errorMessage={errors.name}
        />
        <Grid gap={4} gridTemplateColumns="repeat(2, 1fr)">
          <Input
            placeholder="Permission name..."
            value={permissionName}
            onChange={(e) => setPermissionName(e.target.value)}
            isDisabled={isLoading || Boolean(isDisabled)}
          />
          <Input
            placeholder="User login..."
            value={userLogin}
            onChange={(e) => setUserLogin(e.target.value)}
            isDisabled={isLoading || Boolean(isDisabled)}
          />
          <CheckboxFormControl
            label="Active"
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(errors.active),
            }}
            checkboxProps={{
              'aria-label': 'Active',
              name: 'active',
              isChecked: values.active,
              onChange: handleChange,
            }}
            errorMessage={errors.active}
          />
          <CheckboxFormControl
            label="Protected"
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(errors.protected),
            }}
            checkboxProps={{
              'aria-label': 'Protected',
              name: 'protected',
              isChecked: values.protected,
              onChange: handleChange,
            }}
            errorMessage={errors.protected}
          />
        </Grid>

        <Grid gap={4} gridTemplateColumns="repeat(2, 1fr)">
          <InlineLoader
            isLoading={permissions.isLoading}
            error={ParseSerializeException.getString(permissions.error)}
            retry={permissions.refetch}
            height="fit-content"
          >
            <CheckboxGroupFormControl
              controlProps={{ px: 4 }}
              label="Permissions"
              checkboxGroupProps={{
                isDisabled: isLoading || Boolean(isDisabled),
                value: values.permissionsIds,
                onChange: (value) =>
                  handleChangeCheckboxes('permissionsIds', value),
              }}
            >
              <Stack px={4} spacing={4} maxH="750px" overflowY="scroll">
                {permissionsOptions.map((permission) => {
                  return (
                    <Checkbox
                      isDisabled={isLoading || Boolean(isDisabled)}
                      key={permission.value}
                      value={permission.value}
                    >
                      {permission.label}
                    </Checkbox>
                  );
                })}
              </Stack>
            </CheckboxGroupFormControl>
          </InlineLoader>
          <InlineLoader
            isLoading={users.isLoading}
            error={ParseSerializeException.getString(users.error)}
            retry={users.refetch}
            height="fit-content"
          >
            <CheckboxGroupFormControl
              controlProps={{ px: 4 }}
              label="Users"
              checkboxGroupProps={{
                isDisabled: isLoading || Boolean(isDisabled),
                value: values.usersIds,
                onChange: (value) => handleChangeCheckboxes('usersIds', value),
              }}
            >
              <Stack px={4} spacing={4} maxH="750px" overflowY="scroll">
                {usersOptions.map((user) => {
                  return (
                    <Checkbox
                      isDisabled={isLoading || Boolean(isDisabled)}
                      key={user.value}
                      value={user.value}
                    >
                      {user.label}
                    </Checkbox>
                  );
                })}
              </Stack>
            </CheckboxGroupFormControl>
          </InlineLoader>
        </Grid>
      </Stack>
    );
  },
);

export { CreateRoleForm };
