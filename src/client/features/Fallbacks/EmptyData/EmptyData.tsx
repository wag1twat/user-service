import React from 'react';
import { Badge } from '@chakra-ui/react';

type EmptyDataProps = { isEmpty: boolean; message: string };

const EmptyData: React.FC<EmptyDataProps> = React.memo(
  ({ isEmpty, message, children }) => {
    if (isEmpty) {
      return <Badge colorScheme="red">{message}</Badge>;
    }

    return <>{children}</>;
  },
);

export { EmptyData };
