import { Text, TextProps } from '@chakra-ui/react';
import React from 'react';

type WarningTextProps = TextProps;

const WarningText: React.FC<WarningTextProps> = (props) => {
  return <Text {...props} color="orange.500" />;
};

export { WarningText };
