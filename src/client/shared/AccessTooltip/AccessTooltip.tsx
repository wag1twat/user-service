import { CloseCircleOutlined } from '@ant-design/icons';
import {
  List,
  ListIcon,
  ListItem,
  Tooltip,
  TooltipProps,
} from '@chakra-ui/react';
import { concat, get } from 'lodash';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { CaslPermissionsActionKeysOrString } from 'src/server/casl/casl.permissions';

export interface Accessibility {
  isDisabled: boolean;
  isLoading: boolean;
  message: string;
}

export interface Accessibilities {
  accessibilities?: Accessibility[];
}

interface AvailabesPermissions {
  availables?: CaslPermissionsActionKeysOrString[];
}

interface AccessTooltipProps
  extends TooltipProps,
    Accessibilities,
    AvailabesPermissions {
  children: React.FC<{
    isLoadingAccessibilities: boolean;
    isDisabledAccessibilities: boolean;
  }>;
}

const Messages: React.FC<Accessibilities> = ({ accessibilities = [] }) => {
  return (
    <List>
      {accessibilities
        .filter((accessibility) => accessibility.isDisabled)
        .map((accessibility, i) => {
          return (
            <ListItem key={i}>
              <ListIcon
                verticalAlign="middle"
                as={CloseCircleOutlined}
                color="red.500"
              />
              {accessibility.message}
            </ListItem>
          );
        })}
    </List>
  );
};

const AccessTooltip: React.FC<AccessTooltipProps> = React.memo(
  ({ accessibilities = [], availables = [], children, ...props }) => {
    const { permissions } = useAuth();

    const permissionsAccessibilities = React.useMemo<Accessibility[]>(() => {
      return availables.map((available) => {
        return {
          isLoading: Boolean(permissions?.isLoading),
          isDisabled: !Boolean(get(permissions?.availables, available)),
          message: `You doesn't have permission [${available}]`,
          key: available,
        };
      });
    }, [availables, permissions?.availables, permissions?.isLoading]);

    const mergedAccessibilities = React.useMemo(
      () => concat(accessibilities, permissionsAccessibilities),
      [accessibilities, permissionsAccessibilities],
    );

    return (
      <Tooltip
        isDisabled={mergedAccessibilities.every(
          (accessibility) => !accessibility.isDisabled,
        )}
        label={<Messages accessibilities={mergedAccessibilities} />}
        placement="auto"
        {...props}
      >
        {children({
          isLoadingAccessibilities: mergedAccessibilities.some(
            (accessibility) => accessibility.isLoading,
          ),
          isDisabledAccessibilities: mergedAccessibilities.some(
            (accessibility) => accessibility.isDisabled,
          ),
        })}
      </Tooltip>
    );
  },
);

export { AccessTooltip };
