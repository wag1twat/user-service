import React from 'react';
import { Box, BoxProps } from '@chakra-ui/react';

export const Card = (props: BoxProps) => {
  return <Box borderWidth={1} rounded="sm" bg="white" p={4} {...props} />;
};
