import { Text } from '@chakra-ui/react';
import React from 'react';
import { RouterLink } from '../RouterLink';
import { useRoutes } from '../hooks';
import { User } from 'src/server/entities/client.types';

type OwnerCellProps = { owner: User | null };

const OwnerCell: React.FC<OwnerCellProps> = React.memo(({ owner }) => {
  const { user } = useRoutes();

  return (
    <>
      {owner && (
        <RouterLink
          p={0}
          variant="link"
          colorScheme="blue"
          href={user(owner.id)}
        >
          {owner.login}
        </RouterLink>
      )}
      {!owner && <Text>system</Text>}
    </>
  );
});

export { OwnerCell };
