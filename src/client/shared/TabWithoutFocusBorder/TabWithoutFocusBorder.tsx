import { Tab, TabProps } from '@chakra-ui/react';
import React from 'react';

const TabWithoutFocusBorder: React.FC<TabProps> = (props) => {
  return (
    <Tab
      _focus={{
        boxShadow: 'none',
      }}
      {...props}
    />
  );
};

export { TabWithoutFocusBorder };
