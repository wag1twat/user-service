import { ButtonProps, Button } from '@chakra-ui/react';
import { Skeleton } from '@chakra-ui/react';
import React from 'react';
import { AccessTooltip } from 'src/client/shared';
import { CaslPermissionsActionKeysOrString } from 'src/server/casl/casl.permissions';
import { Accessibilities } from '../AccessTooltip';

interface ToggleActionButtonProps extends Accessibilities {
  isLoading: boolean;
  isActive: boolean;
  isLoadingAction: boolean;
  isLoadingRevert: boolean;
  Action: () => void;
  Revert: () => void;
  ActionName: string;
  RevertName: string;
  ActionIcon:
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | undefined;
  RevertIcon:
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | undefined;

  availables: CaslPermissionsActionKeysOrString[];
}

const ToggleActionButton: React.FC<ToggleActionButtonProps> = React.memo(
  ({
    isActive,
    isLoading,
    Action,
    ActionIcon,
    ActionName,
    isLoadingAction,
    Revert,
    RevertIcon,
    RevertName,
    isLoadingRevert,
    availables,
    accessibilities,
  }) => {
    const getProps = React.useCallback<(props: ButtonProps) => ButtonProps>(
      (props) => {
        return {
          isLoading: isLoadingAction || isLoadingRevert || props.isLoading,
          isDisabled: isLoading || props.isDisabled,
          'aria-label': isActive ? RevertName : ActionName,
          children: isActive ? RevertName : ActionName,
          rightIcon: isActive ? RevertIcon : ActionIcon,
          onClick: isActive ? Revert : Action,
          colorScheme: isActive ? 'red' : 'blue',
          variant: isActive ? 'outline' : 'solid',
        };
      },
      [
        Action,
        ActionIcon,
        ActionName,
        Revert,
        RevertIcon,
        RevertName,
        isActive,
        isLoading,
        isLoadingAction,
        isLoadingRevert,
      ],
    );
    return (
      <AccessTooltip
        availables={availables}
        accessibilities={accessibilities}
        shouldWrapChildren
      >
        {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
          <Skeleton width="100%" isLoaded={!isLoadingAccessibilities}>
            <Button
              {...getProps({
                isLoading: isLoadingAccessibilities,
                isDisabled: isDisabledAccessibilities,
              })}
            />
          </Skeleton>
        )}
      </AccessTooltip>
    );
  },
);

export { ToggleActionButton };
