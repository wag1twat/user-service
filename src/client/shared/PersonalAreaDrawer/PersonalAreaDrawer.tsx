import {
  Button,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerHeader,
  DrawerCloseButton,
  DrawerFooter,
  DrawerBody,
  ButtonGroup,
  DrawerProps,
} from '@chakra-ui/react';
import React from 'react';
import { Disclosure } from 'src/client/shared/types';

interface PersonalAreaDrawerProps {
  header: string;
  cancelText?: string;
  confirmText: string;
  disclosure?: Disclosure;
  onConfirm?: () => void;
  isConfirmLoading: boolean;
  isDisabledConfirm?: boolean;
  size?: DrawerProps['size'];
}

export function PersonalAreaDrawer(
  props: React.PropsWithChildren<PersonalAreaDrawerProps>,
) {
  const {
    disclosure,
    header,
    confirmText,
    cancelText = 'Cancel',
    children,
    onConfirm,
    isConfirmLoading,
    isDisabledConfirm,
    size,
  } = props;

  return (
    <>
      <Drawer
        isOpen={Boolean(disclosure?.isOpen)}
        placement="right"
        onClose={disclosure?.onClose ?? (() => void 0)}
        size={size}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>{header}</DrawerHeader>
          <DrawerBody paddingTop={0} marginTop={4}>
            {children}
          </DrawerBody>
          <DrawerFooter>
            <ButtonGroup>
              <Button variant="outline" mr={3} onClick={disclosure?.onClose}>
                {cancelText}
              </Button>
              <Button
                colorScheme="blue"
                onClick={onConfirm}
                isLoading={isConfirmLoading}
                isDisabled={isDisabledConfirm}
              >
                {confirmText}
              </Button>
            </ButtonGroup>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
}
