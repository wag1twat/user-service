import { useLocalStorageTableAccordionFilterSettings } from './table-filters-accordion-settings.manager';
import { useLocalStorageTableSettingsSettings } from './table-settings.manager';

const useLocalStorageManager = (key: string) => {
  const filter = useLocalStorageTableAccordionFilterSettings(
    `${key}-filter-settings`,
  );
  const table = useLocalStorageTableSettingsSettings(`${key}-table-settings`);

  return { filter, table };
};

export { useLocalStorageManager };
