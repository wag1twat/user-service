import { has } from 'lodash';
import { useLocalStorage } from './useLocalStorage';
import { isClient } from 'src/shared/constants';

interface TableSettings {
  limit: number;
}

const isTableSettings = (settings: unknown): settings is TableSettings => {
  return has(settings, 'limit');
};

const defaultSettings: TableSettings = {
  limit: 5,
};

const useLocalStorageTableSettingsSettings = (
  key: `${string}-table-settings`,
) => {
  const [settings, setSettings] = useLocalStorage(key, (key) => {
    if (isClient) {
      const json = globalThis.window.localStorage.getItem(key);

      if (json === null) {
        return defaultSettings;
      }

      const parsedSettings = JSON.parse(json);

      if (isTableSettings(parsedSettings)) {
        return parsedSettings;
      }

      return defaultSettings;
    }

    return defaultSettings;
  });

  return { settings, setSettings };
};

export { useLocalStorageTableSettingsSettings };
export type { TableSettings };
