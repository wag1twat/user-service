import { ExpandedIndex } from '@chakra-ui/react';
import { has } from 'lodash';
import { useLocalStorage } from './useLocalStorage';
import { isClient } from 'src/shared/constants';

interface AccordionFilterSettings {
  defaultIndex: ExpandedIndex;
}

const isAccordionFilterSettings = (
  settings: unknown,
): settings is { defaultIndex: ExpandedIndex } => {
  return has(settings, 'defaultIndex');
};

const defaultSettings: AccordionFilterSettings = {
  defaultIndex: [],
};

const useLocalStorageTableAccordionFilterSettings = (
  key: `${string}-filter-settings`,
) => {
  const [settings, setSettings] = useLocalStorage(key, (key) => {
    if (isClient) {
      const json = globalThis.window.localStorage.getItem(key);

      if (json === null) {
        return defaultSettings;
      }

      const parsedSettings = JSON.parse(json);

      if (isAccordionFilterSettings(parsedSettings)) {
        return parsedSettings;
      }

      return defaultSettings;
    }

    return defaultSettings;
  });

  return { settings, setSettings };
};

export { useLocalStorageTableAccordionFilterSettings };
export type { AccordionFilterSettings };
