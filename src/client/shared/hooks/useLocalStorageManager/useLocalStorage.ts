import React from 'react';

export const useLocalStorage = <K extends string = string, Value = unknown>(
  key: K,
  getter: (key: K) => Value,
) => {
  const [value, setValue] = React.useState<Value>(() => {
    return getter(key);
  });

  React.useEffect(() => {
    globalThis.window.localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue] as const;
};
