import { useToast, UseToastOptions } from '@chakra-ui/react';
import React from 'react';

const useUIToasts = (options?: UseToastOptions) => {
  const toast = useToast(
    Object.assign({ isClosable: true, position: 'top' }, options),
  );

  const toastes = React.useMemo(
    () => ({
      success: (title?: string) =>
        toast({
          title,
          status: 'success',
        }),
      error: (title?: string) => toast({ title, status: 'error' }),
      warning: (title?: string) =>
        toast({
          title,
          status: 'warning',
        }),
      info: (title?: string) => toast({ title, status: 'info' }),
    }),
    [toast],
  );

  return toastes;
};

export { useUIToasts };
