import React from 'react';
import { DateTimeUtils } from 'src/shared/helpers';

export const useTimeInSystem = (createdAt?: string) => {
  const inSystem = React.useMemo(() => {
    if (createdAt) {
      const { days, hours, minutes } =
        DateTimeUtils.diffObjectFromNow(createdAt);

      return { days, hours, minutes };
    }
    return { days: 0, hours: 0, minutes: 0 };
  }, [createdAt]);

  const keys = Object.keys(inSystem) as Array<keyof typeof inSystem>;

  if (keys.length === 0) return '';

  return keys
    .filter((key) => Number(inSystem[key]) > 0)
    .map((key) => `${inSystem[key]} ${key}`)
    .join(' ');
};
