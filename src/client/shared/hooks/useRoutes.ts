import { useRouter } from 'next/router';
import React from 'react';
import { User } from 'src/server/entities/client.types';

export const useRoutes = () => {
  const router = useRouter();

  const user = (id: User['id']) => `/users/${id}`;

  const goUser = React.useCallback(
    (id: User['id']) => router.push(user(id)),
    [router],
  );

  return { user, goUser };
};
