import { useDisclosure } from '@chakra-ui/react';
import { useCallback, useState } from 'react';

const useStateDisclosure = <State = unknown>() => {
  const [state, setState] = useState<State>();

  const { isOpen, ...disclosure } = useDisclosure();

  const onOpenWithState = useCallback(
    (nextState: State) => {
      setState(nextState);
      disclosure.onOpen();
    },
    [disclosure],
  );

  const onCloseWithReset = useCallback(() => {
    setState(undefined);
    disclosure.onClose();
  }, [disclosure]);

  return {
    state,
    isOpen,
    onOpenWithState,
    onOpen: disclosure.onOpen,
    onClose: disclosure.onClose,
    onCloseWithReset,
    setState,
  };
};

export { useStateDisclosure };
