import React from 'react';

const forceUpdateReducer = (i: number) => i + 1;

export const useForceUpdate = () => {
  const [, forceUpdate] = React.useReducer(forceUpdateReducer, 0);

  return forceUpdate;
};
