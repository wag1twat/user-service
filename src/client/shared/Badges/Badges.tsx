import { Badge, Flex, Skeleton, Text } from '@chakra-ui/react';
import React from 'react';
import { DateTimeUtils } from 'src/shared/helpers/datetime.utils';
import { useTimeInSystem } from '../hooks';

interface BadgeWrapperProps {
  label: string;
  isLoaded: boolean;
}
const BadgeWrapper = (props: React.PropsWithChildren<BadgeWrapperProps>) => {
  return (
    <Flex justifyContent="space-between" alignItems="center">
      <Badge>{props.label}</Badge>
      <Skeleton isLoaded={props.isLoaded} minH="20px" minW="200px">
        {props.children}
      </Skeleton>
    </Flex>
  );
};

interface InSystemTimeBadgeProps {
  createdAt?: string;
  isLoaded: boolean;
}

export const InSystemTimeBadge = ({
  isLoaded,
  createdAt,
}: React.PropsWithChildren<InSystemTimeBadgeProps>) => {
  const timeInSystem = useTimeInSystem(createdAt);
  return (
    <BadgeWrapper label="In system time" isLoaded={isLoaded}>
      <Text textAlign="end">{timeInSystem}</Text>
    </BadgeWrapper>
  );
};

interface LastUpdatedBadgeProps {
  updatedAt?: string;
  isLoaded: boolean;
}
export const LastUpdatedBadge = ({
  isLoaded,
  updatedAt,
}: React.PropsWithChildren<LastUpdatedBadgeProps>) => {
  return (
    <BadgeWrapper label="Last updated" isLoaded={isLoaded}>
      {updatedAt && (
        <Text textAlign="end">{DateTimeUtils.toDateTime(updatedAt)}</Text>
      )}
    </BadgeWrapper>
  );
};

interface IsActiveBadgeProps {
  isActive: boolean;
  isLoaded: boolean;
}
export const IsActiveBadge = ({
  isLoaded,
  isActive,
}: React.PropsWithChildren<IsActiveBadgeProps>) => {
  return (
    <BadgeWrapper label="Is active" isLoaded={isLoaded}>
      <Flex justifyContent="flex-end">
        <BooleanBadge is={isActive} />
      </Flex>
    </BadgeWrapper>
  );
};

interface BooleanBadgeProps {
  is: boolean;
}

export const BooleanBadge = ({ is }: BooleanBadgeProps) => {
  return (
    <Badge colorScheme={is ? 'green' : 'red'}>{is ? 'true' : 'false'}</Badge>
  );
};
