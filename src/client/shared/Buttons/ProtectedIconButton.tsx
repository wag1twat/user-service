import { SafetyCertificateOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const ProtectedIcon = SafetyCertificateOutlined;

const ProtectedIconButton = React.memo(
  ({ label, colorScheme = 'blue', ...props }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<ProtectedIcon />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} {...props}>
        <ProtectedIcon />
      </IconButton>
    );
  },
);

export { ProtectedIconButton, ProtectedIcon };
