import { IconButtonProps as CoreIconButtonProps } from '@chakra-ui/react';

interface IconButtonProps extends Omit<CoreIconButtonProps, 'children'> {
  label?: string;
}

export type { IconButtonProps };
