import { PlayCircleOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const ActivateIcon = PlayCircleOutlined;

const ActivateIconButton = React.memo(
  ({ label, colorScheme = 'blue', ...props }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<ActivateIcon />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} {...props}>
        <ActivateIcon />
      </IconButton>
    );
  },
);

export { ActivateIconButton, ActivateIcon };
