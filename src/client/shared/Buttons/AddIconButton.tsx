import { FolderAddOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const AddIconButton = React.memo(
  ({ label, colorScheme = 'blue', ...props }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<FolderAddOutlined />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} {...props}>
        <FolderAddOutlined />
      </IconButton>
    );
  },
);

export { AddIconButton };
