import { SaveOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const SaveIconButton = React.memo(
  ({ label, colorScheme = 'blue', ...props }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<SaveOutlined />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} {...props}>
        <SaveOutlined />
      </IconButton>
    );
  },
);

export { SaveIconButton };
