import { FormOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const UpdateIconButton = React.memo(
  ({ label, colorScheme = 'blue', ...props }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<FormOutlined />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} {...props}>
        <FormOutlined />
      </IconButton>
    );
  },
);

export { UpdateIconButton };
