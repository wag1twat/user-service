import { DeleteOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const DeleteIconButton = React.memo(
  ({
    label,
    colorScheme = 'red',
    variant = 'outline',
    ...props
  }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          variant={variant}
          rightIcon={<DeleteOutlined />}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} variant={variant} {...props}>
        <DeleteOutlined />
      </IconButton>
    );
  },
);

export { DeleteIconButton };
