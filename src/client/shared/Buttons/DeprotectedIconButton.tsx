import { StopOutlined } from '@ant-design/icons';
import { Button, IconButton } from '@chakra-ui/react';
import React from 'react';
import { IconButtonProps } from './types';

const DeprotectedIcon = StopOutlined;

const DeprotectedIconButton = React.memo(
  ({
    label,
    colorScheme = 'red',
    variant = 'outline',
    ...props
  }: IconButtonProps) => {
    if (label) {
      return (
        <Button
          colorScheme={colorScheme}
          rightIcon={<DeprotectedIcon />}
          variant={variant}
          {...props}
        >
          {label}
        </Button>
      );
    }

    return (
      <IconButton colorScheme={colorScheme} variant={variant} {...props}>
        <DeprotectedIcon />
      </IconButton>
    );
  },
);

export { DeprotectedIconButton, DeprotectedIcon };
