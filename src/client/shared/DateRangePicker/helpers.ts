import { DateTime } from 'luxon';
import { DateRangePickerProps } from './DateRangePicker';

export const validateDateRange = (
  range?: [DateRangePickerProps['startDate'], DateRangePickerProps['endDate']],
): typeof range => {
  if (range) {
    const [startDate, endDate] = range;

    const isValid = Boolean(
      [startDate, endDate].filter(
        (date) => date && DateTime.fromISO(date).isValid,
      ).length,
    );

    if (isValid) {
      return [startDate, endDate];
    }
  }

  return undefined;
};
