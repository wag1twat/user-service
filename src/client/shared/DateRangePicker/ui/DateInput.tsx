import { As, forwardRef, Input, InputProps } from '@chakra-ui/react';
import React from 'react';

const DateInput = forwardRef<InputProps, As<typeof Input>>(
  ({ value, onClick, ...props }, ref) => {
    return (
      <Input ref={ref} value={value} onClick={onClick} readOnly {...props} />
    );
  },
);

export { DateInput };
