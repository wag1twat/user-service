import { CaretLeftOutlined, CaretRightOutlined } from '@ant-design/icons';
import { Flex, IconButton, Stack, Text } from '@chakra-ui/react';
import { DateTime } from 'luxon';
import React from 'react';
import { ReactDatePickerCustomHeaderProps } from 'react-datepicker';

const Header: React.FC<ReactDatePickerCustomHeaderProps> = React.memo(
  ({ monthDate, decreaseYear, decreaseMonth, increaseYear, increaseMonth }) => {
    return (
      <Stack spacing={1}>
        <Flex alignItems="center" justifyContent="space-between">
          <IconButton aria-label="Prev month" size="sm" onClick={decreaseMonth}>
            <CaretLeftOutlined />
          </IconButton>
          <Text>{DateTime.fromJSDate(monthDate).toFormat('d LLLL')}</Text>
          <IconButton aria-label="Next month" size="sm" onClick={increaseMonth}>
            <CaretRightOutlined />
          </IconButton>
        </Flex>
        <Flex alignItems="center" justifyContent="space-between">
          <IconButton aria-label="Prev month" size="sm" onClick={decreaseYear}>
            <CaretLeftOutlined />
          </IconButton>
          <Text>{DateTime.fromJSDate(monthDate).toFormat('yyyy')}</Text>
          <IconButton aria-label="Next month" size="sm" onClick={increaseYear}>
            <CaretRightOutlined />
          </IconButton>
        </Flex>
      </Stack>
    );
  },
);

export { Header };
