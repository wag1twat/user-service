import {
  Box,
  FormControl,
  FormLabel /* useColorMode */,
  Stack,
} from '@chakra-ui/react';
import DatePicker from 'react-datepicker';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { DateTimeUtils } from 'src/shared/helpers';
import { DateInput, Header } from './ui';
// TODO: theming

export interface DateRangePickerProps {
  label?: string;
  showTimeSelect?: boolean;
  timeIntervals?: number;
  startDate: string | undefined;
  endDate: string | undefined;
  onChange(
    startDate: DateRangePickerProps['startDate'],
    endDate: DateRangePickerProps['endDate'],
  ): void;
}

export function DateRangePicker({
  label,
  showTimeSelect = true,
  timeIntervals = 15,
  startDate,
  endDate,
  onChange,
}: DateRangePickerProps) {
  return (
    <FormControl position="static">
      {label && <FormLabel>{label}</FormLabel>}
      <Stack spacing={1}>
        <DatePicker
          dateFormat="MMMM d, yyyy h:mm aa"
          showTimeSelect={showTimeSelect}
          timeIntervals={timeIntervals}
          isClearable
          shouldCloseOnSelect
          selected={startDate ? DateTimeUtils.fromISOtoJSDate(startDate) : null}
          onChange={(date) => {
            onChange(
              date ? DateTimeUtils.fromJStoISO(date) : undefined,
              endDate,
            );
          }}
          selectsStart
          startDate={
            startDate ? DateTimeUtils.fromISOtoJSDate(startDate) : null
          }
          endDate={endDate ? DateTimeUtils.fromISOtoJSDate(endDate) : null}
          maxDate={endDate ? DateTimeUtils.fromISOtoJSDate(endDate) : null}
          showPopperArrow={false}
          placeholderText="Start date..."
          customInput={<DateInput />}
          popperContainer={(props) => (
            <Box
              {...props}
              __css={{
                '.react-datepicker-popper': {
                  zIndex: 4,
                },
              }}
            />
          )}
          calendarContainer={(props) => <Box {...props} />}
          renderCustomHeader={(props) => <Header {...props} />}
        />
        <DatePicker
          dateFormat="MMMM d, yyyy h:mm aa"
          showTimeSelect={showTimeSelect}
          timeIntervals={timeIntervals}
          isClearable
          shouldCloseOnSelect
          selected={endDate ? DateTimeUtils.fromISOtoJSDate(endDate) : null}
          onChange={(date) => {
            onChange(
              startDate,
              date ? DateTimeUtils.fromJStoISO(date) : undefined,
            );
          }}
          selectsEnd
          startDate={
            startDate ? DateTimeUtils.fromISOtoJSDate(startDate) : null
          }
          endDate={endDate ? DateTimeUtils.fromISOtoJSDate(endDate) : null}
          minDate={startDate ? DateTimeUtils.fromISOtoJSDate(startDate) : null}
          showPopperArrow={false}
          placeholderText="End date..."
          customInput={<DateInput />}
          popperContainer={(props) => (
            <Box
              {...props}
              __css={{
                '.react-datepicker-popper': {
                  zIndex: 4,
                },
              }}
            />
          )}
          calendarContainer={(props) => <Box {...props} />}
          renderCustomHeader={(props) => <Header {...props} />}
        />
      </Stack>
    </FormControl>
  );
}
