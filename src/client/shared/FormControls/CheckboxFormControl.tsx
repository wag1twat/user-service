import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
  FormHelperText,
  CheckboxProps,
  Checkbox,
} from '@chakra-ui/react';

export type CheckboxFormControlProps = {
  controlProps?: FormControlProps;
  checkboxProps?: CheckboxProps;
  label?: string;
  errorMessage?: string;
  helpMessage?: string;
};

const CheckboxFormControl: React.FC<CheckboxFormControlProps> = React.memo(
  ({
    controlProps,
    checkboxProps,
    label,
    errorMessage,
    helpMessage,
    children,
  }) => {
    return (
      <FormControl {...controlProps}>
        {label && <FormLabel>{label}</FormLabel>}
        <Checkbox {...checkboxProps}>{children}</Checkbox>
        <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>
        <FormHelperText px={4}>{helpMessage}</FormHelperText>
      </FormControl>
    );
  },
);

export { CheckboxFormControl };
