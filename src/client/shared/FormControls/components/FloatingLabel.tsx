import {
  FormLabel,
  FormLabelProps,
  InputProps,
  useColorModeValue,
  useTheme,
} from '@chakra-ui/react';
import React from 'react';

interface FloatingLabelProps extends FormLabelProps {
  inputSize: InputProps['size'];
}

const labelProps = (
  size: string | 'xs' | 'sm' | 'md' | 'lg',
): FormLabelProps => {
  const my: Record<string | 'xs' | 'sm' | 'md' | 'lg', number> = {
    lg: 3,
    md: 2,
    sm: 1,
    xs: 0.5,
  };

  return {
    top: 0,
    left: 0,
    zIndex: 2,
    position: 'absolute',
    pointerEvents: 'none',
    mx: 3,
    px: 1,
    my: my[size],
    transformOrigin: 'left top',
  };
};

const FloatingLabel: React.FC<FloatingLabelProps> = React.memo(
  ({ inputSize, ...props }) => {
    const bgLabel = useColorModeValue('white', 'gray.700');

    const theme = useTheme();

    const size = React.useMemo<Exclude<InputProps['size'], undefined>>(() => {
      return inputSize || theme.components.Input.defaultProps.size;
    }, [inputSize, theme.components.Input.defaultProps.size]);

    return <FormLabel bg={bgLabel} {...labelProps(size)} {...props} />;
  },
);

export { FloatingLabel };
