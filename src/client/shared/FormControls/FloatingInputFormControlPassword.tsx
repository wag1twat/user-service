import React from 'react';
import {
  FormControl,
  FormErrorMessage,
  Input,
  InputRightElement,
  IconButton,
  FormHelperText,
  Flex,
} from '@chakra-ui/react';
import { EyeOutlined } from '@ant-design/icons';
import { FloatingInputFormControlProps } from './FloatingInputFormControl';
import { FloatingLabel } from './components';

const FloatingInputFormControlPassword: React.FC<FloatingInputFormControlProps> =
  React.memo(
    ({ controlProps, inputProps, label, helpMessage, errorMessage }) => {
      const [type, setType] = React.useState<'password' | 'text'>('password');

      const handlePasswordVisibility = React.useCallback(() => {
        if (type === 'password') {
          setType('text');
        }
        if (type === 'text') {
          setType('password');
        }
      }, [type]);
      return (
        <Flex
          __css={{
            '.chakra-form-control': {
              'input:not(:placeholder-shown) + label, .chakra-select__wrapper + label':
                {
                  transform: 'scale(0.85) translateY(-24px)',
                },
            },
          }}
        >
          <FormControl
            variant="floating"
            {...controlProps}
            _focusWithin={{
              label: {
                transform: 'scale(0.85) translateY(-24px)',
              },
            }}
          >
            <Input type={type} placeholder=" " {...inputProps} />
            <FloatingLabel inputSize={inputProps?.size}>{label}</FloatingLabel>
            <InputRightElement>
              <IconButton
                aria-label="Toggle password visibility"
                onClick={handlePasswordVisibility}
                isDisabled={inputProps?.isDisabled}
              >
                <EyeOutlined />
              </IconButton>
            </InputRightElement>
            <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>{' '}
            <FormHelperText px={4}>{helpMessage}</FormHelperText>
          </FormControl>
        </Flex>
      );
    },
  );

export { FloatingInputFormControlPassword };
