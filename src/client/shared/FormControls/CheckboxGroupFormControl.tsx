import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
  FormHelperText,
  CheckboxGroupProps,
  CheckboxGroup,
} from '@chakra-ui/react';

export type CheckboxGroupFormControlProps = {
  controlProps?: FormControlProps;
  checkboxGroupProps?: CheckboxGroupProps;
  label: string;
  errorMessage?: string;
  helpMessage?: string;
};

const CheckboxGroupFormControl: React.FC<CheckboxGroupFormControlProps> =
  React.memo(
    ({
      controlProps,
      checkboxGroupProps,
      label,
      errorMessage,
      helpMessage,
      children,
    }) => {
      return (
        <FormControl {...controlProps}>
          <FormLabel>{label}</FormLabel>
          <CheckboxGroup {...checkboxGroupProps}>{children}</CheckboxGroup>
          <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>
          <FormHelperText px={4}>{helpMessage}</FormHelperText>
        </FormControl>
      );
    },
  );

export { CheckboxGroupFormControl };
