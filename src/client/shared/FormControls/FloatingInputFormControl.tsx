import React from 'react';
import {
  FormControl,
  FormErrorMessage,
  Input,
  FormControlProps,
  InputProps,
  FormHelperText,
  Flex,
} from '@chakra-ui/react';
import { FloatingLabel } from './components';

export type FloatingInputFormControlProps = {
  controlProps?: Omit<FormControlProps, 'variant'>;
  inputProps?: Omit<InputProps, 'placeholder' | 'type'>;
  label: string;
  errorMessage?: string;
  helpMessage?: string;
};

const FloatingInputFormControl: React.FC<FloatingInputFormControlProps> =
  React.memo(
    ({ controlProps, inputProps, label, errorMessage, helpMessage }) => {
      return (
        <Flex
          __css={{
            '.chakra-form-control': {
              'input:not(:placeholder-shown) + label, .chakra-select__wrapper + label':
                {
                  transform: 'scale(0.85) translateY(-24px)',
                },
            },
          }}
        >
          <FormControl
            {...controlProps}
            _focusWithin={{
              label: {
                transform: 'scale(0.85) translateY(-24px)',
              },
            }}
          >
            <Input placeholder=" " type="text" {...inputProps} />
            <FloatingLabel inputSize={inputProps?.size}>{label}</FloatingLabel>
            <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>
            <FormHelperText px={4}>{helpMessage}</FormHelperText>
          </FormControl>
        </Flex>
      );
    },
  );

export { FloatingInputFormControl };
