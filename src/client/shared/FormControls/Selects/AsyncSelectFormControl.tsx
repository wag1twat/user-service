import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
  FormHelperText,
  Box,
} from '@chakra-ui/react';
import AsyncSelect, { AsyncProps } from 'react-select/async';
import { GroupBase } from 'react-select';
import { isClient } from 'src/shared/constants';
import { SelectOption, SelectSize } from './types';
import { useComponents } from './components';
import { useStyles } from './components/useStyles';

export type AsyncSelectFormControlProps<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
> = {
  controlProps?: FormControlProps;
  selectProps: AsyncProps<Option, IsMulti, Group>;
  label: string;
  errorMessage?: string;
  helpMessage?: string;
  size?: SelectSize;
};

function AsyncSelectFormControl<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
>({
  controlProps,
  selectProps,
  label,
  errorMessage,
  helpMessage,
  size,
}: AsyncSelectFormControlProps<Option, IsMulti, Group>) {
  const styles = useStyles<Option, IsMulti, Group>();

  const components = useComponents<Option, IsMulti, Group>({
    size,
    controlProps,
  });

  return (
    <FormControl {...controlProps}>
      <FormLabel>{label}</FormLabel>
      <Box width="full">
        {isClient && (
          <AsyncSelect<Option, IsMulti, Group>
            {...selectProps}
            menuPortalTarget={document.body}
            styles={styles}
            components={components}
          />
        )}
      </Box>
      <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>
      <FormHelperText px={4}>{helpMessage}</FormHelperText>
    </FormControl>
  );
}

export { AsyncSelectFormControl };
