import { GroupBase, SelectComponentsConfig } from 'react-select';
import { CloseOutlined, DownOutlined } from '@ant-design/icons';
import { SelectOption } from '../types';
import {
  Box,
  CircularProgress,
  Icon,
  Tag,
  TagLabel,
  Input,
  TagCloseButton,
  useColorModeValue,
  Stack,
  Divider,
  chakra,
} from '@chakra-ui/react';
import { omit, pick } from 'lodash';
import { AsyncSelectFormControlProps } from '../AsyncSelectFormControl';
import React from 'react';
import { loadingSizes, useSelectStyleConfig } from './useStyleConfig';

export type UseComponentsProps<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
> = Pick<
  AsyncSelectFormControlProps<Option, IsMulti, Group>,
  'controlProps' | 'size'
>;

export function useComponents<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
>(
  props: UseComponentsProps<Option, IsMulti, Group>,
): SelectComponentsConfig<Option, IsMulti, Group> {
  const { size = 'sm', controlProps } = props;

  const { styles } = useSelectStyleConfig(size);

  return {
    SelectContainer: (props) => {
      return (
        <Box
          {...props.innerProps}
          data-invalid={Boolean(controlProps?.isInvalid)}
          data-focus={Boolean(props.isFocused)}
          data-disabled={Boolean(props.isDisabled)}
          {...styles.container}
        >
          {props.children}
        </Box>
      );
    },
    IndicatorsContainer: (props) => {
      return (
        <Box {...styles.indicatorsContainer}>
          <Stack spacing={1} direction="row">
            {props.children}
          </Stack>
        </Box>
      );
    },
    DownChevron: () => {
      return <Icon as={DownOutlined} {...styles.downChevron} />;
    },
    CrossIcon: () => {
      return <Icon as={CloseOutlined} {...styles.crossIcon} />;
    },
    LoadingIndicator: (props) => {
      const color = useColorModeValue('gray.500', 'blue.200');
      return (
        <Box {...props.innerProps} {...styles.loadingIndicator}>
          <CircularProgress
            isIndeterminate
            color={color}
            size={loadingSizes[size]}
          />
        </Box>
      );
    },
    ClearIndicator: (props) => {
      const {
        selectProps: {
          components: { CrossIcon },
        },
      } = props;
      return (
        <Box {...props.innerProps} role="button" {...styles.clearIndicator}>
          {CrossIcon && <CrossIcon />}
        </Box>
      );
    },
    IndicatorSeparator: (props) => {
      return (
        <chakra.span
          as={Divider}
          {...props.innerProps}
          {...styles.indicatorSeparator}
        />
      );
    },
    DropdownIndicator: (props) => {
      const {
        selectProps: {
          components: { DownChevron },
        },
      } = props;
      return (
        <Box role="button" {...props.innerProps} {...styles.dropdownIndicator}>
          {DownChevron && <DownChevron />}
        </Box>
      );
    },
    Control: (props) => {
      return (
        <Box
          ref={props.innerRef}
          data-disabled={Boolean(props.isDisabled)}
          {...props.innerProps}
          {...styles.control}
        >
          {props.children}
        </Box>
      );
    },
    SingleValue: (props) => {
      return (
        <Box {...props.innerProps} {...styles.singleValue}>
          {props.children}
        </Box>
      );
    },
    ValueContainer: (props) => {
      return (
        <Box
          {...props.innerProps}
          data-has-value={Boolean(props.hasValue)}
          data-is-multi={Boolean(props.isMulti)}
          {...styles.valueContainer}
        >
          {props.children}
        </Box>
      );
    },
    MultiValue: (props) => {
      return (
        <props.components.Container {...props}>
          <props.components.Label {...props} />
          <props.components.Remove {...props} innerProps={props.removeProps} />
        </props.components.Container>
      );
    },
    MultiValueContainer: (props) => {
      return (
        <Tag {...props.innerProps} m="2px" p={0} size={size}>
          {props.children}
        </Tag>
      );
    },
    MultiValueLabel: (props) => {
      return (
        <TagLabel {...props.innerProps} {...styles.multiValueLabel}>
          {props.children}
        </TagLabel>
      );
    },
    MultiValueRemove: (props) => {
      return (
        <Box p={0} {...props.innerProps}>
          <TagCloseButton {...styles.multiValueRemove} />
        </Box>
      );
    },
    Input: (props) => {
      const pickProps = pick(omit(props, 'size'), [
        'autoCapitalize',
        'autoComplete',
        'autoCorrect',
        'id',
        'isDisabled',
        'onBlur',
        'onChange',
        'onFocus',
        'spellCheck',
        'tabIndex',
        'type',
        'value',
        'aria-autocomplete',
        'aria-expanded',
        'aria-haspopup',
        'aria-errormessage',
        'aria-invalid',
        'aria-label',
        'aria-labelledby',
        'role',
        'aria-describedby',
      ]);
      return (
        <Input
          size={size}
          variant="unstyled"
          ref={props.innerRef}
          {...pickProps}
          {...styles.input}
        />
      );
    },
    Placeholder: (props) => {
      return (
        <Box {...props.innerProps} {...styles.placeholder}>
          {props.children}
        </Box>
      );
    },
    Menu: (props) => {
      return (
        <Box ref={props.innerRef} {...props.innerProps} {...styles.menu}>
          {props.children}
        </Box>
      );
    },
    MenuList: (props) => {
      return (
        <Box
          ref={props.innerRef}
          maxHeight={props.maxHeight}
          {...props.innerProps}
          {...styles.menuList}
        >
          {props.children}
        </Box>
      );
    },
    Option: (props) => {
      return (
        <Box ref={props.innerRef} {...props.innerProps} {...styles.option}>
          {props.children}
        </Box>
      );
    },
    LoadingMessage: (props) => {
      return (
        <Box {...props.innerProps} {...styles.loadingMessage}>
          {props.children}
        </Box>
      );
    },
    NoOptionsMessage: (props) => {
      return (
        <Box {...props.innerProps} {...styles.noOptionsMessage}>
          {props.children}
        </Box>
      );
    },
  };
}
