import {
  BoxProps,
  ButtonProps,
  DividerProps,
  IconProps,
  InputProps,
} from '@chakra-ui/react';
import { useMultiStyleConfig, useTheme } from '@chakra-ui/system';
import { StylesConfig } from 'react-select';
import { SelectSize } from '../types';

const indicatorSizes: Record<SelectSize, string> = {
  xs: '.75rem',
  sm: '1rem',
  md: '1.25rem',
  lg: '1.5rem',
};

export const loadingSizes = indicatorSizes;
const crossIconSizes = indicatorSizes;
const downChevronSizes = indicatorSizes;

export const useSelectStyleConfig = (size: SelectSize) => {
  const theme = useTheme();

  const { zIndices } = theme;

  const defaultInputStyles = useMultiStyleConfig('Input', {
    size,
    variant: 'outline',
  });

  const indexes: Record<keyof Pick<StylesConfig, 'menu'>, number> = {
    menu: zIndices.dropdown,
  };

  const styles: {
    clearIndicator: BoxProps;
    container: BoxProps;
    control: BoxProps;
    dropdownIndicator: BoxProps;
    group: BoxProps;
    groupHeading: BoxProps;
    indicatorsContainer: BoxProps;
    indicatorSeparator: DividerProps;
    input: InputProps;
    loadingIndicator: BoxProps;
    loadingMessage: BoxProps;
    menu: BoxProps;
    menuList: BoxProps;
    menuPortal: BoxProps;
    multiValue: BoxProps;
    multiValueLabel: BoxProps;
    multiValueRemove: ButtonProps;
    noOptionsMessage: BoxProps;
    option: BoxProps;
    placeholder: BoxProps;
    singleValue: BoxProps;
    valueContainer: BoxProps;
    downChevron: IconProps;
    crossIcon: IconProps;
  } = {
    container: {
      position: 'relative',
      width: 'full',
      display: 'flex',
      alignItems: 'center',
      __css: {
        _light: {
          background: 'whiteAlpha.900',
          border: '1px solid',
          borderRadius: size,
          borderColor: 'gray.200',
        },
        _dark: {
          background: 'gray.800',
          border: '1px solid',
          borderRadius: size,
          borderColor: 'whiteAlpha.300',
        },
        fontSize: defaultInputStyles.addon.fontSize,
        minH: defaultInputStyles.addon.h,
        _hover:
          '_hover' in defaultInputStyles.field
            ? defaultInputStyles.field._hover
            : {},
        '&[data-disabled="true"]':
          '_disabled' in defaultInputStyles.field
            ? defaultInputStyles.field._disabled
            : {},
        '&[data-invalid="true"][data-focus="false"]':
          '_invalid' in defaultInputStyles.field
            ? defaultInputStyles.field._invalid
            : {},
        '&[data-invalid="true"][data-focus="true"]':
          '_focus' in defaultInputStyles.field
            ? defaultInputStyles.field._focus
            : {},
        '&[data-invalid="false"][data-focus="true"]':
          '_focus' in defaultInputStyles.field
            ? defaultInputStyles.field._focus
            : {},
      },
    },
    valueContainer: {
      background: 'transparent',
      boxSizing: 'border-box',
      alignItems: 'center',
      flex: 1,
      flexWrap: 'wrap',
      overflow: 'hidden',
      position: 'relative',
      display: 'grid',
      __css: {
        WebkitOverflowScrolling: 'touch',
        px: defaultInputStyles.addon.px,
        '&[data-has-value="true"][data-is-multi="true"]': { display: 'flex' },
      },
    },
    menu: {
      borderRadius: 4,
      boxShadow:
        '0 0 5px 0 var(--chakra-colors-gray-100), 0 0 5px 0 var(--chakra-colors-gray-100)',

      marginBottom: 2,
      marginTop: 2,
      position: 'absolute',
      top: '100%',
      width: '100%',
      zIndex: indexes.menu,
      __css: {
        _light: { background: 'whiteAlpha.900' },
        _dark: { background: 'gray.800' },
        fontSize: defaultInputStyles.addon.fontSize,
      },
    },

    option: {
      background: 'transparent',
      boxSizing: 'border-box',
      cursor: 'default',
      display: 'block',
      fontSize: 'inherit',
      userSelect: 'none',
      width: '100%',
      __css: {
        _light: {
          color: 'blackAlpha.900',
          _hover: {
            color: 'whiteAlpha.900',
            background: 'blue.200',
          },
          _active: { background: 'blue.300' },
          __css: {
            WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
          },
        },
        _dark: {
          color: 'whiteAlpha.900',
          _hover: {
            color: 'blackAlpha.900',
            background: 'blue.100',
          },
          _active: { background: 'blue.200' },
          __css: {
            WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
          },
        },
        px: defaultInputStyles.addon.px,
        py: defaultInputStyles.addon.px,
      },
    },
    clearIndicator: {
      display: 'flex',
      transition: 'color 150ms',
    },
    control: {
      boxSizing: 'border-box',
      alignItems: 'center',
      background: 'transparent',
      boxShadow: undefined,
      cursor: 'default',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
      outline: '0 !important',
      position: 'relative',
      transition: 'all 100ms',
      width: 'full',
      __css: {
        '&[data-disabled="true"]': {
          pointerEvents: 'none',
        },
      },
    },
    dropdownIndicator: {
      display: 'flex',
      transition: 'color 150ms',
    },
    group: {},
    groupHeading: {},
    indicatorsContainer: {
      alignItems: 'center',
      alignSelf: 'stretch',
      display: 'flex',
      flexShrink: 0,
      background: 'transparent',
      padding: 1,
    },
    indicatorSeparator: {
      orientation: 'vertical',
      height: 'auto',
      __css: {
        _light: {
          borderColor: 'blackAlpha.500',
        },
        _dark: {
          borderColor: 'whiteAlpha.500',
        },
      },
    },
    input: {
      display: 'inline-grid',
      flex: '1 1 auto',
      gridArea: '1 / 1 / 2 / 3',
      gridTemplateColumns: '0 min-content',
      visibility: 'visible',
      _after: {
        border: 0,
        content: `'attr(data-value) " "'`,
        font: 'inherit',
        gridArea: '1 / 2',
        margin: 0,
        minWidth: '2px',
        outline: 0,
        padding: 0,
        visibility: 'hidden',
        whiteSpace: 'pre',
      },
    },
    loadingIndicator: {
      background: 'transparent',
      alignSelf: 'center',
      display: 'flex',
      textAlign: 'center',
      transition: 'color 150ms',
      verticalAlign: 'middle',
      fontSize: 'inherit',
    },
    loadingMessage: {
      color: 'gray.200',
      padding: '8px 12px',
      textAlign: 'center',
    },
    menuList: {
      background: 'transparent',
      overflowY: 'auto',
      position: 'relative',
      fontSize: 'inherit',
      py: 1,
      __css: {
        WebkitOverflowScrolling: 'touch',
      },
    },
    menuPortal: {},
    multiValue: {},
    multiValueLabel: {
      padding: 1,
    },
    multiValueRemove: {
      m: 0,
      p: 1,
      _light: {
        _hover: {
          color: 'red.500',
        },
      },
      _dark: {
        _hover: {
          color: 'red.300',
        },
      },
    },
    noOptionsMessage: {
      color: 'gray.200',
      padding: '8px 12px',
      textAlign: 'center',
    },
    placeholder: {
      gridArea: '1 / 1 / 2 / 3',
      fontSize: 'inherit',
      __css: {
        _light: {
          color: 'blackAlpha.500',
        },
        _dark: {
          color: 'whiteAlpha.500',
        },
      },
    },
    singleValue: {
      background: 'transparent',
      boxSizing: 'border-box',
      gridArea: '1 / 1 / 2 / 3',
      maxWidth: '100%',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    downChevron: {
      transform: 'scale(1, 0.5)',
      fontSize: downChevronSizes[size],
      __css: {
        _light: {
          color: 'blackAlpha.500',
        },
        _dark: {
          color: 'whiteAlpha.500',
        },
      },
    },
    crossIcon: {
      fontSize: crossIconSizes[size],
      __css: {
        _light: {
          color: 'blackAlpha.500',
          _hover: {
            color: 'red.500',
          },
        },
        _dark: {
          color: 'whiteAlpha.500',
          _hover: {
            color: 'red.300',
          },
        },
      },
    },
  };

  return { indexes, styles };
};
