import { GroupBase, StylesConfig } from 'react-select';
import { SelectOption } from '../types';

export const useStyles = <
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
>(): StylesConfig<Option, IsMulti, Group> => {
  return {
    clearIndicator: () => {
      // console.log('clearIndicator', provided);
      return {};
    },
    container: () => {
      // console.log('container', provided);
      return {};
    },
    control: () => {
      // console.log('control', provided);
      return {};
    },
    dropdownIndicator: () => {
      // console.log('dropdownIndicator', provided);
      return {};
    },
    group: () => {
      // console.log('group', provided);
      return {};
    },
    groupHeading: () => {
      // console.log('groupHeading', provided);
      return {};
    },
    indicatorsContainer: () => {
      // console.log('indicatorsContainer', provided);
      return {};
    },
    indicatorSeparator: () => {
      // console.log('indicatorSeparator', provided);
      return {};
    },
    input: () => {
      // console.log('input', provided);
      return {};
    },
    loadingIndicator: () => {
      // console.log('loadingIndicator', provided);
      return {};
    },
    loadingMessage: () => {
      // console.log('loadingMessage', provided);
      return {};
    },
    menu: () => {
      // console.log('menu', provided);
      return {};
    },
    menuList: () => {
      // console.log('menuList', provided);
      return {};
    },
    menuPortal: (provided) => {
      //  console.log('menuPortal', provided);
      return provided;
    },
    multiValue: () => {
      // console.log('multiValue', provided);
      return {};
    },
    multiValueLabel: () => {
      // console.log('multiValueLabel', provided);
      return {};
    },
    multiValueRemove: () => {
      // console.log('multiValueRemove', provided);
      return {};
    },
    noOptionsMessage: () => {
      // console.log('noOptionsMessage', provided);
      return {};
    },
    option: () => {
      // console.log('option', provided);
      return {};
    },
    placeholder: () => {
      // console.log('placeholder', provided);
      return {};
    },
    singleValue: () => {
      // console.log('singleValue', provided);
      return {};
    },
    valueContainer: () => {
      // console.log('valueContainer', provided);
      return {};
    },
  };
};
