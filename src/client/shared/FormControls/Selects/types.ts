export type SelectOption = {
  label: string;
  value: string;
};

export type SelectSize = 'xs' | 'sm' | 'md' | 'lg';
