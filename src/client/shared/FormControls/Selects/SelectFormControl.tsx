import React from 'react';
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
  FormHelperText,
  Flex,
} from '@chakra-ui/react';
import Select, { GroupBase, Props } from 'react-select';
import { isClient } from 'src/shared/constants';
import { SelectOption, SelectSize } from './types';
import { useComponents, useStyles } from './components';

export type SelectFormControlProps<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
> = {
  controlProps?: FormControlProps;
  selectProps?: Props<Option, IsMulti, Group>;
  label: string;
  errorMessage?: string;
  helpMessage?: string;
  size?: SelectSize;
};

function SelectFormControl<
  Option = SelectOption,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>,
>({
  controlProps,
  selectProps,
  label,
  errorMessage,
  helpMessage,
  size,
}: SelectFormControlProps<Option, IsMulti, Group>) {
  const styles = useStyles<Option, IsMulti, Group>();

  const components = useComponents<Option, IsMulti, Group>({
    size,
    controlProps,
  });
  return (
    <FormControl {...controlProps}>
      <FormLabel>{label}</FormLabel>
      <Flex width="full">
        {isClient && (
          <Select<Option, IsMulti, Group>
            {...selectProps}
            menuPortalTarget={document.body}
            styles={styles}
            components={components}
          />
        )}
      </Flex>
      <FormErrorMessage px={4}>{errorMessage}</FormErrorMessage>
      <FormHelperText px={4}>{helpMessage}</FormHelperText>
    </FormControl>
  );
}

export { SelectFormControl };
