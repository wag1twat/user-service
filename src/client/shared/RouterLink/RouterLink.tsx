import { Button, ButtonProps } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';

interface RouterLinkProps extends ButtonProps {
  href: string;
}

const RouterLink: React.FC<React.PropsWithChildren<RouterLinkProps>> =
  React.memo(({ href, ...props }) => {
    const router = useRouter();

    const handleClick = React.useCallback(
      (href: string) => (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        router.push(href);
      },
      [router],
    );

    return (
      <Button onClick={handleClick(href)} p={'0px 8px 0px 8px'} {...props} />
    );
  });

export { RouterLink };
