import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { chakra } from '@chakra-ui/system';
import { IconButton, Th } from '@chakra-ui/react';
import React from 'react';
import { HeaderGroup, TableSortByToggleProps } from 'react-table';
import { Order } from 'src/server/queries/queries-sort';

interface ArrowsProps extends TableSortByToggleProps {
  isHiddenDown: boolean;
  isHiddenUp: boolean;
}

function Arrows(props: ArrowsProps) {
  const { isHiddenDown, isHiddenUp, ...rest } = props;

  return (
    <IconButton size="sm" ml={2} aria-label="Sort by" {...rest}>
      <chakra.span fontSize="xs">
        <chakra.span role="button">
          <ArrowDownOutlined
            aria-label="sorted descending"
            hidden={isHiddenDown}
          />
        </chakra.span>
        <chakra.span role="button">
          <ArrowUpOutlined aria-label="sorted ascending" hidden={isHiddenUp} />
        </chakra.span>
      </chakra.span>
    </IconButton>
  );
}

type SortByProps<D extends object> = {
  column: HeaderGroup<D>;
};

function TableHeader<D extends object>({ column }: SortByProps<D>) {
  const { sortBy, order, sortByServer } = column;

  const [orders] = React.useState<Array<Order>>(['DESC', 'ASC', undefined]);

  const nextOrder = React.useMemo(
    () =>
      orders.indexOf(order) !== orders.length - 1
        ? orders[orders.indexOf(order) + 1]
        : orders[0],
    [order, orders],
  );

  if (sortByServer) {
    return (
      <Th {...column.getHeaderProps()}>
        {column.render('Header')}
        {!column.disableSortBy && (
          <Arrows
            {...column.getSortByToggleProps({
              onClick: () => {
                sortByServer({
                  sortBy: column.id,
                  order: nextOrder,
                });
              },
            })}
            isHiddenDown={Boolean(column.id === sortBy && order === 'ASC')}
            isHiddenUp={Boolean(column.id === sortBy && order === 'DESC')}
          />
        )}
      </Th>
    );
  }

  return (
    <Th {...column.getHeaderProps()}>
      {column.render('Header')}
      {!column.disableSortBy && (
        <Arrows
          {...column.getSortByToggleProps()}
          isHiddenDown={Boolean(!column.isSortedDesc && column.isSorted)}
          isHiddenUp={Boolean(column.isSortedDesc && column.isSorted)}
        />
      )}
    </Th>
  );
}

export { TableHeader };
