import React from 'react';
import {
  Table as ChakraTable,
  Thead,
  Tr,
  Tbody,
  Td,
  Box,
  useBreakpoint,
  Stack,
  Flex,
  TableRowProps,
  TableHeadProps,
  BoxProps,
} from '@chakra-ui/react';
import { useTheme } from '@chakra-ui/system';
import { useTable, Column, useSortBy, Row } from 'react-table';
import { get } from 'lodash';
import { TableHeader } from './ui';

// TODO: settings by api

type CardBreakpoints = 'xs' | 'sm' | 'md' | 'lg';

const defaultCardBreakpoint: CardBreakpoints = 'sm';

export interface TableProps<T extends object> {
  columns: Column<T>[];
  data?: T[];
  Card?: React.ComponentType<TableRowProps & Row<T>>;
  cardBreakpoint?: CardBreakpoints;
  headProps?: TableHeadProps;
  containerProps?: BoxProps;
}

function Table<T extends object>({
  columns,
  data = [],
  cardBreakpoint = defaultCardBreakpoint,
  Card,
  headProps,
  containerProps,
}: React.PropsWithChildren<TableProps<T>>) {
  const { headerGroups, rows, getTableProps, getTableBodyProps, prepareRow } =
    useTable(
      {
        columns,
        data,
      },
      useSortBy,
    );

  const { colors } = useTheme();

  const breakpoint = useBreakpoint();

  if (breakpoint && cardBreakpoint === breakpoint) {
    return (
      <Box width="100%">
        <Stack spacing={8}>
          {rows.map((row) => {
            prepareRow(row);

            if (Card) {
              return <Card {...row.getRowProps()} {...row} />;
            }

            return (
              <Stack
                spacing={4}
                p={4}
                _dark={{
                  background: 'grey.900',
                  boxShadow: `0 1px 2px ${get(colors, 'white')}`,
                }}
                _light={{
                  background: 'white',
                  boxShadow: `0 1px 2px ${get(colors, 'grey.500')}`,
                }}
                {...row.getRowProps()}
              >
                {row.cells.map((cell) => {
                  return (
                    <Stack spacing={4} direction="row" {...cell.getCellProps()}>
                      <Flex alignItems="center">
                        {cell.column.render('Header')}:
                      </Flex>
                      <Flex alignItems="center">{cell.render('Cell')}</Flex>
                    </Stack>
                  );
                })}
              </Stack>
            );
          })}
        </Stack>
      </Box>
    );
  }
  return (
    <Box width="100%" {...containerProps}>
      <ChakraTable {...getTableProps()}>
        <Thead
          zIndex={1}
          _dark={{
            background: 'grey.900',
            boxShadow: `0 1px 2px ${get(colors, 'white')}`,
          }}
          _light={{
            background: 'white',
            boxShadow: `0 1px 2px ${get(colors, 'grey.500')}`,
          }}
          position="sticky"
          top="80px"
          {...headProps}
        >
          {headerGroups.map((headerGroup) => (
            <Tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => {
                return <TableHeader key={column.id} column={column} />;
              })}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);

            return (
              <Tr {...row.getRowProps()}>
                {row.cells.map((cell) => (
                  <Td {...cell.getCellProps()}>{cell.render('Cell')}</Td>
                ))}
              </Tr>
            );
          })}
        </Tbody>
      </ChakraTable>
    </Box>
  );
}

export { Table };
