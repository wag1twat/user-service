import { PostHookOptions, usePost } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';
import { MutationKey } from 'react-query';
import { User } from 'src/server/entities/client.types';
import { SignupUserDto } from 'src/server/controllers/user/dto/signup-user.dto';

export const useSignup = (
  key: MutationKey,
  options?: PostHookOptions<
    User & { access_token: string },
    SerializeException,
    { payload: SignupUserDto }
  >,
) => {
  return usePost<
    User & { access_token: string },
    SerializeException,
    { payload: SignupUserDto }
  >(key, '/api/v1/user/signup', options);
};
