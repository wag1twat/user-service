import { Stack, Flex, Text, Button } from '@chakra-ui/react';
import React from 'react';
import { PersonalAreaDrawer, useUIToasts } from 'src/client/shared';
import { useAuth, useGlobalDisclosures } from 'src/client/providers';
import { useSignup } from './model';
import { SignupUserForm } from 'src/client/features';
import { ParseSerializeException } from 'src/shared/helpers';
import { useCreateUserForm } from '../AddUserModal/model';

const SignUpDrawer = () => {
  const { setAccess } = useAuth();

  const { signin, signup } = useGlobalDisclosures();

  const handleClickSignIn = React.useCallback(() => {
    signin?.onOpen();
    signup?.onClose();
  }, [signin, signup]);

  const { success, error } = useUIToasts();

  const signupHook = useSignup(['sign-up'], {
    onSuccess: (data) => {
      success('Sign up successfully');
      if (setAccess) {
        setAccess(data);
      }
      signup?.onClose();
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  const form = useCreateUserForm((values) =>
    signupHook.post({ payload: values }),
  );

  return (
    <PersonalAreaDrawer
      header="Create your account"
      confirmText="Sign up"
      disclosure={signup}
      onConfirm={() => form.handleSubmit()}
      isConfirmLoading={signupHook.isLoading}
      size="md"
    >
      <Stack spacing={4} mt={4}>
        <SignupUserForm
          values={form.values}
          errors={form.errors}
          handleChange={form.handleChange}
          isLoading={signupHook.isLoading}
        />
        <Flex alignItems="center" justifyContent="space-between">
          <Text>Are you have account?</Text>
          <Button size="sm" onClick={handleClickSignIn}>
            Sign in
          </Button>
        </Flex>
      </Stack>
    </PersonalAreaDrawer>
  );
};

export { SignUpDrawer };
