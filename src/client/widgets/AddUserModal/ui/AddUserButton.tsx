import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { AddIconButton, AccessTooltip } from 'src/client/shared';

type AddUserButtonProps = { onOpen: () => void };

const AddUserButton: React.FC<AddUserButtonProps> = React.memo(({ onOpen }) => {
  const { isActive, deactiveMessage } = useAuth();

  return (
    <Box width="fit-content">
      <AccessTooltip
        availables={['AVAILABLE_CREATE_USER']}
        accessibilities={[
          {
            isLoading: false,
            isDisabled: !isActive,
            message: deactiveMessage,
          },
        ]}
        width="inherit"
      >
        {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
          <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
            <AddIconButton
              aria-label="Add user"
              label="Add user"
              onClick={onOpen}
              isLoading={isLoadingAccessibilities}
              isDisabled={isDisabledAccessibilities}
            />
          </Skeleton>
        )}
      </AccessTooltip>
    </Box>
  );
});

export { AddUserButton };
