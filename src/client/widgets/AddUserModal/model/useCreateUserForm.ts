import { useFormik } from 'formik';
import * as yup from 'yup';
import { CreateUserDto } from 'src/server/controllers/user/dto/create-user.dto';
import { UserEntityConfig } from 'src/server/entities/user.entity';

export const CreateUserSchema: yup.SchemaOf<CreateUserDto> = yup.object({
  login: yup
    .string()
    .min(UserEntityConfig.login.minLength)
    .max(UserEntityConfig.login.maxLength)
    .required(UserEntityConfig.login.requiredMessage),
  firstName: yup
    .string()
    .min(UserEntityConfig.firstName.minLength)
    .max(UserEntityConfig.firstName.maxLength)
    .required(UserEntityConfig.firstName.requiredMessage),
  lastName: yup
    .string()
    .min(UserEntityConfig.lastName.minLength)
    .max(UserEntityConfig.lastName.maxLength)
    .required(UserEntityConfig.lastName.requiredMessage),
  email: yup
    .string()
    .email(UserEntityConfig.email.notMatchMessage)
    .required(UserEntityConfig.email.requiredMessage),
  password: yup
    .string()
    .min(UserEntityConfig.password.minLength)
    .max(UserEntityConfig.password.maxLength)
    .matches(UserEntityConfig.password.regexp, {
      message: UserEntityConfig.password.notMatchMessage,
    })
    .required(UserEntityConfig.password.requiredMessage),
  active: yup.boolean().defined(),
  protected: yup.boolean().defined(),
});

export const useCreateUserForm = (
  onSubmit: (values: CreateUserDto) => void,
) => {
  return useFormik<CreateUserDto>({
    enableReinitialize: true,
    validationSchema: CreateUserSchema,
    initialValues: {
      login: '',
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      active: true,
      protected: false,
    },
    onSubmit,
  });
};
