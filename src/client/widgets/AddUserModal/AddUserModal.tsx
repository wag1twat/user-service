import { Button, useDisclosure, ButtonGroup, Flex } from '@chakra-ui/react';
import React from 'react';
import { CreateUserForm } from '../../features/Forms/CreateUserForm';
import { SaveIconButton, useUIToasts } from 'src/client/shared';
import { useAddUser } from 'src/client/entities/user/model';
import { ParseSerializeException } from 'src/shared/helpers';
import { Dialog } from '../Dialog';
import { AddUserButton } from './ui';
import { useCreateUserForm } from './model';

type AddUserModalProps = { refetch: () => void };

const AddUserModal: React.FC<AddUserModalProps> = React.memo(({ refetch }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { success, error } = useUIToasts();

  const addHook = useAddUser({
    onSuccess: () => {
      success('User adding successfully');
      refetch();
      onClose();
    },
    onError: (err) => error(ParseSerializeException.getString(err)),
  });

  const form = useCreateUserForm((values) => {
    addHook.post({ payload: values });
  });

  React.useEffect(() => {
    if (addHook.status === 'success') {
      form.resetForm();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.resetForm, addHook.status]);

  return (
    <>
      <AddUserButton onOpen={onOpen} />
      <Dialog
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
        header="Add user"
        footer={
          <ButtonGroup spacing={4}>
            <Button variant="ghost" colorScheme="blue" onClick={onClose}>
              Close
            </Button>
            <SaveIconButton
              label="Save"
              aria-label="Save user"
              variant="solid"
              colorScheme="blue"
              isDisabled={addHook.isLoading || !form.isValid}
              onClick={() => form.handleSubmit()}
              isLoading={addHook.isLoading}
            />
          </ButtonGroup>
        }
      >
        <Flex width="full" my={2}>
          <CreateUserForm
            values={form.values}
            errors={form.errors}
            handleChange={form.handleChange}
            isLoading={addHook.isLoading}
          />
        </Flex>
      </Dialog>
    </>
  );
});

export { AddUserModal };
