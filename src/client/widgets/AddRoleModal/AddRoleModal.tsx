import { Button, useDisclosure, ButtonGroup } from '@chakra-ui/react';
import React from 'react';
import { useUIToasts } from 'src/client/shared';
import { CreateRoleForm } from '../../features/Forms/CreateRoleForm';
import { useAddRole } from 'src/client/entities/role/model';
import { ParseSerializeException } from 'src/shared/helpers';
import { Dialog } from '../Dialog';
import { AddRoleButton } from './ui';
import { useCreateRoleForm } from './model';

type AddRoleModalProps = { refetch: () => void };

const AddRoleModal: React.FC<AddRoleModalProps> = React.memo(({ refetch }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { success, error } = useUIToasts();

  const addHook = useAddRole(['add-role'], {
    onSuccess: () => {
      success('User adding successfully');
      refetch();
      onClose();
    },
    onError: (err) => error(ParseSerializeException.getString(err)),
  });

  const form = useCreateRoleForm(
    {
      name: '',
      active: true,
      protected: false,
      permissionsIds: [],
      usersIds: [],
    },
    (values) => {
      addHook.post({ payload: values });
    },
  );

  React.useEffect(() => {
    if (addHook.status === 'success') {
      form.resetForm();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.resetForm, addHook.status]);

  return (
    <>
      <AddRoleButton onOpen={onOpen} />
      <Dialog
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
        size="4xl"
        scrollBehavior="inside"
        header="Add role"
        footer={
          <ButtonGroup spacing={4}>
            <Button variant="ghost" colorScheme="blue" onClick={onClose}>
              Close
            </Button>
            <Button
              variant="solid"
              colorScheme="blue"
              isDisabled={addHook.isLoading || !form.isValid}
              onClick={() => form.handleSubmit()}
              isLoading={addHook.isLoading}
            >
              Add
            </Button>
          </ButtonGroup>
        }
      >
        <CreateRoleForm
          isOpen={isOpen}
          isLoading={addHook.isLoading}
          values={form.values}
          errors={form.errors}
          handleChange={form.handleChange}
          handleChangeCheckboxes={(fieldName, value) =>
            form.setFieldValue(fieldName, value)
          }
        />
      </Dialog>
    </>
  );
});

export { AddRoleModal };
