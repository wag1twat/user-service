import { useFormik } from 'formik';
import * as yup from 'yup';
import { CreateRoleDto } from 'src/server/controllers/role/dto/create-role.dto';
import { RoleEntityConfig } from 'src/server/entities/role.entity';

export const CreateRoleSchema: yup.SchemaOf<CreateRoleDto> = yup.object({
  name: yup
    .string()
    .min(RoleEntityConfig.name.minLength)
    .max(RoleEntityConfig.name.maxLength)
    .required(RoleEntityConfig.name.requiredMessage),
  protected: yup.boolean().required(),
  active: yup.boolean().required(),
  usersIds: yup.array().of(yup.string()).notRequired(),
  permissionsIds: yup.array().of(yup.string()).notRequired(),
});

export const useCreateRoleForm = (
  initialValues: CreateRoleDto,
  onSubmit: (values: CreateRoleDto) => void = () => void 0,
) => {
  return useFormik<CreateRoleDto>({
    enableReinitialize: true,
    validationSchema: CreateRoleSchema,
    initialValues,
    onSubmit,
  });
};
