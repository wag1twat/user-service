import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { AddIconButton, AccessTooltip } from 'src/client/shared';

type AddRoleButtonProps = { onOpen: () => void };

const AddRoleButton: React.FC<AddRoleButtonProps> = React.memo(({ onOpen }) => {
  const { isActive, deactiveMessage } = useAuth();
  return (
    <Box width="fit-content">
      <AccessTooltip
        availables={['AVAILABLE_CREATE_ROLE']}
        accessibilities={[
          {
            isLoading: false,
            isDisabled: !isActive,
            message: deactiveMessage,
          },
        ]}
        width="inherit"
      >
        {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
          <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
            <AddIconButton
              aria-label="Add role"
              label="Add role"
              onClick={onOpen}
              colorScheme="blue"
              isLoading={isLoadingAccessibilities}
              isDisabled={isDisabledAccessibilities}
            />
          </Skeleton>
        )}
      </AccessTooltip>
    </Box>
  );
});

export { AddRoleButton };
