import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { AddIconButton, AccessTooltip } from 'src/client/shared';

type AddPermissionButtonProps = { onOpen: () => void };

const AddPermissionButton: React.FC<AddPermissionButtonProps> = React.memo(
  ({ onOpen }) => {
    const { isActive, deactiveMessage } = useAuth();
    return (
      <Box width="fit-content">
        <AccessTooltip
          availables={['AVAILABLE_CREATE_PERMISSIONS']}
          accessibilities={[
            {
              isLoading: false,
              isDisabled: !isActive,
              message: deactiveMessage,
            },
          ]}
          width="inherit"
          shouldWrapChildren
        >
          {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
            <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
              <AddIconButton
                aria-label="Add permission"
                label="Add permission"
                onClick={onOpen}
                colorScheme="blue"
                isLoading={isLoadingAccessibilities}
                isDisabled={isDisabledAccessibilities}
              />
            </Skeleton>
          )}
        </AccessTooltip>
      </Box>
    );
  },
);

export { AddPermissionButton };
