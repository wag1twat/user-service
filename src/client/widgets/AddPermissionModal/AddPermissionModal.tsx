import { Button, ButtonGroup, Stack, useDisclosure } from '@chakra-ui/react';
import React from 'react';
import { useAddPermission } from 'src/client/entities/permission/model';
import {
  CheckboxFormControl,
  FloatingInputFormControl,
  SaveIconButton,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import { Dialog } from '../Dialog';
import { useAddPermissionForm } from './model';
import { AddPermissionButton } from './ui';

type AddPermissionModalProps = { refetch: () => void };

const AddPermissionModal: React.FC<AddPermissionModalProps> = ({ refetch }) => {
  const { success, error } = useUIToasts();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const addHook = useAddPermission(['add-permission'], {
    resetOnSuccess: true,
    onSuccess: () => {
      success('Add permission successfully');
      refetch();
      onClose();
    },
    onError: (err) => error(ParseSerializeException.getString(err)),
  });

  const form = useAddPermissionForm(
    { name: '', active: true, protected: false },
    (values) => addHook.post({ payload: values }),
  );

  React.useEffect(() => {
    if (addHook.status === 'success') {
      form.resetForm();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addHook.status, form.resetForm]);

  React.useEffect(() => {
    if (!isOpen) {
      form.resetForm();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.resetForm, isOpen]);

  return (
    <>
      <AddPermissionButton onOpen={onOpen} />
      <Dialog
        isOpen={isOpen}
        onClose={onClose}
        header="Add permission"
        footer={
          <ButtonGroup spacing={4}>
            <Button variant="ghost" colorScheme="blue" onClick={onClose}>
              Close
            </Button>
            <SaveIconButton
              variant="solid"
              isDisabled={!form.isValid || addHook.isLoading}
              onClick={() => form.handleSubmit()}
              isLoading={addHook.isLoading}
              label="Save"
              aria-label="Save"
            />
          </ButtonGroup>
        }
      >
        <Stack spacing={4} my={2}>
          <FloatingInputFormControl
            label="Name"
            errorMessage={form.errors.name}
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(form.errors.name),
            }}
            inputProps={{
              name: 'name',
              value: form.values.name,
              onChange: form.handleChange,
            }}
          />
          <CheckboxFormControl
            errorMessage={form.errors.active}
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(form.errors.active),
            }}
            checkboxProps={{
              name: 'active',
              isChecked: form.values.active,
              onChange: form.handleChange,
            }}
          >
            Active
          </CheckboxFormControl>
          <CheckboxFormControl
            errorMessage={form.errors.protected}
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(form.errors.protected),
            }}
            checkboxProps={{
              name: 'protected',
              isChecked: form.values.protected,
              onChange: form.handleChange,
            }}
          >
            Protected
          </CheckboxFormControl>
        </Stack>
      </Dialog>
    </>
  );
};

export { AddPermissionModal };
