import { useFormik } from 'formik';
import * as yup from 'yup';
import { CreatePermissionDto } from 'src/server/controllers/permission/dto/create-permission.dto';
import { PermissionEntityConfig } from 'src/server/entities/permission.entity';

const CreatePermissionSchema: yup.SchemaOf<CreatePermissionDto> = yup.object({
  name: yup
    .string()
    .min(PermissionEntityConfig.name.minLength)
    .max(PermissionEntityConfig.name.maxLength)
    .required(PermissionEntityConfig.name.requiredMessage),
  active: yup.boolean().required(),
  protected: yup.boolean().required(),
});

export type CreatePermissionFormType = CreatePermissionDto;

export const useAddPermissionForm = (
  initialValues: CreatePermissionFormType,
  onSubmit: (values: CreatePermissionFormType) => void = () => void 0,
) => {
  return useFormik<CreatePermissionFormType>({
    enableReinitialize: true,
    validationSchema: CreatePermissionSchema,
    initialValues,
    onSubmit,
  });
};
