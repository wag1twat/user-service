import { Button, ButtonGroup, Stack, Flex } from '@chakra-ui/react';
import React from 'react';
import { UpdateRoleForm } from 'src/client/features';
import { useUpdateRoleModalContext } from './providers';
import { SaveIconButton, WarningText } from 'src/client/shared';
import { Dialog } from '../Dialog';
import { useUpdateRoleForm } from './model';

type UpdateRoleModalProps = {
  isOpen: boolean;
  onClose: () => void;
};

const UpdateRoleModal: React.FC<UpdateRoleModalProps> = React.memo(
  ({ isOpen, onClose }) => {
    const {
      roleName = '',
      initialValues,
      isLoading,
      isSaveLoading,
      isDisabled,
      isUserRole,
      handleSubmit,
    } = useUpdateRoleModalContext();

    const form = useUpdateRoleForm(initialValues, handleSubmit);

    return (
      <Dialog
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
        size="4xl"
        scrollBehavior="inside"
        header={`Update role ${roleName}`}
        footer={
          <ButtonGroup spacing={4}>
            <Button variant="ghost" colorScheme="blue" onClick={onClose}>
              Close
            </Button>
            <SaveIconButton
              label="Save"
              aria-label="Save role"
              variant="solid"
              colorScheme="blue"
              isDisabled={isSaveLoading || isDisabled || !form.isValid}
              onClick={() => form.handleSubmit()}
              isLoading={isLoading || isSaveLoading}
            />
          </ButtonGroup>
        }
      >
        <Stack spacing={4} width="full">
          {isUserRole && (
            <Flex pb={4}>
              <WarningText>
                Attention! This role belongs to you. Change its capabilities
                more carefully.
              </WarningText>
            </Flex>
          )}
          <UpdateRoleForm
            isOpen={isOpen}
            isLoading={isLoading}
            isDisabled={isSaveLoading}
            values={form.values}
            errors={form.errors}
            handleChange={form.handleChange}
            handleChangeCheckboxes={(fieldName, value) =>
              form.setFieldValue(fieldName, value)
            }
          />
        </Stack>
      </Dialog>
    );
  },
);

export { UpdateRoleModal };
