import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { Accessibilities, AccessTooltip } from 'src/client/shared';
import { UpdateIconButton } from 'src/client/shared';

interface UpdateRoleModalButtonProps extends Accessibilities {
  onOpen: () => void;
}

const UpdateRoleModalButton: React.FC<UpdateRoleModalButtonProps> = React.memo(
  ({ onOpen, accessibilities = [] }) => {
    const { isActive, deactiveMessage } = useAuth();

    return (
      <Box width="fit-content">
        <AccessTooltip
          availables={['AVAILABLE_UPDATE_ROLE']}
          accessibilities={[
            {
              isLoading: false,
              isDisabled: !isActive,
              message: deactiveMessage,
            },
            ...accessibilities,
          ]}
          width="inherit"
        >
          {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
            <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
              <UpdateIconButton
                aria-label="Update role"
                onClick={onOpen}
                isLoading={isLoadingAccessibilities}
                isDisabled={isDisabledAccessibilities}
              />
            </Skeleton>
          )}
        </AccessTooltip>
      </Box>
    );
  },
);

export { UpdateRoleModalButton };
