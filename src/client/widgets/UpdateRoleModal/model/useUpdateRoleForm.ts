import { useFormik } from 'formik';
import * as yup from 'yup';
import { UpdateRoleDto } from 'src/server/controllers/role/dto/update-role.dto';
import { RoleEntityConfig } from 'src/server/entities/role.entity';

export const UpdateRoleSchema: yup.SchemaOf<UpdateRoleDto> = yup.object({
  name: yup
    .string()
    .min(RoleEntityConfig.name.minLength)
    .max(RoleEntityConfig.name.maxLength)
    .required(RoleEntityConfig.name.requiredMessage),
  usersIds: yup.array().of(yup.string().required()).notRequired(),
  permissionsIds: yup.array().of(yup.string().required()).notRequired(),
});

export const useUpdateRoleForm = (
  initialValues: UpdateRoleDto,
  onSubmit: (values: UpdateRoleDto) => void = () => void 0,
) => {
  return useFormik<UpdateRoleDto>({
    enableReinitialize: true,
    validationSchema: UpdateRoleSchema,
    initialValues,
    onSubmit,
  });
};
