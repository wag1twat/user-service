import { find, map } from 'lodash';
import React from 'react';
import { QueryStatus } from 'react-query';
import { usePermissions } from 'src/client/entities/permissions/model';
import { useRole, useRoleUpdate } from 'src/client/entities/role/model';
import { useUsers } from 'src/client/entities/users/model';
import { useAuth } from 'src/client/providers';
import { useUIToasts } from 'src/client/shared';
import { UpdateRoleDto } from 'src/server/controllers/role/dto/update-role.dto';
import { Role } from 'src/server/entities/client.types';
import { ParseSerializeException } from 'src/shared/helpers';

type UpdateRoleModalContext = {
  roleName: string | undefined;
  initialValues: UpdateRoleDto;
  status: QueryStatus;
  isLoading: boolean;
  isDisabled: boolean;
  isSaveLoading: boolean;
  isUserRole: boolean;
  handleSubmit: (values: UpdateRoleDto) => void;
};

type UpdateRoleModalProviderProps = {
  isOpen: boolean;
  id: Role['id'] | undefined;
  refetch: () => void;
};

const updateRoleModalContext = React.createContext<UpdateRoleModalContext>({
  roleName: '',
  initialValues: {
    name: '',
    usersIds: [],
    permissionsIds: [],
  },
  status: 'idle',
  isLoading: false,
  isDisabled: false,
  isSaveLoading: false,
  isUserRole: false,
  handleSubmit: () => void 0,
});

const UpdateRoleModalProvider: React.FC<UpdateRoleModalProviderProps> = ({
  isOpen,
  id,
  refetch,
  ...props
}) => {
  const { roles: userRoles, permissions: userPermisions } = useAuth();

  const isUserRole = React.useMemo(() => {
    return Boolean(find(userRoles?.data, (role) => role.id === id));
  }, [id, userRoles?.data]);

  const { success, error } = useUIToasts();

  const role = useRole(['role-update-role', id], {
    params: { id: id },
    enabled: isOpen && Boolean(id),
  });

  const permissions = usePermissions(['edit-role-permissions', isOpen, id], {
    params: {
      rolesIds: id ? [id] : undefined,
    },
    enabled: Boolean(id && isOpen),
  });

  const users = useUsers(['edit-role-users', isOpen, id], {
    params: {
      rolesIds: id ? [id] : undefined,
    },
    enabled: Boolean(id && isOpen),
  });

  const permissionsIds = React.useMemo(
    () => map(permissions?.data?.data, (permission) => permission.id),
    [permissions?.data],
  );

  const usersIds = React.useMemo(
    () => map(users?.data?.data, (user) => user.id),
    [users?.data],
  );

  const initialValues = React.useMemo<UpdateRoleDto>(
    () => ({
      name: role.data?.data.name || '',
      usersIds,
      permissionsIds,
    }),
    [permissionsIds, role.data?.data.name, usersIds],
  );

  const updateHook = useRoleUpdate(['update-role'], {
    onSuccess: () => {
      success('Update role successfully');
      role.refetch();
      permissions.refetch();
      users.refetch();
      refetch();
      if (isUserRole) {
        userPermisions?.refetch();
        userRoles?.refetch();
      }
    },
    onError: (err) => error(ParseSerializeException.getString(err)),
  });

  const handleSubmit = React.useCallback(
    (values: UpdateRoleDto) => {
      if (id) {
        updateHook.patch({ payload: values, params: { id } });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [id, updateHook.patch],
  );

  return (
    <updateRoleModalContext.Provider
      value={{
        roleName: role.data?.data.name,
        initialValues,
        status: updateHook.status,
        isLoading:
          role.isLoading ||
          userPermisions?.isLoading ||
          userRoles?.isLoading ||
          permissions.isLoading ||
          users.isLoading,
        isDisabled: false,
        isSaveLoading: updateHook.isLoading,
        isUserRole,
        handleSubmit,
      }}
      {...props}
    />
  );
};

const useUpdateRoleModalContext = () => {
  return React.useContext(updateRoleModalContext);
};

export { UpdateRoleModalProvider, useUpdateRoleModalContext };
