import React from 'react';
import { UserOutlined, LogoutOutlined } from '@ant-design/icons';
import {
  Button,
  ButtonGroup,
  chakra,
  Flex,
  Heading,
  IconButton,
  Skeleton,
  useTheme,
} from '@chakra-ui/react';
import { RouterLink } from 'src/client/shared';
import { useAuth, useGlobalDisclosures } from 'src/client/providers';
import { get } from 'lodash';
import { ToggleThemeModeButton } from 'src/client/features';
import { SignInDrawer } from '../SignInDrawer';
import { SignUpDrawer } from '../SignUpDrawer';
import { MeDrawer } from 'src/client/entities/me';

export function Header() {
  const { me, isAuth, logout } = useAuth();

  const { signin, signup, profile } = useGlobalDisclosures();

  const { colors } = useTheme();
  return (
    <>
      <Flex
        as="nav"
        position="sticky"
        top={0}
        p={4}
        width="100%"
        justifyContent="space-between"
        alignItems="center"
        height="80px"
        zIndex={1051}
        _dark={{
          background: 'grey.900',
          boxShadow: `0 1px 2px ${get(colors, 'white')}`,
        }}
        _light={{
          background: 'white',
          boxShadow: `0 1px 2px ${get(colors, 'grey.500')}`,
        }}
      >
        <RouterLink href="/" variant="link">
          <Heading size="md" color="gray.500">
            <chakra.span color="currentcolor">user-service</chakra.span>{' '}
          </Heading>
        </RouterLink>
        <ButtonGroup spacing={4}>
          <Skeleton isLoaded={!Boolean(me?.isLoading)} hidden={isAuth === true}>
            <Button onClick={signup?.onOpen} colorScheme="teal" variant="ghost">
              Sign up
            </Button>
          </Skeleton>
          <Skeleton isLoaded={!Boolean(me?.isLoading)} hidden={isAuth === true}>
            <Button onClick={signin?.onOpen} colorScheme="teal">
              Sign in
            </Button>
          </Skeleton>
          <Skeleton
            isLoaded={!Boolean(me?.isLoading)}
            hidden={isAuth === false}
            as={Flex}
          >
            <RouterLink href="/users" variant="link">
              Users
            </RouterLink>
          </Skeleton>
          <Skeleton
            isLoaded={!Boolean(me?.isLoading)}
            hidden={isAuth === false}
            as={Flex}
          >
            <RouterLink href="/roles" variant="link">
              Roles
            </RouterLink>
          </Skeleton>
          <Skeleton
            isLoaded={!Boolean(me?.isLoading)}
            hidden={isAuth === false}
            as={Flex}
          >
            <RouterLink href="/permissions" variant="link">
              Permissions
            </RouterLink>
          </Skeleton>
          <Skeleton
            isLoaded={!Boolean(me?.isLoading)}
            hidden={isAuth === false}
          >
            <Button
              onClick={profile?.onOpen}
              colorScheme="teal"
              leftIcon={<UserOutlined />}
            >
              Profile
            </Button>
          </Skeleton>
          <ToggleThemeModeButton />
          <Skeleton
            isLoaded={!Boolean(me?.isLoading)}
            hidden={isAuth === false}
          >
            <IconButton aria-label="Logout" onClick={logout}>
              <LogoutOutlined />
            </IconButton>
          </Skeleton>
        </ButtonGroup>
      </Flex>
      <SignInDrawer />
      <SignUpDrawer />
      <MeDrawer />
    </>
  );
}
