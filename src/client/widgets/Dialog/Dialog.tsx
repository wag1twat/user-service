import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  ModalProps,
} from '@chakra-ui/react';
import React from 'react';

interface DialogProps extends ModalProps {
  header: React.ReactNode;
  footer: React.ReactNode;
}

const Dialog: React.FC<DialogProps> = React.memo(
  ({ header, footer, children, ...props }) => {
    return (
      <Modal
        isCentered
        motionPreset="slideInBottom"
        scrollBehavior="inside"
        {...props}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{header}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>{children}</ModalBody>
          <ModalFooter>{footer}</ModalFooter>
        </ModalContent>
      </Modal>
    );
  },
);

export { Dialog };
