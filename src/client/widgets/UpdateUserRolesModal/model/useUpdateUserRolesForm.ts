import { useFormik } from 'formik';
import { UpdateUserRolesDto } from 'src/server/controllers/user/dto/update-user-roles.dto';
import * as yup from 'yup';

const UpdateUserRolesSchema: yup.SchemaOf<UpdateUserRolesDto> = yup.object({
  rolesIds: yup.array().of(yup.string().required()).required(),
});

const useUpdateUserRolesForm = (
  initialValues: UpdateUserRolesDto,
  onSubmit: (values: UpdateUserRolesDto) => void,
) => {
  return useFormik<UpdateUserRolesDto>({
    enableReinitialize: true,
    validationSchema: UpdateUserRolesSchema,
    initialValues,
    onSubmit,
  });
};

export { useUpdateUserRolesForm };
