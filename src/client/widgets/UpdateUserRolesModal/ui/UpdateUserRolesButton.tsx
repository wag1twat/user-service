import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import {
  UpdateIconButton,
  AccessTooltip,
  Accessibilities,
} from 'src/client/shared';

interface UpdateUserRolesButtonProps extends Accessibilities {
  onOpen: () => void;
}

const UpdateUserRolesButton: React.FC<UpdateUserRolesButtonProps> = React.memo(
  ({ accessibilities = [], onOpen }) => {
    const { isActive, deactiveMessage } = useAuth();

    return (
      <Box width="fit-content">
        <AccessTooltip
          availables={['AVAILABLE_UPDATE_ROLE', 'AVAILABLE_UPDATE_USER']}
          accessibilities={[
            {
              isLoading: false,
              isDisabled: !isActive,
              message: deactiveMessage,
            },
            ...accessibilities,
          ]}
          width="inherit"
        >
          {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
            <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
              <UpdateIconButton
                aria-label="Update roles"
                label="Update roles"
                onClick={onOpen}
                colorScheme="blue"
                isLoading={isLoadingAccessibilities}
                isDisabled={isDisabledAccessibilities}
              />
            </Skeleton>
          )}
        </AccessTooltip>
      </Box>
    );
  },
);

export { UpdateUserRolesButton };
