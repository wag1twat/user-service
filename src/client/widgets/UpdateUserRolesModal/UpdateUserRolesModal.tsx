import {
  Button,
  useDisclosure,
  ButtonGroup,
  Checkbox,
  Stack,
  FormControl,
  FormErrorMessage,
  Input,
} from '@chakra-ui/react';
import { map } from 'lodash';
import React from 'react';
import {
  Accessibilities,
  CheckboxGroupFormControl,
  SaveIconButton,
  useDebounce,
  useUIToasts,
} from 'src/client/shared';
import { useAuth } from 'src/client/providers';
import { useRoles } from 'src/client/entities/roles/model';
import { useUpdateUserRoles } from 'src/client/entities/user/model';
import { ParseSerializeException } from 'src/shared/helpers';
import { InlineLoader } from 'src/client/features';
import { Dialog } from '../Dialog';
import { UpdateUserRolesButton } from './ui';
import { useUpdateUserRolesForm } from './model';
import { User } from 'src/server/entities/client.types';

interface UpdateUserRolesModalProps extends Accessibilities {
  id: User['id'] | undefined;
  refetch: () => void;
}

const UpdateUserRolesModal: React.FC<UpdateUserRolesModalProps> = React.memo(
  ({ id, accessibilities, refetch }) => {
    const { success, error } = useUIToasts();

    const { permissions, me, roles: meRoles } = useAuth();

    const { isOpen, onOpen, onClose } = useDisclosure();

    const [roleName, setRoleName] = React.useState<string>('');

    const debouncedRoleName = useDebounce(roleName, 200);

    const roles = useRoles(['user-update-roles', isOpen, debouncedRoleName], {
      params: { name: debouncedRoleName },
      enabled: isOpen,
      resetOnError: !isOpen,
      resetOnSuccess: !isOpen,
    });

    const userRoles = useRoles(['user-update-roles-user-roles', id, isOpen], {
      params: { usersIds: id ? [id] : undefined },
      enabled: Boolean(id) && isOpen,
    });

    const userRolesIds = React.useMemo(
      () => map(userRoles.data?.data, (role) => role.id),
      [userRoles.data?.data],
    );

    const { patch, isLoading } = useUpdateUserRoles(['user-update-roles'], {
      resetOnSuccess: true,
      onSuccess: () => {
        success('Update roles successfully');
        userRoles.refetch();
        refetch();
        if (id === me?.data?.id) {
          permissions?.refetch();
          meRoles?.refetch();
          roles?.refetch();
        }
      },
      onError: (err) => {
        error(ParseSerializeException.getString(err));
      },
    });

    const { values, setFieldValue, isValid, errors, handleSubmit, resetForm } =
      useUpdateUserRolesForm({ rolesIds: userRolesIds }, (values) => {
        if (id) {
          patch({
            payload: values,
            params: { id },
          });
        }
      });

    const isDisabledSet = React.useMemo(
      () => !isValid || isLoading || roles.isLoading,
      [isValid, isLoading, roles.isLoading],
    );

    React.useEffect(() => {
      if (!isOpen) {
        resetForm();
      }
    }, [isOpen, resetForm]);

    return (
      <>
        <UpdateUserRolesButton
          onOpen={onOpen}
          accessibilities={accessibilities}
        />
        <Dialog
          isCentered
          onClose={onClose}
          isOpen={isOpen}
          motionPreset="slideInBottom"
          scrollBehavior="inside"
          header="Update roles"
          footer={
            <ButtonGroup spacing={4}>
              <Button variant="ghost" colorScheme="blue" onClick={onClose}>
                Close
              </Button>
              <SaveIconButton
                variant="solid"
                isDisabled={isDisabledSet}
                onClick={() => handleSubmit()}
                isLoading={isLoading || roles.isLoading}
                label="Save"
                aria-label="Save"
              />
            </ButtonGroup>
          }
        >
          <Stack spacing={4}>
            <Input
              placeholder="Role name..."
              value={roleName}
              onChange={(e) => setRoleName(e.target.value)}
              isDisabled={isLoading}
            />
            <FormControl isInvalid={!isValid}>
              <InlineLoader
                isLoading={roles.isLoading}
                error={ParseSerializeException.getString(roles.error)}
                retry={roles.refetch}
                height="fit-content"
              >
                <CheckboxGroupFormControl
                  controlProps={{ px: 4 }}
                  label="Roles"
                  checkboxGroupProps={{
                    isDisabled: isLoading,
                    value: values.rolesIds,
                    onChange: (value) => setFieldValue('rolesIds', value),
                  }}
                >
                  <Stack>
                    {map(roles.data?.data, (role) => {
                      return (
                        <Checkbox key={role.id} value={role.id}>
                          {role.name}
                        </Checkbox>
                      );
                    })}
                  </Stack>
                </CheckboxGroupFormControl>
              </InlineLoader>
              <FormErrorMessage>{errors.rolesIds}</FormErrorMessage>
            </FormControl>
          </Stack>
        </Dialog>
      </>
    );
  },
);

export { UpdateUserRolesModal };
