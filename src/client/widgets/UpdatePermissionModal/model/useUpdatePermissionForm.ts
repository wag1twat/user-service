import { useFormik } from 'formik';
import * as yup from 'yup';
import { UpdatePermissionDto } from 'src/server/controllers/permission/dto/update-permission.dto';
import { PermissionEntityConfig } from 'src/server/entities/permission.entity';

const UpdatePermissionSchema: yup.SchemaOf<UpdatePermissionDto> = yup.object({
  name: yup
    .string()
    .min(PermissionEntityConfig.name.minLength)
    .max(PermissionEntityConfig.name.maxLength)
    .required(PermissionEntityConfig.name.requiredMessage),
});

export type UpdatePermissionFormType = UpdatePermissionDto;

export const useUpdatePermissionForm = (
  initialValues: UpdatePermissionFormType,
  onSubmit: (values: UpdatePermissionFormType) => void = () => void 0,
) => {
  return useFormik<UpdatePermissionFormType>({
    enableReinitialize: true,
    validationSchema: UpdatePermissionSchema,
    initialValues,
    onSubmit,
  });
};
