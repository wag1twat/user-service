import { Box, Skeleton } from '@chakra-ui/react';
import React from 'react';
import { useAuth } from 'src/client/providers';
import { Accessibilities, AccessTooltip } from 'src/client/shared';
import { UpdateIconButton } from 'src/client/shared';

interface UpdatePermissionModalButtonProps extends Accessibilities {
  onOpen: () => void;
}

const UpdatePermissionModalButton: React.FC<UpdatePermissionModalButtonProps> =
  React.memo(({ accessibilities = [], onOpen }) => {
    const { isActive, deactiveMessage } = useAuth();

    return (
      <Box width="fit-content">
        <AccessTooltip
          availables={['AVAILABLE_UPDATE_PERMISSIONS']}
          accessibilities={[
            {
              isLoading: false,
              isDisabled: !isActive,
              message: deactiveMessage,
            },
            ...accessibilities,
          ]}
          width="inherit"
        >
          {({ isLoadingAccessibilities, isDisabledAccessibilities }) => (
            <Skeleton width="inherit" isLoaded={!isLoadingAccessibilities}>
              <UpdateIconButton
                aria-label="Update permission"
                onClick={onOpen}
                isLoading={isLoadingAccessibilities}
                isDisabled={isDisabledAccessibilities}
              />
            </Skeleton>
          )}
        </AccessTooltip>
      </Box>
    );
  });

export { UpdatePermissionModalButton };
