import { Button, ButtonGroup, Stack } from '@chakra-ui/react';
import React from 'react';
import {
  usePermission,
  useUpdatePermission,
} from 'src/client/entities/permission/model';
import {
  FloatingInputFormControl,
  SaveIconButton,
  useUIToasts,
} from 'src/client/shared';
import { PermissionEntity } from 'src/server/entities/permission.entity';
import { ParseSerializeException } from 'src/shared/helpers';
import { Dialog } from '../Dialog';
import { useUpdatePermissionForm } from './model';

type UpdatePermissionModalProps = {
  isOpen: boolean;
  onClose: () => void;
  refetch: () => void;
  permissionId: PermissionEntity['id'] | undefined;
};

const UpdatePermissionModal: React.FC<UpdatePermissionModalProps> = React.memo(
  ({ permissionId, isOpen, onClose, refetch }) => {
    const { success, error } = useUIToasts();

    const permission = usePermission(
      ['update-permssions', permissionId, isOpen],
      {
        params: { id: permissionId },
        enabled: Boolean(permissionId && isOpen),
      },
    );

    const updateHook = useUpdatePermission(['update-permission'], {
      resetOnSuccess: true,
      onSuccess: () => {
        success('Update permission successfully');
        refetch();
      },
      onError: (err) => error(ParseSerializeException.getString(err)),
    });

    const form = useUpdatePermissionForm(
      {
        name: permission.data?.data.name || '',
      },
      (values) => {
        if (permission.data?.data.id) {
          updateHook.patch({
            payload: values,
            params: { id: permission.data?.data.id },
          });
        }
      },
    );

    React.useEffect(() => {
      if (!isOpen) {
        form.resetForm();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form.resetForm, isOpen]);

    return (
      <Dialog
        isOpen={isOpen}
        onClose={onClose}
        header={`Update permission ${permission.data?.data.name}`}
        footer={
          <ButtonGroup spacing={4}>
            <Button variant="ghost" colorScheme="blue" onClick={onClose}>
              Close
            </Button>
            <SaveIconButton
              variant="solid"
              isDisabled={!form.isValid || updateHook.isLoading}
              onClick={() => form.handleSubmit()}
              isLoading={updateHook.isLoading}
              label="Save"
              aria-label="Save"
            />
          </ButtonGroup>
        }
      >
        <Stack spacing={4} my={2}>
          <FloatingInputFormControl
            label="Name"
            errorMessage={form.errors.name}
            controlProps={{
              isRequired: true,
              isInvalid: Boolean(form.errors.name),
            }}
            inputProps={{
              name: 'name',
              value: form.values.name,
              onChange: form.handleChange,
              isDisabled: permission.isLoading || updateHook.isLoading,
            }}
          />
        </Stack>
      </Dialog>
    );
  },
);

export { UpdatePermissionModal };
