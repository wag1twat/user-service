import { Stack, Flex, Button, Text } from '@chakra-ui/react';
import React from 'react';
import { useAuth, useGlobalDisclosures } from 'src/client/providers';
import {
  FloatingInputFormControl,
  FloatingInputFormControlPassword,
  PersonalAreaDrawer,
  useUIToasts,
} from 'src/client/shared';
import { ParseSerializeException } from 'src/shared/helpers';
import { useSignin, useSigninForm } from './model';

const SignInDrawer = () => {
  const { error } = useUIToasts();

  const { signin, signup } = useGlobalDisclosures();

  const { setAccess } = useAuth();

  const signinHook = useSignin(['sign-in'], {
    resetOnSuccess: true,
    onSuccess: (data) => {
      if (setAccess) {
        setAccess(data);
      }
      signin?.onClose();
    },
    onError: (err) => {
      error(ParseSerializeException.getString(err));
    },
  });

  const handleClickSignUp = React.useCallback(() => {
    signin?.onClose();
    signup?.onOpen();
  }, [signin, signup]);

  const form = useSigninForm((values) => signinHook.post({ payload: values }));

  return (
    <PersonalAreaDrawer
      header="Login in your account"
      confirmText="Sign in"
      disclosure={signin}
      onConfirm={() => form.handleSubmit()}
      isConfirmLoading={Boolean(signinHook?.isLoading)}
      isDisabledConfirm={!form.isValid}
      size="md"
    >
      <Stack spacing={4} mt={4}>
        <FloatingInputFormControl
          label="Login"
          controlProps={{
            id: 'sign-in-login',
            isRequired: true,
            isInvalid: Boolean(form.errors.login),
          }}
          inputProps={{
            'aria-label': 'Login',
            name: 'login',
            value: form.values.login ?? '',
            onChange: form.handleChange,
            isDisabled: Boolean(signinHook?.isLoading),
          }}
          errorMessage={form.errors.login}
        />
        <FloatingInputFormControlPassword
          label="Password"
          controlProps={{
            id: 'sign-in-password',
            isRequired: true,
            isInvalid: Boolean(form.errors.password),
          }}
          inputProps={{
            'aria-label': 'Password',
            name: 'password',
            value: form.values.password ?? '',
            onChange: form.handleChange,
            isDisabled: Boolean(signinHook?.isLoading),
          }}
          errorMessage={form.errors.password}
        />
        <Flex alignItems="center" justifyContent="space-between">
          <Text>Are you doesn't have account?</Text>
          <Button size="sm" onClick={handleClickSignUp}>
            Sign up
          </Button>
        </Flex>
      </Stack>
    </PersonalAreaDrawer>
  );
};

export { SignInDrawer };
