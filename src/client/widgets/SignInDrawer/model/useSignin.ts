import { LoginDto } from 'src/server/controllers/auth/dto/login-dto';
import { PostHookOptions, usePost } from 'src/shared/axios';
import { SerializeException } from 'src/shared/helpers';
import { MutationKey } from 'react-query';

export const useSignin = (
  key: MutationKey,
  options?: PostHookOptions<
    { access_token: string },
    SerializeException,
    { payload: LoginDto }
  >,
) => {
  return usePost<
    { access_token: string },
    SerializeException,
    { payload: LoginDto }
  >(key, 'api/v1/auth/login', options);
};
