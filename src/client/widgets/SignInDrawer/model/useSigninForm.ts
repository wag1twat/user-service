import { useFormik } from 'formik';
import { LoginDto } from 'src/server/controllers/auth/dto/login-dto';
import { UserEntityConfig } from 'src/server/entities/user.entity';
import * as yup from 'yup';

export const SigninSchema = yup.object({
  login: yup
    .string()
    .min(UserEntityConfig.login.minLength)
    .max(UserEntityConfig.login.maxLength)
    .required(UserEntityConfig.login.requiredMessage),
  password: yup
    .string()
    .min(UserEntityConfig.password.minLength)
    .max(UserEntityConfig.password.maxLength)
    .matches(UserEntityConfig.password.regexp, {
      message: UserEntityConfig.password.notMatchMessage,
    })
    .required(UserEntityConfig.password.requiredMessage),
});

export type SigninFormType = LoginDto;

export const useSigninForm = (onSubmit: (values: LoginDto) => void) => {
  return useFormik<SigninFormType>({
    enableReinitialize: true,
    validationSchema: SigninSchema,
    initialValues: {
      login: 'root',
      password: 'Root@123',
    },
    onSubmit,
  });
};
