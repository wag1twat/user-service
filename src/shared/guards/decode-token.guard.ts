import { has } from 'lodash';

export interface JwtPayload {
  id: string;
  login: string;
  iat: number;
  exp: number;
}

const isDecodeToken = (decode: unknown): decode is JwtPayload => {
  return (
    has(decode, 'id') &&
    has(decode, 'login') &&
    has(decode, 'iat') &&
    has(decode, 'exp')
  );
};

export { isDecodeToken };
