import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import React from 'react';
import { useQuery, UseQueryOptions } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { axi } from './axios-instance';

export type QueryKey = unknown[];

export interface GetHookOptions<
  TQueryFnData = unknown,
  TParams extends object = object,
  TError extends SerializeException = SerializeException,
> extends Pick<
    UseQueryOptions<
      AxiosResponse<TQueryFnData>,
      AxiosError<TError>,
      AxiosResponse<TQueryFnData>,
      QueryKey
    >,
    | 'refetchInterval'
    | 'retry'
    | 'retryDelay'
    | 'enabled'
    | 'cacheTime'
    | 'staleTime'
  > {
  config?: Omit<AxiosRequestConfig, 'params'>;
  params?: TParams;
  resetOnSuccess?: boolean;
  resetOnError?: boolean;
  onSuccess?: (data: TQueryFnData) => void;
  onError?: (error?: SerializeException) => void;
}

export function useGet<
  TQueryFnData = unknown,
  TParams extends object = object,
  TError extends SerializeException = SerializeException,
>(
  queryKey: QueryKey,
  path: string,
  {
    config,
    params,
    resetOnError,
    resetOnSuccess,
    onSuccess,
    onError,
    ...options
  }: GetHookOptions<TQueryFnData, TParams, TError> = {},
) {
  const {
    data,
    error,
    isLoading,
    isFetching,
    isSuccess,
    isError,
    status,
    refetch,
    remove,
  } = useQuery({
    queryFn: () => axi.get(path, { ...config, params }),
    queryKey,
    onSuccess: (response) => {
      if (onSuccess) {
        onSuccess(response.data);
      }
    },
    onError: (error) => {
      if (onError) {
        onError(error.response?.data);
      }
    },
    refetchIntervalInBackground: false,
    refetchOnWindowFocus: false,
    ...options,
  });

  React.useEffect(() => {
    if (resetOnSuccess && isSuccess) {
      remove();
    }

    if (resetOnError && isError) {
      remove();
    }
  }, [resetOnSuccess, isSuccess, isError, resetOnError, remove]);

  React.useEffect(() => {
    return () => {
      remove();
    };
  }, [remove]);

  const native = React.useCallback(
    (config?: AxiosRequestConfig) => axi.get<TQueryFnData>(path, config),
    [path],
  );

  return {
    data: data?.data,
    error: error?.response?.data,
    isLoading,
    isFetching,
    status,
    refetch,
    remove,
    native,
  };
}
