import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import React from 'react';
import { MutationKey, useMutation, UseMutationOptions } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { axi } from './axios-instance';

export interface RemoveHookOptions<
  TData = unknown,
  TError extends SerializeException = SerializeException,
  TVariables = unknown,
> extends Pick<
    UseMutationOptions<
      AxiosResponse<{ data: TData }>,
      AxiosError<TError>,
      TVariables,
      unknown
    >,
    'retry' | 'retryDelay' | 'onSettled'
  > {
  resetOnSuccess?: boolean;
  resetOnError?: boolean;
  onSuccess?: (data: TData) => void;
  onResetWhenSuccess?: () => void;
  onError?: (error?: SerializeException) => void;
  onResetWhenError?: () => void;
  config?: AxiosRequestConfig<unknown>;
}

export function useRemove<
  TData = unknown,
  TError extends SerializeException = SerializeException,
  TVariables = unknown,
>(
  mutationKey: MutationKey,
  path: string,
  {
    resetOnSuccess = false,
    resetOnError = false,
    config,
    onSuccess,
    onError,
    onResetWhenError,
    onResetWhenSuccess,
    ...options
  }: RemoveHookOptions<TData, TError, TVariables> = {},
) {
  const { status, data, error, isLoading, isSuccess, isError, mutate, reset } =
    useMutation({
      mutationFn: (params) => {
        return axi.delete(path, {
          ...config,
          params,
        });
      },
      mutationKey,
      onSuccess: (response) => {
        if (onSuccess) {
          onSuccess(response.data.data);
        }
      },
      onError: (error) => {
        if (onError) {
          onError(error.response?.data);
        }
      },
      ...options,
    });

  const remove = React.useCallback(
    (variables: TVariables = Object.create({})) => {
      mutate(variables);
    },
    [mutate],
  );

  React.useEffect(() => {
    if (resetOnSuccess && isSuccess) {
      reset();
      if (onResetWhenSuccess) {
        onResetWhenSuccess();
      }
    }

    if (resetOnError && isError) {
      reset();
      if (onResetWhenError) {
        onResetWhenError();
      }
    }
  }, [
    resetOnSuccess,
    onResetWhenSuccess,
    isSuccess,
    resetOnError,
    onResetWhenError,
    isError,
    reset,
  ]);

  return {
    remove,
    data: data?.data,
    error: error?.response?.data,
    isLoading,
    reset,
    status,
  };
}
