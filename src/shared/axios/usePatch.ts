import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import React from 'react';
import { MutationKey, useMutation, UseMutationOptions } from 'react-query';
import { SerializeException } from 'src/shared/helpers';
import { axi } from './axios-instance';

export interface PatchHookOptions<
  TData = unknown,
  TError extends SerializeException = SerializeException,
  TVariables extends { payload?: object; params?: object } = {
    payload?: object;
    params?: object;
  },
  TContext = unknown,
> extends Pick<
    UseMutationOptions<
      AxiosResponse<{ data: TData }>,
      AxiosError<TError>,
      TVariables,
      TContext
    >,
    'retry' | 'retryDelay' | 'onSettled'
  > {
  resetOnSuccess?: boolean;
  resetOnError?: boolean;
  onSuccess?: (data: TData) => void;
  onResetWhenSuccess?: () => void;
  onError?: (error?: SerializeException) => void;
  onResetWhenError?: () => void;
  config?: Omit<AxiosRequestConfig, 'params'>;
}

export function usePatch<
  TData = unknown,
  TError extends SerializeException = SerializeException,
  TVariables extends { payload?: object; params?: object } = {
    payload?: object;
    params?: object;
  },
  TContext = unknown,
>(
  mutationKey: MutationKey,
  path: string,
  {
    resetOnSuccess = false,
    resetOnError = false,
    config,
    onSuccess,
    onError,
    onResetWhenError,
    onResetWhenSuccess,
    ...options
  }: PatchHookOptions<TData, TError, TVariables, TContext> = {},
) {
  const { status, data, error, isLoading, isSuccess, isError, mutate, reset } =
    useMutation({
      mutationFn: ({ payload, params }) =>
        axi.patch(path, payload, {
          ...config,
          params,
        }),
      mutationKey,
      onSuccess: (response) => {
        if (onSuccess) {
          onSuccess(response.data.data);
        }
      },
      onError: (error) => {
        if (onError) {
          onError(error.response?.data);
        }
      },
      ...options,
    });

  const patch = React.useCallback(
    (variables: TVariables) => {
      mutate(variables);
    },
    [mutate],
  );

  React.useEffect(() => {
    if (resetOnSuccess && isSuccess) {
      reset();
      if (onResetWhenSuccess) {
        onResetWhenSuccess();
      }
    }

    if (resetOnError && isError) {
      reset();
      if (onResetWhenError) {
        onResetWhenError();
      }
    }
  }, [
    resetOnSuccess,
    onResetWhenSuccess,
    isSuccess,
    resetOnError,
    onResetWhenError,
    isError,
    reset,
  ]);

  React.useEffect(() => {
    return () => {
      reset();
    };
  }, [reset]);

  return {
    patch,
    data: data?.data,
    error: error?.response?.data,
    isLoading,
    reset,
    status,
  };
}
