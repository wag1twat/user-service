import axios from 'axios';

export const setToken = (token: string) =>
  globalThis.localStorage.setItem('access_token', token);
const getToken = () => globalThis.localStorage.getItem('access_token');

export const removeToken = () =>
  globalThis.localStorage.removeItem('access_token');

const axi = axios.create({ baseURL: '/' });

axi.interceptors.request.use(async (config) => {
  const token = getToken();

  if (token) {
    config.headers = Object.assign(config.headers, {
      Authorization: `Bearer ${token}`,
    });
  }

  return config;
});

export { axi };
