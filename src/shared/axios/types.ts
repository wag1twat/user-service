import { SerializeException } from 'src/shared/helpers';

/**
 * @deprecated
 */
export type Options<T> = {
  enabled?: boolean;
  refetchInerval?: number;
  retryCount?: number;
  retryDelay?: number;
  onSuccess?: (data: T) => void;
  onError?: (error: SerializeException) => void;
};

/**
 * @deprecated
 */
export enum Statuses {
  'idle' = 'idle',
  'success' = 'success',
  'failed' = 'failded',
  'loading' = 'loading',
}
