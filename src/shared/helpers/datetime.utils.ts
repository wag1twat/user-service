import { DateTime, LocaleOptions } from 'luxon';

class DateUtils {
  private localeOptions: LocaleOptions;
  constructor(localeOptions: LocaleOptions) {
    this.localeOptions = localeOptions;
  }
  /**
   *
   * @param date string
   * @returns 25 февр. 2021 г., 15:10
   */
  toDateTime(date: string) {
    return DateTime.fromISO(date).toLocaleString(
      DateTime.DATETIME_MED,
      this.localeOptions,
    );
  }
  /**
   *
   * @param date string
   * @returns 25.02.2021 15:10
   */
  toDateTimeNumericFromISO = (date: string) =>
    DateTime.fromISO(date).toFormat('dd.LL.yyyy HH:mm', this.localeOptions);

  /**
   *
   * @param date string
   * @returns 25.02.21 15:11
   */
  toTimeShortNumericFromISO = (date: string) =>
    DateTime.fromISO(date).toFormat('dd.LL.yy HH:mm', this.localeOptions);

  /**
   *
   * @param date string
   * @returns 25.02.2021
   */
  toDate = (date: string) =>
    DateTime.fromISO(date).toFormat('dd.LL.yyyy', this.localeOptions);

  /**
   *
   * @param date string
   * @returns 25 февр., 15:10
   */
  toShortDateTime = (date: string) =>
    DateTime.fromISO(date).toFormat('dd LLL HH:mm', this.localeOptions);

  /**
   *
   * @param date string
   * @returns 25 февр., 15:10
   */
  toShortISODateTime = (date: string) =>
    DateTime.fromISO(date).toFormat("yyyy-LL-dd'T'HH:mm", this.localeOptions);

  /**
   *
   * @returns 2021-02-25T15:10
   */
  getNowShortISODateTime = () =>
    DateTime.local().toFormat("yyyy-LL-dd'T'HH:mm", this.localeOptions);

  /**
   *
   * @returns //=> '1982-05-25T00:00:00.000Z'
   */
  getNowISO = () => DateTime.local(this.localeOptions).toISO();

  /**
   *
   * @returns
   */

  fromJStoISO = (date: Date) => DateTime.fromJSDate(date).toISO();

  /**
   * @returns 2022-04-04T14:04:28.558Z
   */

  getNow = () => DateTime.local(this.localeOptions);

  /**
   *
   * @returns Return the difference between two DateTimes as a Duration at object format

   */
  diffObjectFromNow = (date: string) => {
    return this.getNow()
      .diff(DateTime.fromISO(date, this.localeOptions), [
        'years',
        'months',
        'days',
        'hours',
        'minutes',
        'seconds',
      ])
      .toObject();
  };

  /*  */
  fromISOtoJSDate = (date: string) => {
    return DateTime.fromISO(date, this.localeOptions).toJSDate();
  };
}

export const DateTimeUtils = new DateUtils({ locale: 'en' });
