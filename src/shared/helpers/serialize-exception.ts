import { get, join } from 'lodash';
import { DateTimeUtils } from './datetime.utils';

export class SerializeException {
  constructor(data?: Partial<SerializeException>) {
    Object.assign(this, data);
  }
  statusCode = 500;
  message = 'Oops, something went wrong...';
  timestamp: string = DateTimeUtils.getNow().toString();
  path: string | null = null;
  error: string | object | null = null;
}

const separator = ' : ';

const joinStatus = (statusCode: number, error: string) =>
  join([statusCode, error], separator);

export class ParseSerializeException {
  static getString(error: SerializeException | undefined) {
    if (error?.error) {
      if (typeof error === 'object') {
        const message = get(error.error, 'message');
        const detail = get(error.error, 'detail');

        if (message) {
          if (
            Array.isArray(message) &&
            message.every((msg) => typeof msg === 'string')
          ) {
            return joinStatus(error.statusCode, message.join(', ').trim());
          }

          if (typeof message === 'string') {
            return joinStatus(error.statusCode, message);
          }
        }

        if (detail) {
          if (typeof detail === 'string') {
            return joinStatus(error.statusCode, detail);
          }
        }
      }
    }

    if (error) {
      return joinStatus(error.statusCode, error.message);
    }

    return '';
  }
}
