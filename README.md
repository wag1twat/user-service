# NestJS + NextJS Application

- Based on NestJS

- Templates based on NextJS
- Feature-sliced architecture
- No-state management libraries (usage only hooks, contexts)
- Axios for api and react-query handler
- UI-kit Chakra-ui

- Tests only NestJS

- Hosted on http://188.124.47.225:3000/

## How run run?

- npm install

- for production: npm run start:prod

- for development: npm run start:dev

### Optional scripts

- "format": "prettier --write \"src/**/\*.ts\"",
- "format:check": "prettier --check \"src/**/_.ts\"",
- "prebuild": "rimraf dist",
- "build": "npm run build:next && npm run build:nest",
- "build:next": "next build",
- "build:nest": "nest build --path ./tsconfig.server.json",
- "start": "node dist/server/main.js",
- "start:next": "next dev",
- "start:dev": "NODE_ENV=development nest build --webpack --webpackPath webpack-hmr.config.js --watch --path tsconfig.server.json",
- "start:server:dev": "NODE_ENV=development nest start --path ./tsconfig.server.json --watch",
- "start:debug": "NODE_ENV=test nest start --path ./tsconfig.server.json --debug --watch",
- "start:prod": "NODE_ENV=production node dist/server/main",
- "lint": "eslint \"{src,apps,libs,test}/\*\*/_{.ts,.tsx}\" --fix",
- "pretest": "NODE_ENV=test && npm run test:schema:drop && npm run test:migration:run",
- "test": "jest",
- "test:reports": "npm run pretest && jest --ci --reporters=default --reporters=jest-junit",
- "test:watch": "npm run pretest && jest --watch",
- "test:cov": "npm run pretest && jest --coverage",
- "test:debug": "node --inspect-brk -r tsconfig-paths/register -r ts-node/register node_modules/.bin/jest --runInBand",
- "test:e2e": "jest --config ./test/jest-e2e.json",
- "typeorm": "ts-node --transpile-only ./node_modules/typeorm/cli.js --config src/server/ormconfig.ts",
- "build:typeorm": "ts-node --transpile-only ./node_modules/typeorm/cli.js --config dist/server/ormconfig.js",
- "build:migration:run": "NODE_ENV=production npm run build:typeorm migration:run",
- "dev:migration:generate": "NODE_ENV=development npm run typeorm migration:generate -- --name dev",
- "test:migration:generate": "NODE_ENV=test npm run typeorm migration:generate -- --name test",
- "prod:migration:generate": "NODE_ENV=production npm run typeorm migration:generate -- --name prod",
- "dev:migration:run": "NODE_ENV=development npm run typeorm migration:run",
- "test:migration:run": "NODE_ENV=test npm run typeorm migration:run",
- "prod:migration:run": "NODE_ENV=production npm run typeorm migration:run",
- "dev:migration:revert": "NODE_ENV=development npm run typeorm migration:revert",
- "tet:migration:revert": "NODE_ENV=test npm run typeorm migration:revert",
- "prod:migration:revert": "NODE_ENV=production npm run typeorm migration:revert",
- "dev:schema:drop": "NODE_ENV=development npm run typeorm schema:drop",
- "test:schema:drop": "NODE_ENV=test npm run typeorm schema:drop",
- "prod:schema:drop": "NODE_ENV=production npm run typeorm schema:drop"
