module.exports = {
  useFileSystemPublicRoutes: true,
  swcMinify: false,
  publicRuntimeConfig: {
    version: process.env.VERSION,
  },
  future: {
    webpack5: true,
  },
  // webpack: (config) => {
  //   config.resolve.fallback = {
  //     fs: false,
  //     process: false,
  //     util: false,
  //     path: false,
  //     'mock-aws-s3': false,
  //     os: false,
  //     nock: false,
  //     'cache-manager': false,
  //     stream: false,
  //     crypto: false,
  //     child_process: false,
  //     net: false,
  //     tls: false,
  //     assert: false,
  //   };

  //   return config;
  // },
};
