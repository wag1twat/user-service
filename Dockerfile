FROM node:16.14.2-alpine as build
 
RUN apk --no-cache add --virtual .builds-deps build-base python3

WORKDIR /app

# Install dependencies
COPY package*.json ./

RUN npm install -g npm@8.6.0 
RUN npm install @next/swc-linux-x64-gnu
RUN npm install @next/swc-linux-x64-musl
RUN npm install

# Bundle app source
COPY . ./

RUN npm run build

FROM node:16.14.2-alpine

RUN apk --no-cache add --virtual .builds-deps build-base python3

WORKDIR /app

COPY package*.json ./

RUN npm install -g npm@8.6.0 
RUN npm install @next/swc-linux-x64-gnu
RUN npm install @next/swc-linux-x64-musl
RUN npm install

COPY --from=build /app/.next /app/.next
COPY --from=build /app/dist/ /app/dist

CMD ["npm", "run", "start:prod"]

